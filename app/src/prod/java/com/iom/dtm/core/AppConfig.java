package com.iom.dtm.core;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AppConfig {
  public static final String TAG = "AppConfig";
  public static final boolean DEBUG = Boolean.parseBoolean("false");
  public static final int LOCATION_REQUEST_PER_TIME = 5000; // Get Location per 5 seconds

  // API Info
  // Sinatra Local
  //public static final String BASE_URL = "http://192.168.1.43:8000/";
  // Server Local
  public static final String BASE_URL = "https://dtmapp.iom.int/api/";
  public static final String TERM_AND_CONDITION_URL = "https://www.iom.int/terms-conditions/";
  public static final int API_VERSION = 1;

  // Amazon S3
  public static final String S3_BUCKET_NAME = "smartsoftasia-test";
  public static final String S3_KEY = "us-west-2:70091a6c-ae82-4ea3-92a9-a2dd89ef5dfd";
  public static final String S3_FOLDER_NAME = "files";
  public static final String S3_BUCKET_PATH = "https://s3-us-west-2.amazonaws.com/smartsoftasia-test/";

  // App Info
  public static final String IMAGE_FOLDER_NAME = "DTM";
}
