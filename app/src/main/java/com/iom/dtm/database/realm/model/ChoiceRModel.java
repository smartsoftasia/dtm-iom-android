package com.iom.dtm.database.realm.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ChoiceRModel extends RealmObject {
  public static final String TAG = "ChoiceRModel";

  @PrimaryKey
  public long id;

  public String title;

  public boolean additionalRequired;

  public Long relativeQuestionId;

  public void setAdditionalRequired(boolean additionalRequired) {
    this.additionalRequired = additionalRequired;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setRelativeQuestionId(Long relativeQuestionId) {
    this.relativeQuestionId = relativeQuestionId;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
