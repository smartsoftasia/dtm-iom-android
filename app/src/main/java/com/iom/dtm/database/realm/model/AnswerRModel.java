package com.iom.dtm.database.realm.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AnswerRModel extends RealmObject {
  public static final String TAG = "AnswerRModel";

  public String questionId;

  public String value;

  public String choiceId;

  public int answersTimes;

  public void setChoiceId(String choiceId) {
    this.choiceId = choiceId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getChoiceId() {
    return choiceId;
  }

  public String getQuestionId() {
    return questionId;
  }

  public String getValue() {
    return value == null ? "" : value;
  }

  public void setAnswersTimes(int answersTimes) {
    this.answersTimes = answersTimes;
  }

  public int getAnswersTimes() {
    return answersTimes;
  }

  @Override
  public String toString() {
    return "AnswerRModel{" +
        "choiceId='" + String.valueOf(choiceId) + '\'' +
        ", questionId='" + String.valueOf(questionId) + '\'' +
        ", value='" + String.valueOf(value) + '\'' +
        '}';
  }
}
