package com.iom.dtm.database.realm.model;

import io.realm.RealmObject;

/**
 * Created by Nott on 8/31/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class TagRModel extends RealmObject {
  public static final String TAG = "TagRModel";

  public String id;

  public String title;

  public boolean additionalRequired;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isAdditionalRequired() {
    return additionalRequired;
  }

  public void setAdditionalRequired(boolean additionalRequired) {
    this.additionalRequired = additionalRequired;
  }

  @Override
  public String toString() {
    return "TagRModel{" +
        "additionalRequired=" + additionalRequired +
        ", id='" + id + '\'' +
        ", title='" + title + '\'' +
        '}';
  }
}
