package com.iom.dtm.database.realm;

import android.content.Context;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class RealmManager {
  public static final String TAG = "RealmManager";

  public static void init(Context context) {
    RealmConfiguration realmConfig = new RealmConfiguration.Builder(
        context).deleteRealmIfMigrationNeeded().build();
    Realm.setDefaultConfiguration(realmConfig);
  }

  public static Realm getRealm() {
    return Realm.getDefaultInstance();
  }
}
