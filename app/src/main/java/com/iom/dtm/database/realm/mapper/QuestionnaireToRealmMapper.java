package com.iom.dtm.database.realm.mapper;

import com.iom.dtm.database.realm.model.ChoiceRModel;
import com.iom.dtm.database.realm.model.QuestionCategoryRModel;
import com.iom.dtm.database.realm.model.QuestionGroupRModel;
import com.iom.dtm.database.realm.model.QuestionRModel;
import com.iom.dtm.database.realm.model.QuestionnaireRModel;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.model.Questionnaire;
import io.realm.RealmList;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionnaireToRealmMapper {
  public static final String TAG = "QuestionnaireToRealmMapper";

  public static QuestionnaireRModel transform(Questionnaire questionnaire) {
    if (questionnaire == null) {
      return null;
    }

    QuestionnaireRModel rModel = new QuestionnaireRModel();
    rModel.setId(Long.parseLong(questionnaire.id));
    rModel.setPublishAt(questionnaire.publishAt.toString());

    RealmList<QuestionCategoryRModel> questionCategoryRModels = new RealmList<>();

    for (int i = 0; i < questionnaire.questionCategories.size(); i++) {
      questionCategoryRModels.add(transform(questionnaire.questionCategories.get(i)));
    }
    rModel.setQuestionCategories(questionCategoryRModels);
    return rModel;
  }

  public static QuestionCategoryRModel transform(QuestionCategory questionCategory) {
    if (questionCategory == null) {
      return null;
    }

    QuestionCategoryRModel rModel = new QuestionCategoryRModel();
    rModel.setId(Long.parseLong(questionCategory.id));
    rModel.setTitle(questionCategory.title);

    RealmList<QuestionGroupRModel> questionGroupRModels = new RealmList<>();
    for (int i = 0; i < questionCategory.questionGroupList.size(); i++) {
      questionGroupRModels.add(transform(questionCategory.questionGroupList.get(i)));
    }
    rModel.setQuestionGroupRModelRealmObject(questionGroupRModels);

    return rModel;
  }

  public static QuestionGroupRModel transform(QuestionGroup questionGroup) {
    if (questionGroup == null) {
      return null;
    }

    QuestionGroupRModel rModel = new QuestionGroupRModel();
    rModel.setId(Long.parseLong(questionGroup.id));
    rModel.setType(questionGroup.type);
    rModel.setTitle(questionGroup.title);

    RealmList<QuestionRModel> questionRModels = new RealmList<>();
    for (int i = 0; i < questionGroup.questions.size(); i++) {
      questionRModels.add(transform(questionGroup.questions.get(i)));
    }
    rModel.setQuestionRModels(questionRModels);
    return rModel;
  }

  public static QuestionRModel transform(Question question) {
    if (question == null) {
      return null;
    }

    QuestionRModel rModel = new QuestionRModel();
    rModel.setId(Long.parseLong(question.id));
    rModel.setTitle(question.title);
    rModel.setType(question.type);

    RealmList<ChoiceRModel> choiceRModels = new RealmList<>();
    for (int i = 0; i < question.choices.size(); i++) {
      choiceRModels.add(transform(question.choices.get(i)));
    }

    rModel.setChoiceRModels(choiceRModels);
    return rModel;
  }

  public static ChoiceRModel transform(Choice choice) {
    if (choice == null) {
      return null;
    }

    ChoiceRModel rModel = new ChoiceRModel();
    rModel.setId(Long.parseLong(choice.id));
    rModel.setTitle(choice.title);
    rModel.setAdditionalRequired(choice.additionalRequired);
    rModel.setRelativeQuestionId(choice.relativeQuestionId);
    return rModel;
  }
}
