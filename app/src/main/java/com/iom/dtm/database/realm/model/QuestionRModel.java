package com.iom.dtm.database.realm.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionRModel extends RealmObject {
  public static final String TAG = "QuestionRModel";

  @PrimaryKey
  public long id;

  public String title;

  public String type;

  public RealmList<ChoiceRModel> choiceRModels;

  public void setChoiceRModels(RealmList<ChoiceRModel> choiceRModels) {
    this.choiceRModels = choiceRModels;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setType(String type) {
    this.type = type;
  }
}
