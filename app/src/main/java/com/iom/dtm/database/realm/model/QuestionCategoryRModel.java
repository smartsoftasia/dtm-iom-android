package com.iom.dtm.database.realm.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionCategoryRModel extends RealmObject {
  public static final String TAG = "QuestionCategoryRModel";

  @PrimaryKey
  public long id;

  public String title;

  public RealmList<QuestionGroupRModel> questionGroupRModelRealmObject;

  public void setId(long id) {
    this.id = id;
  }

  public void setQuestionGroupRModelRealmObject(
      RealmList<QuestionGroupRModel> questionGroupRModelRealmObject) {
    this.questionGroupRModelRealmObject = questionGroupRModelRealmObject;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
