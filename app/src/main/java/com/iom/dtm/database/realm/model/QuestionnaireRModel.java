package com.iom.dtm.database.realm.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionnaireRModel extends RealmObject {
  public static final String TAG = "QuestionnaireRModel";

  @PrimaryKey
  public long id;

  public String publishAt;

  public RealmList<QuestionCategoryRModel> questionCategories;

  public void setId(long id) {
    this.id = id;
  }

  public void setPublishAt(String publishAt) {
    this.publishAt = publishAt;
  }

  public void setQuestionCategories(RealmList<QuestionCategoryRModel> questionCategories) {
    this.questionCategories = questionCategories;
  }
}
