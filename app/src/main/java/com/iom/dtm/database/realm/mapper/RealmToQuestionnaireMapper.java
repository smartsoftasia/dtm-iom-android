package com.iom.dtm.database.realm.mapper;

import com.iom.dtm.database.realm.model.ChoiceRModel;
import com.iom.dtm.database.realm.model.QuestionCategoryRModel;
import com.iom.dtm.database.realm.model.QuestionGroupRModel;
import com.iom.dtm.database.realm.model.QuestionRModel;
import com.iom.dtm.database.realm.model.QuestionnaireRModel;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.model.Questionnaire;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class RealmToQuestionnaireMapper {
  public static final String TAG = "QuestionnaireToRealmMapper";

  public static Questionnaire transform(QuestionnaireRModel questionnaire) {
    if (questionnaire == null) {
      return null;
    }

    Questionnaire rModel = new Questionnaire();
    rModel.id = String.valueOf(questionnaire.id);

    List<QuestionCategory> questionCategoryRModels = new ArrayList<>();

    for (int i = 0; i < questionnaire.questionCategories.size(); i++) {
      questionCategoryRModels.add(transform(questionnaire.questionCategories.get(i)));
    }
    rModel.questionCategories = questionCategoryRModels;
    return rModel;
  }

  public static QuestionCategory transform(QuestionCategoryRModel questionCategory) {
    if (questionCategory == null) {
      return null;
    }

    QuestionCategory rModel = new QuestionCategory();
    rModel.id = String.valueOf(questionCategory.id);
    rModel.title = questionCategory.title;

    List<QuestionGroup> questionGroupRModels = new ArrayList<>();
    for (int i = 0; i < questionCategory.questionGroupRModelRealmObject.size(); i++) {
      questionGroupRModels.add(transform(questionCategory.questionGroupRModelRealmObject.get(i)));
    }
    rModel.questionGroupList = questionGroupRModels;

    return rModel;
  }

  public static QuestionGroup transform(QuestionGroupRModel questionGroup) {
    if (questionGroup == null) {
      return null;
    }

    QuestionGroup rModel = new QuestionGroup();
    rModel.id = String.valueOf(questionGroup.id);
    rModel.title = questionGroup.title;
    rModel.type = questionGroup.type;

    List<Question> questionRModels = new ArrayList<>();
    for (int i = 0; i < questionGroup.questionRModels.size(); i++) {
      questionRModels.add(transform(questionGroup.questionRModels.get(i)));
    }

    rModel.questions = questionRModels;
    return rModel;
  }

  public static Question transform(QuestionRModel question) {
    if (question == null) {
      return null;
    }

    Question rModel = new Question();
    rModel.id = String.valueOf(question.id);
    rModel.title = question.title;
    rModel.type = question.type;

    List<Choice> choiceRModels = new ArrayList<>();
    for (int i = 0; i < question.choiceRModels.size(); i++) {
      choiceRModels.add(transform(question.choiceRModels.get(i)));
    }

    rModel.choices = choiceRModels;
    return rModel;
  }

  public static Choice transform(ChoiceRModel choice) {
    if (choice == null) {
      return null;
    }

    Choice rModel = new Choice();
    rModel.id = String.valueOf(choice.id);
    rModel.additionalRequired = choice.additionalRequired;
    rModel.relativeQuestionId = choice.relativeQuestionId;
    rModel.title = choice.title;
    return rModel;
  }
}
