package com.iom.dtm.database.realm.mapper;

import com.iom.dtm.database.realm.model.TagRModel;
import com.iom.dtm.domain.model.Tag;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nott on 8/31/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class TagsRealmMapper {
  public static final String TAG = "TagsRealmMapper";

  public static TagRModel transform(Tag tag) {
    if (tag == null) {
      return null;
    }

    TagRModel rModel = new TagRModel();
    rModel.setId(tag.id);
    rModel.setTitle(tag.title);
    rModel.setAdditionalRequired(tag.additionalRequired);

    return rModel;
  }

  public static Tag transform(TagRModel rModel) {
    if (rModel == null) {
      return null;
    }

    Tag tag = new Tag();
    tag.id = rModel.id;
    tag.title = rModel.title;
    tag.additionalRequired = rModel.additionalRequired;

    return tag;
  }

  public static List<TagRModel> transformToRealm(List<Tag> tag) {
    if (tag == null) {
      return null;
    }
    List<TagRModel> list = new ArrayList<>();
    for (int i = 0; i < tag.size(); i++) {
      list.add(transform(tag.get(i)));
    }

    return list;
  }

  public static List<Tag> transformToModel(List<TagRModel> rModel) {
    if (rModel == null) {
      return null;
    }

    List<Tag> list = new ArrayList<>();
    for (int i = 0; i < rModel.size(); i++) {
      list.add(transform(rModel.get(i)));
    }

    return list;
  }
}
