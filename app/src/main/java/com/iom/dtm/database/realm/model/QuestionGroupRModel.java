package com.iom.dtm.database.realm.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupRModel extends RealmObject {
  public static final String TAG = "QuestionGroupRModel";

  @PrimaryKey
  public long id;

  public String title;

  public String type;

  public RealmList<QuestionRModel> questionRModels;

  public void setId(long id) {
    this.id = id;
  }

  public void setQuestionRModels(RealmList<QuestionRModel> questionRModels) {
    this.questionRModels = questionRModels;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setType(String type) {
    this.type = type;
  }
}
