package com.iom.dtm.database.realm.model;

import io.realm.RealmObject;

/**
 * Created by Nott on 8/31/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AnswerTimesRModel extends RealmObject {
  public static final String TAG = "AnswerTimesRModel";

  public String id;
  public int answerTimes;

  public void setAnswerTimes(int answerTimes) {
    this.answerTimes = answerTimes;
  }

  public void setId(String id) {
    this.id = id;
  }
}
