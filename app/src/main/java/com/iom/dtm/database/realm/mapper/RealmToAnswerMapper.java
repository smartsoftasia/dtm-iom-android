package com.iom.dtm.database.realm.mapper;

import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.rest.form.AnswerDetailForm;
import com.smartsoftasia.ssalibrary.helper.Logger;

/**
 * Created by Nott on 8/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class RealmToAnswerMapper {
  public static final String TAG = "RealmToAnswerMapper";

  public static AnswerDetailForm transform(AnswerRModel answerForm) {
    if (answerForm == null) {
      return null;
    }
    AnswerDetailForm form = new AnswerDetailForm();
    form.setQuestionId(answerForm.questionId);
    form.setChoiceId(answerForm.choiceId);
    form.setValue(answerForm.value);
    return form;
  }
}
