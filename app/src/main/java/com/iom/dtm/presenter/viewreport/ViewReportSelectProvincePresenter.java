package com.iom.dtm.presenter.viewreport;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetAddressListInteractor;
import com.iom.dtm.domain.model.Admin0;
import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.presenter.BaseSharedPreferencePresenter;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.model.Admin1CheckBoxViewModel;
import com.iom.dtm.view.viewinterface.ViewReportSelectProvinceViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public class ViewReportSelectProvincePresenter extends BaseSharedPreferencePresenter implements Presenter {
  public static final String TAG = "ViewReportSelectProvinc";

  private ViewReportSelectProvinceViewInterface mView;
  private GetAddressListInteractor mGetAddressListInteractor;
  private HashMap<String, Admin1> admin1HashMap;
  private List<Admin1CheckBoxViewModel> viewModels;

  @Inject
  public ViewReportSelectProvincePresenter(GetAddressListInteractor getAddressListInteractor, SharedPreference sharedPreference) {
    super(sharedPreference);
    this.mGetAddressListInteractor = getAddressListInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mGetAddressListInteractor.unsubscribe();
  }

  public void initialize(Event event) {
    mGetAddressListInteractor.setEventId(event.id);
    mGetAddressListInteractor.execute(new GetAddressSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  public void onAddOrUpdateClick(Admin1CheckBoxViewModel viewModel, boolean isChecked) {
    if (isChecked) {
      admin1HashMap.put(viewModel.t.reference, viewModel.t);
    } else {
      admin1HashMap.remove(viewModel.t.reference);
    }
  }

  public void setView(ViewReportSelectProvinceViewInterface view) {
    this.mView = view;
  }

  public void onNextClick() {
    if (admin1HashMap == null) {
      return;
    }
    if (admin1HashMap.isEmpty()) {
      if (mView != null) {
        mView.displayPleaseSelectAdmin1();
      }
    } else {
      if (mView != null) {
        mView.openSelectQuestionsFragment(new ArrayList<>(admin1HashMap.values()));
      }
    }
  }

  private void appendViewModel(List<Admin1> admin1s) {
    admin1HashMap = new HashMap<>();
    List<Admin1CheckBoxViewModel> viewModels = new ArrayList<>();
    for (int i = 0; i < admin1s.size(); i++) {
      Admin1CheckBoxViewModel viewModel = new Admin1CheckBoxViewModel(admin1s.get(i), admin1s.get(i).name);
      viewModels.add(viewModel);
    }

    this.viewModels = viewModels;
    if (mView != null) {
      mView.appendCheckBoxList(this.viewModels);
    }
  }

  public void onSelectAllClick() {
    if (viewModels == null || viewModels.isEmpty()) {
      return;
    }
    for (int i = 0; i < viewModels.size(); i++) {
      viewModels.get(i).setChecked(true);
      onAddOrUpdateClick(viewModels.get(i), true);
    }

    if (mView != null) {
      mView.onSelectAllCheckBox();
    }
  }

  public void onDeselectAllClick() {
    if (viewModels == null || viewModels.isEmpty()) {
      return;
    }
    for (int i = 0; i < viewModels.size(); i++) {
      viewModels.get(i).setChecked(false);
      onAddOrUpdateClick(viewModels.get(i), false);
    }

    if (mView != null) {
      mView.onDeselectAllCheckBox();
    }
  }

  private class GetAddressSubscriber extends DTMApiSubscriber<Admin0> {
    @Override
    public void onNext(Admin0 admin0) {
      appendViewModel(admin0.admin1s);
      if (mView != null) {
        mView.onHideLoading();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }
    }
  }
}
