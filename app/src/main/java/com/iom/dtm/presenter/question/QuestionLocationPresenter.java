package com.iom.dtm.presenter.question;

import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.view.viewinterface.QuestionLocationViewInterface;
import com.iom.dtm.view.viewinterface.QuestionStringViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;
import javax.inject.Inject;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionLocationPresenter extends BaseQuestionPresenter implements Presenter {
  public static final String TAG = "QuestionStringPresenter";

  protected Question mQuestion;
  protected QuestionLocationViewInterface mView;

  @Inject
  public QuestionLocationPresenter(AnswerController answerController) {
    super(answerController);
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  public void setView(QuestionLocationViewInterface view) {
    this.mView = view;
  }

  public void initialize(Question question) {
    this.mQuestion = question;

    setUpQuestion();
    setUpAnswer();
  }

  public void onSelectLocationClick() {
    if (mView != null) {
      mView.onStartSelectLocationActivity(mQuestion.id, answerTimes);
    }
  }

  private void setUpAnswer() {
    this.rAnswerModel =
        answerController.getAnswer(mQuestion.id, answerTimes) == null ? new AnswerRModel()
                                                                      : answerController.getAnswer(
                                                                          mQuestion.id,
                                                                          answerTimes);
    this.rAnswerModel.setAnswersTimes(answerTimes);

    this.rAnswerModel.setQuestionId(mQuestion.id);
    if (mView != null) {
      mView.setUpAnswer(rAnswerModel.getValue());
    }
  }

  private void setUpQuestion() {
    if (mView != null) {
      mView.setUpQuestion(mQuestion);
    }
  }
}
