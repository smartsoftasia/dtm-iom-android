package com.iom.dtm.presenter.viewreport;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.PostGetViewReportInteractor;
import com.iom.dtm.domain.model.ViewReport;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.presenter.BaseSharedPreferencePresenter;
import com.iom.dtm.rest.form.ViewReportForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.ViewReportSummaryViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 9/15/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewReportSummaryPresenter extends BaseViewReportPresenter implements Presenter {
  public static final String TAG = "ViewReportSummaryPresenter";

  protected PostGetViewReportInteractor mPostGetViewReportInteractor;
  protected ViewReportSummaryViewInterface mView;
  private ViewReport mViewReport;

  @Inject
  public ViewReportSummaryPresenter(SharedPreference sharedPreference,
                                    PostGetViewReportInteractor postGetViewReportInteractor) {
    super(sharedPreference);
    this.mPostGetViewReportInteractor = postGetViewReportInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {

  }

  @Override
  public void restart() {

  }

  public void setView(ViewReportSummaryViewInterface view) {
    this.mView = view;
  }

  public void initialize(ViewReport viewReport) {
    this.mViewReport = viewReport;
    if (mView != null) {
      mView.updateViewReport(viewReport);
    }
  }

  public void getReportClick() {
    if (mViewReport == null) {
      return;
    }
    ViewReportForm form = new ViewReportForm(mViewReport.getEvent().id, mViewReport.getStartDate(),
        mViewReport.getEndDate(), mViewReport.getAdmin1s(), mViewReport.getQuestions());
    mPostGetViewReportInteractor.setViewReportForm(form);
    mPostGetViewReportInteractor.execute(new PostGetViewReportSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private class PostGetViewReportSubscriber extends DTMApiSubscriber<Object> {
    @Override
    public void onNext(Object o) {
      if (mView != null) {
        mView.onHideLoading();
        mView.displayReportHasBeenSent();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
      }
    }
  }
}
