package com.iom.dtm.presenter;

import android.text.TextUtils;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.domain.model.RelativeQuestion;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.helper.Validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Nott on 9/22/2016 AD.
 * DTM
 */

public abstract class BaseValidateQuestionPresenter extends BaseSharedPreferencePresenter {
  public static final String TAG = "BaseValidateQuestionPresenter";

  protected HashMap<String, RelativeQuestion> relativeQuestionHashMap = new HashMap<>();
  protected List<CategoryValidate> categoryTitlesForValidate = new ArrayList<>();
  protected List<Question> questionList = new ArrayList<>();
  protected HashMap<String, AnswerRModel> answerHashMap = new HashMap<>();
  private boolean mIsFullQuestionnaire;

  protected abstract void formHasBeenValidated();

  protected abstract void displayFieldRequired(String category, String question);

  protected abstract void displayInvalidEmail(String category, String question);

  protected abstract void questionCategoryPositionForAnswerInvalidated(int position);

  public BaseValidateQuestionPresenter(SharedPreference sharedPreference,
                                       boolean isFullQuestionnaire) {
    super(sharedPreference);
    if (isFullQuestionnaire) {
      getQuestionList();
      getRelativeQuestion();
    }
  }

  private void getRelativeQuestion() {
    Questionnaire questionnaire = QuestionnaireController.getQuestionnaire();
    for (int i = 0; i < questionnaire.questionCategories.size(); i++) {
      QuestionCategory category = questionnaire.questionCategories.get(i);
      for (int j = 0; j < category.questionGroupList.size(); j++) {
        QuestionGroup group = category.questionGroupList.get(j);
        for (int k = 0; k < group.questions.size(); k++) {
          Question question = group.questions.get(k);
          if (question.choices != null && question.choices.size() > 0) {
            for (int l = 0; l < question.choices.size(); l++) {
              Choice choice = question.choices.get(l);
              if (!TextUtils.isEmpty(String.valueOf(choice.relativeQuestionId))) {
                RelativeQuestion model = new RelativeQuestion();
                model.setQuestionId(question.id);
                model.setChoiceId(choice.id);
                model.setRelativeQuestionId(String.valueOf(choice.relativeQuestionId));
                relativeQuestionHashMap.put(String.valueOf(choice.relativeQuestionId), model);
              }
            }
          }
        }
      }
    }
  }

  protected void onFormValidation() {
    if (mIsFullQuestionnaire) {

    }
    getAnswerList();

    for (int i = 0; i < questionList.size(); i++) {
      Question question = questionList.get(i);
      int answerTimes = calculateAnswerTimes(question.id);
      AnswerRModel answerRModel;
      CategoryValidate validate = categoryTitlesForValidate.get(i);

      for (int m = 1; m <= answerTimes; m++) {
        Logger.d(TAG, "QuestionID " + question.id + " AnswerTimes " + answerTimes);
        if (answerHashMap.containsKey(question.id + AppConstant.UNDERSCORE + m)) {
          answerRModel = answerHashMap.get(question.id + AppConstant.UNDERSCORE + m);
        } else {
          answerRModel = new AnswerRModel();
          answerRModel.setQuestionId(question.id);
        }

        switch (question.getType()) {
          case Question.STRING:
            if (relativeQuestionHashMap.containsKey(question.id)) { // Handle Relative Question
              RelativeQuestion relativeQuestion = relativeQuestionHashMap.get(question.id);
              AnswerRModel relativeQuestionAnswer = answerHashMap.get(
                  relativeQuestion.questionId + AppConstant.UNDERSCORE + m);

              if (relativeQuestionAnswer == null) { // Can't find answer of relative question
                return;
              }

              if (relativeQuestionAnswer.getChoiceId()
                  .equalsIgnoreCase(relativeQuestion.getChoiceId())) {
                if (TextUtils.isEmpty(answerRModel.getValue())) {
                  displayFieldRequired(validate.title, question.title);
                  questionCategoryPositionForAnswerInvalidated(validate.position);
                  return;
                }
                Logger.d(TAG, "RELATIVE QUESTION VALIDATED");
              }
            } else {
              if (TextUtils.isEmpty(answerRModel.getValue())) {
                displayFieldRequired(validate.title, question.title);
                questionCategoryPositionForAnswerInvalidated(validate.position);
                return;
              }
            }
            Logger.d(TAG, "STRING VALIDATED");
            break;
          case Question.DATE_PICKER:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "DATE_PICKER VALIDATED");
            break;
          case Question.NUMBER_PICKER:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "NUMBER_PICKER VALIDATED");
            break;
          case Question.LOCATION:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "LOCATION VALIDATED");
            break;
          case Question.SINGLE_CHOICE:
            if (!TextUtils.isEmpty(answerRModel.getChoiceId())) {
              Logger.d(TAG, "SINGLE_CHOICE VALIDATED");
              for (int j = 0; j < question.choices.size(); j++) {
                if (question.choices.get(j).id.equalsIgnoreCase(answerRModel.getChoiceId())) {
                  if (question.choices.get(j).additionalRequired) {
                    if (TextUtils.isEmpty(answerRModel.getValue())) {
                      displayFieldRequired(validate.title, question.title);
                      questionCategoryPositionForAnswerInvalidated(validate.position);
                      return;
                    } else {
                      Logger.d(TAG, "SINGLE_CHOICE EXTRA VALIDATED");
                    }
                  }
                }
              }
            } else {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            break;
          case Question.NUMBER:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "NUMBER VALIDATED");
            break;
          case Question.EMAIL:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            } else if (!Validator.isValidEmail(answerRModel.getValue())) {
              displayInvalidEmail(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "EMAIL VALIDATED");
            break;
          case Question.TEXT:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "TEXT VALIDATED");
            break;
          case Question.DROP_DOWN:
            if (!TextUtils.isEmpty(answerRModel.getChoiceId())) {
              Logger.d(TAG, "DROP_DOWN VALIDATED");
              for (int j = 0; j < question.choices.size(); j++) {
                if (question.choices.get(j).id.equalsIgnoreCase(answerRModel.getChoiceId())) {
                  if (question.choices.get(j).additionalRequired) {
                    if (TextUtils.isEmpty(answerRModel.getValue())) {
                      displayFieldRequired(validate.title, question.title);
                      questionCategoryPositionForAnswerInvalidated(validate.position);
                      return;
                    } else {
                      Logger.d(TAG, "DROP_DOWN EXTRA VALIDATED");
                    }
                  }
                }
              }
            } else {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            break;
          case Question.ADMIN_1_PRIORITY:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "ADMIN_1_PRIORITY VALIDATED");
            break;
          case Question.ADMIN_1:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "ADMIN_1 VALIDATED");
            break;
          case Question.ADMIN_2:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "ADMIN_2 VALIDATED");
            break;
          case Question.ADMIN_3:
            if (TextUtils.isEmpty(answerRModel.getValue())) {
              displayFieldRequired(validate.title, question.title);
              questionCategoryPositionForAnswerInvalidated(validate.position);
              return;
            }
            Logger.d(TAG, "ADMIN_3 VALIDATED");
            break;
        }
      }
    }
    formHasBeenValidated();
  }

  protected void onFormValidationForDebugMode() {
    if (mIsFullQuestionnaire) {
      getQuestionList();
    }
    getAnswerList();
    formHasBeenValidated();
  }

  protected int calculateAnswerTimes(String questionId) {
    if (AnswerController.timesRModelHashMap.containsKey(questionId)) {
      return AnswerController.timesRModelHashMap.get(questionId).answerTimes;
    } else {
      return 1;
    }
  }

  protected String getMessageForToastFormatDisplay(String category, String question) {
    return category + AppConstant.LINE_SEPARATOR + question + AppConstant.HYPHEN_WITH_SPACE;
  }

  private void getQuestionList() {
    questionList.clear();
    categoryTitlesForValidate.clear();
    Questionnaire questionnaire = QuestionnaireController.getQuestionnaire();
    for (int i = 0; i < questionnaire.questionCategories.size(); i++) {
      QuestionCategory category = questionnaire.questionCategories.get(i);
      for (int j = 0; j < category.questionGroupList.size(); j++) {
        QuestionGroup group = category.questionGroupList.get(j);
        for (int k = 0; k < group.questions.size(); k++) {
          CategoryValidate validate = new CategoryValidate(i, category.title);
          categoryTitlesForValidate.add(validate);
          questionList.add(group.questions.get(k));
        }
      }
    }
  }

  private void getAnswerList() {
    answerHashMap = AnswerController.getAnswerHashMap();
    Logger.d("Answers", answerHashMap.toString());
  }

  protected class CategoryValidate {
    int position;
    String title;

    CategoryValidate(int position, String title) {
      this.position = position;
      this.title = title;
    }
  }
}
