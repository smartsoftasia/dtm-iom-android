package com.iom.dtm.presenter;

import android.text.TextUtils;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.interactor.GetEnumeratorReportListInteractor;
import com.iom.dtm.domain.interactor.GetReportViaEmailInteractor;
import com.iom.dtm.domain.interactor.SearchReportInteractor;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.rest.model.Meta;
import com.iom.dtm.view.viewinterface.ViewEnumeratorReportViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 6/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewEnumeratorReportPresenter implements Presenter {
  public static final String TAG = "ViewEnumeratorReportPresenter";

  protected ViewEnumeratorReportViewInterface mView;
  private SearchReportInteractor mSearchReportInteractor;
  private GetEnumeratorReportListInteractor mGetEnumeratorReportListInteractor;
  private GetReportViaEmailInteractor mGetReportViaEmailInteractor;
  private Enumerator mEnumerator;
  private Report mReport;
  private boolean isLoading = false;
  private String mTextQuery;

  @Inject
  public ViewEnumeratorReportPresenter(SearchReportInteractor searchReportInteractor, GetEnumeratorReportListInteractor getEnumeratorReportListInteractor,
                                       GetReportViaEmailInteractor getReportViaEmailInteractor) {
    this.mSearchReportInteractor = searchReportInteractor;
    this.mGetEnumeratorReportListInteractor = getEnumeratorReportListInteractor;
    this.mGetReportViaEmailInteractor = getReportViaEmailInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mSearchReportInteractor.unsubscribe();
    mGetEnumeratorReportListInteractor.unsubscribe();
    mGetReportViaEmailInteractor.unsubscribe();
  }

  public void setView(ViewEnumeratorReportViewInterface view) {
    this.mView = view;
  }

  public void initialize(Enumerator enumerator) {
    this.mEnumerator = enumerator;

    if (enumerator == null) {
      return;
    }

    mGetEnumeratorReportListInteractor.setEnumeratorId(mEnumerator.id);
    mGetEnumeratorReportListInteractor.execute(new GetEnumeratorReportSubscriberApi());
    this.isLoading = true;

    if (mView != null) {
      mView.updateEnumeratorDetails(enumerator);
      mView.onDisplayLoading();
    }
  }

  public void getNextPage() {
    if (!TextUtils.isEmpty(mTextQuery)) {
      if (!isLoading) {
        if (mSearchReportInteractor.isNextPage()) {
          mSearchReportInteractor.setTextQuery(mTextQuery);
          mSearchReportInteractor.setEnumeratorId(mEnumerator.id);
          mSearchReportInteractor.execute(new SearchReportSubscriberApi());
          this.isLoading = true;
        }
      }
    } else {
      if (!isLoading) {
        if (mGetEnumeratorReportListInteractor.isNextPage()) {
          mGetEnumeratorReportListInteractor.execute(new GetEnumeratorReportSubscriberApi());
          this.isLoading = true;
        }
      }
    }
  }

  public void onTextChanged(String textQuery) {
    this.mTextQuery = textQuery;

    if (textQuery.length() == 0) {
      if (mView != null) {
        mView.clearList();
      }
      mGetEnumeratorReportListInteractor.reset();
      mGetEnumeratorReportListInteractor.setEnumeratorId(mEnumerator.id);
      mGetEnumeratorReportListInteractor.execute(new GetEnumeratorReportSubscriberApi());
      this.isLoading = true;
      return;
    }

    if (!isLoading) {
      mSearchReportInteractor.reset();
      mSearchReportInteractor.setTextQuery(textQuery);
      mSearchReportInteractor.setEnumeratorId(mEnumerator.id);
      mSearchReportInteractor.execute(new SearchReportSubscriberApi());
      this.isLoading = true;
    }
    if (mView != null) {
      mView.clearList();
      mView.onDisplayLoading();
    }
  }

  public Enumerator getEnumerator() {
    return mEnumerator;
  }

  public Report getReport() {
    return mReport;
  }

  public void onDownLoadClick(Report report) {
    this.mReport = report;
    mGetReportViaEmailInteractor.setResultId(mReport.id);
    mGetReportViaEmailInteractor.execute(new SentReportSubscriberApi());
    if (mView != null) {
      mView.displayLoadingProgressBar();
    }
  }

  private class GetEnumeratorReportSubscriberApi extends DTMApiSubscriber<List<Report>> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      isLoading = false;
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      isLoading = false;
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }

    @Override
    public void onNext(List<Report> reports) {
      isLoading = false;
      if (mView != null) {
        mView.appendReportList(reports);
        mView.onHideLoading();
      }
    }
  }

  private class SearchReportSubscriberApi extends DTMApiSubscriber<List<Report>> {

    @Override
    public void onCompleted() {
      super.onCompleted();
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      isLoading = false;
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      isLoading = false;
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }

    @Override
    public void onNext(List<Report> reports) {
      isLoading = false;
      if (mView != null) {
        mView.appendReportList(reports);
        mView.onHideLoading();
      }
    }
  }

  private class SentReportSubscriberApi extends DTMApiSubscriber<Object> {
    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.hideLoadingProgressBar();
        mView.onOpenReportHasBeenSentDialog(mReport, mEnumerator);
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.hideLoadingProgressBar();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.hideLoadingProgressBar();
        mView.onError(e);
      }
    }
  }
}
