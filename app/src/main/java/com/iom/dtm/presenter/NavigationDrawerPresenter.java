package com.iom.dtm.presenter;

import com.iom.dtm.R;
import com.iom.dtm.domain.interactor.GetUserInteractor;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.domain.subscriber.AppSubscriber;
import com.iom.dtm.view.fragment.NavigationDrawerFragment;
import com.iom.dtm.view.model.NavigationDrawerModel;
import com.iom.dtm.view.viewinterface.NavigationDrawerViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by androiddev01 on 12/21/2015 AD.
 * NavigationDrawerPresenter
 */
public class NavigationDrawerPresenter implements Presenter {
  public static final String TAG = "NavigationDrawerPresenter";

  private static final int DRAWER_HOME_PROFILE = 0;
  private static final int DRAWER_HOME_ABOUT = 1;
  private static final int DRAWER_VIEW_REPORT = 2;
  private static final int DRAWER_QUESTIONNAIRE = 3;
  private static final int DRAWER_DTM_FORM = 4;
  private static final int DRAWER_ENUMERATORS = 5;
  private static final int DRAWER_LOG_IN = 6;
  private static final int DRAWER_LOG_OUT = 7;
  private static final int DRAWER_ABOUT = 8;

  private ArrayList<Integer> mDrawerActions = new ArrayList<>();
  private NavigationDrawerViewInterface mView;
  private NavigationDrawerFragment.NavigationDrawerListener mNavigationDrawerListener;

  private GetUserInteractor mGetUserInteractor;
  private User mUser;

  @Inject
  public NavigationDrawerPresenter(GetUserInteractor getUserInteractor) {
    this.mGetUserInteractor = getUserInteractor;
  }

  @Override
  public void create() {
    // Do nothing.
  }

  @Override
  public void start() {
    mGetUserInteractor.execute(new GetUserSubscriber());
  }

  @Override
  public void resume() {
    // Do nothing.
  }

  @Override
  public void pause() {
    // Do nothing.
  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mGetUserInteractor.unsubscribe();
  }

  public void initializeDrawer() {
    if (mView == null) {
      return;
    }

    List<NavigationDrawerModel> models = new ArrayList<>();

    mDrawerActions.clear();

    if (mUser == null || mUser.isNewUser()) {
      addDrawerMenu(models, R.string.drawer_home_about, DRAWER_HOME_ABOUT);
      addDrawerMenu(models, R.string.drawer_questionnaire, DRAWER_QUESTIONNAIRE);
      addDrawerMenu(models, R.string.drawer_log_in, DRAWER_LOG_IN);
    } else {
      addDrawerMenu(models, R.string.drawer_home_profile, DRAWER_HOME_PROFILE);
      addDrawerMenu(models, R.string.drawer_dtm_form, DRAWER_DTM_FORM);
      if (mUser.getUserType().equalsIgnoreCase(User.ENUMERATORS_ADMIN)) {
        addDrawerMenu(models, R.string.drawer_enumerators, DRAWER_ENUMERATORS);
      }
      addDrawerMenu(models, R.string.drawer_view_report, DRAWER_VIEW_REPORT);
      addDrawerMenu(models, R.string.drawer_about, DRAWER_ABOUT);
      addDrawerMenu(models, R.string.drawer_log_out, DRAWER_LOG_OUT);
    }

    mView.setNavigationItems(models);
  }

  private void addDrawerMenu(List<NavigationDrawerModel> models, int title,
                             int reference) {
    models.add(new NavigationDrawerModel(title));
    mDrawerActions.add(reference);
  }

  public void itemSelected(int position) {
    if (mNavigationDrawerListener == null) {
      return;
    }

    switch (mDrawerActions.get(position)) {
      case DRAWER_HOME_PROFILE:
        mNavigationDrawerListener.openHomeFragment();
        break;
      case DRAWER_HOME_ABOUT:
        mNavigationDrawerListener.openHomeAboutFragment();
        break;
      case DRAWER_VIEW_REPORT:
        mNavigationDrawerListener.openViewReportFragment();
        break;
      case DRAWER_QUESTIONNAIRE:
        mNavigationDrawerListener.openQuestionnaireForGeneralPublicFragment();
        break;
      case DRAWER_DTM_FORM:
        mNavigationDrawerListener.openDTMFormFragment();
        break;
      case DRAWER_ENUMERATORS:
        mNavigationDrawerListener.openEnumeratorsFragment();
        break;
      case DRAWER_ABOUT:
        mNavigationDrawerListener.openAboutFragment();
        break;
      case DRAWER_LOG_OUT:
        mNavigationDrawerListener.logout();
        break;
      case DRAWER_LOG_IN:
        mNavigationDrawerListener.login();
        break;
    }

    mNavigationDrawerListener.hideDrawer();
  }

  public void setView(NavigationDrawerViewInterface view) {
    this.mView = view;
    initializeDrawer();
  }

  public void setNavigationDrawerListener(
      NavigationDrawerFragment.NavigationDrawerListener mNavigationDrawerListener) {
    this.mNavigationDrawerListener = mNavigationDrawerListener;
  }

  private class GetUserSubscriber extends AppSubscriber<User> {

    @Override
    public void onNext(User user) {
      super.onNext(user);
      mUser = user;
      initializeDrawer();
    }

    @Override
    public void onError(Throwable e) {

    }
  }
}
