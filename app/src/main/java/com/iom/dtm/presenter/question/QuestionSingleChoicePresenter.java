package com.iom.dtm.presenter.question;

import android.text.TextUtils;

import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.view.viewinterface.QuestionSingleChoiceViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/16/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionSingleChoicePresenter extends BaseQuestionPresenter implements Presenter {
  public static final String TAG = "QuestionSingleChoicePresenter";
  protected Question mQuestion;
  protected List<Choice> mChoiceList;
  protected int mIndex = -1;
  protected QuestionSingleChoiceViewInterface mView;

  @Inject
  public QuestionSingleChoicePresenter(AnswerController answerController) {
    super(answerController);
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    mView = null;
  }

  @Override
  public void restart() {

  }

  public void setView(QuestionSingleChoiceViewInterface view) {
    this.mView = view;
  }

  public void initialize(Question question) {
    this.mQuestion = question;
    this.mChoiceList = mQuestion.choices;

    setUpQuestion();
    setUpAnswer();
  }

  public void onSingleChoiceSelected(int index) {
    this.mIndex = index;

    if (mIndex >= mChoiceList.size()) {
      return;
    }

    rAnswerModel.setChoiceId(mChoiceList.get(mIndex).id);

    if (!mChoiceList.get(mIndex).additionalRequired) {
      rAnswerModel.setValue(null);
    }

    updateAnswer();
  }

  public void checkAdditionalRequired() {
    if (mView != null) {
      mView.additionalAnswer(mChoiceList.get(mIndex).additionalRequired);
    }
  }

  public void additionalAnswer(String s) {
    if (mIndex == -1) {
      return;
    }
    if (mChoiceList.get(mIndex).additionalRequired) {
      rAnswerModel.setValue(s);
    } else {
      rAnswerModel.setValue(null);
    }
    updateAnswer();
  }

  private void setUpQuestion() {
    if (mView != null) {
      mView.setUpQuestion(mQuestion);
    }
  }

  private void setUpAnswer() {
    this.rAnswerModel =
        answerController.getAnswer(mQuestion.id, answerTimes) == null ? new AnswerRModel()
            : answerController.getAnswer(
            mQuestion.id,
            answerTimes);
    this.rAnswerModel.setAnswersTimes(answerTimes);

    this.rAnswerModel.setQuestionId(mQuestion.id);
    if (mChoiceList == null) {
      return;
    }

    for (int i = 0; i < mChoiceList.size(); i++) {
      if (mChoiceList.get(i).id.equalsIgnoreCase(rAnswerModel.choiceId)) {
        mIndex = i;
        break;
      }
    }

    if (mView != null) {
      if (mIndex == -1) {
        return;
      }
      mView.setUpAnswer(mIndex, rAnswerModel.getValue(),
          mChoiceList.get(mIndex).additionalRequired);
    }
  }

  private void updateAnswer() {
    if (mIndex != -1) {
      answerController.addOrUpdateAnswer(mQuestion.id, rAnswerModel);
    } else {
      answerController.deleteAnswer(mQuestion.id, answerTimes);
    }
  }
}
