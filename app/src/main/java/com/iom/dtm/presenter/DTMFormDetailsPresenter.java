package com.iom.dtm.presenter;

import android.content.Intent;

import com.iom.dtm.R;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.PhotoVideoController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetQuestionnaireFromDatabaseInteractor;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.subscriber.AppSubscriber;
import com.iom.dtm.view.viewinterface.DTMFormDetailsViewInterface;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/4/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DTMFormDetailsPresenter extends BaseValidateQuestionPresenter implements Presenter {
  public static final String TAG = "DTMFormDetailsPresenter";

  protected GetQuestionnaireFromDatabaseInteractor mGetQuestionnaireFromDatabaseInteractor;
  protected DTMFormDetailsViewInterface mView;
  protected AnswerController mAnswerController;
  protected PhotoVideoController mPhotoVideoController;

  @Inject
  public DTMFormDetailsPresenter(SharedPreference sharedPreference,
                                 GetQuestionnaireFromDatabaseInteractor getQuestionnaireFromDatabaseInteractor,
                                 AnswerController answerController,
                                 PhotoVideoController photoVideoController) {
    super(sharedPreference, true);
    this.mGetQuestionnaireFromDatabaseInteractor = getQuestionnaireFromDatabaseInteractor;
    this.mAnswerController = answerController;
    this.mPhotoVideoController = photoVideoController;
    this.mAnswerController.checkAnswerOnDatabase();
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {
    mGetQuestionnaireFromDatabaseInteractor.execute(new GetQuestionnaireSubscriber());
  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  @Override
  public void restart() {

  }

  @Override
  protected void formHasBeenValidated() {
    if (mView != null) {
      mView.displayAnswerCorrected();
      mView.changeToSummaryPage();
    }
  }

  @Override
  protected void displayFieldRequired(String category, String question) {
    if (mView != null) {
      mView.displayAnswerRequired(
          TranslationHelper.get(R.string.summary_dtm_form_fragment_requires_answer,
              getMessageForToastFormatDisplay(category, question)));
    }
  }

  @Override
  protected void displayInvalidEmail(String category, String question) {
    if (mView != null) {
      mView.displayAnswerRequired(
          TranslationHelper.get(R.string.summary_dtm_form_fragment_invalid_email,
              getMessageForToastFormatDisplay(category, question)));
    }
  }

  @Override
  protected void questionCategoryPositionForAnswerInvalidated(int position) {
    if (mView != null) {
      mView.openQuestionCategory(position);
    }
  }

  public void setView(DTMFormDetailsViewInterface view) {
    this.mView = view;
  }

  public void clearAnswerSingleton() {
    mAnswerController.clearAnswerSingleton();
  }

  public void onDeleteClick() {
    sharedPreference.writeEventId(null);
    clearAnswerSingleton();
    mAnswerController.clearAnswerOnDatabase();
  }

  public void onSaveClick() {
    mAnswerController.saveAnswerToRealm();
  }

  public void onSendFormClick() {
    onFormValidation();
  }

  public void onMediaActivityResult(int requestCode, int resultCode, Intent data) {
    mPhotoVideoController.setMedia(requestCode, resultCode, data);
  }

  private class GetQuestionnaireSubscriber extends AppSubscriber<List<QuestionCategory>> {

    @Override
    public void onError(Throwable e) {
      super.onError(e);
    }

    @Override
    public void onNext(List<QuestionCategory> questionCategories) {
      super.onNext(questionCategories);
      if (mView != null) {
        mView.setUpQuestionCategory(questionCategories);
      }
    }
  }
}
