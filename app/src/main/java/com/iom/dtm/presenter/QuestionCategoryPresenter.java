package com.iom.dtm.presenter;

import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.view.viewinterface.QuestionCategoryViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionCategoryPresenter implements Presenter {
  public static final String TAG = "QuestionCategoryPresenter";

  protected QuestionCategoryViewInterface mView;
  protected QuestionCategory mQuestionCategory;
  private List<QuestionGroup> mGroupList;

  @Inject
  public QuestionCategoryPresenter() {
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  @Override
  public void restart() {

  }

  public void initialize(QuestionCategory questionCategory) {
    this.mQuestionCategory = questionCategory;
    this.mGroupList = mQuestionCategory.questionGroupList;
    handleQuestionGroup();
  }

  private void handleQuestionGroup() {
    if (mGroupList == null || mGroupList.size() == 0) {
      if (mView != null) {
        mView.displayNoQuestion();
      }
    } else {
      for (int i = 0; i < mGroupList.size(); i++) {
        QuestionGroup item = mGroupList.get(i);
        switch (item.getType()) {
          case QuestionGroup.MALE_FEMALE:
            if (mView != null) {
              mView.appendQuestionGroupMaleFemale(item, i);
            }
            break;
          case QuestionGroup.REPEAT:
            if (mView != null) {
              mView.appendQuestionGroupRepeat(item, i);
            }
            break;
          case QuestionGroup.NO_QUESTION:
            if (mView != null) {
              mView.appendQuestionGroupNoQuestion(item, i);
            }
            break;
          case QuestionGroup.ORDERING_NUMBER:
            if (mView != null) {
              mView.appendQuestionGroupOrderingNumber(item, i);
            }
            break;
          default:
            if (mView != null) {
              mView.appendQuestionGroupNormal(item, i);
            }
            break;
        }
      }
    }
  }

  public void setView(QuestionCategoryViewInterface view) {
    this.mView = view;
  }
}
