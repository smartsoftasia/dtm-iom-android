package com.iom.dtm.presenter.viewreport;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetQuestionnaireFromApiByIdInteractor;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.presenter.BaseSharedPreferencePresenter;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.model.QuestionCheckBoxViewModel;
import com.iom.dtm.view.viewinterface.viewreport.ViewReportSelectQuestionViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Jeremy on 10/7/2016 AD.
 * ViewReportSelectQuestionPresenter
 */

public class ViewReportSelectQuestionPresenter extends BaseSharedPreferencePresenter implements Presenter {
  public static final String TAG = "ViewReportSelectProvinc";

  private ViewReportSelectQuestionViewInterface mView;
  private GetQuestionnaireFromApiByIdInteractor mGetQuestionnaireFromApiByIdInteractor;
  private HashMap<String, Question> questionHashMap;
  private List<QuestionCheckBoxViewModel> viewModels;

  @Inject
  public ViewReportSelectQuestionPresenter(GetQuestionnaireFromApiByIdInteractor getQuestionnaireFromApiByIdInteractor, SharedPreference sharedPreference) {
    super(sharedPreference);
    this.mGetQuestionnaireFromApiByIdInteractor = getQuestionnaireFromApiByIdInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mGetQuestionnaireFromApiByIdInteractor.unsubscribe();
  }

  public void initialize(Event event) {
    mGetQuestionnaireFromApiByIdInteractor.setEvent(event);
    mGetQuestionnaireFromApiByIdInteractor.execute(new GetQuestionnaireSubsciber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  public void onAddOrUpdateClick(QuestionCheckBoxViewModel viewModel, boolean isChecked) {
    if (isChecked) {
      questionHashMap.put(viewModel.t.id, viewModel.t);
    } else {
      questionHashMap.remove(viewModel.t.id);
    }
  }

  public void setView(ViewReportSelectQuestionViewInterface view) {
    this.mView = view;
  }

  public void onNextClick() {
    if (questionHashMap == null) {
      return;
    }
    if (questionHashMap.isEmpty()) {
      if (mView != null) {
        mView.displayPleaseSelectQuestion();
      }
    } else {
      if (mView != null) {
        mView.openSelectTimePeriodFragment(new ArrayList<>(questionHashMap.values()));
      }
    }
  }

  private void appendViewModel(Questionnaire questionnaire) {
    questionHashMap = new HashMap<>();
    List<QuestionCheckBoxViewModel> viewModels = new ArrayList<>();

    for (int i = 0; i < questionnaire.questionCategories.size(); i++) {
      QuestionCategory questionCategory = questionnaire.questionCategories.get(i);
      for (int j = 0; j < questionCategory.questionGroupList.size(); j++) {
        QuestionGroup questionGroup = questionCategory.questionGroupList.get(j);
        for (int k = 0; k < questionGroup.questions.size(); k++) {
          Question question = questionGroup.questions.get(k);
          QuestionCheckBoxViewModel viewModel = new QuestionCheckBoxViewModel(question, question.title);
          if (k == 0) {
            viewModel.setSubHeaderCheckbox(questionGroup.getTitle());
          }
          if (j == 0 && k == 0) {
            viewModel.setHeaderCheckbox(questionCategory.getTitle());
          }
          viewModels.add(viewModel);
        }
      }
    }

    this.viewModels = viewModels;

    if (mView != null) {
      mView.appendCheckBoxList(this.viewModels);
    }
  }

  public void onSelectAllClick() {
    if (viewModels == null || viewModels.isEmpty()) {
      return;
    }
    for (int i = 0; i < viewModels.size(); i++) {
      viewModels.get(i).setChecked(true);
      onAddOrUpdateClick(viewModels.get(i), true);
    }

    if (mView != null) {
      mView.onSelectAllCheckBox();
    }
  }

  public void onDeselectAllClick() {
    if (viewModels == null || viewModels.isEmpty()) {
      return;
    }
    for (int i = 0; i < viewModels.size(); i++) {
      viewModels.get(i).setChecked(false);
      onAddOrUpdateClick(viewModels.get(i), false);
    }

    if (mView != null) {
      mView.onDeselectAllCheckBox();
    }
  }

  private class GetQuestionnaireSubsciber extends DTMApiSubscriber<Questionnaire> {
    @Override
    public void onNext(Questionnaire questionnaire) {
      appendViewModel(questionnaire);

      if (mView != null) {
        mView.onHideLoading();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }
    }
  }
}
