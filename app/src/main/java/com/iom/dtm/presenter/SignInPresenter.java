package com.iom.dtm.presenter;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.SignInInteractor;
import com.iom.dtm.domain.interactor.FacebookSignInInteractor;
import com.iom.dtm.domain.interactor.GoogleSignInInteractor;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.helper.DebugAppHelper;
import com.iom.dtm.rest.form.SignInForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.SignInViewInterface;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SignInPresenter implements Presenter {
  public static final String TAG = "SignInPresenter";

  protected SignInViewInterface mView;

  protected SignInInteractor mSignInInteractor;
  protected FacebookSignInInteractor mFacebookSignInInteractor;
  protected GoogleSignInInteractor mGoogleSignInInteractor;
  protected SharedPreference mSharedPreference;

  @Inject
  public SignInPresenter(SignInInteractor signInInteractor,
                         FacebookSignInInteractor mFacebookSignInInteractor,
                         GoogleSignInInteractor mGoogleSignInInteractor,
                         SharedPreference sharedPreference) {
    this.mSignInInteractor = signInInteractor;
    this.mFacebookSignInInteractor = mFacebookSignInInteractor;
    this.mGoogleSignInInteractor = mGoogleSignInInteractor;
    this.mSharedPreference = sharedPreference;
  }

  @Override
  public void pause() {

  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mSignInInteractor.unsubscribe();
    this.mFacebookSignInInteractor.unsubscribe();
    this.mGoogleSignInInteractor.unsubscribe();
    this.mView = null;
  }

  public void setView(SignInViewInterface view) {
    this.mView = view;
  }

  public void initialize() {
    if (mView != null) mView.onEmailEmpty();
  }

  public void loginWithFacebook(String facebookToken) {
    if (mView != null) mView.onDisplayLoading();
    mFacebookSignInInteractor.setFacebookId(facebookToken);
    mFacebookSignInInteractor.execute(new LogInSocialSubscriberApi());
  }

  public void loginWithGoogle(String googleToken) {
    if (mView != null) mView.onDisplayLoading();
    mGoogleSignInInteractor.setGoogleId(googleToken);
    mGoogleSignInInteractor.execute(new LogInSocialSubscriberApi());
  }

  public void validateForm(SignInForm signInForm) {
    boolean isValidate = true;
    if (!signInForm.isPasswordValid()) {
      if (mView != null) mView.onPasswordInvalid();
      isValidate = false;
    }
    if (signInForm.isPasswordEmpty()) {
      if (mView != null) mView.onPasswordEmpty();
      isValidate = false;
    }
    if (!signInForm.isEmailValid()) {
      if (mView != null) mView.onEmailInvalid();
      isValidate = false;
    }
    if (signInForm.isEmailEmpty()) {
      if (mView != null) mView.onEmailEmpty();
      isValidate = false;
    }

    if (isValidate) {
      if (mView != null) mView.onDisplayLoading();
      mSignInInteractor.setSignInForm(signInForm);
      mSignInInteractor.execute(new SignInSubscriberApi());
    }
  }

  private class SignInSubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onNext(User user) {
      super.onNext(user);
      if (mView != null) {
        mView.onHideLoading();
        mView.openHomeActivity();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == 401) {
          mView.onError(TranslationHelper.get(R.string.error_login_failed));
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (AppConfig.DEBUG) {
        mSharedPreference.writeUser(DebugAppHelper.getInstance().getUser());
        if (mView != null) {
          mView.onHideLoading();
          mView.openHomeActivity();
        }
      } else {
        if (mView != null) {
          mView.onHideLoading();
          mView.onError(e);
        }
      }
    }
  }

  private class LogInSocialSubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onNext(User user) {
      super.onNext(user);
      if (mView != null) {
        mView.onHideLoading();
      }
      if (user.isNewUser()) {
        if (mView != null) {
          mView.openSignUpActivity();
        }
      } else {
        if (mView != null) {
          mView.openHomeActivity();
        }
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(errorApiResult.t.getErrorMessage());
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }
}




