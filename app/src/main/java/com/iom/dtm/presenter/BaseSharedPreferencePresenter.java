package com.iom.dtm.presenter;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.model.User;

/**
 * Created by Nott on 9/12/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BaseSharedPreferencePresenter {

  protected SharedPreference sharedPreference;

  public BaseSharedPreferencePresenter(SharedPreference sharedPreference) {
    this.sharedPreference = sharedPreference;
  }

  protected User getUser() {
    return sharedPreference.readUser();
  }
}
