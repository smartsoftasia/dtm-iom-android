package com.iom.dtm.presenter.question;

import android.text.TextUtils;

import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.view.viewinterface.QuestionDropDownViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionDropDownPresenter extends BaseQuestionPresenter implements Presenter {
  public static final String TAG = "QuestionStringPresenter";

  protected Question mQuestion;
  protected QuestionDropDownViewInterface mView;
  private List<Choice> choices;

  @Inject
  public QuestionDropDownPresenter(AnswerController answerController) {
    super(answerController);
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  public void setView(QuestionDropDownViewInterface view) {
    this.mView = view;
  }

  public void initialize(Question question) {
    this.mQuestion = question;
    setUpQuestion();
    setUpAnswer();
  }

  public void onItemIsSelected(int position) {
    rAnswerModel.setChoiceId(choices.get(position).id);

    if (!TextUtils.isEmpty(choices.get(position).id)) {
      answerController.addOrUpdateAnswer(mQuestion.id, rAnswerModel);
    } else {
      answerController.deleteAnswer(mQuestion.id, answerTimes);
    }

    if (mView != null) {
      mView.isHasAdditionalRequiredEditText(choices.get(position).additionalRequired);
    }
  }

  public void onOtherTextChanged(String other) {
    rAnswerModel.setValue(other);
    answerController.addOrUpdateAnswer(mQuestion.id, rAnswerModel);
  }

  public List<Choice> getChoices() {
    return choices;
  }

  private void setUpQuestion() {
    if (mView != null) {
      mView.setUpQuestion(mQuestion);
    }

    this.choices = mQuestion.choices;
  }

  private void setUpAnswer() {
    this.rAnswerModel =
        answerController.getAnswer(mQuestion.id, answerTimes) == null ? new AnswerRModel()
            : answerController.getAnswer(
            mQuestion.id,
            answerTimes);
    this.rAnswerModel.setAnswersTimes(answerTimes);

    this.rAnswerModel.setQuestionId(mQuestion.id);

    if (!TextUtils.isEmpty(rAnswerModel.getValue())) {
      for (int i = 0; i < choices.size(); i++) {
        if (rAnswerModel.getValue().equalsIgnoreCase(choices.get(i).id)) {
          if (mView != null) {
            mView.setUpAnswer(choices.get(i).title);
          }
          break;
        }
      }
    }
  }
}
