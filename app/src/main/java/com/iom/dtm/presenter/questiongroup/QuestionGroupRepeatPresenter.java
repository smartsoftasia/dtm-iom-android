package com.iom.dtm.presenter.questiongroup;

import com.iom.dtm.database.realm.model.AnswerTimesRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.view.viewinterface.QuestionGroupRepeatViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupRepeatPresenter implements Presenter {
  public static final String TAG = "QuestionGroupPresenter";

  protected QuestionGroupRepeatViewInterface mView;
  private QuestionGroup mQuestionGroup;
  private List<Question> mQuestionList;
  private List<Question> mQuestionAdmin;
  private AnswerController mAnswerController;
  private int mIndex = 1;

  @Inject
  public QuestionGroupRepeatPresenter(AnswerController answerController) {
    this.mAnswerController = answerController;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  @Override
  public void restart() {

  }

  public void setView(QuestionGroupRepeatViewInterface view) {
    this.mView = view;
  }

  public void initialize(QuestionGroup questionGroup) {
    this.mQuestionGroup = questionGroup;
    this.mQuestionList = mQuestionGroup.questions;

    setUpQuestionGroup();
    for (int i = 0; i < handleRepeatTimes(); i++) {
      handleQuestion();
    }
  }

  public void handleQuestion() {
    if (mQuestionList == null || mQuestionList.size() == 0) {
      if (mView != null) {
        //mView.displayNoQuestion();
      }
    } else {
      if (mView != null) {
        mView.addLabel(mQuestionGroup.title, mIndex);
      }
      for (int i = 0; i < mQuestionList.size(); i++) {
        Question item = mQuestionList.get(i);
        setUpAnswerTimes(item);
        switch (item.getType()) {
          case Question.STRING:
            if (mView != null) {
              mView.appendQuestionString(item, mIndex);
            }
            break;
          case Question.DATE_PICKER:
            if (mView != null) {
              mView.appendQuestionDatePicker(item, mIndex);
            }
            break;
          case Question.NUMBER_PICKER:
            if (mView != null) {
              //mView.appendQuestionNumberPicker(item, mIndex);
              mView.appendQuestionNumber(item, mIndex);
            }
            break;
          case Question.LOCATION:
            if (mView != null) {
              mView.appendQuestionLocation(item, mIndex);
            }
            break;
          case Question.SINGLE_CHOICE:
            if (mView != null) {
              mView.appendQuestionSingleChoice(item, mIndex);
            }
            break;
          case Question.NUMBER:
            if (mView != null) {
              mView.appendQuestionNumber(item, mIndex);
            }
            break;
          case Question.EMAIL:
            if (mView != null) {
              mView.appendQuestionEmail(item, mIndex);
            }
            break;
          case Question.ATTACHMENT:
            if (mView != null) {
              mView.appendQuestionAttachment(item, mIndex);
            }
            break;
          case Question.TEXT:
            if (mView != null) {
              mView.appendQuestionText(item, mIndex);
            }
            break;
          case Question.DROP_DOWN:
            if (mView != null) {
              mView.appendQuestionDropDown(item, i);
            }
            break;
          case Question.ADMIN_1:
            mQuestionAdmin = new ArrayList<>();
            mQuestionAdmin.add(0, item);
            break;
          case Question.ADMIN_1_PRIORITY:
            mQuestionAdmin = new ArrayList<>();
            mQuestionAdmin.add(0, item);
            break;
          case Question.ADMIN_2:
            mQuestionAdmin.add(1, item);
            break;
          case Question.ADMIN_3:
            mQuestionAdmin.add(2, item);
            appendQuestionAdmin(mIndex);
            break;
          default:
            if (mView != null) {
              mView.appendQuestionString(item, mIndex);
            }
            break;
        }
      }
      mIndex++;
    }
  }

  private void appendQuestionAdmin(int answerTime) {
    QuestionGroup questionGroup = new QuestionGroup();
    questionGroup.questions = mQuestionAdmin;
    if (mView != null) {
      mView.appendQuestionAdmin(questionGroup, answerTime);
    }
  }

  private void setUpQuestionGroup() {
    if (mView != null) {
      mView.setUpQuestionGroup(mQuestionGroup);
    }
  }

  private int handleRepeatTimes() {
    int answerTimes = 1;
    if (AnswerController.timesRModelHashMap == null) {
      return answerTimes;
    }

    for (int i = 0; i < mQuestionList.size(); i++) {
      if (AnswerController.timesRModelHashMap.containsKey(mQuestionList.get(i).id)) {
        int temp = AnswerController.timesRModelHashMap.get(mQuestionList.get(i).id).answerTimes;
        if (temp > answerTimes) {
          answerTimes = temp;
        }
      }
    }

    return answerTimes;
  }

  private void setUpAnswerTimes(Question question) {
    if (mIndex > 1) {
      AnswerTimesRModel rModel = new AnswerTimesRModel();
      rModel.setId(question.id);
      rModel.setAnswerTimes(mIndex);
      mAnswerController.updateAnswerTimes(question.id, rModel);
    }
  }
}