package com.iom.dtm.presenter;

import com.iom.dtm.domain.interactor.SendDeviceTokenInteractor;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.form.RegisterTokenForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 17/10/2559.
 * dtm-iom-android
 */

public class RegisterFCMPresenter implements Presenter {
  public static final String TAG = "RegisterFCMPresenter";

  private SendDeviceTokenInteractor mSendDeviceTokenInteractor;

  @Inject
  public RegisterFCMPresenter(SendDeviceTokenInteractor mSendDeviceTokenInteractor) {
    this.mSendDeviceTokenInteractor = mSendDeviceTokenInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    mSendDeviceTokenInteractor.unsubscribe();
  }

  public void sendToken(RegisterTokenForm registerTokenForm, String userId) {
    mSendDeviceTokenInteractor.setRegisterTokenForm(registerTokenForm);
    mSendDeviceTokenInteractor.setUserId(userId);
    mSendDeviceTokenInteractor.execute(new SendTokenSubscriber());
  }

  private class SendTokenSubscriber extends DTMApiSubscriber<Object> {
    @Override
    public void onCompleted() {
      Logger.d(TAG, "FCM registration completed");
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      Logger.e(TAG, errorApiResult.t.getErrorMessage());
    }

    @Override
    public void onAppError(Throwable e) {
      Logger.e(TAG, e.toString());
    }
  }
}
