package com.iom.dtm.presenter.question;

import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;

/**
 * Created by Nott on 8/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BaseQuestionPresenter {
  protected static final String TAG = "BaseQuestionPresenter";
  protected static final String UNDERSCORE = "_";

  protected AnswerController answerController;
  protected AnswerRModel rAnswerModel;
  protected int answerTimes = 1;

  public BaseQuestionPresenter(AnswerController answerController) {
    this.answerController = answerController;
  }

  public void setAnswerTimes(int answerTimes) {
    this.answerTimes = answerTimes;
  }
}
