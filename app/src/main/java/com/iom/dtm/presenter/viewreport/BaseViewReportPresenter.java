package com.iom.dtm.presenter.viewreport;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.presenter.BaseSharedPreferencePresenter;

/**
 * Created by Nott on 9/26/2016 AD.
 * DTM
 */

public abstract class BaseViewReportPresenter extends BaseSharedPreferencePresenter {
  public static final String TAG = "BaseViewReportPresenter";

  public BaseViewReportPresenter(SharedPreference sharedPreference) {
    super(sharedPreference);
  }
}
