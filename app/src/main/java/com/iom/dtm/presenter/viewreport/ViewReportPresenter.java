package com.iom.dtm.presenter.viewreport;

import com.iom.dtm.core.AppConfig;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.interactor.GetEventAllTimeInteractor;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.ViewReportViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 9/27/2016 AD.
 * DTM
 */

public class ViewReportPresenter implements Presenter {
  public static final String TAG = "ViewReportPresenter";

  protected ViewReportViewInterface mView;
  private GetEventAllTimeInteractor mGetEventAllTimeInteractor;
  private Event mEvent;

  @Inject
  public ViewReportPresenter(GetEventAllTimeInteractor getEventAllTimeInteractor) {
    this.mGetEventAllTimeInteractor = getEventAllTimeInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    this.mEvent = null;
    mGetEventAllTimeInteractor.unsubscribe();
  }

  public void onSelectEventClick() {
    mGetEventAllTimeInteractor.execute(new GetEventAllTimeSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  public void onGetReportClick() {
    if (mEvent == null) {
      if (mView != null) {
        mView.displayPleaseSelectEvent();
      }
    } else {
      if (mView != null) {
        mView.openViewReportHomeActivity(mEvent);
      }
    }
  }

  public void eventSelected(Event event) {
    this.mEvent = event;
  }

  public void setView(ViewReportViewInterface view) {
    this.mView = view;
  }

  private class GetEventAllTimeSubscriber extends DTMApiSubscriber<List<Event>> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }

      if (AppConfig.DEBUG) {
        List<Event> events = new ArrayList<>();
        Event event = new Event();
        event.id = "1";
        event.setTitle("Debug Event");
        events.add(event);

        if (mView != null) {
          mView.displayEventListDialog(events);
          mView.onHideLoading();
        }
      }
    }

    @Override
    public void onNext(List<Event> events) {
      if (mView != null) {
        mView.displayEventListDialog(events);
        mView.onHideLoading();
      }
    }
  }
}
