package com.iom.dtm.presenter.questiongroup;

import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.view.viewinterface.QuestionGroupMaleFemaleViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.presenter.Presenter;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupMaleFemalePresenter implements Presenter {
  public static final String TAG = "QuestionGroupPresenter";
  protected static final String UNDERSCORE = "_";

  protected QuestionGroupMaleFemaleViewInterface mView;
  protected QuestionGroup mQuestionGroup;
  protected List<Question> mQuestionList;
  protected AnswerController mAnswerController;
  protected AnswerRModel maleR, femaleR, totalR;
  private long mMale = 0, mFemale = 0;

  @Inject
  public QuestionGroupMaleFemalePresenter(AnswerController answerController) {
    this.mAnswerController = answerController;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  @Override
  public void restart() {

  }

  public void setView(QuestionGroupMaleFemaleViewInterface view) {
    this.mView = view;
  }

  public void initialize(QuestionGroup questionGroup) {
    this.mQuestionGroup = questionGroup;
    this.mQuestionList = mQuestionGroup.questions;

    setUpQuestion();
    setUpAnswer();
  }

  public void onMaleNumberHasChanged(long male) {
    this.mMale = male;
    calculateTotalNumber();
    if (mMale >= 0) {
      maleR.setValue(String.valueOf(mMale));
      mAnswerController.addOrUpdateAnswer(mQuestionList.get(0).id, maleR);
    } else {
      mAnswerController.deleteAnswer(mQuestionList.get(0).id, 1);
    }
  }

  public void onFemaleNumberHasChanged(long female) {
    this.mFemale = female;
    calculateTotalNumber();
    if (mFemale >= 0) {
      femaleR.setValue(String.valueOf(mFemale));
      mAnswerController.addOrUpdateAnswer(mQuestionList.get(1).id, femaleR);
    } else {
      mAnswerController.deleteAnswer(mQuestionList.get(1).id, 1);
    }
  }

  private void calculateTotalNumber() {
    if (mView != null) {
      Long total = mMale + mFemale;
      mView.setUpTotalNumber(total);
      if (total >= 0) {
        totalR.setValue(String.valueOf(total));
        mAnswerController.addOrUpdateAnswer(mQuestionList.get(2).id, totalR);
      } else {
        mAnswerController.deleteAnswer(mQuestionList.get(2).id, 1);
      }
    }
  }

  private void setUpAnswer() {
    this.maleR =
        mAnswerController.getAnswer(mQuestionList.get(0).id, 1) == null ? new AnswerRModel()
                                                                        : mAnswerController.getAnswer(
                                                                            mQuestionList.get(0).id,
                                                                            1);
    this.femaleR =
        mAnswerController.getAnswer(mQuestionList.get(1).id, 1) == null ? new AnswerRModel()
                                                                        : mAnswerController.getAnswer(
                                                                            mQuestionList.get(1).id,
                                                                            1);
    this.totalR =
        mAnswerController.getAnswer(mQuestionList.get(2).id, 1) == null ? new AnswerRModel()
                                                                        : mAnswerController.getAnswer(
                                                                            mQuestionList.get(2).id,
                                                                            1);

    maleR.setAnswersTimes(1);
    femaleR.setAnswersTimes(1);
    totalR.setAnswersTimes(1);

    maleR.setQuestionId(mQuestionList.get(0).id);
    femaleR.setQuestionId(mQuestionList.get(1).id);
    totalR.setQuestionId(mQuestionList.get(2).id);

    if (mView != null) {
      mView.setUpAnswer(maleR.getValue(), femaleR.getValue(), totalR.getValue());
    }
  }

  private void setUpQuestion() {
    if (mView != null) {
      mView.setUpQuestion(mQuestionGroup);
    }
  }
}