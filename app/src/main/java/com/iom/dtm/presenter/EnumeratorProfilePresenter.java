package com.iom.dtm.presenter;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.interactor.DeactivateEnumeratorInteractor;
import com.iom.dtm.domain.interactor.EditEnumeratorProfileInteractor;
import com.iom.dtm.domain.interactor.GetEnumeratorProfileInteractor;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.rest.model.Meta;
import com.iom.dtm.view.viewinterface.EnumeratorProfileViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 6/20/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EnumeratorProfilePresenter implements Presenter {
  public static final String TAG = "EnumeratorProfilePresenter";

  protected GetEnumeratorProfileInteractor mGetEnumeratorProfileInteractor;
  protected EditEnumeratorProfileInteractor mEditEnumeratorProfileInteractor;
  protected DeactivateEnumeratorInteractor mDeactivateEnumeratorInteractor;
  protected EnumeratorProfileViewInterface mView;
  protected Enumerator mEnumerator;

  @Inject
  public EnumeratorProfilePresenter(GetEnumeratorProfileInteractor getEnumeratorProfileInteractor,
                                    EditEnumeratorProfileInteractor editEnumeratorProfileInteractor,
                                    DeactivateEnumeratorInteractor deactivateEnumeratorInteractor) {
    this.mGetEnumeratorProfileInteractor = getEnumeratorProfileInteractor;
    this.mEditEnumeratorProfileInteractor = editEnumeratorProfileInteractor;
    this.mDeactivateEnumeratorInteractor = deactivateEnumeratorInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    mGetEnumeratorProfileInteractor.unsubscribe();
    mEditEnumeratorProfileInteractor.unsubscribe();
    mDeactivateEnumeratorInteractor.unsubscribe();
    this.mView = null;
  }

  public void setView(EnumeratorProfileViewInterface view) {
    this.mView = view;
  }

  public void initialize(Enumerator enumerator) {
    this.mEnumerator = enumerator;

    if (mView != null) {
      mView.updateEnumeratorProfile(mEnumerator);
    }
  }

  public void onDeactivateClick() {
    mDeactivateEnumeratorInteractor.setEnumeratorId(mEnumerator.id);
    mDeactivateEnumeratorInteractor.execute(new DeactivateEnumeratorSubscriber());

    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private class DeactivateEnumeratorSubscriber extends DTMApiSubscriber<Object> {
    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.onHideLoading();
        mView.goBackToEnumeratorsFragmentAfterDeactivated();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }
    }
  }
}
