package com.iom.dtm.presenter;

import android.text.TextUtils;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.interactor.DeactivateEnumeratorInteractor;
import com.iom.dtm.domain.interactor.GetEnumeratorListInteractor;
import com.iom.dtm.domain.interactor.SearchEnumeratorInteractor;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.EnumeratorsViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 7/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EnumeratorsPresenter implements Presenter {
  public static final String TAG = "EnumeratorsPresenter";

  protected EnumeratorsViewInterface mView;
  protected SearchEnumeratorInteractor mSearchEnumeratorInteractor;
  protected GetEnumeratorListInteractor mGetEnumeratorListInteractor;
  protected DeactivateEnumeratorInteractor mDeactivateEnumeratorInteractor;
  protected boolean isLoading = false;
  private String mEnumeratorId;
  private String mTextQuery;

  @Inject
  public EnumeratorsPresenter(SearchEnumeratorInteractor searchEnumeratorInteractor,
                              GetEnumeratorListInteractor getEnumeratorListInteractor,
                              DeactivateEnumeratorInteractor deactivateEnumeratorInteractor) {
    this.mSearchEnumeratorInteractor = searchEnumeratorInteractor;
    this.mGetEnumeratorListInteractor = getEnumeratorListInteractor;
    this.mDeactivateEnumeratorInteractor = deactivateEnumeratorInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mSearchEnumeratorInteractor.unsubscribe();
    mGetEnumeratorListInteractor.unsubscribe();
    mDeactivateEnumeratorInteractor.unsubscribe();
  }

  public void setView(EnumeratorsViewInterface view) {
    this.mView = view;
  }

  public void initialize() {
    if (mView != null) {
      mView.onDisplayLoading();
    }
    mGetEnumeratorListInteractor.execute(new GetEnumeratorListSubscriberApi());
    this.isLoading = true;
  }

  public void getNextPage() {
    if (!TextUtils.isEmpty(mTextQuery)) {
      if (!isLoading) {
        if (mSearchEnumeratorInteractor.isNextPage()) {
          mSearchEnumeratorInteractor.setKeyQuery(mTextQuery);
          mSearchEnumeratorInteractor.execute(new SearchEnumeratorListSubscriberApi());
          this.isLoading = true;
        }
      }
    } else {
      if (!isLoading) {
        if (mGetEnumeratorListInteractor.isNextPage()) {
          mGetEnumeratorListInteractor.execute(new GetEnumeratorListSubscriberApi());
          this.isLoading = true;
        }
      }
    }
  }

  public void onTextChanged(String textQuery) {
    this.mTextQuery = textQuery;

    if (textQuery.length() == 0) {
      if (mView != null) {
        mView.clearEnumeratorList();
      }
      mGetEnumeratorListInteractor.reset();
      mGetEnumeratorListInteractor.execute(new GetEnumeratorListSubscriberApi());
      this.isLoading = true;
      return;
    }

    if (!isLoading) {
      mSearchEnumeratorInteractor.setKeyQuery(textQuery);
      mSearchEnumeratorInteractor.execute(new SearchEnumeratorListSubscriberApi());
      this.isLoading = true;
    }

    if (mView != null) {
      mView.clearEnumeratorList();
      mView.onDisplayLoading();
    }
  }

  public void onEnumeratorDialogClick(String enumeratorId) {
    this.mEnumeratorId = enumeratorId;
  }

  public void onDeactivateClick() {
    mDeactivateEnumeratorInteractor.setEnumeratorId(mEnumeratorId);
    mDeactivateEnumeratorInteractor.execute(new DeactivateEnumeratorSubscriberApi());

    if (mView != null) {
      mView.displayLoadingForDeactivated();
    }
  }

  private class DeactivateEnumeratorSubscriberApi extends DTMApiSubscriber<Object> {

    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.hideLoadingForDeactivated();
        mView.clearEnumeratorList();
        initialize();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.hideLoadingForDeactivated();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.hideLoadingForDeactivated();
        mView.onError(e);
      }
    }
  }

  private class GetEnumeratorListSubscriberApi extends DTMApiSubscriber<List<Enumerator>> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      isLoading = false;
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      isLoading = false;
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }
    }

    @Override
    public void onNext(List<Enumerator> s) {
      isLoading = false;
      if (mView != null) {
        mView.appendEnumeratorList(s);
        mView.onHideLoading();
      }
    }
  }

  private class SearchEnumeratorListSubscriberApi extends DTMApiSubscriber<List<Enumerator>> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      isLoading = false;
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      isLoading = false;
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }
    }

    @Override
    public void onNext(List<Enumerator> s) {
      isLoading = false;
      if (mView != null) {
        mView.appendEnumeratorListFromSearch(s);
        mView.onHideLoading();
      }
    }
  }
}
