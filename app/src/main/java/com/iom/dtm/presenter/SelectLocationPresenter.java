package com.iom.dtm.presenter;

import com.google.android.gms.maps.model.LatLng;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.view.viewinterface.SelectLocationViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import javax.inject.Inject;

/**
 * Created by Nott on 8/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SelectLocationPresenter implements Presenter {
  public static final String TAG = "SelectLocationPresenter";
  protected static final String UNDERSCORE = "_";

  protected SelectLocationViewInterface mView;
  protected AnswerController mAnswerController;
  private String mQuestionId;
  private int mAnswerTimes = 1;
  private LatLng mLatLng = new LatLng(0.0, 0.0);

  @Inject
  public SelectLocationPresenter(AnswerController answerController) {
    this.mAnswerController = answerController;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  @Override
  public void restart() {

  }

  public void initialize(String questionId, int answerTime) {
    this.mQuestionId = questionId;
    this.mAnswerTimes = answerTime;
  }

  public void setView(SelectLocationViewInterface view) {
    this.mView = view;
  }

  public void onLatLngChanged(LatLng latLng) {
    this.mLatLng = latLng;
  }

  public void onConfirmClick() {
    DecimalFormat df = new DecimalFormat("#.######");
    df.setRoundingMode(RoundingMode.CEILING);

    if (mLatLng == null) {
      return;
    }
    AnswerRModel answerRModel = new AnswerRModel();
    answerRModel.setQuestionId(mQuestionId + UNDERSCORE + mAnswerTimes);
    answerRModel.setValue(df.format(mLatLng.latitude) + "," + df.format(mLatLng.longitude));
    answerRModel.setAnswersTimes(mAnswerTimes);

    mAnswerController.addOrUpdateAnswer(mQuestionId, answerRModel);

    if (mView != null) {
      mView.goBackToPreviousActivity();
    }
  }
}
