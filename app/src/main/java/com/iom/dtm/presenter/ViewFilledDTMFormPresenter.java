package com.iom.dtm.presenter;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.DownloadReportInteractor;
import com.iom.dtm.domain.interactor.GetReportViaEmailInteractor;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.rest.model.Meta;
import com.iom.dtm.view.viewinterface.ViewFilledDTMFormViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Nott on 6/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewFilledDTMFormPresenter implements Presenter {
  public static final String TAG = "ViewFilledDTMFormPresenter";
  private static final String KEY = "Key ";

  private ViewFilledDTMFormViewInterface mView;
  protected GetReportViaEmailInteractor mGetReportViaEmailInteractor;
  protected SharedPreference mSharedPreference;
  protected Report mReport;

  @Inject
  public ViewFilledDTMFormPresenter(GetReportViaEmailInteractor getReportViaEmailInteractor, SharedPreference sharedPreference) {
    this.mGetReportViaEmailInteractor = getReportViaEmailInteractor;
    this.mSharedPreference = sharedPreference;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    mView = null;
    mGetReportViaEmailInteractor.unsubscribe();
  }

  public void initialize(Report report) {
    this.mReport = report;

    Map<String, String> extraHeaders = new HashMap<>();
    extraHeaders.put("Authorization", getAuthorizationHeader(mSharedPreference.readUserToken()));
    if (mView != null) {
      mView.settingUpWebView(extraHeaders);
    }
  }

  public void onDownLoadClick() {
    if (mView != null) {
      mView.onDisplayLoading();
    }
    mGetReportViaEmailInteractor.setResultId(mReport.id);
    mGetReportViaEmailInteractor.execute(new DownloadReportSubscriberApi());
  }

  public void setView(ViewFilledDTMFormViewInterface view) {
    this.mView = view;
  }

  private String getAuthorizationHeader(String token) {
    return KEY + token;
  }

  private class DownloadReportSubscriberApi extends DTMApiSubscriber<Object> {
    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.onHideLoading();
        mView.displayReportHasBeenSentDialog();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }
}
