package com.iom.dtm.presenter.viewreport;

import com.iom.dtm.view.viewinterface.viewreport.ViewReportSelectPeriodTimeViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.Date;

import javax.inject.Inject;

/**
 * Created by Nott on 11/10/2559.
 * dtm-iom-android
 */

public class ViewReportSelectPeriodTimePresenter implements Presenter {
  public static final String TAG = "ViewReportSelectPeriod";

  private Date startDate, endDate;
  private ViewReportSelectPeriodTimeViewInterface mView;

  @Inject
  public ViewReportSelectPeriodTimePresenter() {
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {

  }

  @Override
  public void restart() {

  }

  public void setView(ViewReportSelectPeriodTimeViewInterface view) {
    this.mView = view;
  }

  public void startDateHasBeenSelected(Date date) {
    this.startDate = date;
  }

  public void endDateHasBeenSelected(Date date) {
    this.endDate = date;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void onNextClick() {
    if (startDate == null || endDate == null) {
      if (mView != null) {
        mView.displaySelectDate();
      }
    } else {
      if (mView != null) {
        mView.openViewReportSummaryFragment(startDate, endDate);
      }
    }
  }
}
