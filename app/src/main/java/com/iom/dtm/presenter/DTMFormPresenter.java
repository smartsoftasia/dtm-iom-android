package com.iom.dtm.presenter;

import com.iom.dtm.core.AppConfig;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.database.realm.mapper.RealmToQuestionnaireMapper;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetEventInteractor;
import com.iom.dtm.domain.interactor.GetQuestionnaireFromApiInteractor;
import com.iom.dtm.domain.interactor.GetTagsInteractor;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.domain.model.Tag;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.DTMFormViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DTMFormPresenter extends BaseSharedPreferencePresenter implements Presenter {
  public static final String TAG = "DTMFormPresenter";

  protected Questionnaire mQuestionnaire;
  protected List<String> questionCategoryList = new ArrayList<>();
  protected DTMFormViewInterface mView;

  private int mDTMCategoryPosition = 0, mPositionSpinnerIndex = 0, mPositionEvent = 0;
  private QuestionnaireController mQuestionnaireController;
  private GetQuestionnaireFromApiInteractor mGetQuestionnaireFromApiInteractor;
  private GetTagsInteractor mGetTagsInteractor;
  private GetEventInteractor mGetEventInteractor;
  private List<Event> mEvents;

  @Inject
  public DTMFormPresenter(SharedPreference sharedPreference,
                          GetQuestionnaireFromApiInteractor getQuestionnaireFromApiInteractor,
                          QuestionnaireController questionnaireController,
                          GetTagsInteractor getTagsInteractor,
                          GetEventInteractor getEventInteractor) {
    super(sharedPreference);
    this.mGetQuestionnaireFromApiInteractor = getQuestionnaireFromApiInteractor;
    this.mQuestionnaireController = questionnaireController;
    this.mGetTagsInteractor = getTagsInteractor;
    this.mGetEventInteractor = getEventInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {
    mGetEventInteractor.execute(new GetEventSubscriber());

    if (RealmToQuestionnaireMapper.transform(
        mQuestionnaireController.getQuestionnaireFromDatabase()) == null) {
      return;
    }

    mQuestionnaire = RealmToQuestionnaireMapper.transform(
        mQuestionnaireController.getQuestionnaireFromDatabase());

    questionCategoryList.clear();

    for (int i = 0; i < mQuestionnaire.questionCategories.size(); i++) {
      questionCategoryList.add(mQuestionnaire.questionCategories.get(i).getTitle());
    }

    if (mView != null) {
      mView.appendListOfQuestionCategory(questionCategoryList);
    }
  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mGetQuestionnaireFromApiInteractor.unsubscribe();
    mGetTagsInteractor.unsubscribe();
    mGetEventInteractor.unsubscribe();
  }

  public void setView(DTMFormViewInterface view) {
    this.mView = view;
  }

  public void openDTMForm(int position) {
    String eventId = sharedPreference.readEventId();
    this.mDTMCategoryPosition = position;
    if (mPositionSpinnerIndex == 0 || mPositionSpinnerIndex == -1) {
      if (mView != null) {
        mView.displayPleaseSelectEventBeforeStartForm();
      }
      return;
    }
    if (eventId != null) {
      if (mPositionSpinnerIndex - 1 != 0 || mPositionSpinnerIndex - 1 != -1) {
        if (!mEvents.get(mPositionSpinnerIndex - 1).id.equalsIgnoreCase(eventId)) {
          if (mView != null) {
            mView.displaySelectNewEventConfirmation();
          }
          return;
        }
      }
    }
    checkDTMFormVersion();
  }

  public void onSpinnerItemSelected(int position) {
    this.mPositionSpinnerIndex = position;
  }

  private void updateEventList() {
    String eventId = sharedPreference.readEventId();
    List<Event> eventList = new ArrayList<>();
    eventList.add(Event.getDefaultEvent());

    if (mEvents != null) {
      eventList.addAll(mEvents);
      for (int i = 0; i < eventList.size(); i++) {
        if (eventId == null) {
          break;
        }
        if (eventList.get(i).id.equalsIgnoreCase(eventId)) {
          mPositionEvent = i;
          break;
        }
      }
    }

    if (mView != null) {
      mView.updateEventListSpinner(eventList, mPositionEvent);
    }
  }

  public void checkDTMFormVersion() {
    mGetQuestionnaireFromApiInteractor.setFullQuestionnaire(true);
    mGetQuestionnaireFromApiInteractor.setEvent(mEvents.get(mPositionSpinnerIndex - 1));
    mGetQuestionnaireFromApiInteractor.execute(new GetQuestionnaireSubscriber());
    if (mView != null) {
      mView.displayLoading();
    }
  }

  private class GetQuestionnaireSubscriber extends DTMApiSubscriber<Questionnaire> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.hideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
      mQuestionnaireController.clearQuestionnaireCreatedDateLocal();
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.hideLoading();
        mView.onError(e);
      }
      mQuestionnaireController.clearQuestionnaireCreatedDateLocal();
    }

    @Override
    public void onNext(Questionnaire questionnaire) {
      super.onNext(questionnaire);
      mGetTagsInteractor.execute(new GetTagsSubscriber());
    }
  }

  private class GetTagsSubscriber extends DTMApiSubscriber<List<Tag>> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.hideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.hideLoading();
        mView.onError(e);
      }
    }

    @Override
    public void onNext(List<Tag> tags) {
      super.onNext(tags);

      mQuestionnaireController.saveTagsOnDatabase(tags);
      mQuestionnaireController.setTagsOnSingleton();
      if (mView != null) {
        mView.hideLoading();
        mView.displayGeneratingFormMessage();
        mView.openDTMFormDetailsActivity(mDTMCategoryPosition);
      }
    }
  }

  private class GetEventSubscriber extends DTMApiSubscriber<List<Event>> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (AppConfig.DEBUG) {
        List<Event> eventList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
          Event event = new Event();
          event.id = i + "";
          event.title = "Event Test " + i;
          eventList.add(event);
        }
        mEvents = eventList;
        updateEventList();
      }
      if (mView != null) {
        mView.onError(e);
      }
    }

    @Override
    public void onNext(List<Event> events) {
      mEvents = events;
      updateEventList();
    }
  }
}
