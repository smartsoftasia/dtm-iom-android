package com.iom.dtm.presenter.questiongroup;

import android.text.TextUtils;
import com.google.common.collect.Iterables;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.presenter.question.BaseQuestionPresenter;
import com.iom.dtm.view.model.QuestionNumberViewModel;
import com.iom.dtm.view.viewinterface.QuestionGroupOrderingNumberViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.presenter.Presenter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupOrderingNumberPresenter extends BaseQuestionPresenter
    implements Presenter {
  public static final String TAG = "QuestionGroupPresenter";

  protected QuestionGroupOrderingNumberViewInterface mView;
  protected QuestionGroup mQuestionGroup;
  protected List<Question> mQuestionList;
  private List<Integer> orders = new ArrayList<>();
  private int mMaxSize = 0;
  private List<QuestionNumberViewModel> viewModels = new ArrayList<>();

  @Inject
  public QuestionGroupOrderingNumberPresenter(AnswerController answerController) {
    super(answerController);
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  @Override
  public void restart() {

  }

  public void setView(QuestionGroupOrderingNumberViewInterface view) {
    this.mView = view;
  }

  public void initialize(QuestionGroup questionGroup) {
    this.mQuestionGroup = questionGroup;
    this.mQuestionList = mQuestionGroup.questions;

    setUpQuestionGroup();
    handleQuestion();
  }

  public void handleQuestion() {
    if (mQuestionList == null || mQuestionList.size() == 0) {
      if (mView != null) {
        //mView.displayNoQuestion();
      }
    } else {
      if (mView != null) {
        setUpAnswer();
      }
    }
  }

  public List<Integer> getOrders() {
    Collections.sort(orders);
    return orders;
  }

  public void removeOrder(int position) {
    orders.remove(position);
  }

  public void removeOrderObject(int object) {
    for (int i = 0; i < orders.size(); i++) {
      if (object == orders.get(i)) {
        orders.remove(i);
        return;
      }
    }
  }

  public void replaceOrder(int position, int old) {
    orders.remove(position);
    orders.add(old);
  }

  public void replaceOrderObject(int newNumber, int old) {
    for (int i = 0; i < orders.size(); i++) {
      if (newNumber == orders.get(i)) {
        orders.remove(i);
        break;
      }
    }
    orders.add(old);
  }

  public void addOrder(int old) {
    orders.add(old);
  }

  public void onItemSelect(AnswerRModel answerRModel) {
    Logger.e("onItemSelect", answerRModel.getValue());
    if (!TextUtils.isEmpty(answerRModel.getValue())) {
      answerController.addOrUpdateAnswer(answerRModel.questionId, answerRModel);
    } else {
      answerController.deleteAnswer(answerRModel.questionId, answerTimes);
    }
  }

  public int getMaxSize() {
    return mMaxSize;
  }

  public void onTextChanged(String key) {
    AnswerRModel noneOfAboveAnswer = answerController.getAnswer(Iterables.getLast(mQuestionList).id,
        answerTimes);
    if (noneOfAboveAnswer == null) {
      return;
    }

    if (TextUtils.isEmpty(noneOfAboveAnswer.getValue())) {
      return;
    }

    String[] strings = TextUtils.split(noneOfAboveAnswer.getValue(), AppConstant.COMMA);
    String index = strings[0];
    if (!TextUtils.isEmpty(key)) {
      String answer = index + AppConstant.COMMA + key;
      noneOfAboveAnswer.setValue(answer);
    } else {
      noneOfAboveAnswer.setValue(index);
    }
    Logger.e(TAG, noneOfAboveAnswer.getValue());
    answerController.addOrUpdateAnswer(noneOfAboveAnswer.questionId, noneOfAboveAnswer);
  }

  private void setUpQuestionGroup() {
    if (mView != null) {
      mView.setUpQuestionGroup(mQuestionGroup);
    }
  }

  private void setUpAnswer() {
    this.mMaxSize = mQuestionList.size();
    for (int i = 0; i < mQuestionList.size(); i++) {
      AnswerRModel answerRModel = answerController.getAnswer(mQuestionList.get(i).id, answerTimes);
      QuestionNumberViewModel model = new QuestionNumberViewModel();
      model.setQuestion(mQuestionList.get(i));

      if (i != Iterables.size(mQuestionList) - 1) {
        if (answerRModel != null) {
          model.setAnswerRModel(answerRModel);
          if (TextUtils.isEmpty(answerRModel.getValue())) {
            orders.add(i + 1);
          }
        } else {
          orders.add(i + 1);
        }
      } else {
        if (answerRModel != null) {
          if (TextUtils.isEmpty(answerRModel.getValue())) {
            orders.add(i + 1);
          } else if (answerRModel.getValue().contains(AppConstant.COMMA)) {
            String[] strings = TextUtils.split(answerRModel.getValue(),
                AppConstant.COMMA);
            answerRModel.setValue(strings[0]);
            model.setAnswerRModel(answerRModel);
            if (mView != null) {
              mView.setUpOtherEditText(true, strings[1]);
            }
          } else {
            if (isInteger(answerRModel.getValue())) {
              model.setAnswerRModel(answerRModel);
              if (mView != null) {
                mView.setUpOtherEditText(true, AppConstant.EMPTY);
              }
            } else {
              model.setAnswerRModel(answerRModel);
            }
          }
        } else {
          orders.add(i + 1);
        }
      }
      viewModels.add(model);
    }

    mView.appendQuestion(viewModels);
  }

  private boolean isInteger(String s) {
    try {
      Integer.parseInt(s);
    } catch (NumberFormatException e) {
      return false;
    } catch (NullPointerException e) {
      return false;
    }
    return true;
  }
}