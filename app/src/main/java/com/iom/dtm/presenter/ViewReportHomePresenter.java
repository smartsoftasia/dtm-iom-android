package com.iom.dtm.presenter;

import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.ViewReport;
import com.iom.dtm.view.viewinterface.ViewReportHomeViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 7/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewReportHomePresenter implements Presenter {
  public static final String TAG = "ViewReportHomePresenter";

  private Event mEvent;
  private ViewReportHomeViewInterface mView;
  private List<Admin1> admin1s;
  private List<Question> questions;
  private Date startDate, endDate;

  @Inject
  public ViewReportHomePresenter() {
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  public void admin1Selected(List<Admin1> admin1s) {
    this.admin1s = admin1s;
  }

  public void questionSelected(List<Question> questions) {
    this.questions = questions;
  }

  public void setStartEndDate(Date startDate, Date endDate) {
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public void initialize(Event event) {
    this.mEvent = event;
  }

  public void setView(ViewReportHomeViewInterface view) {
    this.mView = view;
  }

  public Event getEvent() {
    return mEvent;
  }

  public ViewReport getViewReport() {
    ViewReport viewReport = new ViewReport();
    viewReport.setEvent(mEvent);
    viewReport.setAdmin1s(admin1s);
    viewReport.setQuestions(questions);
    viewReport.setStartDate(startDate);
    viewReport.setEndDate(endDate);
    return viewReport;
  }

}
