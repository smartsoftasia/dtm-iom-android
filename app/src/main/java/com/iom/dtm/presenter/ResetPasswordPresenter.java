package com.iom.dtm.presenter;

import com.iom.dtm.R;
import com.iom.dtm.domain.interactor.ResetPasswordInteractor;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.form.ResetPasswordForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.ResetPasswordViewInterface;
import com.smartsoftasia.ssalibrary.domain.subscriber.DefaultSubscriber;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

public class ResetPasswordPresenter implements Presenter {
  public static final String TAG = "ResetPasswordPresenter";

  private ResetPasswordViewInterface mView;

  private ResetPasswordInteractor mResetPasswordInteractor;

  @Inject
  public ResetPasswordPresenter(ResetPasswordInteractor resetPasswordInteractor) {
    this.mResetPasswordInteractor = resetPasswordInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  public void setView(ResetPasswordViewInterface view) {
    this.mView = view;
  }

  public void initialize() {
    if (mView != null) mView.onDisplayResetPassword();
  }

  public void resetClick(ResetPasswordForm resetPasswordForm) {

    boolean isValid = true;
    if (!resetPasswordForm.isEmailValid()) {
      if (mView != null) mView.onInvalidEmail();
      isValid = false;
    }

    if (resetPasswordForm.isEmailEmpty()) {
      if (mView != null) mView.onEmptyEmail();
      isValid = false;
    }

    if (isValid) {
      if (mView != null) mView.onDisplayLoading();
      mResetPasswordInteractor.setResetPasswordForm(resetPasswordForm);
      mResetPasswordInteractor.execute(new ResetPasswordSubscriber());
    }
  }

  private class ResetPasswordSubscriber extends DTMApiSubscriber {

    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.onEmailAlreadySent();
        mView.onHideLoading();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == 422) {
          mView.onError(TranslationHelper.get(R.string.error_cant_find_email));
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }
    }
  }
}





