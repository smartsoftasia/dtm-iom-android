package com.iom.dtm.presenter;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.interactor.GetUserFromAPIInteractor;
import com.iom.dtm.domain.interactor.GetUserInteractor;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.domain.subscriber.AppSubscriber;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.HomeViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 6/29/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class HomePresenter implements Presenter {
  public static final String TAG = "HomePresenter";

  protected GetUserFromAPIInteractor mGetUserFromAPIInteractor;
  protected GetUserInteractor mGetUserInteractor;
  protected HomeViewInterface mView;
  protected User mUser;

  @Inject
  public HomePresenter(GetUserFromAPIInteractor getUserFromAPIInteractor,
                       GetUserInteractor getUserInteractor) {
    this.mGetUserFromAPIInteractor = getUserFromAPIInteractor;
    this.mGetUserInteractor = getUserInteractor;
  }

  @Override
  public void create() {
  }

  @Override
  public void start() {
    mGetUserInteractor.execute(new GetUserSubscriber());
  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mGetUserFromAPIInteractor.unsubscribe();
    mGetUserInteractor.unsubscribe();
  }

  public void setView(HomeViewInterface view) {
    this.mView = view;
  }

  private class GetUserFormAPISubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onNext(User user) {
      mUser = user;
      if (mView != null) {
        mView.onUpdateProfile(mUser);
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
        mView.onUnauthorized();
      }
    }

    @Override
    public void onAppError(Throwable e) {
      //
    }
  }

  private class GetUserSubscriber extends AppSubscriber<User> {

    @Override
    public void onNext(User user) {
      mUser = user;
      if (mView != null) {
        mView.onUpdateProfile(mUser);
      }
      mGetUserFromAPIInteractor.execute(new GetUserFormAPISubscriberApi());
    }

    @Override
    public void onError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
      }
    }
  }
}
