package com.iom.dtm.presenter.question;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.PhotoVideoController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.interactor.GetImageInteractor;
import com.iom.dtm.domain.interactor.GetVideoInteractor;
import com.iom.dtm.domain.interactor.UploadFileToS3Interactor;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.subscriber.AppSubscriber;
import com.iom.dtm.helper.JSONHelper;
import com.iom.dtm.rest.form.AttachmentForm;
import com.iom.dtm.view.viewinterface.QuestionPhotoVideoViewInterface;
import com.smartsoftasia.rxcamerapicker.Sources;
import com.smartsoftasia.ssalibrary.helper.GsonHelper;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.presenter.Presenter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionPhotoVideoPresenter extends BaseQuestionPresenter implements Presenter {
  public static final String TAG = "QuestionPhotoVideoPresenter";

  protected QuestionPhotoVideoViewInterface mView;
  private Question mQuestion;
  private PhotoVideoController mPhotoVideoController;
  private UploadFileToS3Interactor mUploadFileToS3Interactor;
  private GetImageInteractor mGetImageInteractor;
  private GetVideoInteractor mGetVideoInteractor;
  private HashMap<String, String> tagSelectedHashMap;
  private AttachmentForm attachmentForm;
  private String mTAGAdditionalRequired, mTAGAdditionalRequiredID;

  @Inject
  public QuestionPhotoVideoPresenter(AnswerController answerController,
                                     PhotoVideoController photoVideoController,
                                     UploadFileToS3Interactor uploadFileToS3Interactor,
                                     GetImageInteractor getImageInteractor,
                                     GetVideoInteractor getVideoInteractor) {
    super(answerController);
    this.mPhotoVideoController = photoVideoController;
    this.mUploadFileToS3Interactor = uploadFileToS3Interactor;
    this.mGetImageInteractor = getImageInteractor;
    this.mGetVideoInteractor = getVideoInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  public void setView(QuestionPhotoVideoViewInterface view) {
    this.mView = view;
  }

  public void initialize(Question question) {
    this.mQuestion = question;
    this.tagSelectedHashMap = new HashMap<>();
    setUpQuestion();
    setUpAnswer();
  }

  private void setUpQuestion() {
    if (mView != null) {
      mView.setUpQuestion(mQuestion);
      mView.setUpTAGs(QuestionnaireController.getTags());
    }

    PhotoVideoController.MediaData mediaData = mPhotoVideoController.getMediaData();

    if (mediaData != null) {
      if (mediaData.questionId.equalsIgnoreCase(mQuestion.id)) {
        if (mView != null) {
          mView.activityResult(mediaData.requestCode, mediaData.resultCode, mediaData.data);
          mPhotoVideoController.clearMedia();
        }
      }
    }
  }

  private void setUpAnswer() {
    this.rAnswerModel =
        answerController.getAnswer(mQuestion.id, answerTimes) == null ? new AnswerRModel()
                                                                      : answerController.getAnswer(
                                                                          mQuestion.id,
                                                                          answerTimes);
    this.rAnswerModel.setAnswersTimes(answerTimes);

    this.rAnswerModel.setQuestionId(mQuestion.id);

    Logger.d(TAG, "Attachment json data " + rAnswerModel.getValue());

    AttachmentForm form = JSONHelper.getAttachmentFormFromJson(rAnswerModel.getValue());
    if (form != null) {
      attachmentForm = new AttachmentForm();
      attachmentForm.url = form.url;
      attachmentForm.comment = form.comment;
      attachmentForm.tagForms = form.tagForms;
      attachmentForm.type = form.type;
    } else {
      attachmentForm = new AttachmentForm();
      attachmentForm.setType("");
      attachmentForm.setComment("");
      attachmentForm.setUrl("");
    }
    Logger.e(TAG, "Attachment " + attachmentForm.toString());
    if (mView != null) {
      mView.setUpAnswer(attachmentForm.getTAGsId(), attachmentForm.getTAGOtherString(),
          attachmentForm.getComment(), attachmentForm.getUrl());
    }
  }

  public void onImageChosen(Sources sources) {
    attachmentForm.setType(AttachmentForm.PHOTO);
    mGetImageInteractor.setSources(sources);
    mGetImageInteractor.execute(new GetImageSubscriber());
  }

  public void onVideoChosen(Sources sources) {
    attachmentForm.setType(AttachmentForm.VIDEO);
    mGetVideoInteractor.setSources(sources);
    mGetVideoInteractor.execute(new GetVideoSubscriber());
  }

  public void onPhotoVideoSelected() {
    mPhotoVideoController.setMedia(mQuestion.id);
  }

  public void onTAGsCheckBoxSelected(String id, boolean isChecked) {
    if (isChecked) {
      tagSelectedHashMap.put(id, id);
    } else {
      tagSelectedHashMap.remove(id);
    }

    List<String> tagId = new ArrayList<>(tagSelectedHashMap.values());
    attachmentForm.setTagForms(tagId, mTAGAdditionalRequiredID, mTAGAdditionalRequired);
    saveAnswer();
  }

  public void onTAGsAdditionalRequired(String tagId, String other) {
    this.mTAGAdditionalRequired = other;
    this.mTAGAdditionalRequiredID = tagId;
    attachmentForm.setTAGAdditionalRequired(tagId, other);
    saveAnswer();
  }

  public void onRemarkEditTextHasChanged(String comment) {
    attachmentForm.setComment(comment);
    saveAnswer();
  }

  private void saveAnswer() {
    rAnswerModel.setValue(getJSONString(attachmentForm));

    Logger.e(TAG, "Attachment json before save " + attachmentForm.toString());

    if (!TextUtils.isEmpty(attachmentForm.getUrl())) {
      answerController.addOrUpdateAnswer(mQuestion.id, rAnswerModel);
    } else {
      answerController.deleteAnswer(mQuestion.id, answerTimes);
    }

    Logger.e(TAG, "Attachment json after save " + attachmentForm.toString());
  }

  private void setURLForForm(String url) {
    attachmentForm.url = url;
    saveAnswer();
    Logger.d(TAG, "FileURL " + url);
  }

  private String getJSONString(AttachmentForm attachmentForm) {
    Gson gson = GsonHelper.newBuilder().create();
    return gson.toJson(attachmentForm);
  }

  private void uploadPhoto(String path) {
    mUploadFileToS3Interactor.setMfilePath(path);
    mUploadFileToS3Interactor.execute(new UploadPhotoSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private void uploadVideo(String path) {
    mUploadFileToS3Interactor.setMfilePath(path);
    mUploadFileToS3Interactor.execute(new UploadVideoSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private class GetImageSubscriber extends AppSubscriber<String> {

    @Override
    public void onNext(String s) {
      uploadPhoto(s);
    }
  }

  private class GetVideoSubscriber extends AppSubscriber<String> {

    @Override
    public void onNext(String videoFullPath) {
      uploadVideo(videoFullPath);
    }
  }

  private class UploadPhotoSubscriber extends AppSubscriber<String> {

    @Override
    public void onNext(String s) {
      setURLForForm(s);
      if (mView != null) {
        mView.onHideLoading();
        mView.displayFileHasBeenUploaded();
        mView.updateClickForPreviewLick(s);
      }
    }

    @Override
    public void onError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }

  private class UploadVideoSubscriber extends AppSubscriber<String> {

    @Override
    public void onNext(String s) {
      setURLForForm(s);
      if (mView != null) {
        mView.onHideLoading();
        mView.displayFileHasBeenUploaded();
        mView.updateClickForPreviewLick(s);
      }
    }

    @Override
    public void onError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }
}
