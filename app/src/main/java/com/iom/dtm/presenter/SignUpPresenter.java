package com.iom.dtm.presenter;

import android.support.annotation.IdRes;

import com.iom.dtm.R;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetUserInteractor;
import com.iom.dtm.domain.interactor.SignUpInteractor;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.domain.subscriber.AppSubscriber;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.form.SignUpDetailsForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.SignUpViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 5/25/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SignUpPresenter implements Presenter {
  public static final String TAG = "SignUpPresenter";

  private SignUpViewInterface mView;
  private SignUpDetailsForm mSignUpDetailsForm;
  private boolean mIsMale = true;
  protected SignUpInteractor mSignUpInteractor;
  protected GetUserInteractor mGetUserInteractor;
  protected SharedPreference mSharedPreference;
  private User mUser;

  @Inject
  public SignUpPresenter(SignUpInteractor signUpInteractor, GetUserInteractor getUserInteractor,
                         SharedPreference sharedPreference) {
    this.mSignUpInteractor = signUpInteractor;
    this.mGetUserInteractor = getUserInteractor;
    this.mSharedPreference = sharedPreference;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {
    mGetUserInteractor.execute(new GetUserSubscriber());
  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mSharedPreference.writeUser(null);
    mSignUpInteractor.unsubscribe();
    mGetUserInteractor.unsubscribe();
  }

  public void setView(SignUpViewInterface view) {
    this.mView = view;
  }

  public void sendFormForNormal(SignUpDetailsForm form) {
    mSignUpDetailsForm = form;
    mSignUpDetailsForm.setGender(mIsMale);
    mSignUpInteractor.setSignUpDetailsForm(mSignUpDetailsForm);
    mSignUpInteractor.execute(new SignUpSubscriberApi());
    if (mView != null) mView.onDisplayLoading();
  }

  public void sendFormForSocial(SignUpDetailsForm form) {
    mSignUpDetailsForm = form;
    mSignUpDetailsForm.setGender(mIsMale);
    if (mUser.facebookId != null) {
      mSignUpDetailsForm.setFacebookId(mUser.facebookId);
    } else {
      mSignUpDetailsForm.setGoogleId(mUser.googleId);
    }
    mSignUpInteractor.setSignUpDetailsForm(mSignUpDetailsForm);
    mSignUpInteractor.execute(new SignUpSubscriberApi());
    if (mView != null) mView.onDisplayLoading();
  }

  public void onGenderRadioButtonSelected(@IdRes int idRadioButton) {
    switch (idRadioButton) {
      case R.id.radiobutton_sign_up_male:
        mIsMale = true;
        break;
      case R.id.radiobutton_sign_up_female:
        mIsMale = false;
        break;
    }
  }

  public void onDoneClick() {
    if (mUser == null) {
      if (mView != null) {
        mView.registerNormalType();
      }
    } else {
      if (mView != null) {
        mView.registerSocialType();
      }
    }
  }

  private class SignUpSubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.onHideLoading();
        mView.onOpenHomeActivity();
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(errorApiResult.t.getErrorMessage());
      }
    }
  }

  private class GetUserSubscriber extends AppSubscriber<User> {

    @Override
    public void onNext(User user) {
      super.onNext(user);
      if (user != null) {
        mUser = user;
        if (mView != null) {
          mView.updateUserForSocialAccount(user);
        }
      }
    }
  }
}


