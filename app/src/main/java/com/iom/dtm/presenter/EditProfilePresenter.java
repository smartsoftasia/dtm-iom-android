package com.iom.dtm.presenter;

import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.interactor.CompressImageInteractor;
import com.iom.dtm.domain.interactor.DeactivateProfileInteractor;
import com.iom.dtm.domain.interactor.EditUserProfileInteractor;
import com.iom.dtm.domain.interactor.GetImageInteractor;
import com.iom.dtm.domain.interactor.GetUserFromAPIInteractor;
import com.iom.dtm.domain.interactor.GetUserInteractor;
import com.iom.dtm.domain.interactor.ReplaceRoleSendToNewOneInteractor;
import com.iom.dtm.domain.interactor.UploadFileToS3Interactor;
import com.iom.dtm.domain.interactor.UserVerificationInteractor;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.domain.subscriber.AppSubscriber;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.domain.subscriber.GetImageSubscriberTest;
import com.iom.dtm.domain.subscriber.listener.GetImageListener;
import com.iom.dtm.rest.form.EditProfileForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.HomeProfileViewInterface;
import com.smartsoftasia.rxcamerapicker.Sources;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 6/13/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EditProfilePresenter implements Presenter, GetImageListener {
  public static final String TAG = "EditProfilePresenter";

  protected GetUserFromAPIInteractor mGetUserFromAPIInteractor;
  protected GetUserInteractor mGetUserInteractor;
  protected EditUserProfileInteractor mEditUserProfileInteractor;
  protected DeactivateProfileInteractor mDeactivateProfileInteractor;
  protected UserVerificationInteractor mUserVerificationInteractor;
  protected ReplaceRoleSendToNewOneInteractor mReplaceRoleSendToNewOneInteractor;
  protected CompressImageInteractor mCompressImageInteractor;
  protected UploadFileToS3Interactor mUploadFileToS3Interactor;
  protected GetImageInteractor mGetImageInteractor;
  private HomeProfileViewInterface mView;
  private User mUser;

  @Inject
  public EditProfilePresenter(GetUserFromAPIInteractor getUserFromAPIInteractor,
                              GetUserInteractor getUserInteractor,
                              EditUserProfileInteractor editUserProfileInteractor,
                              DeactivateProfileInteractor deactivateProfileInteractor,
                              UserVerificationInteractor userVerificationInteractor,
                              ReplaceRoleSendToNewOneInteractor replaceRoleSendToNewOneInteractor,
                              CompressImageInteractor compressImageInteractor,
                              UploadFileToS3Interactor uploadFileToS3Interactor,
                              GetImageInteractor getImageInteractor) {
    this.mGetUserFromAPIInteractor = getUserFromAPIInteractor;
    this.mGetUserInteractor = getUserInteractor;
    this.mEditUserProfileInteractor = editUserProfileInteractor;
    this.mDeactivateProfileInteractor = deactivateProfileInteractor;
    this.mUserVerificationInteractor = userVerificationInteractor;
    this.mReplaceRoleSendToNewOneInteractor = replaceRoleSendToNewOneInteractor;
    this.mCompressImageInteractor = compressImageInteractor;
    this.mUploadFileToS3Interactor = uploadFileToS3Interactor;
    this.mGetImageInteractor = getImageInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {
    mGetUserInteractor.execute(new GetUserSubscriber());
  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mGetUserFromAPIInteractor.unsubscribe();
    mGetUserInteractor.unsubscribe();
    mEditUserProfileInteractor.unsubscribe();
    mDeactivateProfileInteractor.unsubscribe();
    mUserVerificationInteractor.unsubscribe();
    mReplaceRoleSendToNewOneInteractor.unsubscribe();
    mCompressImageInteractor.unsubscribe();
    mUploadFileToS3Interactor.unsubscribe();
  }

  @Override
  public void getImageOnError(Throwable e) {
    if (mView != null) {
      mView.onError(e);
    }
  }

  @Override
  public void getImageOnNext(String absolutePath) {
    uploadFile(absolutePath);
  }

  public void setView(HomeProfileViewInterface view) {
    this.mView = view;
  }

  public void onProfileImageClick() {
    if (mView != null) mView.onDisplayImageChooser();
  }

  public void onDeactivateProfileClick(String email, String password) {
    mUserVerificationInteractor.setEmail(email);
    mUserVerificationInteractor.setPassword(password);
    mUserVerificationInteractor.execute(new DeactivateVerifySubscriberApi());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  public void onDeactivateDialogClick() {
    if (mUser == null) {
      return;
    }

    switch (mUser.userType) {
      case User.ENUMERATORS_ADMIN: {
        if (mView != null) {
          mView.onDisplayDeactivateDialog(true);
        }
        break;
      }
      default: {
        if (mView != null) {
          mView.onDisplayDeactivateDialog(false);
        }
      }
    }
  }

  public void onReplaceRoleVerifyClick(String email, String password) {
    mUserVerificationInteractor.setEmail(email);
    mUserVerificationInteractor.setPassword(password);
    mUserVerificationInteractor.execute(new ReplaceRoleVerifySubscriberApi());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  public void onReplaceRoleSendToNewOneClick(String email, String comment) {
    mReplaceRoleSendToNewOneInteractor.setEmail(email);
    mReplaceRoleSendToNewOneInteractor.setComment(comment);
    mReplaceRoleSendToNewOneInteractor.execute(new ReplaceRoleSendToNewOneSubscriberApi());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  public void onImageChoose(Sources sources) {
    mGetImageInteractor.setSources(sources);
    mGetImageInteractor.execute(GetImageSubscriberTest.create(this));
  }

  public void onSaveClick(EditProfileForm form) {
    mEditUserProfileInteractor.setChangeImage(false);
    mEditUserProfileInteractor.setEditProfileForm(form);
    mEditUserProfileInteractor.execute(new EditUserSubscriberApi());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private void uploadFile(String path) {
    mUploadFileToS3Interactor.setMfilePath(path);
    mUploadFileToS3Interactor.execute(new UploadFileSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private void updateUser(String imagePath) {
    mEditUserProfileInteractor.setImageURL(imagePath);
    mEditUserProfileInteractor.setChangeImage(true);
    mEditUserProfileInteractor.execute(new EditUserSubscriberApi());
  }

  private class ReplaceRoleSendToNewOneSubscriberApi extends DTMApiSubscriber<Object> {

    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.onHideLoading();
        mView.logout();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == 401) {
          mView.onReplaceRoleSendToNewOneFailed();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }

  private class ReplaceRoleVerifySubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.onHideLoading();
        mView.onReplaceRoleSendToNewOneDialog();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == 401) {
          mView.onReplaceRoleUserVerifyFailed();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }

  private class DeactivateVerifySubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onCompleted() {
      if (mView != null) {
        mDeactivateProfileInteractor.execute(new DeactivateProfileSubscriberApi());
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == 401) {
          mView.onDeactivatedUserVerifyFailed();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }

  private class UploadFileSubscriber extends AppSubscriber<String> {

    @Override
    public void onNext(String s) {
      if (mView != null) {
        mView.updateImageProfile(s);
      }

      updateUser(s);
    }

    @Override
    public void onError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }

  private class DeactivateProfileSubscriberApi extends DTMApiSubscriber<Object> {

    @Override
    public void onCompleted() {
      if (mView != null) {
        mView.onHideLoading();
        mView.logout();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }
  }

  private class GetUserFormAPISubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onNext(User user) {
      mUser = user;
      if (mView != null) {
        mView.onUpdateProfile(mUser);
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
        mView.onUnauthorized();
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
      }
    }
  }

  private class GetUserSubscriber extends AppSubscriber<User> {

    @Override
    public void onNext(User user) {
      mUser = user;
      if (mView != null) {
        mView.onUpdateProfile(mUser);
      }
      mGetUserFromAPIInteractor.execute(new GetUserFormAPISubscriberApi());
    }

    @Override
    public void onError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
      }
      mGetUserFromAPIInteractor.execute(new GetUserFormAPISubscriberApi());
    }
  }

  private class EditUserSubscriberApi extends DTMApiSubscriber<User> {

    @Override
    public void onNext(User user) {
      mUser = user;
      if (mView != null) {
        mView.onUpdateProfile(mUser);
        mView.onHideLoading();
        mView.displayProfileHasBeenUpdatedToast();
      }
    }

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
        mView.onHideLoading();
      }
    }
  }
}
