package com.iom.dtm.presenter;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.AnswerInteractor;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.RelativeQuestion;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.form.AnswerForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.SummaryDTMFormViewInterface;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SummaryDTMFormPresenter extends BaseValidateQuestionPresenter implements Presenter {
  public static final String TAG = "SummaryDTMFormPresenter";

  protected QuestionnaireController mQuestionnaireController;
  protected AnswerController mAnswerController;
  protected AnswerInteractor mAnswerInteractor;
  protected SummaryDTMFormViewInterface mView;
  protected List<String> categoryTitles = new ArrayList<>();

  @Inject
  public SummaryDTMFormPresenter(QuestionnaireController questionnaireController,
                                 AnswerController answerController,
                                 AnswerInteractor answerInteractor,
                                 SharedPreference sharedPreference) {
    super(sharedPreference, true);
    this.mQuestionnaireController = questionnaireController;
    this.mAnswerController = answerController;
    this.mAnswerInteractor = answerInteractor;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mAnswerInteractor.unsubscribe();
  }

  @Override
  protected void formHasBeenValidated() {
    addHyphenOnRelativeQuestion();
    sendAnswer();
  }

  @Override
  protected void displayFieldRequired(String category, String question) {
    if (mView != null) {
      mView.displayAnswerRequired(
          TranslationHelper.get(R.string.summary_dtm_form_fragment_requires_answer,
              getMessageForToastFormatDisplay(category, question)));
    }
  }

  @Override
  protected void displayInvalidEmail(String category, String question) {
    if (mView != null) {
      mView.displayAnswerRequired(
          TranslationHelper.get(R.string.summary_dtm_form_fragment_invalid_email,
              getMessageForToastFormatDisplay(category, question)));
    }
  }

  @Override
  protected void questionCategoryPositionForAnswerInvalidated(int position) {
    if (mView != null) {
      mView.openQuestionCategory(position);
    }
  }

  public void setView(SummaryDTMFormViewInterface view) {
    this.mView = view;
  }

  public void initialize() {
    for (int i = 0;
         i < mQuestionnaireController.getQuestionnaireFromDatabase().questionCategories.size();
         i++) {
      categoryTitles.add(
          mQuestionnaireController.getQuestionnaireFromDatabase().questionCategories.get(i).title);
    }
    if (mView != null) {
      mView.appendListQuestionCategory(categoryTitles);
    }
  }

  public void onSubmitClick() {
    mAnswerController.saveAnswerToRealm();
/*    if (AppConfig.DEBUG) {
      onFormValidationForDebugMode();
    } else {
      onFormValidation();
    }*/

    onFormValidationForDebugMode();
  }

  private void addHyphenOnRelativeQuestion() {
    for (int i = 0; i < questionList.size(); i++) {
      Question question = questionList.get(i);
      int answerTimes = calculateAnswerTimes(question.id);
      for (int m = 1; m <= answerTimes; m++) {
        AnswerRModel answerRModel;
        if (answerHashMap.containsKey(question.id + AppConstant.UNDERSCORE + m)) {
          answerRModel = answerHashMap.get(question.id + AppConstant.UNDERSCORE + m);
        } else {
          answerRModel = new AnswerRModel();
          answerRModel.setQuestionId(question.id);
        }
        if (relativeQuestionHashMap.containsKey(question.id)) {
          RelativeQuestion relativeQuestion = relativeQuestionHashMap.get(question.id);
          AnswerRModel relativeQuestionAnswer = answerHashMap.get(
              relativeQuestion.questionId + AppConstant.UNDERSCORE + m);

          if (relativeQuestionAnswer == null) {
            return;
          }

          if (!relativeQuestionAnswer.getChoiceId()
              .equalsIgnoreCase(relativeQuestion.getChoiceId())) {
            answerRModel.setValue(AppConstant.HYPHEN);
            mAnswerController.addOrUpdateAnswer(question.id, answerRModel);
          }
        }
      }
    }
  }

  private void sendAnswer() {
    AnswerForm form = new AnswerForm(mAnswerController.getAnswerForm());
    form.setEventId(sharedPreference.readEventId());
    mAnswerInteractor.setAnswerForm(form);
    mAnswerInteractor.execute(new AnswerSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private class AnswerSubscriber extends DTMApiSubscriber<Object> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        if (responseCode == AppConstant.UNAUTHORIZED_RESPONSE) {
          mView.onUnauthorized();
        } else {
          mView.onError(errorApiResult.t.getErrorMessage());
        }
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }

    @Override
    public void onCompleted() {
      sharedPreference.writeEventId(null);
      mAnswerController.clearAnswerSingleton();
      mAnswerController.clearAnswerOnDatabase();

      if (mView != null) {
        mView.onHideLoading();
        mView.displayDTMSubmitSuccessfulDialog();
      }
    }
  }
}
