package com.iom.dtm.presenter.questiongroup;

import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.view.viewinterface.QuestionGroupViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupPresenter implements Presenter {
  public static final String TAG = "QuestionGroupPresenter";

  protected QuestionGroupViewInterface mView;
  private QuestionGroup mQuestionGroup;
  private List<Question> mQuestionList;
  private List<Question> mQuestionAdmin;

  @Inject
  public QuestionGroupPresenter() {
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    this.mView = null;
  }

  @Override
  public void restart() {

  }

  public void setView(QuestionGroupViewInterface view) {
    this.mView = view;
  }

  public void initialize(QuestionGroup questionGroup) {
    this.mQuestionGroup = questionGroup;
    this.mQuestionList = mQuestionGroup.questions;

    setUpQuestionGroup();
    handleQuestion();
  }

  public void handleQuestion() {

    if (mQuestionList == null || mQuestionList.size() == 0) {
      if (mView != null) {
        //mView.displayNoQuestion();
      }
    } else {
      for (int i = 0; i < mQuestionList.size(); i++) {
        Question item = mQuestionList.get(i);
        switch (item.getType()) {
          case Question.STRING:
            if (mView != null) {
              mView.appendQuestionString(item, i);
            }
            break;
          case Question.DATE_PICKER:
            if (mView != null) {
              mView.appendQuestionDatePicker(item, i);
            }
            break;
          case Question.NUMBER_PICKER:
            if (mView != null) {
              //mView.appendQuestionNumberPicker(item, i);
              mView.appendQuestionNumber(item, i);
            }
            break;
          case Question.LOCATION:
            if (mView != null) {
              mView.appendQuestionLocation(item, i);
            }
            break;
          case Question.SINGLE_CHOICE:
            if (mView != null) {
              mView.appendQuestionSingleChoice(item, i);
            }
            break;
          case Question.NUMBER:
            if (mView != null) {
              mView.appendQuestionNumber(item, i);
            }
            break;
          case Question.EMAIL:
            if (mView != null) {
              mView.appendQuestionEmail(item, i);
            }
            break;
          case Question.ATTACHMENT:
            if (mView != null) {
              mView.appendQuestionAttachment(item, i);
            }
            break;
          case Question.TEXT:
            if (mView != null) {
              mView.appendQuestionText(item, i);
            }
            break;
          case Question.DROP_DOWN:
            if (mView != null) {
              mView.appendQuestionDropDown(item, i);
            }
            break;
          case Question.ADMIN_1:
            mQuestionAdmin = new ArrayList<>();
            mQuestionAdmin.add(0, item);
            break;
          case Question.ADMIN_1_PRIORITY:
            mQuestionAdmin = new ArrayList<>();
            mQuestionAdmin.add(0, item);
            break;
          case Question.ADMIN_2:
            mQuestionAdmin.add(1, item);
            break;
          case Question.ADMIN_3:
            mQuestionAdmin.add(2, item);
            appendQuestionAdmin();
            break;
          default:
            if (mView != null) {
              mView.appendQuestionString(item, i);
            }
            break;
        }
      }
    }
  }

  private void appendQuestionAdmin() {
    QuestionGroup questionGroup = new QuestionGroup();
    questionGroup.questions = mQuestionAdmin;
    if (mView != null) {
      mView.appendQuestionAdmin(questionGroup, 1);
    }
  }

  private void setUpQuestionGroup() {
    if (mView != null) {
      mView.setUpQuestionGroup(mQuestionGroup);
    }
  }
}