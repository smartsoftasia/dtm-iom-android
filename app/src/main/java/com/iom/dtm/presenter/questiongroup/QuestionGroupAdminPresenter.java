package com.iom.dtm.presenter.questiongroup;

import android.text.TextUtils;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetAddressListInteractor;
import com.iom.dtm.domain.model.Admin0;
import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Admin2;
import com.iom.dtm.domain.model.Admin3;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.presenter.question.BaseQuestionPresenter;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.QuestionGroupAdminViewInterface;
import com.smartsoftasia.ssalibrary.presenter.Presenter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupAdminPresenter extends BaseQuestionPresenter implements Presenter {
  public static final String TAG = "QuestionGroupPresenter";

  private GetAddressListInteractor mGetAddressListInteractor;
  private QuestionGroupAdminViewInterface mView;
  private QuestionGroup mQuestionGroup;
  private Admin0 mAdmin0;
  private HashMap<String, Admin1> mAdmin1Hash = new HashMap<>();
  private HashMap<String, Admin2> mAdmin2Hash = new HashMap<>();
  private HashMap<String, Admin3> mAdmin3Hash = new HashMap<>();
  private String referenceAdmin1, referenceAdmin2, referenceAdmin3;
  private AnswerRModel admin1AnswerRModel, admin2AnswerRModel, admin3AnswerRModel;
    private SharedPreference mSharedPreference;

  @Inject
  public QuestionGroupAdminPresenter(GetAddressListInteractor getAddressListInteractor,
                                     AnswerController answerController, SharedPreference sharedPreference) {
    super(answerController);
    this.mGetAddressListInteractor = getAddressListInteractor;
      this.mSharedPreference = sharedPreference;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void restart() {

  }

  @Override
  public void destroy() {
    this.mView = null;
    mGetAddressListInteractor.unsubscribe();
  }

  public void setView(QuestionGroupAdminViewInterface view) {
    this.mView = view;
  }

  public void initialize(QuestionGroup questionGroup, int answerTime) {
      mGetAddressListInteractor.setEventId(mSharedPreference.readEventId());
    mGetAddressListInteractor.execute(new AddressSubscriber());
    this.mQuestionGroup = questionGroup;
    this.answerTimes = answerTime;
  }

  public void onAdmin1Click() {
    if (mAdmin1Hash == null) {
      return;
    }
    if (mView != null) {
      mView.displayAdmin1List(new ArrayList<Admin1>(mAdmin1Hash.values()));
    }
  }

  public void onAdmin2Click() {
    if (mAdmin2Hash == null) {
      return;
    }
    mAdmin2Hash.clear();
    addHashMapAdmin2();
    if (mView != null) {
      mView.displayAdmin2List(new ArrayList<Admin2>(mAdmin2Hash.values()));
    }
  }

  public void onAdmin3Click() {
    if (mAdmin3Hash == null) {
      return;
    }
    mAdmin3Hash.clear();
    addHashMapAdmin3();
    if (mView != null) {
      mView.displayAdmin3List(new ArrayList<Admin3>(mAdmin3Hash.values()));
    }
  }

  public void saveAdmin1(String admin1) {
    if (admin1.length() != 0) {
      referenceAdmin1 = getReferenceFromFullText(admin1);
      admin1AnswerRModel.setValue(referenceAdmin1);
      answerController.addOrUpdateAnswer(mQuestionGroup.questions.get(0).id, admin1AnswerRModel);
    } else {
      answerController.deleteAnswer(mQuestionGroup.questions.get(0).id, answerTimes);
    }
  }

  public void saveAdmin2(String admin2) {
    if (admin2.length() != 0) {
      referenceAdmin2 = getReferenceFromFullText(admin2);
      admin2AnswerRModel.setValue(referenceAdmin2);
      answerController.addOrUpdateAnswer(mQuestionGroup.questions.get(1).id, admin2AnswerRModel);
    } else {
      answerController.deleteAnswer(mQuestionGroup.questions.get(1).id, answerTimes);
    }
  }

  public void saveAdmin3(String admin3) {
    if (admin3.length() != 0) {
      referenceAdmin3 = getReferenceFromFullText(admin3);
      admin3AnswerRModel.setValue(referenceAdmin3);
      answerController.addOrUpdateAnswer(mQuestionGroup.questions.get(2).id, admin3AnswerRModel);
    } else {
      answerController.deleteAnswer(mQuestionGroup.questions.get(2).id, answerTimes);
    }
  }

  private void setUpQuestion() {
    for (int i = 0; i < mQuestionGroup.questions.size(); i++) {
      Question question = mQuestionGroup.questions.get(i);

      switch (question.getType()) {
        case Question.ADMIN_1:
          if (mView != null) {
            mView.setUpQuestionAdmin1(question.title);
          }
          break;
        case Question.ADMIN_2:
          if (mView != null) {
            mView.setUpQuestionAdmin2(question.title);
          }
          break;
        case Question.ADMIN_3:
          if (mView != null) {
            mView.setUpQuestionAdmin3(question.title);
          }
          break;
      }
    }
  }

  private void setUpAnswer() {
    this.admin1AnswerRModel =
        answerController.getAnswer(mQuestionGroup.questions.get(0).id, answerTimes) == null
        ? new AnswerRModel()
        : answerController.getAnswer(mQuestionGroup.questions.get(0).id, answerTimes);
    this.admin2AnswerRModel =
        answerController.getAnswer(mQuestionGroup.questions.get(1).id, answerTimes) == null
        ? new AnswerRModel()
        : answerController.getAnswer(mQuestionGroup.questions.get(1).id, answerTimes);
    this.admin3AnswerRModel =
        answerController.getAnswer(mQuestionGroup.questions.get(2).id, answerTimes) == null
        ? new AnswerRModel()
        : answerController.getAnswer(mQuestionGroup.questions.get(2).id, answerTimes);

    this.admin1AnswerRModel.setAnswersTimes(answerTimes);
    this.admin2AnswerRModel.setAnswersTimes(answerTimes);
    this.admin3AnswerRModel.setAnswersTimes(answerTimes);

    this.admin1AnswerRModel.setQuestionId(mQuestionGroup.questions.get(0).id);
    this.admin2AnswerRModel.setQuestionId(mQuestionGroup.questions.get(1).id);
    this.admin3AnswerRModel.setQuestionId(mQuestionGroup.questions.get(2).id);

    if (!TextUtils.isEmpty(admin1AnswerRModel.getValue())) {
      this.referenceAdmin1 = admin1AnswerRModel.getValue();
      if (mAdmin1Hash.containsKey(referenceAdmin1)) {
        if (mView != null) {
          mView.setUpAnswerAdmin1(mAdmin1Hash.get(referenceAdmin1).displayAdmin());
        }
      }
      addHashMapAdmin2();
    }
    if (!TextUtils.isEmpty(admin2AnswerRModel.getValue())) {
      this.referenceAdmin2 = admin2AnswerRModel.getValue();
      if (mAdmin2Hash.containsKey(referenceAdmin2)) {
        if (mView != null) {
          mView.setUpAnswerAdmin2(mAdmin2Hash.get(referenceAdmin2).displayAdmin());
        }
      }
      addHashMapAdmin3();
    }

    if (!TextUtils.isEmpty(admin3AnswerRModel.getValue())) {
      if (mAdmin3Hash.containsKey(admin3AnswerRModel.getValue())) {
        if (mView != null) {
          mView.setUpAnswerAdmin3(mAdmin3Hash.get(admin3AnswerRModel.getValue()).displayAdmin());
        }
      }
    }
  }

  private void handleQuestionAndAnswer() {
    addHashMapAdmin1();
    setUpQuestion();
    setUpAnswer();
  }

  private void addHashMapAdmin1() {
    for (int i = 0; i < mAdmin0.admin1s.size(); i++) {
      Admin1 admin1 = mAdmin0.admin1s.get(i);
      mAdmin1Hash.put(admin1.reference, admin1);
    }
  }

  private void addHashMapAdmin2() {
    Admin1 admin1 = mAdmin1Hash.get(referenceAdmin1);
    for (int i = 0; i < admin1.admin2s.size(); i++) {
      Admin2 admin2 = admin1.admin2s.get(i);
      mAdmin2Hash.put(admin2.reference, admin2);
    }
  }

  private void addHashMapAdmin3() {
    Admin2 admin2 = mAdmin2Hash.get(referenceAdmin2);
    for (int i = 0; i < admin2.admin3s.size(); i++) {
      Admin3 admin3 = admin2.admin3s.get(i);
      mAdmin3Hash.put(admin3.reference, admin3);
    }
  }

  private String getReferenceFromFullText(String temp) {
    return temp.substring(temp.lastIndexOf("(") + 1, temp.lastIndexOf(")"));
  }

  private class AddressSubscriber extends DTMApiSubscriber<Admin0> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onError(errorApiResult.t.getErrorMessage());
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
      }
    }

    @Override
    public void onNext(Admin0 admin0) {
      mAdmin0 = admin0;
      handleQuestionAndAnswer();
    }
  }
}