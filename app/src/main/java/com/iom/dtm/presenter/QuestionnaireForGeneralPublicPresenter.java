package com.iom.dtm.presenter;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.AnswerLiteInteractor;
import com.iom.dtm.domain.interactor.GetQuestionnaireFromApiInteractor;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.domain.subscriber.DTMApiSubscriber;
import com.iom.dtm.rest.form.AnswerForm;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.iom.dtm.view.viewinterface.QuestionnaireForGeneralPublicViewInterface;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.presenter.Presenter;

import javax.inject.Inject;

/**
 * Created by Nott on 9/30/2016 AD.
 * DTM
 */

public class QuestionnaireForGeneralPublicPresenter extends BaseValidateQuestionPresenter
    implements Presenter {
  public static final String TAG = "QuestionnaireForGeneral";

  private QuestionnaireForGeneralPublicViewInterface mView;
  private GetQuestionnaireFromApiInteractor mGetQuestionnaireFromApiInteractor;
  private AnswerLiteInteractor mAnswerLiteInteractor;
  private AnswerController mAnswerController;
  private Questionnaire mQuestionnaire;
  private boolean mIsSelectedLocationOrAttachment = false;

  @Inject
  public QuestionnaireForGeneralPublicPresenter(SharedPreference sharedPreference,
                                                GetQuestionnaireFromApiInteractor getQuestionnaireFromApiInteractor,
                                                AnswerController answerController, AnswerLiteInteractor answerLiteInteractor) {
    super(sharedPreference, false);
    this.mGetQuestionnaireFromApiInteractor = getQuestionnaireFromApiInteractor;
    this.mAnswerLiteInteractor = answerLiteInteractor;
    this.mAnswerController = answerController;
  }

  @Override
  public void create() {

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {
    if (mIsSelectedLocationOrAttachment) {
      mIsSelectedLocationOrAttachment = false;
      if (mView != null) {
        mView.recreateFragment();
      }
    }
  }

  @Override
  public void pause() {
    mIsSelectedLocationOrAttachment = true;
  }

  @Override
  public void destroy() {
    this.mView = null;
    mIsSelectedLocationOrAttachment = false;
    mGetQuestionnaireFromApiInteractor.unsubscribe();
    mAnswerLiteInteractor.unsubscribe();
  }

  @Override
  public void restart() {

  }

  public void initialize() {
    mGetQuestionnaireFromApiInteractor.setFullQuestionnaire(false);
    mGetQuestionnaireFromApiInteractor.setEvent(new Event());
    mGetQuestionnaireFromApiInteractor.execute(new GetQuestionnaireFromApiSubscriber());
    if (mView != null) {
      mView.displayGeneratingForm();
    }
  }

  public void setView(QuestionnaireForGeneralPublicViewInterface view) {
    this.mView = view;
  }

  public void onSubmitClick() {
    onFormValidation();
  }

  private void setUpQuestionnaireForGP(Questionnaire questionnaireForGP) {
    if (questionnaireForGP == null
        || questionnaireForGP.questionCategories == null
        || questionnaireForGP.questionCategories.size() == 0) {
      if (mView != null) {
        mView.displayNoQuestionnaire();
      }
      return;
    }

    this.mQuestionnaire = questionnaireForGP;
    if (mView != null) {
      mView.setUpQuestionnaire(questionnaireForGP.questionCategories);
    }
  }

  @Override
  protected void formHasBeenValidated() {
    sendAnswer();
  }

  @Override
  protected void displayFieldRequired(String category, String question) {
    if (mView != null) {
      mView.displayAnswerRequired(
          TranslationHelper.get(R.string.summary_dtm_form_fragment_requires_answer,
              toastFormat(question)));
    }
  }

  @Override
  protected void displayInvalidEmail(String category, String question) {
    if (mView != null) {
      mView.displayEmailInvalid(
          TranslationHelper.get(R.string.summary_dtm_form_fragment_invalid_email,
              toastFormat(question)));
    }
  }

  @Override
  protected void questionCategoryPositionForAnswerInvalidated(int position) {
    // Do nothing
  }

  public void sendAnswer() {
    AnswerForm form = new AnswerForm(mAnswerController.getAnswerForm());
    mAnswerLiteInteractor.setAnswerForm(form);
    mAnswerLiteInteractor.execute(new AnswerSubscriber());
    if (mView != null) {
      mView.onDisplayLoading();
    }
  }

  private String toastFormat(String question) {
    return question + AppConstant.HYPHEN_WITH_SPACE;
  }

  private void getQuestionList() {
    questionList.clear();
    categoryTitlesForValidate.clear();
    Questionnaire questionnaire = mQuestionnaire;
    for (int i = 0; i < questionnaire.questionCategories.size(); i++) {
      QuestionCategory category = questionnaire.questionCategories.get(i);
      for (int j = 0; j < category.questionGroupList.size(); j++) {
        QuestionGroup group = category.questionGroupList.get(j);
        for (int k = 0; k < group.questions.size(); k++) {
          CategoryValidate validate = new CategoryValidate(i, category.title);
          categoryTitlesForValidate.add(validate);
          questionList.add(group.questions.get(k));
        }
      }
    }
  }

  private void clearAnswer() {
    if (!AppConfig.DEBUG) {
      mAnswerController.clearAnswerSingleton();
      mAnswerController.clearAnswerOnDatabase();
    }
  }

  private class GetQuestionnaireFromApiSubscriber extends DTMApiSubscriber<Questionnaire> {
    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onError(errorApiResult.t.getErrorMessage());
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onError(e);
      }
    }

    @Override
    public void onNext(Questionnaire questionnaire) {
      setUpQuestionnaireForGP(questionnaire);
      getQuestionList();
    }
  }

  private class AnswerSubscriber extends DTMApiSubscriber<Object> {

    @Override
    public void onApiError(int responseCode, ErrorApiResult errorApiResult) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(errorApiResult.t.getErrorMessage());
      }
    }

    @Override
    public void onAppError(Throwable e) {
      if (mView != null) {
        mView.onHideLoading();
        mView.onError(e);
      }
    }

    @Override
    public void onCompleted() {
      clearAnswer();
      if (mView != null) {
        mView.onHideLoading();
        mView.displayDTMSubmitSuccessfulDialog();
      }
    }
  }
}
