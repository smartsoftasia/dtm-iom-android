package com.iom.dtm.services.amazon.s3;

import android.support.annotation.NonNull;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.iom.dtm.core.AppConfig;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.helper.MD5;
import java.io.File;
import java.io.FileNotFoundException;
import javax.inject.Inject;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Nott on 8/31/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class S3Uploader {
  public static final String TAG = "s3Uploader";

  private TransferUtility mTransferUtility;

  @Inject
  public S3Uploader(TransferUtility transferUtility) {
    this.mTransferUtility = transferUtility;
  }

  public Observable<String> rxUploadFile(@NonNull final String filePath) {
    return Observable.create(new Observable.OnSubscribe<String>() {
      @Override
      public void call(Subscriber<? super String> subscriber) {
        uploadFile(subscriber, filePath);
      }
    });
  }

  private void uploadFile(@NonNull Subscriber<? super String> subscriber,
                          @NonNull String filePath) {
    String fileName = String.format(AppConfig.S3_FOLDER_NAME + "/%s_%s", System.currentTimeMillis(),
        MD5.encrypt(filePath));

    File file = new File(filePath);

    if (!file.exists()) {
      subscriber.onError(new FileNotFoundException());
    }

    TransferObserver observer = mTransferUtility.upload(AppConfig.S3_BUCKET_NAME, fileName, file,
        CannedAccessControlList.PublicRead);
    observer.setTransferListener(new RxTransferListener(subscriber, fileName));
  }

  private class RxTransferListener implements TransferListener {
    private Subscriber<? super String> mSubscriber;
    private String mFileName;

    public RxTransferListener(Subscriber<? super String> subscriber, String fileName) {
      mSubscriber = subscriber;
      mFileName = fileName;
    }

    @Override
    public void onStateChanged(int id, TransferState state) {
      switch (state) {
        case WAITING:
          break;
        case IN_PROGRESS:
          break;
        case PAUSED:
          break;
        case RESUMED_WAITING:
          break;
        case COMPLETED:
          mSubscriber.onNext(AppConfig.S3_BUCKET_PATH + mFileName);
          mSubscriber.onCompleted();
          break;
        case CANCELED:
          mSubscriber.onError(new S3CanceledException());
          break;
        case FAILED:
          mSubscriber.onError(new S3FailedException());
          break;
        case WAITING_FOR_NETWORK:
          break;
        case PART_COMPLETED:
          break;
        case PENDING_CANCEL:
          break;
        case PENDING_PAUSE:
          break;
        case PENDING_NETWORK_DISCONNECT:
          break;
        case UNKNOWN:
          mSubscriber.onCompleted();
          break;
      }
    }

    @Override
    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
    }

    @Override
    public void onError(int id, Exception ex) {
      mSubscriber.onError(ex);
    }
  }
}
