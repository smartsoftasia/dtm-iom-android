package com.iom.dtm.services.amazon.s3;

/**
 * Created by Nott on 8/31/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class S3CanceledException extends Throwable {
  public static final String TAG = "S3CanceledException";

  public S3CanceledException() {
  }
}