package com.iom.dtm.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.HomeComponent;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.presenter.DTMFormPresenter;
import com.iom.dtm.view.adapter.DTMFormMenuAdapter;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.viewinterface.DTMFormViewInterface;
import com.smartsoftasia.ssalibrary.view.adapter.BaseLinearAdapter;
import com.smartsoftasia.ssalibrary.view.widget.BaseSpinner;
import com.smartsoftasia.ssalibrary.view.widget.LinearListView;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by androiddev03 on 7/6/2559.
 * DTMFormFragment
 */
public class DTMFormFragment extends BaseAppFragment
    implements DTMFormViewInterface, BaseLinearAdapter.OnItemClickListener {
  public static final String TAG = "DTMFormFragment";

  public interface OnFragmentInteractionListener {
    void onOpenDTMFormDetailsActivity(int position);
  }

  private OnFragmentInteractionListener mListener;

  @Inject
  DTMFormPresenter presenter;
  @BindView(R.id.linearListView_dtm_form)
  LinearListView linearListView;
  @BindView(R.id.linearlayout_dtm_form_fragment)
  LinearLayout linearLayout;
  @BindView(R.id.spinner_dtm_form_fragment)
  BaseSpinner spinner;

  DTMFormMenuAdapter menuAdapter;

  public static DTMFormFragment newInstance() {
    DTMFormFragment fragment = new DTMFormFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  public int getViewResourceId() {
    return R.layout.fragment_dtm_form;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    initView();
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(R.string.action_bar_dtm_form_fragment);
    if (menuAdapter == null) {
      menuAdapter = new DTMFormMenuAdapter(new ArrayList<String>(), getContext());
      menuAdapter.setItemClickListener(this);
    }
    linearListView.setAdapter(menuAdapter);
  }

  protected void initView() {
    menuAdapter.setItemClickListener(this);
    linearListView.setAdapter(menuAdapter);
  }

  @Override
  protected void initialize() {
    this.getComponent(HomeComponent.class).inject(this);
    this.presenter.setView(this);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void displayLoading() {
    displayProgressDialogLoading(R.string.dtm_from_fragment_a_downloading_message);
  }

  @Override
  public void hideLoading() {
    hideProgressDialog();
  }

  @Override
  public void appendListOfQuestionCategory(List<String> questionCategoryList) {
    if (menuAdapter != null) {
      linearLayout.setVisibility(View.VISIBLE);
      menuAdapter.clear();
      menuAdapter.setItems(questionCategoryList);
    }
  }

  @Override
  public void onItemClick(int position) {
    presenter.openDTMForm(position);
  }

  @Override
  public void openDTMFormDetailsActivity(int position) {
    if (mListener != null) {
      mListener.onOpenDTMFormDetailsActivity(position);
    }
  }

  @Override
  public void displayGeneratingFormMessage() {
    displayToastMessage(R.string.dtm_from_fragment_a_waiting_for_creating_form);
  }

  @Override
  public void displayPleaseSelectEventBeforeStartForm() {
    displayToastMessage(R.string.dtm_from_fragment_select_display_please_select_event);
  }

  @Override
  public void displaySelectNewEventConfirmation() {
    final ConfirmationDialog dialog = new ConfirmationDialog.Builder().setTitleDialog(
        getString(R.string.dtm_from_fragment_select_new_event))
        .setOkTextButton(getString(R.string.global_ok))
        .setCancelTextButton(getString(R.string.global_cancel))
        .createDialog();
    dialog.setConfirmationDialogListener(new ConfirmationDialog.ConfirmationDialogListener() {
      @Override
      public void onOkClick() {
        presenter.checkDTMFormVersion();
        dialog.dismiss();
      }

      @Override
      public void onCancelClick() {
        dialog.dismiss();
      }
    });
    dialog.show(getFragmentManager(), ConfirmationDialog.TAG);
  }

  @Override
  public void updateEventListSpinner(List<Event> eventList, int positionSelected) {
    ArrayAdapter<Event> dataAdapter = new ArrayAdapter<Event>(getContext(),
        R.layout.item_event_spinner_view, eventList);

    dataAdapter.setDropDownViewResource(R.layout.item_event_spinner);

    spinner.setAdapter(dataAdapter);
    spinner.setSelection(positionSelected);
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        presenter.onSpinnerItemSelected(i);
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });
  }

  @OnClick(R.id.button_dtm_form_fragment_start)
  public void onStartClick(View v) {
    presenter.openDTMForm(0);
  }
}
