package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.QuestionGroup;
import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;
import java.util.List;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionCategoryViewInterface extends BaseViewInterface {
  void appendQuestionGroupNormal(QuestionGroup questionGroup, int position);

  void appendQuestionGroupRepeat(QuestionGroup questionGroup, int position);

  void appendQuestionGroupMaleFemale(QuestionGroup questionGroup, int position);

  void appendQuestionGroupNoQuestion(QuestionGroup questionGroup, int position);

  void appendQuestionGroupOrderingNumber(QuestionGroup questionGroup, int position);

  void displayNoQuestion();
}
