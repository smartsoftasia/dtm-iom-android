package com.iom.dtm.view.viewinterface;

import com.iom.dtm.view.model.NavigationDrawerModel;
import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;

import java.util.List;

/**
 * Created by Nott on 19/04/2016 AD.
 * NavigationDrawerViewInterface
 */
public interface NavigationDrawerViewInterface extends BaseViewInterface {

  void setNavigationItems(List<NavigationDrawerModel> navigationItems);

}



