package com.iom.dtm.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.BindView;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.dependency.component.HomeComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.presenter.EnumeratorsPresenter;
import com.iom.dtm.view.adapter.EnumeratorAdapter;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.dialog.EnumeratorDialog;
import com.iom.dtm.view.viewinterface.EnumeratorsViewInterface;
import com.smartsoftasia.ssalibrary.view.adapter.OnAdapterClick;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

/**
 * Created by androiddev03 on 7/6/2559.
 * EnumeratorsFragment
 */
public class EnumeratorsFragment extends BaseAppFragment
    implements EnumeratorsViewInterface, OnAdapterClick,
    EnumeratorDialog.EnumeratorProfileDialogListener, SearchView.OnQueryTextListener,
    EnumeratorAdapter.AdapterListener {
  public static final String TAG = "EnumeratorsFragment";
  private Timer timer = new Timer();

  public interface OnFragmentInteractionListener {
    void onOpenEnumeratorProfileActivity(Enumerator enumerator);

    void onOpenViewEnumeratorReportActivity(Enumerator enumerator);
  }

  @Inject
  EnumeratorsPresenter presenter;
  @BindView(R.id.recyclerview_enumerator_fragment)
  RecyclerView recyclerView;
  @BindView(R.id.textview_enumerator_fragment_no_enumerators)
  BaseTextView noItemTextView;
  @BindView(R.id.progressbar_enumerator_fragment)
  ProgressBar progressBar;

  protected EnumeratorAdapter mAdapter;
  private OnFragmentInteractionListener mListener;
  private EnumeratorDialog enumeratorDialog;
  private ConfirmationDialog deactivateConfirmationDialog;

  public static EnumeratorsFragment newInstance() {
    EnumeratorsFragment fragment = new EnumeratorsFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);

    setActionbarTitle(R.string.action_bar_enumerators_fragment);
    mAdapter = new EnumeratorAdapter(new ArrayList<Enumerator>(), getContext());
    mAdapter.setAdapterClickListener(this);
    mAdapter.setListener(this);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(mAdapter);
    recyclerView.addOnScrollListener(new RecyclerOnScrollListener());
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_enumerators;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_enumerators, menu);

    SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
    searchView.setQueryHint(getString(R.string.global_search_with_dot));
    searchView.setOnQueryTextListener(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void initialize() {
    this.getComponent(HomeComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize();
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onAdapterClick(int position, Object item, RecyclerView.ViewHolder view) {
    presenter.onEnumeratorDialogClick(((Enumerator) item).id);

    enumeratorDialog = EnumeratorDialog.newInstance((Enumerator) mAdapter.getItem(position));
    enumeratorDialog.setEnumeratorProfileDialogListener(this);
    enumeratorDialog.show(getFragmentManager(), EnumeratorDialog.TAG);
  }

  @Override
  public void appendEnumeratorList(List<Enumerator> enumerators) {
    mAdapter.appendItems(enumerators);
  }

  @Override
  public void appendEnumeratorListFromSearch(List<Enumerator> enumerators) {
    mAdapter.appendNewsItems(enumerators);
  }

  @Override
  public void onEnumeratorDeleteClick(Enumerator enumerator) {
    presenter.onEnumeratorDialogClick(enumerator.id);
    enumeratorDeleteDialog();
  }

  @Override
  public void onViewReportClick(Enumerator enumerator) {
    if (mListener != null) {
      mListener.onOpenViewEnumeratorReportActivity(enumerator);
    }
    dismissEnumeratorDialog();
  }

  @Override
  public void onProfileClick(Enumerator enumerator) {
    if (mListener != null) {
      mListener.onOpenEnumeratorProfileActivity(enumerator);
    }
    dismissEnumeratorDialog();
  }

  @Override
  public void onDeactivateClick() {
    enumeratorDeleteDialog();
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    return false;
  }

  @Override
  public boolean onQueryTextChange(final String newText) {
    if (timer != null) {
      timer.cancel();
    }

    timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        getActivity().runOnUiThread(new Runnable() {
          @Override
          public void run() {
            presenter.onTextChanged(newText);
          }
        });
      }
    }, AppConstant.SEARCH_DELAY);

    return false;
  }

  @Override
  public void hideLoadingForDeactivated() {
    hideProgressDialog();
  }

  @Override
  public void displayLoadingForDeactivated() {
    dismissEnumeratorDialog();
    dismissDeactivateConfirmationDialog();
    displayProgressDialogLoading();
  }

  @Override
  public void onDisplayLoading() {
    recyclerView.setVisibility(View.GONE);
    noItemTextView.setVisibility(View.GONE);
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void onHideLoading() {
    progressBar.setVisibility(View.GONE);
    recyclerView.setVisibility(mAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
    noItemTextView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
  }

  @Override
  public void clearEnumeratorList() {
    if (mAdapter != null) {
      mAdapter.clear();
    }
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  private void dismissEnumeratorDialog() {
    if (enumeratorDialog != null) {
      enumeratorDialog.dismiss();
    }
  }

  private void dismissDeactivateConfirmationDialog() {
    if (deactivateConfirmationDialog != null) {
      deactivateConfirmationDialog.dismiss();
    }
  }

  private void enumeratorDeleteDialog() {
    deactivateConfirmationDialog = new ConfirmationDialog.Builder().setTitleDialog(
        getString(R.string.enumerators_fragment_deactivate_dialog_title))
        .setOkTextButton(getString(R.string.enumerators_fragment_deactivate_dialog_deactivate))
        .setCancelTextButton(getString(R.string.enumerators_fragment_deactivate_dialog_cancel))
        .createDialog();
    deactivateConfirmationDialog.setConfirmationDialogListener(new ConfirmationDialog.ConfirmationDialogListener() {
      @Override
      public void onOkClick() {
        presenter.onDeactivateClick();
      }

      @Override
      public void onCancelClick() {
        deactivateConfirmationDialog.dismiss();
      }
    });
    deactivateConfirmationDialog.show(getFragmentManager(), ConfirmationDialog.TAG);
  }

  class RecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
      super.onScrollStateChanged(recyclerView, newState);
      LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
      if (layoutManager.findLastVisibleItemPosition() == mAdapter.getItemCount() - 1) {
        presenter.getNextPage();
      }
    }
  }
}