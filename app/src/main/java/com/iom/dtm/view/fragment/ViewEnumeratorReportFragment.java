package com.iom.dtm.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.BindView;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.dependency.component.ViewEnumeratorReportComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.presenter.ViewEnumeratorReportPresenter;
import com.iom.dtm.view.adapter.ViewEnumeratorReportAdapter;
import com.iom.dtm.view.dialog.ReportHasBeenSentDialog;
import com.iom.dtm.view.viewinterface.ViewEnumeratorReportViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseRoundedImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

/**
 * Created by Nott on 6/21/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewEnumeratorReportFragment extends BaseAppFragment
    implements ViewEnumeratorReportViewInterface,
    ViewEnumeratorReportAdapter.ViewEnumeratorReportAdapterListener,
    SearchView.OnQueryTextListener {
  public static final String TAG = "ViewEnumeratorReportFragment";
  private static final String ARG_ENUMERATOR = "arg_enumerator";

  public interface OnFragmentInteractionListener {
    void onOpenViewFilledDTMFormActivity(Enumerator enumerator, Report report);
  }

  @BindView(R.id.textview_enumerator_report_fragment_name)
  BaseTextView nameTextView;
  @BindView(R.id.textview_enumerator_report_fragment_email)
  BaseTextView emailTextView;
  @BindView(R.id.textview_enumerator_report_fragment_phone)
  BaseTextView phoneTextView;
  @BindView(R.id.textview_enumerator_report_fragment_no_reports)
  BaseTextView noReportTextView;
  @BindView(R.id.imageview_enumerator_report_fragment_profile)
  BaseRoundedImageView profileImageView;
  @BindView(R.id.recyclerview_enumerator_report_fragment)
  RecyclerView recyclerView;
  @BindView(R.id.progressbar_enumerator_report_fragment)
  ProgressBar progressBar;

  @Inject
  protected ViewEnumeratorReportPresenter presenter;
  protected ViewEnumeratorReportAdapter mAdapter;
  private OnFragmentInteractionListener mListener;
  private RecyclerOnScrollListener scrollListener = new RecyclerOnScrollListener();
  private Timer timer = new Timer();

  public static ViewEnumeratorReportFragment newInstance(Enumerator enumerator) {
    ViewEnumeratorReportFragment fragment = new ViewEnumeratorReportFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_ENUMERATOR, enumerator);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_view_enumerator_report;
  }

  @Override
  protected void initialize() {
    this.getComponent(ViewEnumeratorReportComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgEnumerator());
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);

    mAdapter = new ViewEnumeratorReportAdapter(new ArrayList<Report>(), getContext());
    mAdapter.setListener(this);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(mAdapter);
    recyclerView.addOnScrollListener(scrollListener);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_view_enumerator_report, menu);

    SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
    searchView.setQueryHint(getString(R.string.global_search_with_dot));
    searchView.setOnQueryTextListener(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onDisplayLoading() {
    recyclerView.setVisibility(View.GONE);
    noReportTextView.setVisibility(View.GONE);
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void onHideLoading() {
    progressBar.setVisibility(View.GONE);
    recyclerView.setVisibility(View.VISIBLE);
    if (mAdapter.getItemCount() == 0) {
      recyclerView.setVisibility(View.GONE);
      noReportTextView.setVisibility(View.VISIBLE);
    } else {
      noReportTextView.setVisibility(View.GONE);
      recyclerView.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void appendReportList(List<Report> reports) {
    mAdapter.appendItems(reports);
  }

  @Override
  public void onDownLoadClick(Report report) {
    presenter.onDownLoadClick(report);
  }

  @Override
  public void onItemClick(Report report) {
    if (mListener != null) {
      mListener.onOpenViewFilledDTMFormActivity(presenter.getEnumerator(), report);
    }
  }

  @Override
  public void onOpenReportHasBeenSentDialog(Report report, Enumerator enumerator) {
    ReportHasBeenSentDialog dialog = ReportHasBeenSentDialog.newInstance(enumerator, report);
    dialog.show(getFragmentManager(), ReportHasBeenSentDialog.TAG);
  }

  @Override
  public void displayLoadingProgressBar() {
    displayProgressDialogLoading();
  }

  @Override
  public void hideLoadingProgressBar() {
    hideProgressDialog();
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    return false;
  }

  @UiThread
  @Override
  public boolean onQueryTextChange(final String newText) {
    if (timer != null) {
      timer.cancel();
    }

    timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        getActivity().runOnUiThread(new Runnable() {
          @Override
          public void run() {
            presenter.onTextChanged(newText);
          }
        });
      }
    }, AppConstant.SEARCH_DELAY);

    return false;
  }

  @Override
  public void clearList() {
    if (mAdapter != null) {
      mAdapter.clear();
    }
  }

  @Override
  public void updateEnumeratorDetails(Enumerator enumerator) {
    if (enumerator == null) {
      return;
    }

    nameTextView.setText(enumerator.getFullName());
    emailTextView.setText(enumerator.getEmail());
    profileImageView.downloadImage(this, enumerator.getProfilePictureUrl(), null);
  }

  private Enumerator getArgEnumerator() {
    if (getArguments() == null) {
      return null;
    } else {
      return getArguments().getParcelable(ARG_ENUMERATOR);
    }
  }

  private class RecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
      super.onScrollStateChanged(recyclerView, newState);
      LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
      if (layoutManager.findLastVisibleItemPosition() == mAdapter.getItemCount() - 1) {
        presenter.getNextPage();
      }
    }
  }
}
