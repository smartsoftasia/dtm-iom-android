package com.iom.dtm.view.fragment;

import android.content.Intent;

import android.support.annotation.StringRes;
import android.view.View;
import android.widget.Toast;

import butterknife.ButterKnife;

import com.iom.dtm.view.activity.BaseAppActivity;
import com.iom.dtm.view.viewinterface.BaseAppViewInterface;
import com.smartsoftasia.ssalibrary.bus.ApplicationBus;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;
import com.smartsoftasia.ssalibrary.view.fragment.BaseFragment;

import javax.inject.Inject;

/**
 * Created by Nott on 4/19/2016 AD.
 * BaseAppFragment
 */
public abstract class BaseAppFragment extends BaseFragment implements BaseAppViewInterface {
  @Inject
  protected ApplicationBus applicationBus;

  @Override
  public void onResume() {
    super.onResume();
    if (applicationBus != null) applicationBus.register(this);
  }

  @Override
  public void onPause() {
    super.onPause();
    if (applicationBus != null) applicationBus.unregister(this);
  }

  /**
   * Gets a component for dependency injection by its type.
   */
  @SuppressWarnings("unchecked")
  protected <C> C getComponent(Class<C> componentType) {
    return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
  }

  public void openGPSSettings() {
    Intent callGPSSettingIntent = new Intent(
        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    startActivity(callGPSSettingIntent);
  }

  protected BaseAppActivity getBaseAppActivity() {
    return (BaseAppActivity) getActivity();
  }

  public static final ButterKnife.Setter<View, Boolean> VISIBILITY = new ButterKnife.Setter<View, Boolean>() {
    @Override
    public void set(View view, Boolean value, int index) {
      view.setVisibility(value ? View.VISIBLE : View.GONE);
    }
  };

  public static final ButterKnife.Setter<View, Boolean> ENABILITY = new ButterKnife.Setter<View, Boolean>() {
    @Override
    public void set(View view, Boolean value, int index) {
      view.setEnabled(value);
    }
  };

  protected void displayProgressDialogLoading() {
    getBaseAppActivity().displayProgressDialogLoading();
  }

  protected void displayProgressDialogLoading(String string) {
    getBaseAppActivity().displayProgressDialogLoading(string);
  }

  protected void displayProgressDialogLoading(@StringRes int stringResource) {
    getBaseAppActivity().displayProgressDialogLoading(getString(stringResource));
  }

  protected void displayToastMessage(String message) {
    getBaseAppActivity().displayToastMessage(message);
  }

  protected void displayToastMessage(@StringRes int message) {
    getBaseAppActivity().displayToastMessage(message);
  }

  protected void displayLongToastMessage(String message) {
    getBaseAppActivity().displayLongToastMessage(message);
  }

  protected void displayLongToastMessage(@StringRes int message) {
    getBaseAppActivity().displayLongToastMessage(message);
  }

  protected void hideKeyboard() {
    getBaseAppActivity().hideKeyboard();
  }

  protected void hideProgressDialog() {
    getBaseAppActivity().hideProgressDialog();
  }

  protected void showSnackBarError(Throwable e) {
    getBaseAppActivity().showSnackBarError(e);
  }

  protected void showSnackBarError(String e) {
    getBaseAppActivity().showSnackBarError(e);
  }

  @Override
  public void onUnauthorized() {
    getBaseAppActivity().onUnauthorized();
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onError(Throwable error) {

  }
}
