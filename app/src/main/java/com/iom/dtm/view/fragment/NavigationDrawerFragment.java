package com.iom.dtm.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.HomeComponent;
import com.iom.dtm.presenter.NavigationDrawerPresenter;
import com.iom.dtm.view.adapter.NavigationAdapter;
import com.iom.dtm.view.model.NavigationDrawerModel;
import com.iom.dtm.view.viewinterface.NavigationDrawerViewInterface;
import com.smartsoftasia.ssalibrary.view.adapter.BaseLinearAdapter;
import com.smartsoftasia.ssalibrary.view.widget.LinearListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends BaseDrawerFragment
    implements NavigationDrawerViewInterface, CompoundButton.OnCheckedChangeListener,
    BaseLinearAdapter.OnItemClickListener {
  private static final String TAG = "NavigationDrawerFragment";

  @BindView(R.id.linearListView_navigation)
  protected LinearListView listView;
  @Inject
  protected NavigationDrawerPresenter navigationDrawerPresenter;
  @Inject
  protected NavigationAdapter navigationAdapter;
  /**
   * Interface implement by the activity
   */
  private NavigationDrawerListener navigationDrawerListener;

  public NavigationDrawerFragment() {

  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    setHasOptionsMenu(true);
    initView();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    //TODO: Move on base fragment like for activity.
    View root = inflater.inflate(R.layout.fragment_navigation_drawer, container);
    ButterKnife.bind(this, root);
    return root;
  }

  //TODO: create abstract methon on BASEfragment
  protected void initView() {
    navigationAdapter.setItemClickListener(this);
    listView.setAdapter(navigationAdapter);
  }

  public int getViewResourceId() {
    return R.layout.fragment_navigation_drawer;
  }

  protected void afterLoadContentView(Bundle savedInstanceState) {
    if (navigationAdapter == null) {
      navigationAdapter = new NavigationAdapter(new ArrayList<NavigationDrawerModel>(),
          getContext());
      navigationAdapter.setItemClickListener(this);
    }
    listView.setAdapter(navigationAdapter);
  }

  @Override
  protected void initialize() {
    this.getComponent(HomeComponent.class).inject(this);
    this.navigationDrawerPresenter.setView(this);
    this.navigationDrawerPresenter.setNavigationDrawerListener(navigationDrawerListener);
  }

  @Override
  public void onStart() {
    super.onStart();
    //TODO: Implement this into the baseFragment by creating a Presenter atribute into the baseFragment.
    navigationDrawerPresenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    //TODO: Implement this into the baseFragment by creating a Presenter atribute into the baseFragment.
    navigationDrawerPresenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    //TODO: Implement this into the baseFragment by creating a Presenter atribute into the baseFragment.
    navigationDrawerPresenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    //TODO: Implement this into the baseFragment by creating a Presenter atribute into the baseFragment.
    navigationDrawerPresenter.destroy();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    try {
      navigationDrawerListener = (NavigationDrawerListener) context;
    } catch (ClassCastException e) {
      throw new ClassCastException("Activity must implement NavigationDrawerListener.");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    navigationDrawerListener = null;
  }

  @Override
  public void onError(String error) {
    getBaseAppActivity().showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    getBaseAppActivity().showSnackBarError(error);
  }

  @Override
  public void onItemClick(int position) {
    navigationDrawerPresenter.itemSelected(position);
  }

  @Override
  public void setNavigationItems(List<NavigationDrawerModel> navigationItems) {
    if (navigationAdapter != null) {
      navigationAdapter.clear();
      navigationAdapter.setItems(navigationItems);
    }
  }

  @Override
  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

  }

  public interface NavigationDrawerListener {
    void openEnumeratorsFragment();

    void openHomeFragment();

    void openViewReportFragment();

    void openHomeAboutFragment();

    void openQuestionnaireForGeneralPublicFragment();

    void openDTMFormFragment();

    void openAboutFragment();

    void logout();

    void login();

    void hideDrawer();
  }
}