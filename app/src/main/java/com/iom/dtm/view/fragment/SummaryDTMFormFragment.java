package com.iom.dtm.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.helper.TextViewHelper;
import com.iom.dtm.presenter.SummaryDTMFormPresenter;
import com.iom.dtm.view.adapter.DTMFormMenuAdapter;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.viewinterface.SummaryDTMFormViewInterface;
import com.smartsoftasia.ssalibrary.view.adapter.BaseLinearAdapter;
import com.smartsoftasia.ssalibrary.view.widget.LinearListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SummaryDTMFormFragment extends BaseAppFragment
    implements SummaryDTMFormViewInterface, ConfirmationDialog.ConfirmationDialogListener,
    BaseLinearAdapter.OnItemClickListener {
  public static final String TAG = "SummaryDTMFormFragment";

  @Inject
  SummaryDTMFormPresenter presenter;
  @BindView(R.id.linearListView_summary_dtm_form_fragment)
  LinearListView linearListView;

  DTMFormMenuAdapter menuAdapter;

  OnFragmentInteractionListener mListener;

  ConfirmationDialog dialog;

  public interface OnFragmentInteractionListener {
    void onOpenQuestionCategory(int position);
  }

  public static SummaryDTMFormFragment newInstance() {
    SummaryDTMFormFragment fragment = new SummaryDTMFormFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_summary_dtm_form;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    if (menuAdapter == null) {
      menuAdapter = new DTMFormMenuAdapter(new ArrayList<String>(), getContext());
      menuAdapter.setItemClickListener(this);
    }
    linearListView.setAdapter(menuAdapter);
  }

  @Override
  protected void initialize() {
    this.getComponent(DTMFormDetailsComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void displayDTMSubmitSuccessfulDialog() {
    if (dialog != null) {
      dialog.dismiss();
    }

    new AlertDialog.Builder(getContext()).setTitle(
        R.string.dtm_from_fragment_summary_successful_dialog_title)
        .setMessage(R.string.dtm_from_fragment_summary_successful_dialog_body)
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            getBaseActivity().finish();
          }
        })
        .setCancelable(false)
        .show();
  }

  @Override
  public void displayAnswerRequired(String questionName) {
    if (dialog != null) {
      dialog.dismiss();
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      displayLongToastMessage(Html.fromHtml(questionName, Html.FROM_HTML_MODE_LEGACY).toString());
    } else {
      displayLongToastMessage(TextViewHelper.removeHtml(questionName));
    }
  }

  @Override
  public void appendListQuestionCategory(List<String> stringList) {
    menuAdapter.clear();
    menuAdapter.setItems(stringList);
  }

  /**
   * DTMSubmitSuccessfulDialog
   * For Submit click
   */
  @Override
  public void onOkClick() {
    presenter.onSubmitClick();
  }

  /**
   * DTMSubmitSuccessfulDialog
   * For Review click
   */
  @Override
  public void onCancelClick() {
    if (dialog != null) {
      dialog.dismiss();
    }

    if (mListener != null) {
      mListener.onOpenQuestionCategory(0);
    }
  }

  @Override
  public void onItemClick(int position) {
    openQuestionCategory(position);
  }

  @Override
  public void openQuestionCategory(int position) {
    if (mListener != null) {
      mListener.onOpenQuestionCategory(position);
    }
  }

  @OnClick(R.id.button_summary_dtm_form_fragment_submit)
  public void onSubmitClick(View v) {
/*    dialog = new ConfirmationDialog.Builder().setTitleDialog(getString(
        AppConfig.DEBUG ? R.string.debug_dtm_from_fragment_summary_dialog_title
            : R.string.dtm_from_fragment_summary_dialog_title))
        .setCancelTextButton(getString(R.string.dtm_from_fragment_summary_dialog_review))
        .setOkTextButton(getString(R.string.dtm_from_fragment_summary_dialog_submit))
        .createDialog();*/
    dialog = new ConfirmationDialog.Builder()
        .setTitleDialog(getString(R.string.debug_dtm_from_fragment_summary_dialog_title))
        .setCancelTextButton(getString(R.string.dtm_from_fragment_summary_dialog_review))
        .setOkTextButton(getString(R.string.dtm_from_fragment_summary_dialog_submit))
        .createDialog();
    dialog.setConfirmationDialogListener(this);
    dialog.show(getFragmentManager(), ConfirmationDialog.TAG);
  }
}
