package com.iom.dtm.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerViewEnumeratorReportComponent;
import com.iom.dtm.dependency.component.ViewEnumeratorReportComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.view.fragment.ViewEnumeratorReportFragment;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;

/**
 * Created by Nott on 6/21/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewEnumeratorReportActivity extends BaseAppActivity
    implements HasComponent<ViewEnumeratorReportComponent>,
    ViewEnumeratorReportFragment.OnFragmentInteractionListener {
  public static final String TAG = "ViewEnumeratorReportActivity";
  private static final String ARG_ENUMERATOR = "arg_enumerator";

  public static Intent newInstance(Context context, Enumerator enumerator) {
    Intent intent = new Intent();
    intent.putExtra(ARG_ENUMERATOR, enumerator);
    intent.setClass(context, ViewEnumeratorReportActivity.class);
    return intent;
  }

  ViewEnumeratorReportComponent mComponent;

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    replaceFragment(R.id.container, ViewEnumeratorReportFragment.newInstance(getArgEnumerator()));
    setActionbarTitle(R.string.action_bar_view_enumerator_report_fragment);
    setBackOnActionbar();
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerViewEnumeratorReportComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_view_enumerator_report;
  }

  @Override
  public ViewEnumeratorReportComponent getComponent() {
    return mComponent;
  }

  @Override
  public void onOpenViewFilledDTMFormActivity(Enumerator enumerator, Report report) {
    startActivity(ViewFilledDTMFormActivity.newInstance(getApplicationContext(), enumerator, report));
  }

  private Enumerator getArgEnumerator() {
    if (getIntent() == null) {
      return null;
    } else {
      return getIntent().getParcelableExtra(ARG_ENUMERATOR);
    }
  }
}
