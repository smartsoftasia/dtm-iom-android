package com.iom.dtm.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerViewReportHomeComponent;
import com.iom.dtm.dependency.component.ViewReportHomeComponent;
import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.ViewReportHomePresenter;
import com.iom.dtm.view.fragment.viewreport.BaseCheckBoxFragment;
import com.iom.dtm.view.fragment.viewreport.ViewReportSelectPeriodTimeFragment;
import com.iom.dtm.view.fragment.viewreport.ViewReportSelectProvinceFragment;
import com.iom.dtm.view.fragment.viewreport.ViewReportSelectQuestionsFragment;
import com.iom.dtm.view.fragment.viewreport.ViewReportSummaryFragment;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 7/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewReportHomeActivity extends BaseAppActivity
    implements HasComponent<ViewReportHomeComponent>,
    ViewReportSelectProvinceFragment.FragmentListener,
    ViewReportSelectQuestionsFragment.FragmentListener,
    ViewReportSelectPeriodTimeFragment.FragmentListener,
    ViewReportSummaryFragment.FragmentListener {
  public static final String TAG = "ViewReportHomeActivity";
  private static final String ARG_EVENT = "arg_event";

  public static Intent newInstance(Context context, Event event) {
    Intent intent = new Intent();
    intent.putExtra(ARG_EVENT, event);
    intent.setClass(context, ViewReportHomeActivity.class);
    return intent;
  }

  ViewReportHomeComponent mComponent;

  @Inject
  ViewReportHomePresenter presenter;
  private boolean mIsSelectedAll = true;
  private boolean isOptionMenuVisible = true;
  MenuItem optionMenuItem;

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    displaySelectProvinceFragment(getArgEvent());
    setBackOnActionbar();
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerViewReportHomeComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
    this.presenter.initialize(getArgEvent());
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_view_report_home;
  }

  @Override
  public ViewReportHomeComponent getComponent() {
    return mComponent;
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_checkbox, menu);
    optionMenuItem = menu.findItem(R.id.action_checkbox);
    optionMenuItem.setVisible(isOptionMenuVisible);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    switch (item.getItemId()) {
      case R.id.action_checkbox: {
        if (mIsSelectedAll) {
          if (fragment instanceof BaseCheckBoxFragment) {
            ((BaseCheckBoxFragment) fragment).onSelectAll();
          }
          item.setTitle(R.string.global_clear);
          this.mIsSelectedAll = false;
        } else {
          if (fragment instanceof BaseCheckBoxFragment) {
            ((BaseCheckBoxFragment) fragment).onDeselectAll();
          }
          item.setTitle(R.string.global_select_all);
          this.mIsSelectedAll = true;
        }
      }
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    if (fragment instanceof ViewReportSelectQuestionsFragment) {
      removeFragment(fragment);
      setActionbarTitle(R.string.actionbar_view_report_select_province);
      displayOptionMenu();
    } else if (fragment instanceof ViewReportSelectPeriodTimeFragment) {
      removeFragment(fragment);
      setActionbarTitle(R.string.actionbar_view_report_select_questions);
      displayOptionMenu();
    } else if (fragment instanceof ViewReportSummaryFragment) {
      ((ViewReportSummaryFragment) fragment).onBackPressed();
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public void openSelectQuestionsFragment(List<Admin1> admin1s) {
    presenter.admin1Selected(admin1s);
    displaySelectQuestionsFragment(presenter.getEvent());
    this.mIsSelectedAll = true;
  }

  @Override
  public void openSelectPeriodTimeFragment(List<Question> questions) {
    presenter.questionSelected(questions);
    displaySelectPeriodTimeFragment();
  }

  @Override
  public void openViewReportSummaryFragment(Date startDate, Date endDate) {
    presenter.setStartEndDate(startDate, endDate);
    displayViewReportSummaryFragment();
  }

  @Override
  public void openViewReportSelectTimeFragmentFromSummary() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    removeFragment(fragment);
    setActionbarTitle(R.string.actionbar_view_report_select_time);
  }


  private void displaySelectProvinceFragment(Event event) {
    replaceFragment(R.id.container, ViewReportSelectProvinceFragment.newInstance(event));
    setActionbarTitle(R.string.actionbar_view_report_select_province);
    displayOptionMenu();
  }

  private void displaySelectQuestionsFragment(Event event) {
    addFragment(R.id.container, ViewReportSelectQuestionsFragment.newInstance(event));
    setActionbarTitle(R.string.actionbar_view_report_select_questions);
    displayOptionMenu();
  }

  private void displaySelectPeriodTimeFragment() {
    addFragment(R.id.container, ViewReportSelectPeriodTimeFragment.newInstance());
    setActionbarTitle(R.string.actionbar_view_report_select_time);
    hideOptionMenu();
  }

  private void displayViewReportSummaryFragment() {
    addFragment(R.id.container, ViewReportSummaryFragment.newInstance(presenter.getViewReport()));
    setActionbarTitle(R.string.actionbar_view_report_summary);
    hideOptionMenu();
  }

  private void hideOptionMenu() {
    this.isOptionMenuVisible = false;
    invalidateOptionsMenu();
  }

  private void displayOptionMenu() {
    this.isOptionMenuVisible = true;
    invalidateOptionsMenu();
  }

  private Event getArgEvent() {
    if (getIntent() == null) {
      return null;
    } else {
      return getIntent().getParcelableExtra(ARG_EVENT);
    }
  }
}
