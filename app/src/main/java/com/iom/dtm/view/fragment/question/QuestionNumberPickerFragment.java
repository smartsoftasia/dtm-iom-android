package com.iom.dtm.view.fragment.question;

import android.os.Bundle;
import android.text.Html;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.question.QuestionNumberPickerPresenter;
import com.iom.dtm.view.viewinterface.QuestionNumberPickerViewInterface;
import com.smartsoftasia.ssalibrary.view.dialog.NumberPickerDialog;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionNumberPickerFragment extends BaseQuestionFragment
    implements QuestionNumberPickerViewInterface, NumberPickerDialog.OnNumberPickerListener {
  public static final String TAG = "QuestionNumberPickerFragment";

  @BindView(R.id.textview_question_number_picker_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.edittext_question_number_picker_fragment)
  BaseEditText editText;

  @Inject
  QuestionNumberPickerPresenter presenter;

  public static QuestionNumberPickerFragment newInstance(Question question, int answerTime) {
    QuestionNumberPickerFragment fragment = new QuestionNumberPickerFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_number_picker;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void setUpQuestion(Question question) {
    labelTextView.setText(Html.fromHtml(question.title));
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @OnClick(R.id.edittext_question_number_picker_fragment)
  public void onEditTextClick(View v) {
    NumberPickerDialog dialog = NumberPickerDialog.newInstance()
        .setCancelTextButton(getString(R.string.number_picker_dialog_cancel))
        .setOkTextButton(getString(R.string.number_picker_dialog_ok))
        .setTitleDialog(getString(R.string.number_picker_dialog_title))
        .setMinValue(1)
        .setMaxValue(30);
    dialog.setListener(this);
    dialog.show(getFragmentManager(), NumberPickerDialog.TAG);
  }

  @Override
  public void numberSelected(int i) {
    editText.setText(String.valueOf(i));
    presenter.onNumberSelected(String.valueOf(i));
  }

  @Override
  public void setUpAnswer(String number) {
    editText.setText(number);
  }
}
