package com.iom.dtm.view.viewinterface.viewreport;

import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;

import java.util.Date;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public interface ViewReportSelectPeriodTimeViewInterface extends BaseViewInterface {
  void openViewReportSummaryFragment(Date startDate, Date endDate);

  void displaySelectDate();
}
