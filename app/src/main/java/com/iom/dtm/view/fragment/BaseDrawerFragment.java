package com.iom.dtm.view.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

import com.iom.dtm.R;

/**
 * Created by Nott on 19/04/2016 AD.
 * BaseDrawerFragment
 */
public abstract class BaseDrawerFragment extends BaseAppFragment {
  public static final String TAG = "AbstractNavigationDrawerFragment";

  /**
   * Per the design guidelines, you should show the layout_drawer on launch until the user manually
   * expands it. This shared preference tracks this.
   */
  private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

  protected boolean userLearnedDrawer;
  protected DrawerLayout drawerLayout;
  protected View fragmentContainerView;

  /**
   * Boolean checking drawer open
   *
   * @return Boolean checking drawer open
   */
  public boolean isDrawerOpen() {
    return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
  }

  /**
   * Users of this fragment must call this method to set up the navigation layout_drawer
   * interactions.
   *
   * @param fragmentId   The android:id of this fragment in its activity's layout.
   * @param drawerLayout The DrawerLayout containing this fragment's UI.
   */
  public void setUp(int fragmentId, DrawerLayout drawerLayout) {
    this.fragmentContainerView = getActivity().findViewById(fragmentId);
    this.drawerLayout = drawerLayout;

    // set a custom shadow that overlays the main content when the layout_drawer opens
    this.drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
    // set up the layout_drawer's index view with items and click listener

  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Read in the flag indicating whether or not the user has demonstrated awareness of the
    // layout_drawer. See PREF_USER_LEARNED_DRAWER for details.
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
    userLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);
  }
}

