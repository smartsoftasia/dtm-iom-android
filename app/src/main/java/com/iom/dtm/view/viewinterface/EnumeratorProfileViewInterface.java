package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Enumerator;

/**
 * Created by Nott on 6/20/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface EnumeratorProfileViewInterface extends LoadingViewInterface {

  void updateEnumeratorProfile(Enumerator enumerator);

  void goBackToEnumeratorsFragmentAfterDeactivated();
}
