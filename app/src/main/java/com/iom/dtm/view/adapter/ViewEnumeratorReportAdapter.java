package com.iom.dtm.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.iom.dtm.R;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.helper.DateAppHelper;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nott on 6/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewEnumeratorReportAdapter
    extends BaseAppRecyclerView<ViewEnumeratorReportAdapter.ViewHolder, Report> {
  public static final String TAG = "ViewEnumeratorReportAdapter";

  public interface ViewEnumeratorReportAdapterListener {
    void onDownLoadClick(Report report);

    void onItemClick(Report report);
  }

  private ViewEnumeratorReportAdapterListener mListener;

  public ViewEnumeratorReportAdapter(List<Report> items, Context context) {
    super(items, context);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.adapter_view_enumerator_report, parent, false);

    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, final int position) {
    holder.reportNameTextView.setText(items.get(position).getTitle());
    holder.statsTextView.setText(context.getString(R.string.enumerator_report_adapter_created_date, DateAppHelper.dateTimeToString(items.get(position).getCreatedAt())));
    holder.downloadRelativeLayout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mListener != null) {
          mListener.onDownLoadClick(items.get(position));
        }
      }
    });

    holder.itemRelativeLayout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mListener != null) {
          mListener.onItemClick(items.get(position));
        }
      }
    });
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.relativelayout_view_enumerator_report_adapter_download)
    RelativeLayout downloadRelativeLayout;
    @BindView(R.id.relativelayout_view_enumerator_report_adapter_item)
    RelativeLayout itemRelativeLayout;
    @BindView(R.id.textview_view_enumerator_report_adapter_name)
    BaseTextView reportNameTextView;
    @BindView(R.id.textview_view_enumerator_report_adapter_stats)
    BaseTextView statsTextView;

    public ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  public void setListener(ViewEnumeratorReportAdapterListener listener) {
    this.mListener = listener;
  }
}
