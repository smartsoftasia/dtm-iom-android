package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.QuestionCategory;

import java.util.List;

/**
 * Created by Nott on 9/30/2016 AD.
 * DTM
 */

public interface QuestionnaireForGeneralPublicViewInterface extends LoadingViewInterface {

  void setUpQuestionnaire(List<QuestionCategory> categoryList);

  void displayNoQuestionnaire();

  void displayAnswerRequired(String message);

  void displayEmailInvalid(String message);

  void displayGeneratingForm();

  void recreateFragment();

  void displayDTMSubmitSuccessfulDialog();
}
