package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.QuestionGroup;
import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionGroupMaleFemaleViewInterface extends BaseViewInterface {

  void setUpQuestion(QuestionGroup questionGroup);

  void setUpTotalNumber(long totalNumber);

  void setUpAnswer(String male, String female, String title);
}
