package com.iom.dtm.view.fragment.questiongroup;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.presenter.questiongroup.QuestionGroupRepeatPresenter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.fragment.question.QuestionDatePickerFragment;
import com.iom.dtm.view.fragment.question.QuestionDropDownFragment;
import com.iom.dtm.view.fragment.question.QuestionEmailFragment;
import com.iom.dtm.view.fragment.question.QuestionLocationFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberPickerFragment;
import com.iom.dtm.view.fragment.question.QuestionPhotoVideoFragment;
import com.iom.dtm.view.fragment.question.QuestionSingleChoiceFragment;
import com.iom.dtm.view.fragment.question.QuestionStringFragment;
import com.iom.dtm.view.fragment.question.QuestionTextFragment;
import com.iom.dtm.view.viewinterface.QuestionGroupRepeatViewInterface;
import com.smartsoftasia.ssalibrary.helper.ViewHelper;
import com.smartsoftasia.ssalibrary.view.widget.BaseButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupRepeatFragment extends BaseAppFragment
    implements QuestionGroupRepeatViewInterface {
  public static final String TAG = "QuestionGroupRepeatFragment";
  public static final String ARG_QUESTION_GROUP = "arg_question_group";

  @BindView(R.id.linearlayout_question_group_repeat)
  LinearLayout linearLayout;
  @BindView(R.id.button_question_group_repeat_fragment_add_more)
  BaseButton addMoreButton;

  @Inject
  QuestionGroupRepeatPresenter presenter;

  public static QuestionGroupRepeatFragment newInstance(QuestionGroup questionGroup) {
    QuestionGroupRepeatFragment fragment = new QuestionGroupRepeatFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION_GROUP, questionGroup);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_group_repeat;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgQuestionGroup(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    // Do nothing
  }

  @Override
  public void appendQuestionDatePicker(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(
        QuestionDatePickerFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionLocation(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(
        QuestionLocationFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionNumber(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionNumberFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionSingleChoice(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(
        QuestionSingleChoiceFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionString(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionStringFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionEmail(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionEmailFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionAttachment(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(
        QuestionPhotoVideoFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionText(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionTextFragment.newInstance(question, position));
  }


  @Override
  public void appendQuestionDropDown(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionDropDownFragment.newInstance(question, position));
  }

  @Override
  public void appendQuestionAdmin(QuestionGroup questionGroup, int answerTime) {
    addQuestionGroupToQuestionGroupFragment(
        QuestionGroupAdminFragment.newInstance(questionGroup, answerTime));
  }

  @Override
  public void setUpQuestionGroup(QuestionGroup questionGroup) {
    addMoreButton.setText(getString(R.string.question_group_repeat_fragment));
  }

  @Override
  public void addLabel(String title, int position) {
    View view = LayoutInflater.from(getContext())
        .inflate(R.layout.adapter_textview_question_repeat, linearLayout, false);
    BaseTextView labelTextView = (BaseTextView) view.findViewById(
        R.id.textview_question_group_repeat_fragment_label);
    labelTextView.setPaintFlags(labelTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    labelTextView.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
    labelTextView.setText(title);

    linearLayout.addView(view);
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onError(Throwable error) {

  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  @OnClick(R.id.button_question_group_repeat_fragment_add_more)
  public void onAddMoreClick(View v) {
    presenter.handleQuestion();
  }

  private void addQuestionGroupToQuestionGroupFragment(Fragment fragment) {
    FrameLayout frameLayout = new FrameLayout(getActivity());
    frameLayout.setId(ViewHelper.generateViewId());

    getChildFragmentManager().beginTransaction().add(frameLayout.getId(), fragment, TAG).commit();

    this.linearLayout.addView(frameLayout);
  }

  private QuestionGroup getArgQuestionGroup(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION_GROUP);
    } else {
      return null;
    }
  }
}
