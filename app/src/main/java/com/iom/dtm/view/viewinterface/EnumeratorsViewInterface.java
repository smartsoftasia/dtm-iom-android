package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Enumerator;

import java.util.List;

/**
 * Created by Nott on 7/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface EnumeratorsViewInterface extends LoadingViewInterface {
  void appendEnumeratorList(List<Enumerator> enumerators);

  void displayLoadingForDeactivated();

  void hideLoadingForDeactivated();

  void appendEnumeratorListFromSearch(List<Enumerator> enumerators);

  void clearEnumeratorList();
}
