package com.iom.dtm.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerEditProfileComponent;
import com.iom.dtm.dependency.component.EditProfileComponent;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.view.fragment.EditProfileFragment;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;
import com.smartsoftasia.ssalibrary.helper.Logger;
import javax.inject.Inject;

/**
 * Created by Nott on 6/28/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EditProfileActivity extends BaseAppActivity
    implements HasComponent<EditProfileComponent>,
    EditProfileFragment.OnFragmentInteractionListener {
  public static final String TAG = "EditProfileActivity";

  EditProfileComponent mComponent;

  @Inject
  SharedPreference mSharedPreference;

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerEditProfileComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    replaceFragment(R.id.container, EditProfileFragment.newInstance());
    setBackOnActionbar();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_edit_profile;
  }

  @Override
  public EditProfileComponent getComponent() {
    return mComponent;
  }

  @Override
  public void logout() {
    mSharedPreference.writeUser(null);
    mSharedPreference.writeUserToken(null);
    startNewStackActivity(HomeActivity.class);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    fragment.onActivityResult(requestCode, resultCode, data);
  }
}
