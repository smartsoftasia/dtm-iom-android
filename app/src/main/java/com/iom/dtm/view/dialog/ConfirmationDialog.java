package com.iom.dtm.view.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import butterknife.BindView;
import butterknife.OnClick;
import com.iom.dtm.R;
import com.smartsoftasia.ssalibrary.view.widget.BaseButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

/**
 * Created by Nott on 6/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ConfirmationDialog extends BaseAppDialog {
  public static final String TAG = "ConfirmationDialog";
  private static final String ARG_TITLE = "arg_title";
  private static final String ARG_OK = "arg_ok";
  private static final String ARG_CANCEL = "arg_cancel";

  private String mTitle, mOk, mCancel;

  public interface ConfirmationDialogListener {
    void onOkClick();

    void onCancelClick();
  }

  @BindView(R.id.textview_confirmation_dialog_title)
  BaseTextView titleTextView;
  @BindView(R.id.button_confirmation_dialog_ok)
  BaseButton okButton;
  @BindView(R.id.button_confirmation_dialog_cancel)
  BaseButton cancelButton;
  private ConfirmationDialogListener mListener;

  public static ConfirmationDialog newInstance(String title, String ok, String cancel) {
    ConfirmationDialog fragment = new ConfirmationDialog();
    Bundle args = new Bundle();
    args.putString(ARG_TITLE, title);
    args.putString(ARG_OK, ok);
    args.putString(ARG_CANCEL, cancel);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mTitle = getArguments().getString(ARG_TITLE);
      mOk = getArguments().getString(ARG_OK);
      mCancel = getArguments().getString(ARG_CANCEL);
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_confirmation;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setParams();
  }

  @OnClick(R.id.button_confirmation_dialog_ok)
  public void onOkClick(View v) {
    if (mListener != null) mListener.onOkClick();
  }

  @OnClick(R.id.button_confirmation_dialog_cancel)
  public void onCancelClick(View v) {
    if (mListener != null) mListener.onCancelClick();
  }

  public void setConfirmationDialogListener(ConfirmationDialogListener listener) {
    this.mListener = listener;
  }

  private void setParams() {
    titleTextView.setText(mTitle);
    okButton.setText(mOk);
    cancelButton.setText(mCancel);
  }

  public static class Builder {
    private String nestedTitleDialog;
    private String nestedOkButton;
    private String nestedCancelButton;

    public Builder() {
    }

    public Builder setCancelTextButton(String nestedCancelButton) {
      this.nestedCancelButton = nestedCancelButton;
      return this;
    }

    public Builder setOkTextButton(String nestedOkButton) {
      this.nestedOkButton = nestedOkButton;
      return this;
    }

    public Builder setTitleDialog(String nestedTitleDialog) {
      this.nestedTitleDialog = nestedTitleDialog;
      return this;
    }

    public ConfirmationDialog createDialog() {
      return newInstance(nestedTitleDialog, nestedOkButton, nestedCancelButton);
    }
  }
}
