package com.iom.dtm.view.viewinterface.viewreport;

import com.iom.dtm.domain.model.Question;
import com.iom.dtm.view.model.QuestionCheckBoxViewModel;
import com.iom.dtm.view.viewinterface.BaseCheckboxViewInterface;

import java.util.List;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public interface ViewReportSelectQuestionViewInterface extends BaseCheckboxViewInterface<QuestionCheckBoxViewModel> {
  void openSelectTimePeriodFragment(List<Question> questions);

  void displayPleaseSelectQuestion();
}
