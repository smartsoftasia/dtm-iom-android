package com.iom.dtm.view.dialog;

import android.os.Bundle;
import android.view.View;
import butterknife.BindView;
import butterknife.OnClick;
import com.iom.dtm.R;
import com.iom.dtm.rest.form.SignInForm;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;

/**
 * Created by Nott on 6/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ReplaceRoleVerifyDialog extends BaseAppDialog {
  public static final String TAG = "ReplaceRoleVerifyDialog";

  public interface ReplaceRoleVerifyDialogDialogListener {
    void onReplaceRoleVerifyClick(String email, String password);
  }

  private ReplaceRoleVerifyDialogDialogListener mListener;

  @BindView(R.id.edittext_replace_role_verify_dialog_email)
  BaseEditText emailEditText;
  @BindView(R.id.edittext_replace_role_verify_dialog_password)
  BaseEditText passwordEditText;
  private SignInForm signInForm = new SignInForm();

  public static ReplaceRoleVerifyDialog newInstance() {
    ReplaceRoleVerifyDialog fragment = new ReplaceRoleVerifyDialog();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_replace_role_verify;
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
  }

  public void setReplaceRoleVerifyDialogListener(ReplaceRoleVerifyDialogDialogListener listener) {
    this.mListener = listener;
  }

  @OnClick(R.id.button_replace_role_verify_dialog_verify)
  public void onVerifyClick(View v) {
    signInForm.setEmail(emailEditText.getText().toString());
    signInForm.setPassword(passwordEditText.getText().toString());

    if (signInForm.isFormValid(emailEditText, passwordEditText)) {
      if (mListener != null) {
        mListener.onReplaceRoleVerifyClick(emailEditText.getText().toString(),
            passwordEditText.getText().toString());
      }
    }
  }

  @OnClick(R.id.button_replace_role_verify_dialog_cancel)
  public void onCancelClick(View v) {
    dismiss();
  }
}
