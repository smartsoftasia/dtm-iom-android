package com.iom.dtm.view.fragment.viewreport;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.ViewReportHomeComponent;
import com.iom.dtm.presenter.viewreport.ViewReportSelectPeriodTimePresenter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.viewinterface.viewreport.ViewReportSelectPeriodTimeViewInterface;
import com.smartsoftasia.ssalibrary.helper.DateHelper;
import com.smartsoftasia.ssalibrary.view.dialog.DatePickerDialogFragment;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;

import java.util.Date;

import javax.inject.Inject;

/**
 * Created by Nott on 9/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewReportSelectPeriodTimeFragment extends BaseAppFragment implements ViewReportSelectPeriodTimeViewInterface {
  public static final String TAG = "ViewReportSelectPeriodTimeFragment";

  public interface FragmentListener {
    void openViewReportSummaryFragment(Date startDate, Date endDate);
  }

  public static ViewReportSelectPeriodTimeFragment newInstance() {
    ViewReportSelectPeriodTimeFragment fragment = new ViewReportSelectPeriodTimeFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  private FragmentListener mListener;
  @Inject
  ViewReportSelectPeriodTimePresenter presenter;
  @BindView(R.id.edittext_view_report_select_period_fragment_start_date)
  BaseEditText startEditText;
  @BindView(R.id.edittext_view_report_select_period_fragment_end_date)
  BaseEditText endEditText;

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_view_report_select_period_time;
  }

  @Override
  protected void initialize() {
    this.getComponent(ViewReportHomeComponent.class).inject(this);
    this.presenter.setView(this);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof FragmentListener) {
      mListener = (FragmentListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void openViewReportSummaryFragment(Date startDate, Date endDate) {
    if (mListener != null) {
      mListener.openViewReportSummaryFragment(startDate, endDate);
    }
  }

  @Override
  public void displaySelectDate() {
    displayToastMessage(R.string.view_report_fragment_please_select_period_time);
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @OnClick(R.id.edittext_view_report_select_period_fragment_start_date)
  public void onStartDateClick(View v) {
    DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
    dialogFragment.show(getFragmentManager(), "date_picker_start");
    dialogFragment.setOnDateSelectListener(new DatePickerDialogFragment.OnDateSelect() {
      @Override
      public void onDateSelect(Date date) {
        startEditText.setText(DateHelper.dateTimeToDDMMMMYYYY(date));
        endEditText.setText(null);
        presenter.startDateHasBeenSelected(date);
        presenter.endDateHasBeenSelected(null);
      }
    });
  }

  @OnClick(R.id.edittext_view_report_select_period_fragment_end_date)
  public void onEndDateClick(View v) {
    if (TextUtils.isEmpty(startEditText.getText().toString())) {
      return;
    }

    DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
    dialogFragment.show(getFragmentManager(), "date_picker_end");
    dialogFragment.setMinDate(presenter.getStartDate());
    dialogFragment.setOnDateSelectListener(new DatePickerDialogFragment.OnDateSelect() {
      @Override
      public void onDateSelect(Date date) {
        endEditText.setText(DateHelper.dateTimeToDDMMMMYYYY(date));
        presenter.endDateHasBeenSelected(date);
      }
    });
  }

  @OnClick(R.id.button_next)
  public void onNextClick(View v) {
    presenter.onNextClick();
  }
}
