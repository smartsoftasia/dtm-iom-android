package com.iom.dtm.view.viewinterface;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionGroupRepeatViewInterface extends BaseQuestionGroupViewInterface {
  void addLabel(String title, int position);
}
