package com.iom.dtm.view.viewinterface;

import rx.subjects.PublishSubject;

/**
 * Created by Nott on 7/28/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface NoInternetConnectionViewInterface {
  PublishSubject<Integer> noInternetConnection(Throwable retrofitError);
}
