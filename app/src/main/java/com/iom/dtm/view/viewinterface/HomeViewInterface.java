package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.User;
import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;

/**
 * Created by Nott on 6/29/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface HomeViewInterface extends BaseAppViewInterface {

  void onUpdateProfile(User user);
}
