package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.User;

/**
 * Created by Nott on 6/13/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface HomeProfileViewInterface extends LoadingViewInterface {

  void onDisplayImageChooser();

  void onUpdateProfile(User user);

  void logout();

  void onDisplayDeactivateDialog(boolean isAdmin);

  void onReplaceRoleSendToNewOneDialog();

  void onReplaceRoleUserVerifyFailed();

  void onReplaceRoleSendToNewOneFailed();

  void onDeactivatedUserVerifyFailed();

  void updateImageProfile(String url);

  void displayProfileHasBeenUpdatedToast();
}
