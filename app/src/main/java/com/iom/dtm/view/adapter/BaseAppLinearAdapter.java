package com.iom.dtm.view.adapter;

import android.content.Context;
import com.smartsoftasia.ssalibrary.view.adapter.BaseLinearAdapter;
import java.util.List;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BaseAppLinearAdapter<T> extends BaseLinearAdapter<T> {
  private static final String TAG = "BaseAppLinearAdapter";

  public BaseAppLinearAdapter(List<T> items, Context context) {
    super(items, context);
  }
}
