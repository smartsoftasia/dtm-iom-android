package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Event;
import java.util.List;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface DTMFormViewInterface extends BaseAppViewInterface {

  void openDTMFormDetailsActivity(int position);

  void displayLoading();

  void hideLoading();

  void appendListOfQuestionCategory(List<String> questionCategoryList);

  void displayGeneratingFormMessage();

  void displayPleaseSelectEventBeforeStartForm();

  void displaySelectNewEventConfirmation();

  void updateEventListSpinner(List<Event> eventList, int position);
}
