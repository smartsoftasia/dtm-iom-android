package com.iom.dtm.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.firebase.crash.FirebaseCrash;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerHomeComponent;
import com.iom.dtm.dependency.component.HomeComponent;
import com.iom.dtm.domain.bus.AdminMessage;
import com.iom.dtm.domain.bus.RxBus;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.service.fcm.DTMFirebaseInstanceIdService;
import com.iom.dtm.service.fcm.DTMFirebaseMessagingService;
import com.iom.dtm.service.fcm.DTMRegistrationIntentService;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.fragment.HomeFragment;
import com.iom.dtm.view.fragment.QuestionnaireForGeneralPublicFragment;
import com.iom.dtm.view.fragment.ViewReportFragment;
import com.iom.dtm.view.fragment.AboutFragment;
import com.iom.dtm.view.fragment.NavigationDrawerFragment;
import com.iom.dtm.view.fragment.DTMFormFragment;
import com.iom.dtm.view.fragment.EnumeratorsFragment;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;

import javax.inject.Inject;

/**
 * Created by Nott on 5/26/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class HomeActivity extends NavigationDrawerActivity
    implements HasComponent<HomeComponent>, NavigationDrawerFragment.NavigationDrawerListener,
    ViewReportFragment.OnFragmentInteractionListener,
    QuestionnaireForGeneralPublicFragment.OnFragmentInteractionListener,
    DTMFormFragment.OnFragmentInteractionListener,
    EnumeratorsFragment.OnFragmentInteractionListener,
    ConfirmationDialog.ConfirmationDialogListener, HomeFragment.OnFragmentInteractionListener {
  public static final String TAG = "HomeActivity";
  public static final String ARG_GPQ = "arg_gpq";

  public static Intent newInstanceForReloadGPQ(Context context) {
    Intent intent = new Intent();
    intent.putExtra(ARG_GPQ, true);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    intent.setClass(context, HomeActivity.class);
    return intent;
  }

  private HomeComponent mComponent;
  private ConfirmationDialog logoutDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArgGPQ()) {
      getIntent().putExtra(ARG_GPQ, false);
      openQuestionnaireForGeneralPublicFragment();
    } else {
      checkUserTypeForReplaceFragment();
    }
    registerFCM();
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    isFromNotification();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_home;
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerHomeComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  public HomeComponent getComponent() {
    return mComponent;
  }

  @Override
  public void openHomeFragment() {
    replaceFragment(R.id.container, HomeFragment.newInstance());
  }

  @Override
  public void openHomeAboutFragment() {
    replaceFragment(R.id.container, AboutFragment.newInstance());
  }

  @Override
  public void openDTMFormFragment() {
    replaceFragment(R.id.container, DTMFormFragment.newInstance());
  }

  @Override
  public void openEnumeratorsFragment() {
    replaceFragment(R.id.container, EnumeratorsFragment.newInstance());
  }

  @Override
  public void openViewReportFragment() {
    replaceFragment(R.id.container, ViewReportFragment.newInstance());
  }

  @Override
  public void openQuestionnaireForGeneralPublicFragment() {
    replaceFragment(R.id.container, QuestionnaireForGeneralPublicFragment.newInstance());
  }

  @Override
  public void openAboutFragment() {
    replaceFragment(R.id.container, AboutFragment.newInstance());
  }

  @Override
  public void logout() {
    logoutDialog = new ConfirmationDialog.Builder().setTitleDialog(
        getString(R.string.logout_dialog_title))
        .setOkTextButton(getString(R.string.logout_dialog_logout))
        .setCancelTextButton(getString(R.string.logout_dialog_cancel))
        .createDialog();
    logoutDialog.setConfirmationDialogListener(this);
    logoutDialog.show(getSupportFragmentManager(), ConfirmationDialog.TAG);
  }

  @Override
  public void login() {
    startCompactActivity(SignInActivity.class);
  }

  @Override
  public void onOpenEnumeratorProfileActivity(Enumerator enumerator) {
    startActivity(EnumeratorProfileActivity.newInstance(getApplicationContext(), enumerator));
  }

  @Override
  public void onOpenViewEnumeratorReportActivity(Enumerator enumerator) {
    startActivity(ViewEnumeratorReportActivity.newInstance(getApplicationContext(), enumerator));
  }

  @Override
  public void onOpenEditProfileActivity() {
    startCompactActivity(EditProfileActivity.class);
  }

  @Override
  public void onOpenDTMFormDetailsActivity(int position) {
    startActivity(DTMFormDetailsActivity.newInstance(getApplicationContext(), position));
  }

  @Override
  public void onOpenDTMFormFragment() {
    replaceFragment(R.id.container, DTMFormFragment.newInstance());
  }

  @Override
  public void onOpenEnumeratorsFragment() {
    replaceFragment(R.id.container, EnumeratorsFragment.newInstance());
  }

  @Override
  public void openViewReportHomeActivity(Event event) {
    startActivity(ViewReportHomeActivity.newInstance(getApplicationContext(), event));
  }

  /**
   * Go to SignInActivity after clear User SP
   */
  @Override
  public void onOkClick() {
    mSharedPreference.writeUser(null);
    mSharedPreference.writeUserToken(null);
    startNewStackActivity(HomeActivity.class);
  }

  /**
   * Dismiss Logout dialog
   */
  @Override
  public void onCancelClick() {
    if (logoutDialog != null) {
      logoutDialog.dismiss();
    }
  }

  /**
   * Replace HomeFragment before exit the app
   */
  @Override
  public void onBackPressed() {
    if (mSharedPreference.readUser() != null) {
      if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof HomeFragment) {
        super.onBackPressed();
      } else {
        checkUserTypeForReplaceFragment();
      }
    } else {
      if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof AboutFragment) {
        super.onBackPressed();
      } else {
        replaceFragment(R.id.container, AboutFragment.newInstance());
      }
    }
  }

  private void checkUserTypeForReplaceFragment() {
    if (mSharedPreference.readUser() != null) {
      replaceFragment(R.id.container, HomeFragment.newInstance());
    } else {
      replaceFragment(R.id.container, AboutFragment.newInstance());
    }
  }

  private boolean getArgGPQ() {
    if (getIntent() == null) {
      return false;
    } else {
      return getIntent().getBooleanExtra(ARG_GPQ, false);
    }
  }

  private void registerFCM() {
    if (mSharedPreference.readUser() != null) {
      Intent intent = new Intent(this, DTMRegistrationIntentService.class);
      intent.putExtra(DTMRegistrationIntentService.USER_ID, mSharedPreference.readUser().id);
      startService(intent);
    }
  }

  private void isFromNotification() {
    if (!TextUtils.isEmpty(getIntent().getStringExtra(DTMFirebaseMessagingService.ARG_MESSAGE))) {
      displayAdminMessage(getIntent().getStringExtra(DTMFirebaseMessagingService.ARG_MESSAGE));
    }
  }
}

