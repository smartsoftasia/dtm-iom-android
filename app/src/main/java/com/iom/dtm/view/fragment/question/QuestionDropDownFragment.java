package com.iom.dtm.view.fragment.question;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.question.QuestionDropDownPresenter;
import com.iom.dtm.view.viewinterface.QuestionDropDownViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionDropDownFragment extends BaseQuestionFragment
    implements QuestionDropDownViewInterface {
  public static final String TAG = "QuestionStringFragment";

  @Inject
  QuestionDropDownPresenter presenter;
  @BindView(R.id.edittext_question_drop_down_fragment)
  BaseEditText editText;
  @BindView(R.id.textview_question_drop_down_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.edittext_question_drop_down_fragment_additional)
  BaseEditText otherEditText;

  public static QuestionDropDownFragment newInstance(Question question, int answerTime) {
    QuestionDropDownFragment fragment = new QuestionDropDownFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_drop_down;
  }

  @Override
  public void setUpQuestion(Question question) {
    labelTextView.setText(Html.fromHtml(question.title));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void setUpAnswer(String answer) {
    editText.setText(answer);
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @OnClick(R.id.edittext_question_drop_down_fragment)
  public void onEditTextClick(View v) {
    displayChoicesDialog();
  }

  @Override
  public void isHasAdditionalRequiredEditText(boolean isHad) {
    otherEditText.setVisibility(isHad ? View.VISIBLE : View.GONE);
    if (isHad) {
      otherEditText.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
          presenter.onOtherTextChanged(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
      });
    } else {
      presenter.onOtherTextChanged(null);
      otherEditText.setText(AppConstant.EMPTY);
    }
  }

  private void displayChoicesDialog() {
    final ArrayAdapter<Choice> arrayAdapter = new ArrayAdapter<Choice>(getActivity(),
        android.R.layout.simple_list_item_1);
    arrayAdapter.addAll(presenter.getChoices());
    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        editText.setText(presenter.getChoices().get(i).title);
        presenter.onItemIsSelected(i);
      }
    });
    builderSingle.show();
  }
}
