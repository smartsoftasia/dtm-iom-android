package com.iom.dtm.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.HomeComponent;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.presenter.viewreport.ViewReportPresenter;
import com.iom.dtm.view.viewinterface.ViewReportViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by androiddev03 on 8/6/2559.
 * ViewReportFragment
 */
public class ViewReportFragment extends BaseAppFragment implements ViewReportViewInterface {
  public static final String TAG = "ViewReportFragment";

  public interface OnFragmentInteractionListener {
    void openViewReportHomeActivity(Event event);
  }

  private OnFragmentInteractionListener mListener;
  @Inject
  ViewReportPresenter presenter;
  @BindView(R.id.textview_view_report_fragment)
  BaseTextView selectEventTextView;

  public static ViewReportFragment newInstance() {
    ViewReportFragment fragment = new ViewReportFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  public int getViewResourceId() {
    return R.layout.fragment_view_report;
  }

  @Override
  protected void initialize() {
    this.getComponent(HomeComponent.class).inject(this);
    this.presenter.setView(this);
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(R.string.action_bar_view_report_fragment);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void displayEventListDialog(final List<Event> eventList) {
    final ArrayAdapter<Event> arrayAdapter = new ArrayAdapter<Event>(getActivity(),
        android.R.layout.simple_list_item_1);
    arrayAdapter.addAll(eventList);
    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        presenter.eventSelected(eventList.get(i));
        selectEventTextView.setText(eventList.get(i).title);
      }
    });
    builderSingle.show();
  }

  @Override
  public void openViewReportHomeActivity(Event event) {
    if (mListener != null) {
      mListener.openViewReportHomeActivity(event);
    }
  }

  @Override
  public void displayPleaseSelectEvent() {
    displayToastMessage(R.string.view_report_fragment_please_select_event);
  }

  @OnClick(R.id.button_view_report_fragment_get_report)
  public void onGetReportClick(View v) {
    presenter.onGetReportClick();
  }

  @OnClick(R.id.textview_view_report_fragment)
  public void onSelectEventClick(View v) {
    presenter.onSelectEventClick();
  }
}
