package com.iom.dtm.view.viewinterface;

import java.util.Map;

/**
 * Created by Nott on 6/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface ViewFilledDTMFormViewInterface extends LoadingViewInterface {

  void settingUpWebView(Map<String, String> extraHeaders);

  void displayReportHasBeenSentDialog();
}
