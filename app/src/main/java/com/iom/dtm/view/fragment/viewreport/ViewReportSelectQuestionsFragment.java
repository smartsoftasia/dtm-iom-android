package com.iom.dtm.view.fragment.viewreport;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.ViewReportHomeComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.viewreport.ViewReportSelectQuestionPresenter;
import com.iom.dtm.view.adapter.CheckboxAdapter;
import com.iom.dtm.view.adapter.EnumeratorAdapter;
import com.iom.dtm.view.fragment.EnumeratorsFragment;
import com.iom.dtm.view.model.QuestionCheckBoxViewModel;
import com.iom.dtm.view.viewinterface.viewreport.ViewReportSelectQuestionViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.view.adapter.OnAdapterClick;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 9/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewReportSelectQuestionsFragment extends BaseCheckBoxFragment<QuestionCheckBoxViewModel> implements ViewReportSelectQuestionViewInterface, OnAdapterClick {
  public static final String TAG = "ViewReportSelectQuestionsFragment";
  private static final String ARG_EVENT = "arg_event";


  public interface FragmentListener {
    void openSelectPeriodTimeFragment(List<Question> questions);
  }

  public static ViewReportSelectQuestionsFragment newInstance(Event event) {
    ViewReportSelectQuestionsFragment fragment = new ViewReportSelectQuestionsFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_EVENT, event);
    fragment.setArguments(args);
    return fragment;
  }

  @Inject
  ViewReportSelectQuestionPresenter presenter;
  @BindView(R.id.progressbar_view_report_select_question)
  ProgressBar progressBar;
  @BindView(R.id.recyclerview_view_report_select_questions_fragment)
  RecyclerView recyclerView;
  private FragmentListener mListener;
  private CheckboxAdapter<QuestionCheckBoxViewModel> adapter;

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_view_report_select_questions;
  }

  public void setListener(FragmentListener listener) {
    this.mListener = listener;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    adapter = new CheckboxAdapter<QuestionCheckBoxViewModel>(new ArrayList<QuestionCheckBoxViewModel>(), getContext());
    adapter.setAdapterClickListener(this);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(adapter);
  }

  @Override
  protected void initialize() {
    this.getComponent(ViewReportHomeComponent.class).inject(this);
    this.presenter.initialize(getArgEvent());
    this.presenter.setView(this);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof FragmentListener) {
      mListener = (FragmentListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void openSelectTimePeriodFragment(List<Question> questions) {
    if (mListener != null) {
      mListener.openSelectPeriodTimeFragment(questions);
    }
  }

  @Override
  public void displayPleaseSelectQuestion() {
    displayToastMessage(R.string.view_report_fragment_please_select_question);
  }

  @Override
  public void onDisplayLoading() {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void onHideLoading() {
    progressBar.setVisibility(View.GONE);
  }

  @Override
  public void appendCheckBoxList(List<QuestionCheckBoxViewModel> viewModels) {
    adapter.appendItems(viewModels);
  }

  @Override
  public void onSelectAll() {
    presenter.onSelectAllClick();
  }

  @Override
  public void onDeselectAll() {
    presenter.onDeselectAllClick();
  }

  @Override
  public void onSelectAllCheckBox() {
    adapter.notifyDataSetChanged();
  }

  @Override
  public void onDeselectAllCheckBox() {
    adapter.notifyDataSetChanged();
  }

  @Override
  public void onAdapterClick(int position, Object item, RecyclerView.ViewHolder view) {
    QuestionCheckBoxViewModel model = (QuestionCheckBoxViewModel) item;
    presenter.onAddOrUpdateClick(model, model.isChecked);
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @OnClick(R.id.button_next)
  public void onNextClick(View v) {
    presenter.onNextClick();
  }

  private Event getArgEvent() {
    if (getArguments() == null) {
      return null;
    } else {
      return getArguments().getParcelable(ARG_EVENT);
    }
  }
}
