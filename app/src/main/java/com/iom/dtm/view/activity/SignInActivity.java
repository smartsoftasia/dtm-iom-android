package com.iom.dtm.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.dependency.component.DaggerSignInComponent;
import com.iom.dtm.dependency.component.SignInComponent;
import com.iom.dtm.helper.DebugAppHelper;
import com.iom.dtm.presenter.SignInPresenter;
import com.iom.dtm.rest.form.SignInForm;
import com.iom.dtm.view.viewinterface.SignInViewInterface;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.helper.TextViewHelper;
import com.smartsoftasia.ssalibrary.view.widget.BaseButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.Arrays;

import javax.inject.Inject;

public class SignInActivity extends BaseAppActivity
    implements HasComponent<SignInComponent>, SignInViewInterface,
    GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
  private static final String TAG = "SignInActivity";

  private static final int RC_SIGN_IN = 9001;

  @Inject
  protected SignInPresenter presenter;

  @BindView(R.id.edittext_login_email)
  BaseEditText emailEditText;
  @BindView(R.id.edittext_sign_in_password)
  BaseEditText passwordEditText;
  @BindView(R.id.button_sign_in)
  BaseButton signinButton;
  @BindView(R.id.textview_sign_in_reset_password)
  BaseTextView resetPasswordTextView;
  @BindView(R.id.basebutton_facebook_signin)
  BaseButton facebookButton;
  @BindView(R.id.basebutton_google_signin)
  BaseButton googleButton;
  @BindView(R.id.textview_signin_terms_conditions)
  BaseTextView termsConditionsAgreement;
  @BindView(R.id.button_sign_in_register)
  BaseButton registerButton;
  @BindView(R.id.button_sign_in_skip_login)
  BaseButton skipButton;

  private SignInComponent mComponent;
  private CallbackManager mCallbackManager;
  private GoogleApiClient mGoogleApiClient;

  private final FacebookCallback<LoginResult> mFacebookCallBack = new FacebookCallback<LoginResult>() {
    @Override
    public void onSuccess(LoginResult loginResult) {
      presenter.loginWithFacebook(loginResult.getAccessToken().getToken());
      Log.e("Facebook", loginResult.getAccessToken().getToken());
    }

    @Override
    public void onCancel() {
      onHideLoading();
      Logger.d(TAG, "Facebook login canceled");
    }

    @Override
    public void onError(FacebookException error) {
      Logger.e(TAG, error.toString());
    }
  };

  @Override
  protected void beforeLoadContentView(Bundle savedInstanceState) {
    super.beforeLoadContentView(savedInstanceState);
    initFacebook();
    initGooglePlus();
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(R.string.action_bar_sign_in_activity);

    if (AppConfig.DEBUG) {
      emailEditText.setText(DebugAppHelper.USER_EMAIL);
      passwordEditText.setText(DebugAppHelper.USER_PASSWORD);
    }
    setTermsConditionsLink();
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == RC_SIGN_IN) {
      GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
      handleSignInResult(result);
    } else if (mCallbackManager != null) {
      mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
  }

  private void initFacebook() {
    FacebookSdk.sdkInitialize(getApplicationContext());
    mCallbackManager = CallbackManager.Factory.create();
    LoginManager.getInstance().registerCallback(mCallbackManager, mFacebookCallBack);
  }

  private void googleSignIn() {
    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
    startActivityForResult(signInIntent, RC_SIGN_IN);
  }

  private void handleSignInResult(GoogleSignInResult result) {
    Logger.e(TAG, "handleSignInResult " + result.isSuccess());
    if (result.isSuccess()) {
      GoogleSignInAccount acct = result.getSignInAccount();

      if (acct != null) {
        presenter.loginWithGoogle(acct.getIdToken());
      }
    }
  }

  @Override
  public SignInComponent getComponent() {
    return mComponent;
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerSignInComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
    this.presenter.setView(this);
    this.presenter.initialize();
  }

  private void initGooglePlus() {
    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(
        GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(
        SignInActivity.this.getResources().getString(R.string.google_web_application_client_id))
        .requestEmail()
        .build();
    mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
        .build();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_sign_in;
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void onEmailEmpty() {
    emailEditText.setError(getString(R.string.error_field_required));
    emailEditText.requestFocus();
  }

  @Override
  public void onPasswordEmpty() {
    passwordEditText.setError(getString(R.string.error_field_required));
    passwordEditText.requestFocus();
  }

  @Override
  public void onEmailInvalid() {
    emailEditText.setError(getString(R.string.error_invalid_email));
    emailEditText.requestFocus();
  }

  @Override
  public void onPasswordInvalid() {
    passwordEditText.setError(getString(R.string.error_invalid_password));
    passwordEditText.requestFocus();
  }

  @Override
  public void openHomeActivity() {
    startNewStackActivity(HomeActivity.class);
  }

  @Override
  public void openSignUpActivity() {
    startCompactActivity(SignUpActivity.class);
  }

  @OnClick(R.id.button_sign_in)
  public void onSignInClick(View v) {
    SignInForm signInForm = new SignInForm(emailEditText.getText().toString(),
        passwordEditText.getText().toString());
    presenter.validateForm(signInForm);
  }

  @OnClick(R.id.textview_sign_in_reset_password)
  public void onResetPasswordClick(View v) {
    startCompactActivity(ResetPasswordActivity.class);
  }

  @OnClick(R.id.button_sign_in_register)
  public void onSignUpClick(View v) {
    startCompactActivity(SignUpActivity.class);
  }

  @OnClick(R.id.basebutton_facebook_signin)
  public void onFacebookLogin(View v) {
    LoginManager.getInstance()
        .logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
  }

  @OnClick(R.id.button_sign_in_skip_login)
  public void onSkipClick(View v) {
    startNewStackActivity(HomeActivity.class);
  }

  @OnClick(R.id.basebutton_google_signin)
  public void onGoogleLogin(View v) {
    googleSignIn();
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {

  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  private void setTermsConditionsLink() {
    TextViewHelper.setOnClickLinkWebView(this, termsConditionsAgreement,
        R.string.sign_up_in_term_agreement, R.string.sign_up_in_term_agreement_link,
        AppConfig.TERM_AND_CONDITION_URL, R.color.primary);
  }
}