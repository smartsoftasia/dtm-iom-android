package com.iom.dtm.view.model;

import android.support.annotation.StringRes;

/**
 * Created by androiddev03 on 6/6/2559.
 */
public class NavigationDrawerModel {
  public static final String TAG = "NavigationDrawerModel";

  @StringRes
  public int title;

  public NavigationDrawerModel() {
  }

  public NavigationDrawerModel(@StringRes int title) {
    this.title = title;
  }
}

