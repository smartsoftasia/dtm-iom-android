package com.iom.dtm.view.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.iom.dtm.R;
import com.iom.dtm.view.model.BaseCheckBoxViewModel;
import com.smartsoftasia.ssalibrary.view.widget.BaseCheckBox;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nott on 12/10/2559.
 * dtm-iom-android
 */

public class CheckboxAdapter<T extends BaseCheckBoxViewModel> extends BaseAppRecyclerView<CheckboxAdapter.ViewHolder, T> {
  public static final String TAG = "CheckboxAdapter";


  public CheckboxAdapter(List<T> items, Context context) {
    super(items, context);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.adapter_checkbox, parent, false);

    return new ViewHolder(v);
  }

  @SuppressWarnings("deprecation")
  @Override
  public void onBindViewHolder(final ViewHolder holder, final int position) {
    final T item = items.get(position);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      holder.titleTextView.setText(Html.fromHtml(item.getDisplayCheckBoxTitle(), Html.FROM_HTML_MODE_LEGACY));
    } else {
      holder.titleTextView.setText(Html.fromHtml(item.getDisplayCheckBoxTitle()));
    }

    holder.checkBox.setChecked(item.isChecked);
    if (!TextUtils.isEmpty(item.getHeaderCheckbox())) {
      holder.headerRelativeLayout.setVisibility(View.VISIBLE);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        holder.headerTitleTextView.setText(Html.fromHtml(item.getHeaderCheckbox(), Html.FROM_HTML_MODE_LEGACY));
      } else {
        holder.headerTitleTextView.setText(Html.fromHtml(item.getHeaderCheckbox()));
      }
    } else {
      holder.headerRelativeLayout.setVisibility(View.GONE);
    }

    if (!TextUtils.isEmpty(item.getSubHeaderCheckbox())) {
      holder.subHeaderRelativeLayout.setVisibility(View.VISIBLE);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        holder.subHeaderTitleTextView.setText(Html.fromHtml(item.getSubHeaderCheckbox(), Html.FROM_HTML_MODE_LEGACY));
      } else {
        holder.subHeaderTitleTextView.setText(Html.fromHtml(item.getSubHeaderCheckbox()));
      }
    } else {
      holder.subHeaderRelativeLayout.setVisibility(View.GONE);
    }

    holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        item.setChecked(isChecked);
        if (adapterClickListener != null) {
          adapterClickListener.onAdapterClick(position, item, holder);
        }
      }
    });
    holder.itemLayout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        holder.checkBox.setChecked(!holder.checkBox.isChecked());
        item.setChecked(holder.checkBox.isChecked());
        if (adapterClickListener != null) {
          adapterClickListener.onAdapterClick(position, item, holder);
        }
      }
    });
  }


  public static class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textview_adapter_checkbox_header_title)
    BaseTextView headerTitleTextView;
    @BindView(R.id.textview_adapter_checkbox_sub_header_title)
    BaseTextView subHeaderTitleTextView;
    @BindView(R.id.textview_adapter_checkbox_title)
    BaseTextView titleTextView;
    @BindView(R.id.checkbox_adapter_checkbox_check_box)
    BaseCheckBox checkBox;
    @BindView(R.id.relativelayout_adapter_checkbox_header)
    RelativeLayout headerRelativeLayout;
    @BindView(R.id.relativelayout_adapter_checkbox_sub_header)
    RelativeLayout subHeaderRelativeLayout;
    @BindView(R.id.layout_adapter_checkbox)
    RelativeLayout itemLayout;

    public ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

}
