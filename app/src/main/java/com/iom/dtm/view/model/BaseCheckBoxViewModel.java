package com.iom.dtm.view.model;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public abstract class BaseCheckBoxViewModel<T> {

  public T t;
  public String displayCheckBoxTitle;
  public String headerCheckbox;
  public String subHeaderCheckbox;
  public boolean isChecked;

  public BaseCheckBoxViewModel(T t, String displayCheckBoxTitle) {
    this.t = t;
    this.displayCheckBoxTitle = displayCheckBoxTitle;
  }

  public void setHeaderCheckbox(String headerCheckbox) {
    this.headerCheckbox = headerCheckbox;
  }

  public String getSubHeaderCheckbox() {
    return subHeaderCheckbox;
  }

  public void setSubHeaderCheckbox(String subHeaderCheckbox) {
    this.subHeaderCheckbox = subHeaderCheckbox;
  }

  public String getHeaderCheckbox() {
    return headerCheckbox;
  }

  public String getDisplayCheckBoxTitle() {
    return displayCheckBoxTitle;
  }

  public void setChecked(boolean checked) {
    isChecked = checked;
  }
}
