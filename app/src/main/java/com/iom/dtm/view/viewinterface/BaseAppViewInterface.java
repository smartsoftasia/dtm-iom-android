package com.iom.dtm.view.viewinterface;

import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;

public interface BaseAppViewInterface extends BaseViewInterface {
  void onUnauthorized();
}
