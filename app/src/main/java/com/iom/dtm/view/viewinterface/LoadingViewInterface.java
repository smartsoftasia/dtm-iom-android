package com.iom.dtm.view.viewinterface;

public interface LoadingViewInterface extends BaseAppViewInterface {

  void onDisplayLoading();

  void onHideLoading();

}
