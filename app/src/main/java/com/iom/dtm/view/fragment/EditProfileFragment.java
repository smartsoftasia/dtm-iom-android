package com.iom.dtm.view.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.EditProfileComponent;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.helper.AlertDialogHelper;
import com.iom.dtm.presenter.EditProfilePresenter;
import com.iom.dtm.rest.form.EditProfileForm;
import com.iom.dtm.view.dialog.DeactivateProfileDialog;
import com.iom.dtm.view.dialog.ReplaceRoleSendToNewOneDialog;
import com.iom.dtm.view.dialog.ReplaceRoleVerifyDialog;
import com.iom.dtm.view.viewinterface.HomeProfileViewInterface;
import com.smartsoftasia.rxcamerapicker.Sources;
import com.smartsoftasia.ssalibrary.view.activity.BaseActivity;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseRoundedImageView;

import javax.inject.Inject;

/**
 * Created by Hinda on 7/6/2559.
 * EditProfileFragment
 * RxImagePicker:
 * <a href="https://android-arsenal.com/details/1/3466">
 */
public class EditProfileFragment extends BaseAppFragment
    implements HomeProfileViewInterface, DeactivateProfileDialog.DeactivateProfileDialogListener,
    ReplaceRoleVerifyDialog.ReplaceRoleVerifyDialogDialogListener,
    ReplaceRoleSendToNewOneDialog.ReplaceRoleSendToNewOneDialogListener {
  public static final String TAG = "EditProfileFragment";

  public static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 102;

  OnFragmentInteractionListener mListener;

  public interface OnFragmentInteractionListener {
    void logout();
  }

  @Inject
  EditProfilePresenter presenter;

  @BindView(R.id.imageview_edit_profile_fragment_profile)
  BaseRoundedImageView profileImageView;
  @BindView(R.id.edittext_edit_profile_fragment_first_name)
  BaseEditText firstNameEditText;
  @BindView(R.id.edittext_edit_profile_fragment_last_name)
  BaseEditText lastNameEditText;
  @BindView(R.id.edittext_edit_profile_fragment_organization)
  BaseEditText organizationEditText;
  @BindView(R.id.edittext_edit_profile_fragment_position_title)
  BaseEditText titlePositionEditText;

  private ReplaceRoleVerifyDialog replaceRoleVerifyDialog;

  public static EditProfileFragment newInstance() {
    EditProfileFragment fragment = new EditProfileFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_edit_profile;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(getString(R.string.action_bar_edit_profile_fragment));
  }

  @Override
  protected void initialize() {
    this.getComponent(EditProfileComponent.class).inject(this);
    this.presenter.setView(this);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onUpdateProfile(User user) {
    firstNameEditText.setText(user.getFirstName());
    lastNameEditText.setText(user.getLastName());
    organizationEditText.setText(user.getOrganization());
    titlePositionEditText.setText(user.getPosition());
    profileImageView.downloadImage(this, user.getProfilePictureUrl(), null);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                         int[] grantResults) {
    switch (requestCode) {
      case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          onChooseProfilePicture();
        } else {
          onError(getString(R.string.error_insufficient_permission_to_take_photo));
        }
        break;
      }
      default: {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        break;
      }
    }
  }

  @Override
  public void onDisplayImageChooser() {
    getBaseAppActivity().displayImageChooser(new BaseActivity.OnSelectImageChooserListener() {
      @Override
      public void onGallerySelected() {
        onChooseProfilePicture();
      }

      @Override
      public void onCameraSelected() {
        onTakeProfilePicture();
      }
    });
  }

  @Override
  public void onDeactivateClick(String email, String password) {
    presenter.onDeactivateProfileClick(email, password);
  }

  @Override
  public void onReplaceRoleClick() {
    displayReplaceRoleVerifyDialog();
  }

  @Override
  public void onReplaceRoleVerifyClick(String email, String password) {
    presenter.onReplaceRoleVerifyClick(email, password);
  }

  @Override
  public void onReplaceRoleSendToNewOneDoneClick(String email, String comment) {
    presenter.onReplaceRoleSendToNewOneClick(email, comment);
  }

  @Override
  public void onDisplayLoading() {
    hideKeyboard();
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void displayProfileHasBeenUpdatedToast() {
    displayToastMessage(R.string.edit_profile_fragment_profile_has_been_updated);
  }

  @Override
  public void logout() {
    if (mListener != null) {
      mListener.logout();
    }
  }

  @Override
  public void onReplaceRoleSendToNewOneDialog() {
    displayReplaceRoleSendToNewOneDialog();
  }

  @Override
  public void onReplaceRoleSendToNewOneFailed() {
    AlertDialogHelper.showMessageDialog(getContext(), R.string.error_replace_role_failed_title,
        R.string.error_user_sent_to_new_one_failed_body, true);
  }

  @Override
  public void onReplaceRoleUserVerifyFailed() {
    AlertDialogHelper.showMessageDialog(getContext(), R.string.error_replace_role_failed_title,
        R.string.error_user_verify_failed_body, true);
  }

  @Override
  public void onDeactivatedUserVerifyFailed() {
    AlertDialogHelper.showMessageDialog(getContext(),
        R.string.error_deactivate_profile_verify_failed_title,
        R.string.error_user_verify_failed_body, true);
  }

  @Override
  public void onDisplayDeactivateDialog(boolean isAdmin) {
    DeactivateProfileDialog dialog = DeactivateProfileDialog.newInstance(isAdmin);
    dialog.setDeactivateProfileDialogListener(this);
    dialog.show(getFragmentManager(), DeactivateProfileDialog.TAG);
  }

  @Override
  public void updateImageProfile(String url) {
    profileImageView.downloadImage(this, url, null);
  }

  @OnClick(R.id.imageview_edit_profile_fragment_profile)
  public void onProfileImageClick(View v) {
    presenter.onProfileImageClick();
  }

  @OnClick(R.id.button_edit_profile_fragment_deactivate_account)
  public void onDeactivateAccountClick(View v) {
    presenter.onDeactivateDialogClick();
  }

  @OnClick(R.id.button_edit_profile_fragment_save)
  public void onSaveClick(View v) {
    EditProfileForm form = new EditProfileForm();
    if (form.validateForm(firstNameEditText, lastNameEditText, organizationEditText,
        titlePositionEditText)) {
      presenter.onSaveClick(form);
    }
  }

  private void onChooseProfilePicture() {
    if (!hasRequiredPermission()) {
      requestPermission();
      return;
    }
    presenter.onImageChoose(Sources.GALLERY);
  }

  private void onTakeProfilePicture() {
    presenter.onImageChoose(Sources.CAMERA);
  }

  private void displayReplaceRoleSendToNewOneDialog() {
    if (replaceRoleVerifyDialog != null) {
      replaceRoleVerifyDialog.dismiss();
    }
    ReplaceRoleSendToNewOneDialog dialog = ReplaceRoleSendToNewOneDialog.newInstance();
    dialog.show(getFragmentManager(), ReplaceRoleSendToNewOneDialog.TAG);
    dialog.setReplaceRoleSendToNewOneDialogListener(this);
  }

  private void displayReplaceRoleVerifyDialog() {
    replaceRoleVerifyDialog = ReplaceRoleVerifyDialog.newInstance();
    replaceRoleVerifyDialog.show(getFragmentManager(), ReplaceRoleVerifyDialog.TAG);
    replaceRoleVerifyDialog.setReplaceRoleVerifyDialogListener(this);
  }

  private boolean hasRequiredPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
      int result = ContextCompat.checkSelfPermission(getActivity(), permission);

      return result == PackageManager.PERMISSION_GRANTED;
    } else {
      return true;
    }
  }

  private void requestPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
          PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
    }
  }
}
