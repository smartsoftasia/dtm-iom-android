package com.iom.dtm.view.fragment.viewreport;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.ViewReportHomeComponent;
import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.presenter.viewreport.ViewReportSelectProvincePresenter;
import com.iom.dtm.view.adapter.CheckboxAdapter;
import com.iom.dtm.view.model.Admin1CheckBoxViewModel;
import com.iom.dtm.view.viewinterface.ViewReportSelectProvinceViewInterface;
import com.smartsoftasia.ssalibrary.view.adapter.OnAdapterClick;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nott on 9/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewReportSelectProvinceFragment extends BaseCheckBoxFragment<Admin1CheckBoxViewModel> implements ViewReportSelectProvinceViewInterface, OnAdapterClick {
  public static final String TAG = "ViewReportSelectProvinceFragment";
  private static final String ARG_EVENT = "arg_event";

  public interface FragmentListener {
    void openSelectQuestionsFragment(List<Admin1> admin1s);
  }

  public static ViewReportSelectProvinceFragment newInstance(Event event) {
    ViewReportSelectProvinceFragment fragment = new ViewReportSelectProvinceFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_EVENT, event);
    fragment.setArguments(args);
    return fragment;
  }

  @Inject
  ViewReportSelectProvincePresenter presenter;
  @BindView(R.id.progressbar_view_report_select_province)
  ProgressBar progressBar;
  @BindView(R.id.recyclerview_view_report_select_province_fragment)
  RecyclerView recyclerView;
  private FragmentListener mListener;
  private CheckboxAdapter<Admin1CheckBoxViewModel> adapter;

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_view_report_select_province;
  }

  public void setListener(FragmentListener listener) {
    this.mListener = listener;
  }

  @Override
  protected void initialize() {
    this.getComponent(ViewReportHomeComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgEvent());
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    adapter = new CheckboxAdapter<Admin1CheckBoxViewModel>(new ArrayList<Admin1CheckBoxViewModel>(), getContext());
    adapter.setAdapterClickListener(this);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(adapter);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof FragmentListener) {
      mListener = (FragmentListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onSelectAll() {
    presenter.onSelectAllClick();
  }

  @Override
  public void onDeselectAll() {
    presenter.onDeselectAllClick();
  }

  @Override
  public void onAdapterClick(int position, Object item, RecyclerView.ViewHolder view) {
    Admin1CheckBoxViewModel model = (Admin1CheckBoxViewModel) item;
    presenter.onAddOrUpdateClick(model, model.isChecked);
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onDisplayLoading() {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void onHideLoading() {
    progressBar.setVisibility(View.GONE);
  }

  @Override
  public void appendCheckBoxList(List<Admin1CheckBoxViewModel> t) {
    adapter.appendItems(t);
  }

  @Override
  public void onSelectAllCheckBox() {
    adapter.notifyDataSetChanged();
  }

  @Override
  public void onDeselectAllCheckBox() {
    adapter.notifyDataSetChanged();
  }

  @Override
  public void openSelectQuestionsFragment(List<Admin1> admin1s) {
    if (mListener != null) {
      mListener.openSelectQuestionsFragment(admin1s);
    }
  }

  @Override
  public void displayPleaseSelectAdmin1() {
    displayToastMessage(R.string.view_report_fragment_please_select_province);
  }

  @OnClick(R.id.button_next)
  public void onNextClick(View v) {
    presenter.onNextClick();
  }

  private Event getArgEvent() {
    if (getArguments() == null) {
      return null;
    } else {
      return getArguments().getParcelable(ARG_EVENT);
    }
  }
}
