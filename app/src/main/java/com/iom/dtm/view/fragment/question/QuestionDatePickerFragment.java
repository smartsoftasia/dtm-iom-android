package com.iom.dtm.view.fragment.question;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.helper.DateAppHelper;
import com.iom.dtm.presenter.question.QuestionDatePickerPresenter;
import com.iom.dtm.view.viewinterface.QuestionDatePickerViewInterface;
import com.smartsoftasia.ssalibrary.helper.DateHelper;
import com.smartsoftasia.ssalibrary.view.dialog.DatePickerDialogFragment;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionDatePickerFragment extends BaseQuestionFragment
    implements QuestionDatePickerViewInterface, DatePickerDialogFragment.OnDateSelect {
  public static final String TAG = "QuestionStringFragment";

  @BindView(R.id.textview_question_date_picker_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.edittext_question_date_picker_fragment)
  BaseEditText editText;

  @Inject
  QuestionDatePickerPresenter presenter;

  public static QuestionDatePickerFragment newInstance(Question question, int answerTime) {
    QuestionDatePickerFragment fragment = new QuestionDatePickerFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_date_picker;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onError(Throwable error) {

  }

  @Override
  public void setUpQuestion(Question question) {
    labelTextView.setText(Html.fromHtml(question.title));
  }

  @OnClick(R.id.edittext_question_date_picker_fragment)
  public void onEditTextClick(View v) {
    DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();
    dialogFragment.show(getFragmentManager(), "date_picker");
    dialogFragment.setOnDateSelectListener(this);
  }

  @Override
  public void onDateSelect(Date date) {
    editText.setText(DateHelper.dateTimeToDDMMMMYYYY(date));
    presenter.onDateSelected(DateAppHelper.dateTimeToStringForAPI(date));
  }

  @Override
  public void setUpAnswer(String date) {
    if (TextUtils.isEmpty(date)) {
      editText.setText(date);
    } else {
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
      try {
        Date inputDate = dateFormat.parse(date);
        editText.setText(DateHelper.dateTimeToDDMMMMYYYY(inputDate));
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }
}
