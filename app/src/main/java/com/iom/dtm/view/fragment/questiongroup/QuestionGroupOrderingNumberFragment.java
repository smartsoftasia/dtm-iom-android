package com.iom.dtm.view.fragment.questiongroup;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;

import butterknife.BindView;

import com.google.common.collect.Iterables;
import com.iom.dtm.R;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.presenter.questiongroup.QuestionGroupOrderingNumberPresenter;
import com.iom.dtm.view.adapter.OrderingNumberAdapter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.model.QuestionNumberViewModel;
import com.iom.dtm.view.viewinterface.QuestionGroupOrderingNumberViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;
import com.smartsoftasia.ssalibrary.view.widget.LinearListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupOrderingNumberFragment extends BaseAppFragment
    implements QuestionGroupOrderingNumberViewInterface,
    OrderingNumberAdapter.OrderingNumberAdapterListener {
  public static final String TAG = "QuestionGroupFragment";
  public static final String ARG_QUESTION_GROUP = "arg_question_group";

  @Inject
  QuestionGroupOrderingNumberPresenter presenter;
  @BindView(R.id.textview_question_group_ordering_number_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.linearListView_question_group_ordering_number_fragment)
  LinearListView linearListView;
  @BindView(R.id.edittext_question_group_ordering_other)
  BaseEditText otherEditText;

  protected OrderingNumberAdapter adapter;

  public static QuestionGroupOrderingNumberFragment newInstance(QuestionGroup questionGroup) {
    QuestionGroupOrderingNumberFragment fragment = new QuestionGroupOrderingNumberFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION_GROUP, questionGroup);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    if (adapter == null) {
      adapter = new OrderingNumberAdapter(new ArrayList<QuestionNumberViewModel>(), getContext());
      adapter.setListener(this);
    }
    linearListView.setAdapter(adapter);
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_group_ordering_number;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgQuestionGroup(getArguments()));
    this.presenter.setAnswerTimes(1);
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @SuppressWarnings("deprecation")
  @Override
  public void setUpQuestionGroup(QuestionGroup upQuestionGroup) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      labelTextView.setText(Html.fromHtml(upQuestionGroup.title, Html.FROM_HTML_MODE_LEGACY));
    } else {
      labelTextView.setText(Html.fromHtml(upQuestionGroup.title));
    }
  }

  @Override
  public void setUpOtherEditText(boolean isVisible, String other) {
    otherEditText.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    otherEditText.setText(other);
    otherEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        presenter.onTextChanged(charSequence.toString());
      }

      @Override
      public void afterTextChanged(Editable editable) {

      }
    });
  }

  @Override
  public void appendQuestion(List<QuestionNumberViewModel> questionList) {
    adapter.setItems(questionList);
  }

  @Override
  public void onItemClick(final QuestionNumberViewModel model, final int position) {
    final List<String> menuOrders = new ArrayList<>();
    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
        android.R.layout.simple_list_item_1);

    // Handle the number
    if (position == presenter.getMaxSize() - 1 && presenter.getOrders().size() == 0) {
      // Do nothing
    } else if (position == presenter.getMaxSize() - 1
        && Iterables.getLast(presenter.getOrders()) == presenter.getMaxSize()) {
      menuOrders.add(AppConstant.HYPHEN);
      menuOrders.add(String.valueOf(presenter.getMaxSize()));
    } else {
      for (int i = 0; i < presenter.getOrders().size(); i++) {
        menuOrders.add(String.valueOf(presenter.getOrders().get(i)));
      }
    }
    menuOrders.add(getString(R.string.question_ordering_number_fragment_remove_selected));

    // Create list adapter
    arrayAdapter.addAll(menuOrders);
    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        boolean isLastPositionClick = which == Iterables.size(menuOrders) - 1;

        if (position == presenter.getMaxSize() - 1) { // For on click at last item
          if (TextUtils.isEmpty(model.getAnswerNumber())) {
            if (isLastPositionClick) {
              return;
            }
            model.setAnswerNumber(String.valueOf(arrayAdapter.getItem(which)));

            if (isInteger(
                model.getAnswerNumber())) { // Remove number item when number has been selected
              presenter.removeOrderObject(Integer.parseInt(model.getAnswerNumber()));
              displayOtherEditText();
            }
          } else { // For change existing value
            if (isLastPositionClick) { // For "Not selected" click
              if (isInteger(model.getAnswerNumber())) { // For existing value is integer
                presenter.addOrder(Integer.parseInt(model.getAnswerNumber()));
              } else if (model.getAnswerNumber()
                  .contains(AppConstant.HYPHEN)) { // For existing value is "-"
                // Do nothing
              } else {
                String[] strings = TextUtils.split(model.getAnswerNumber(),
                    AppConstant.COMMA);
                presenter.addOrder(Integer.parseInt(strings[0]));
              }
              model.setAnswerNumber(AppConstant.EMPTY);
              hideOtherEditText();
            } else { // For "-" or integer click
              if (isInteger(
                  String.valueOf(arrayAdapter.getItem(which)))) { // Integer has been selected [New]
                int newNumber = Integer.parseInt(arrayAdapter.getItem(which));
                if (isInteger(model.getAnswerNumber())) { // Existing is Integer [Old]
                  int old = Integer.parseInt(model.getAnswerNumber());
                  presenter.replaceOrderObject(newNumber, old);
                } else { // Existing is "-"
                  presenter.removeOrderObject(newNumber);
                }
                model.setAnswerNumber(String.valueOf(arrayAdapter.getItem(which)));
                displayOtherEditText();
              } else { // "-" has been selected [New]
                hideOtherEditText();
                if (isInteger(model.getAnswerNumber())) { // Existing is Integer [Old]
                  int old = Integer.parseInt(model.getAnswerNumber());
                  presenter.addOrder(old);
                  model.setAnswerNumber(String.valueOf(arrayAdapter.getItem(which)));
                } else { // Existing is "-"
                  return;
                }
              }
            }
          }
        } else { // For on click on another items
          if (TextUtils.isEmpty(model.getAnswerNumber())) {
            if (isLastPositionClick) {
              return;
            }
            model.setAnswerNumber(String.valueOf(arrayAdapter.getItem(which)));
            presenter.removeOrder(which);
          } else {
            int old = Integer.parseInt(model.getAnswerNumber());
            if (isLastPositionClick) {
              model.setAnswerNumber(AppConstant.EMPTY);
              presenter.addOrder(old);
            } else {
              model.setAnswerNumber(String.valueOf(arrayAdapter.getItem(which)));
              presenter.replaceOrder(which, old);
            }
          }
        }
        Logger.e("ItemClick", position + " " + which);
        presenter.onItemSelect(model.getAnswerRModel());
        adapter.updateItem(model, position);
      }
    });
    builderSingle.show();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  private QuestionGroup getArgQuestionGroup(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION_GROUP);
    } else {
      return null;
    }
  }

  private void displayOtherEditText() {
    otherEditText.setVisibility(View.VISIBLE);
    otherEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        presenter.onTextChanged(charSequence.toString());
      }

      @Override
      public void afterTextChanged(Editable editable) {

      }
    });
  }

  private void hideOtherEditText() {
    otherEditText.setText(AppConstant.EMPTY);
    otherEditText.setVisibility(View.GONE);
  }

  private boolean isInteger(String s) {
    try {
      Integer.parseInt(s);
    } catch (NumberFormatException e) {
      return false;
    } catch (NullPointerException e) {
      return false;
    }
    return true;
  }
}
