package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.User;

public interface SignUpViewInterface extends LoadingViewInterface {

  void onOpenHomeActivity();

  void updateUserForSocialAccount(User user);

  void registerNormalType();

  void registerSocialType();
}
