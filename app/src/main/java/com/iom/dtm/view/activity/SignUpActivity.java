package com.iom.dtm.view.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;

import android.widget.RadioGroup;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.dependency.component.DaggerSignUpComponent;
import com.iom.dtm.dependency.component.SignUpComponent;
import com.iom.dtm.domain.model.User;

import com.iom.dtm.presenter.SignUpPresenter;
import com.iom.dtm.rest.form.SignUpDetailsForm;
import com.iom.dtm.view.viewinterface.SignUpViewInterface;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;
import com.smartsoftasia.ssalibrary.helper.TextViewHelper;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;

import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SignUpActivity extends BaseAppActivity
    implements HasComponent<SignUpComponent>, SignUpViewInterface,
    RadioGroup.OnCheckedChangeListener {
  private static final String TAG = "SignUpActivity";
  private SignUpComponent mComponent;
  @Inject
  protected SignUpPresenter presenter;
  @BindView(R.id.edittext_sign_up_first_name)
  BaseEditText fistNameEditText;
  @BindView(R.id.edittext_sign_up_last_name)
  BaseEditText lastNameEditText;
  @BindView(R.id.edittext_sign_up_email)
  BaseEditText emailEditText;
  @BindView(R.id.edittext_sign_up_password)
  BaseEditText passwordEditText;
  @BindView(R.id.textview_sign_up_term_conditions)
  BaseTextView termAndConditionsTextView;
  @BindView(R.id.radiogroup_sign_up_gender)
  RadioGroup genderRadioGroup;
  @BindView(R.id.layout_input_password)
  TextInputLayout passwordLayout;

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(getString(R.string.actionbar_sign_up));
    setBackOnActionbar();

    setTermAndConditionLink();
    genderRadioGroup.check(R.id.radiobutton_sign_up_male);
    genderRadioGroup.setOnCheckedChangeListener(this);
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_sign_up;
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerSignUpComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
    this.presenter.setView(this);
  }

  @Override
  protected void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  protected void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  protected void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public SignUpComponent getComponent() {
    return mComponent;
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading(getString(R.string.global_signing_up));
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @OnClick(R.id.button_sign_up_done)
  public void onDoneClick(View v) {
    presenter.onDoneClick();
  }

  @Override
  public void registerNormalType() {
    SignUpDetailsForm form = new SignUpDetailsForm();
    form.setFirstName(fistNameEditText.getText().toString());
    form.setLastName(lastNameEditText.getText().toString());
    form.setEmail(emailEditText.getText().toString());
    form.setPassword(passwordEditText.getText().toString());

    if (form.validateForm(fistNameEditText, lastNameEditText, emailEditText, passwordEditText)) {
      presenter.sendFormForNormal(form);
    }
  }

  @Override
  public void registerSocialType() {
    SignUpDetailsForm form = new SignUpDetailsForm();
    form.setFirstName(fistNameEditText.getText().toString());
    form.setLastName(lastNameEditText.getText().toString());
    form.setEmail(emailEditText.getText().toString());
    if (form.validateSocialForm(fistNameEditText, lastNameEditText)) {
      presenter.sendFormForSocial(form);
    }
  }

  @Override
  public void onCheckedChanged(RadioGroup group, int checkedId) {
    presenter.onGenderRadioButtonSelected(genderRadioGroup.getCheckedRadioButtonId());
  }

  @Override
  public void onOpenHomeActivity() {
    startNewStackActivity(HomeActivity.class);
  }

  @Override
  public void updateUserForSocialAccount(User user) {
    fistNameEditText.setText(user.firstName);
    lastNameEditText.setText(user.lastName);
    emailEditText.setText(user.email);
    emailEditText.setEnabled(false);
    passwordLayout.setVisibility(View.GONE);
    passwordEditText.setVisibility(View.GONE);
  }

  private void setTermAndConditionLink() {
    TextViewHelper.setOnClickLinkWebView(this, termAndConditionsTextView,
        R.string.sign_up_in_term_agreement, R.string.sign_up_in_term_agreement_link,
        AppConfig.TERM_AND_CONDITION_URL, R.color.primary);
  }
}
