package com.iom.dtm.view.fragment.viewreport;

import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.model.BaseCheckBoxViewModel;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public abstract class BaseCheckBoxFragment<T extends BaseCheckBoxViewModel> extends BaseAppFragment {
  public static final String TAG = "BaseCheckBoxFragment";

  public abstract void onSelectAll();

  public abstract void onDeselectAll();
}
