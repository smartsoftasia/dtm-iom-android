package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionGroup;
import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;

import java.util.List;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface BaseQuestionGroupViewInterface extends BaseViewInterface {

  void appendQuestionString(Question question, int position);

  void appendQuestionDatePicker(Question question, int position);

  void appendQuestionLocation(Question question, int position);

  void appendQuestionSingleChoice(Question question, int position);

  void appendQuestionNumber(Question question, int position);

  void appendQuestionEmail(Question question, int position);

  void appendQuestionAttachment(Question question, int position);

  void appendQuestionText(Question question, int position);

  void appendQuestionAdmin(QuestionGroup questionGroup, int position);

  void appendQuestionDropDown(Question question, int position);

  void setUpQuestionGroup(QuestionGroup questionGroup);
}
