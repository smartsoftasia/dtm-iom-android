package com.iom.dtm.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerViewFilledDTMFormComponent;
import com.iom.dtm.dependency.component.ViewFilledDTMFormComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.view.fragment.ViewFilledDTMFormFragment;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;

/**
 * Created by Nott on 6/20/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewFilledDTMFormActivity extends BaseAppActivity
    implements HasComponent<ViewFilledDTMFormComponent> {
  private static final String TAG = "ViewFilledDTMFormActivity";
  public static final String ARG_ENUMERATOR = "arg_enumerator";
  public static final String ARG_REPORT = "arg_report";

  protected ViewFilledDTMFormComponent mComponent;

  public static Intent newInstance(Context context, Enumerator enumerator, Report report) {
    Intent intent = new Intent();
    intent.putExtra(ARG_ENUMERATOR, enumerator);
    intent.putExtra(ARG_REPORT, report);
    intent.setClass(context, ViewFilledDTMFormActivity.class);
    return intent;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    replaceFragment(R.id.container, ViewFilledDTMFormFragment.newInstance(getArgEnumerator(), getArgReport()));
    setActionbarTitle(R.string.action_bar_view_filled_dtm_form_fragment);
    setBackOnActionbar();
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerViewFilledDTMFormComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_view_filled_dtm_form;
  }

  @Override
  public ViewFilledDTMFormComponent getComponent() {
    return mComponent;
  }

  private Enumerator getArgEnumerator() {
    if (getIntent() == null) {
      return null;
    } else {
      return getIntent().getParcelableExtra(ARG_ENUMERATOR);
    }
  }

  private Report getArgReport() {
    if (getIntent() == null) {
      return null;
    } else {
      return getIntent().getParcelableExtra(ARG_REPORT);
    }
  }
}
