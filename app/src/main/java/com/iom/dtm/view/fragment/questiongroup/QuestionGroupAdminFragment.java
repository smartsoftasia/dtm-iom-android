package com.iom.dtm.view.fragment.questiongroup;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Admin2;
import com.iom.dtm.domain.model.Admin3;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.presenter.questiongroup.QuestionGroupAdminPresenter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.viewinterface.QuestionGroupAdminViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupAdminFragment extends BaseAppFragment
    implements QuestionGroupAdminViewInterface {
  public static final String TAG = "QuestionGroupAdminFragment";
  private static final String ARG_QUESTION_GROUP = "arg_question_group";
  public static final String ARG_ANSWER_TIMES = "arg_answer_time";

  public static QuestionGroupAdminFragment newInstance(QuestionGroup questionGroup,
                                                       int answerTime) {
    QuestionGroupAdminFragment fragment = new QuestionGroupAdminFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION_GROUP, questionGroup);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Inject
  QuestionGroupAdminPresenter presenter;
  @BindView(R.id.textview_question_group_admin_fragment_label_admin_1)
  BaseTextView admin1TextViewLabel;
  @BindView(R.id.textview_question_group_admin_fragment_label_admin_2)
  BaseTextView admin2TextViewLabel;
  @BindView(R.id.textview_question_group_admin_fragment_label_admin_3)
  BaseTextView admin3TextViewLabel;
  @BindView(R.id.edittext_question_group_admin_fragment_admin_1)
  BaseEditText admin1EditText;
  @BindView(R.id.edittext_question_group_admin_fragment_admin_2)
  BaseEditText admin2EditText;
  @BindView(R.id.edittext_question_group_admin_fragment_admin_3)
  BaseEditText admin3EditText;

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_group_admin;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgQuestionGroup(getArguments()),
        getArgAnswerTimes(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void setUpQuestionAdmin1(String label) {
    admin1TextViewLabel.setText(label);
  }

  @Override
  public void setUpQuestionAdmin2(String label) {
    admin2TextViewLabel.setText(label);
  }

  @Override
  public void setUpQuestionAdmin3(String label) {
    admin3TextViewLabel.setText(label);
  }

  @Override
  public void setUpAnswerAdmin1(String admin1) {
    admin1EditText.setText(admin1);
  }

  @Override
  public void setUpAnswerAdmin2(String admin2) {
    admin2EditText.setText(admin2);
  }

  @Override
  public void setUpAnswerAdmin3(String admin3) {
    admin3EditText.setText(admin3);
  }

  @OnClick(R.id.edittext_question_group_admin_fragment_admin_1)
  public void onAdmin1Click(View v) {
    presenter.onAdmin1Click();
  }

  @OnClick(R.id.edittext_question_group_admin_fragment_admin_2)
  public void onAdmin2Click(View v) {
    if (TextUtils.isEmpty(admin1EditText.getText().toString())) {
      return;
    }

    presenter.onAdmin2Click();
  }

  @OnClick(R.id.edittext_question_group_admin_fragment_admin_3)
  public void onAdmin3Click(View v) {
    if (TextUtils.isEmpty(admin2EditText.getText().toString())) {
      return;
    }

    presenter.onAdmin3Click();
  }

  @Override
  public void displayAdmin1List(List<Admin1> list) {
    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
    final ArrayAdapter<Admin1> arrayAdapter = new ArrayAdapter<Admin1>(getActivity(),
        android.R.layout.simple_list_item_1);
    arrayAdapter.addAll(list);
    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        admin1EditText.setText(arrayAdapter.getItem(i).displayAdmin());
        admin2EditText.setText(AppConstant.EMPTY);
        admin3EditText.setText(AppConstant.EMPTY);
        updateAnswer();
      }
    });
    builderSingle.show();
  }

  @Override
  public void displayAdmin2List(List<Admin2> list) {
    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
    final ArrayAdapter<Admin2> arrayAdapter = new ArrayAdapter<Admin2>(getActivity(),
        android.R.layout.simple_list_item_1);
    arrayAdapter.addAll(list);
    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        admin2EditText.setText(arrayAdapter.getItem(i).displayAdmin());
        admin3EditText.setText(AppConstant.EMPTY);
        updateAnswer();
      }
    });
    builderSingle.show();
  }

  @Override
  public void displayAdmin3List(List<Admin3> list) {
    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
    final ArrayAdapter<Admin3> arrayAdapter = new ArrayAdapter<Admin3>(getActivity(),
        android.R.layout.simple_list_item_1);
    arrayAdapter.addAll(list);
    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        admin3EditText.setText(arrayAdapter.getItem(i).displayAdmin());
        updateAnswer();
      }
    });
    builderSingle.show();
  }

  private void updateAnswer() {
    presenter.saveAdmin1(admin1EditText.getText().toString());
    presenter.saveAdmin2(admin2EditText.getText().toString());
    presenter.saveAdmin3(admin3EditText.getText().toString());
  }

  private QuestionGroup getArgQuestionGroup(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION_GROUP);
    } else {
      return null;
    }
  }

  protected int getArgAnswerTimes(Bundle bundle) {
    if (bundle != null) {
      return bundle.getInt(ARG_ANSWER_TIMES);
    } else {
      return 1;
    }
  }
}
