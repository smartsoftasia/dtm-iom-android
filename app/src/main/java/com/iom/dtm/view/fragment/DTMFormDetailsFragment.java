package com.iom.dtm.view.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.helper.AlertDialogHelper;
import com.iom.dtm.helper.TextViewHelper;
import com.iom.dtm.presenter.DTMFormDetailsPresenter;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.viewinterface.DTMFormDetailsViewInterface;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 7/4/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DTMFormDetailsFragment extends BaseAppFragment implements DTMFormDetailsViewInterface {
  public static final String TAG = "DTMFormDetailsFragment";
  public static final String ARG_POSITION = "arg_position";

  @BindView(R.id.viewpager_dtm_form_details_fragment)
  protected ViewPager viewPager;
  @BindView(R.id.tablayout_dtm_from_details_fragment)
  protected TabLayout tabLayout;
  @BindView(R.id.button_dtm_form_details_fragment_send)
  protected FloatingActionButton sendButton;
  protected CollectionPagerAdapter mCollectionPagerAdapter;
  protected int mIndex = -1;
  private int pageAdapterSize = 0;

  @Inject
  DTMFormDetailsPresenter presenter;

  public static DTMFormDetailsFragment newInstance(int position) {
    DTMFormDetailsFragment fragment = new DTMFormDetailsFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_POSITION, position);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(R.string.action_bar_dtm_form_details_fragment);
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_dtm_form_details;
  }

  @Override
  protected void initialize() {
    this.getComponent(DTMFormDetailsComponent.class).inject(this);
    this.presenter.setView(this);
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    if (viewPager != null) {
      mIndex = viewPager.getCurrentItem();
    }
    presenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_dtm_form_details, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_save: {
        presenter.onSaveClick();
        displayLongToastMessage(R.string.dtm_form_details_fragment_answer_has_been_saved);
        break;
      }
      case R.id.action_delete: {
        getBaseAppActivity().hideKeyboard();
        displayDeleteConfirmationDialog();
        break;
      }
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void setUpQuestionCategory(List<QuestionCategory> questionCategory) {
    this.pageAdapterSize = questionCategory.size();

    mCollectionPagerAdapter = new CollectionPagerAdapter(getChildFragmentManager(),
        questionCategory);
    viewPager.setAdapter(mCollectionPagerAdapter);
    viewPager.setOffscreenPageLimit(5);
    tabLayout.setupWithViewPager(viewPager);
    viewPager.setCurrentItem(0);
    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        sendButton.setVisibility(pageAdapterSize == position ? View.GONE : View.VISIBLE);
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    presenter.onMediaActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void displayAnswerRequired(String questionName) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      displayLongToastMessage(Html.fromHtml(questionName, Html.FROM_HTML_MODE_LEGACY).toString());
    } else {
      displayLongToastMessage(TextViewHelper.removeHtml(questionName));
    }
  }

  @Override
  public void openQuestionCategory(int position) {
    if (viewPager != null) {
      viewPager.setCurrentItem(position);
    }
  }

  @Override
  public void changeToSummaryPage() {
    viewPager.setCurrentItem(pageAdapterSize);
  }

  @OnClick(R.id.button_dtm_form_details_fragment_send)
  public void onSendFormClick(View v) {
    presenter.onSendFormClick();
  }

  public void onBackPressedAndHomeBackPressed() {
    presenter.clearAnswerSingleton();
  }

  public int getCurrentCategoryPosition() {
    if (viewPager != null) {
      return viewPager.getCurrentItem();
    }
    return 0;
  }

  @Override
  public void displayAnswerCorrected() {
    displayLongToastMessage(R.string.global_answer_validated);
  }

  private void displayDeleteConfirmationDialog() {
    final ConfirmationDialog deleteDialog = new ConfirmationDialog.Builder().setTitleDialog(
        getString(R.string.dtm_form_details_fragment_deleted_dialog_title))
        .setOkTextButton(getString(R.string.dtm_form_details_fragment_deleted_dialog_ok))
        .setCancelTextButton(getString(R.string.dtm_form_details_fragment_deleted_dialog_cancel))
        .createDialog();

    deleteDialog.setConfirmationDialogListener(new ConfirmationDialog.ConfirmationDialogListener() {
      @Override
      public void onOkClick() {
        presenter.onDeleteClick();
        getActivity().finish();
      }

      @Override
      public void onCancelClick() {
        if (deleteDialog != null) {
          deleteDialog.dismiss();
        }
      }
    });
    deleteDialog.show(getFragmentManager(), ConfirmationDialog.TAG);
  }

  public class CollectionPagerAdapter extends FragmentStatePagerAdapter {
    List<QuestionCategory> questionCategory;

    public CollectionPagerAdapter(FragmentManager fm, List<QuestionCategory> questionCategory) {
      super(fm);
      this.questionCategory = questionCategory;
    }

    @Override
    public Fragment getItem(int i) {
      if (questionCategory.size() == i) {
        return SummaryDTMFormFragment.newInstance();
      }
      switch (i) {
        default:
          return QuestionCategoryFragment.newInstance(questionCategory.get(i));
      }
    }

    @Override
    public int getCount() {
      return questionCategory.size() + 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      if (questionCategory.size() == position) {
        return getString(R.string.dtm_from_fragment_summary);
      }
      switch (position) {
        default:
          return questionCategory.get(position).getTitle();
      }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      // Do nothings
    }
  }

  private int getArgPosition() {
    if (getArguments() != null) {
      return getArguments().getInt(ARG_POSITION, 0);
    } else {
      return 0;
    }
  }
}
