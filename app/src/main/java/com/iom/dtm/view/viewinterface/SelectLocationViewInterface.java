package com.iom.dtm.view.viewinterface;

/**
 * Created by Nott on 8/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface SelectLocationViewInterface extends BaseAppViewInterface {

  void goBackToPreviousActivity();
}
