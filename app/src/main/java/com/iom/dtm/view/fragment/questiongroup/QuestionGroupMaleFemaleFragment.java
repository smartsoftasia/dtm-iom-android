package com.iom.dtm.view.fragment.questiongroup;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import butterknife.BindView;
import butterknife.OnTextChanged;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.presenter.questiongroup.QuestionGroupMaleFemalePresenter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.viewinterface.QuestionGroupMaleFemaleViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupMaleFemaleFragment extends BaseAppFragment
    implements QuestionGroupMaleFemaleViewInterface {
  public static final String TAG = "QuestionGroupRepeatFragment";
  public static final String ARG_QUESTION_GROUP = "arg_question_group";

  @Inject
  QuestionGroupMaleFemalePresenter presenter;
  @BindView(R.id.textview_question_group_male_female_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.edittext_question_group_male_female_fragment_male)
  BaseEditText maleEditText;
  @BindView(R.id.edittext_question_group_male_female_fragment_female)
  BaseEditText femaleEditText;
  @BindView(R.id.edittext_question_group_male_female_fragment_total)
  BaseEditText totalEditText;

  public static QuestionGroupMaleFemaleFragment newInstance(QuestionGroup questionGroup) {
    QuestionGroupMaleFemaleFragment fragment = new QuestionGroupMaleFemaleFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION_GROUP, questionGroup);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_group_male_female;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgQuestionGroup(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onError(Throwable error) {

  }

  @Override
  public void setUpQuestion(QuestionGroup questionGroup) {
    labelTextView.setText(questionGroup.title);
  }

  @Override
  public void setUpTotalNumber(long totalNumber) {
    totalEditText.setText(String.valueOf(totalNumber));
  }

  @Override
  public void setUpAnswer(String male, String female, String title) {
    maleEditText.setText(male);
    femaleEditText.setText(female);
    totalEditText.setText(title);
    setOnTextChanged();
  }

  private void setOnTextChanged() {
    maleEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        presenter.onMaleNumberHasChanged(
            TextUtils.isEmpty(s.toString()) ? 0 : Long.parseLong(s.toString().trim()));
      }
    });
    femaleEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        presenter.onFemaleNumberHasChanged(
            TextUtils.isEmpty(s.toString()) ? 0 : Long.parseLong(s.toString().trim()));
      }
    });
  }

  private QuestionGroup getArgQuestionGroup(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION_GROUP);
    } else {
      return null;
    }
  }
}
