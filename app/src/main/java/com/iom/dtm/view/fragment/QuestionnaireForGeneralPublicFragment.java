package com.iom.dtm.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.dependency.component.HomeComponent;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.presenter.QuestionnaireForGeneralPublicPresenter;
import com.iom.dtm.view.activity.HomeActivity;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.viewinterface.QuestionnaireForGeneralPublicViewInterface;
import com.smartsoftasia.ssalibrary.helper.ViewHelper;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 7/6/2559.
 * QuestionnaireForGeneralPublicFragment
 */
public class QuestionnaireForGeneralPublicFragment extends BaseAppFragment
    implements QuestionnaireForGeneralPublicViewInterface, ConfirmationDialog.ConfirmationDialogListener {
  public static final String TAG = "QuestionnaireForGeneralPublicFragment";

  public interface OnFragmentInteractionListener {
  }

  public static QuestionnaireForGeneralPublicFragment newInstance() {
    QuestionnaireForGeneralPublicFragment fragment = new QuestionnaireForGeneralPublicFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  private OnFragmentInteractionListener mListener;
  private ConfirmationDialog dialog;
  @Inject
  QuestionnaireForGeneralPublicPresenter presenter;
  @BindView(R.id.linearlayout_questionnaire_for_general_public_fragment)
  LinearLayout linearLayout;

  public int getViewResourceId() {
    return R.layout.fragment_questionnaire_for_general_public;
  }

  @Override
  protected void initialize() {
    this.getComponent(HomeComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize();
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(R.string.action_bar_dtm_form_questionnaire_gp);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_questionnaire_general_public, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_send: {
        onSubmitClick();
        break;
      }
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void displayGeneratingForm() {
    displayToastMessage(R.string.dtm_from_fragment_a_waiting_for_creating_form);
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void displayNoQuestionnaire() {
    // Do nothing
  }

  @Override
  public void displayAnswerRequired(String message) {
    displayToastMessage(message);
  }

  @Override
  public void displayEmailInvalid(String message) {
    displayToastMessage(message);
  }

  @Override
  public void setUpQuestionnaire(List<QuestionCategory> categoryList) {
    for (int i = 0; i < categoryList.size(); i++) {
      FrameLayout frameLayout = new FrameLayout(getActivity());
      frameLayout.setId(ViewHelper.generateViewId());

      getFragmentManager().beginTransaction()
          .add(frameLayout.getId(), QuestionCategoryFragment.newInstance(categoryList.get(i)), TAG)
          .commit();
      this.linearLayout.addView(frameLayout);
    }
  }

  @Override
  public void recreateFragment() {
    startActivity(HomeActivity.newInstanceForReloadGPQ(getContext()));
  }

  @Override
  public void displayDTMSubmitSuccessfulDialog() {
    if (dialog != null) {
      dialog.dismiss();
    }

    new AlertDialog.Builder(getContext()).setTitle(
        R.string.dtm_from_fragment_summary_successful_dialog_title)
        .setMessage(R.string.dtm_from_fragment_summary_successful_dialog_body)
        .setPositiveButton(R.string.global_ok, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            getBaseAppActivity().recreateActivity();
          }
        })
        .setCancelable(false)
        .show();
  }

  @Override
  public void onOkClick() {
    presenter.onSubmitClick();
    if (dialog != null) {
      dialog.dismiss();
    }
  }

  @Override
  public void onCancelClick() {
    if (dialog != null) {
      dialog.dismiss();
    }
  }

  private void onSubmitClick() {
    dialog = new ConfirmationDialog.Builder().setTitleDialog(getString(R.string.questionnaire_for_general_public_fragment_dialog_title))
        .setCancelTextButton(getString(R.string.global_cancel))
        .setOkTextButton(getString(R.string.dtm_from_fragment_summary_dialog_submit))
        .createDialog();
    dialog.setConfirmationDialogListener(this);
    dialog.show(getFragmentManager(), ConfirmationDialog.TAG);
  }
}
