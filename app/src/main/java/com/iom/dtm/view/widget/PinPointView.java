package com.iom.dtm.view.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.iom.dtm.R;

/**
 * Created by androiddev01 on 12/23/2015 AD.
 */
public class PinPointView extends LinearLayout {
  public static final String TAG = "PinPoint";

  @BindView(R.id.pin_point)
  protected ImageView pinPointImageView;

  public PinPointView(Context context) {
    super(context);
    init(context, null);
  }

  public PinPointView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs);
  }

  public PinPointView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public PinPointView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init(context, attrs);
  }

  private void init(Context context, AttributeSet attrs) {

    Drawable pinPointSrc = ContextCompat.getDrawable(getContext(), R.drawable.pin_point);

    if (attrs != null) {
      TypedArray a = context.getTheme()
          .obtainStyledAttributes(attrs, R.styleable.PinPointView, 0, 0);

      try {
        pinPointSrc = a.getDrawable(R.styleable.PinPointView_pinPointDrawable);
      } finally {
        a.recycle();
      }
    }

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(
        Context.LAYOUT_INFLATER_SERVICE);
    View rootView = inflater.inflate(R.layout.widget_pin_point, this, true);

    ButterKnife.bind(this, rootView);

    pinPointImageView.setImageDrawable(pinPointSrc);
  }
}
