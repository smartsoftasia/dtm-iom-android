package com.iom.dtm.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.iom.dtm.R;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.rest.form.SignInForm;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DeactivateProfileDialog extends BaseAppDialog {
  public static final String TAG = "DeactivateProfileDialog";
  public static final String ARG_IS_ADMIN = "arg_is_admin";

  public interface DeactivateProfileDialogListener {
    void onDeactivateClick(String email, String password);

    void onReplaceRoleClick();
  }

  @BindView(R.id.edittext_deactivate_user_dialog_email)
  BaseEditText emailEditText;
  @BindView(R.id.edittext_deactivate_user_dialog_password)
  BaseEditText passwordEditText;
  @BindView(R.id.button_deactivate_user_dialog_replace_role)
  BaseTextView replaceRoleTextView;

  private DeactivateProfileDialogListener mListener;
  private SignInForm signInForm = new SignInForm();

  public static DeactivateProfileDialog newInstance(boolean isAdmin) {
    DeactivateProfileDialog fragment = new DeactivateProfileDialog();
    Bundle args = new Bundle();
    args.putBoolean(ARG_IS_ADMIN, isAdmin);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_deactivate_profile;
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
    replaceRoleTextView.setVisibility(getArgIsAdmin() ? View.VISIBLE : View.GONE);
  }

  public void setDeactivateProfileDialogListener(DeactivateProfileDialogListener listener) {
    this.mListener = listener;
  }

  @OnClick(R.id.button_deactivate_user_dialog_deactivate)
  public void onDeactivateClick(View v) {
    signInForm.setEmail(emailEditText.getText().toString());
    signInForm.setPassword(passwordEditText.getText().toString());

    if (signInForm.isFormValid(emailEditText, passwordEditText)) {
      if (mListener != null) {
        mListener.onDeactivateClick(emailEditText.getText().toString(),
            passwordEditText.getText().toString());
      }
    }
  }

  @OnClick(R.id.button_deactivate_user_dialog_replace_role)
  public void onReplaceRoleClick(View v) {
    if (mListener != null) {
      mListener.onReplaceRoleClick();
    }
    if (getDialog() != null) {
      dismiss();
    }
  }

  @OnClick(R.id.button_deactivate_user_dialog_cancel)
  public void onCancelClick(View v) {
    if (getDialog() != null) {
      dismiss();
    }
  }

  private boolean getArgIsAdmin() {
    if (getArguments() != null) {
      return getArguments().getBoolean(ARG_IS_ADMIN);
    } else {
      return false;
    }
  }
}
