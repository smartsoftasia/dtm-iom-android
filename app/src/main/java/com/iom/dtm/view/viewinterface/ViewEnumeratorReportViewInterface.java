package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Report;

import java.util.List;

/**
 * Created by Nott on 6/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface ViewEnumeratorReportViewInterface extends LoadingViewInterface {
  void onOpenReportHasBeenSentDialog(Report report, Enumerator enumerator);

  void updateEnumeratorDetails(Enumerator enumerator);

  void appendReportList(List<Report> reports);

  void clearList();

  void displayLoadingProgressBar();

  void hideLoadingProgressBar();
}
