package com.iom.dtm.view.viewinterface.viewreport;

import com.iom.dtm.domain.model.Admin0;
import com.iom.dtm.view.viewinterface.BaseAppViewInterface;
import java.util.List;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface ViewReportSelectCountryViewInterface extends BaseAppViewInterface {

  void appendCountryList(List<Admin0> admin0List);
}
