package com.iom.dtm.view.viewinterface;

import com.iom.dtm.view.model.BaseCheckBoxViewModel;

import java.util.List;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public interface BaseCheckboxViewInterface<T extends BaseCheckBoxViewModel> extends LoadingViewInterface {

  void appendCheckBoxList(List<T> viewModels);

  void onSelectAllCheckBox();

  void onDeselectAllCheckBox();
}
