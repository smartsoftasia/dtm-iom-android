package com.iom.dtm.view.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.core.DTM;
import com.iom.dtm.dependency.component.ApplicationComponent;
import com.iom.dtm.domain.bus.AdminMessage;
import com.iom.dtm.domain.bus.RxBus;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.helper.AlertDialogHelper;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.view.activity.BaseActivity;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;

import javax.inject.Inject;

import butterknife.ButterKnife;
import rx.functions.Action1;

public abstract class BaseAppActivity extends BaseActivity {
  public static final String TAG = "BaseAppActivity";

  protected abstract void initializeInjector();

  public static final ButterKnife.Setter<View, Boolean> VISIBILITY = new ButterKnife.Setter<View, Boolean>() {
    @Override
    public void set(View view, Boolean value, int index) {
      view.setVisibility(value ? View.VISIBLE : View.GONE);
    }
  };
  private ProgressDialog mProgress;
  private AlertDialog alertDialog;

  @Inject
  SharedPreference mSharedPreference;

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    this.getApplicationComponent().inject(this);
    this.initializeInjector();
  }

  @Override
  protected void onStart() {
    super.onStart();
    this.subscribePushNotification();
  }

  /**
   * Checking error type to show the message on SnackBar
   *
   * @param e App Throwable
   */
  @Override
  public void showSnackBarError(Throwable e) {
    String message = getString(R.string.error_unknown);

    if (e != null) {
      if (AppConfig.DEBUG) {
        Toast.makeText(this, "DEBUG Mode:\n" + e.toString(), Toast.LENGTH_LONG).show();
      }

      if (e instanceof SocketTimeoutException
          || e instanceof UnknownHostException
          || e instanceof ConnectException) {
        message = getString(R.string.error_can_not_connect_server);
      }
    }
    super.showSnackBarError(message);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  public void displayImageChooser(final OnSelectImageChooserListener listener) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setItems(getResources().getStringArray(R.array.picture_select_array),
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            switch (which) {
              case 0:
                if (listener != null) {
                  listener.onGallerySelected();
                }
                break;
              case 1:
                if (listener != null) {
                  listener.onCameraSelected();
                }
                break;
            }
          }
        });
    builder.show();
  }

  public void displayProgressDialogLoading() {
    mProgress = ProgressDialog.show(this, null, getString(R.string.global_loading), true);
    hideKeyboard();
  }

  public void displayProgressDialogLoading(String progressTitle) {
    mProgress = ProgressDialog.show(this, null, progressTitle, true);
    hideKeyboard();
  }

  public void displayToastMessage(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  public void displayToastMessage(@StringRes int message) {
    Toast.makeText(this, getString(message), Toast.LENGTH_SHORT).show();
  }

  public void displayLongToastMessage(String message) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
  }

  public void displayLongToastMessage(@StringRes int message) {
    Toast.makeText(this, getString(message), Toast.LENGTH_LONG).show();
  }

  public void hideProgressDialog() {
    if (mProgress != null) mProgress.dismiss();
  }

  protected ApplicationComponent getApplicationComponent() {
    return ((DTM) getApplication()).getApplicationComponent();
  }

  protected void openGPSSettings() {
    Intent callGPSSettingIntent = new Intent(
        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    startActivity(callGPSSettingIntent);
  }

  private void subscribePushNotification() {
    RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
      @Override
      public void call(Object o) {
        if (o instanceof AdminMessage) {
          displayAdminMessage(((AdminMessage) o).getMessage());
        }
      }
    });
  }

  public void onUnauthorized() {
    mSharedPreference.writeUser(null);
    mSharedPreference.writeUserToken(null);
    startNewStackActivity(HomeActivity.class);
  }

  protected void displayAdminMessage(final String s) {
    Logger.d(TAG, "Message " + s + " Date " + new Date().toString());
    if (!(this).isFinishing()) {

      if (alertDialog != null) {
        alertDialog.dismiss();
        alertDialog = null;
      }

      alertDialog = new AlertDialog.Builder(this)
          .setTitle(getString(R.string.push_notification_dialog_title))
          .setMessage(s)
          .setNegativeButton(R.string.global_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
          }).setCancelable(true)
          .create();
      alertDialog.show();
    }
  }
}
