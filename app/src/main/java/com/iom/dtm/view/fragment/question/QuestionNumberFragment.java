package com.iom.dtm.view.fragment.question;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.text.Html;

import butterknife.BindView;
import butterknife.OnTextChanged;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.question.QuestionNumberPresenter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.viewinterface.QuestionNumberViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionNumberFragment extends BaseQuestionFragment
    implements QuestionNumberViewInterface {
  public static final String TAG = "QuestionStringFragment";

  @Inject
  QuestionNumberPresenter presenter;

  @BindView(R.id.textview_question_number_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.edittext_question_number_fragment)
  BaseEditText editText;

  public static QuestionNumberFragment newInstance(Question question, int answerTime) {
    QuestionNumberFragment fragment = new QuestionNumberFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_number;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void setUpQuestion(Question question) {
    labelTextView.setText(Html.fromHtml(question.title));
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void setUpAnswer(String number) {
    editText.setText(number);
  }

  @OnTextChanged(R.id.edittext_question_number_fragment)
  public void onNumberEditTextChanged(CharSequence s) {
    presenter.onNumberChanged(s.toString());
  }
}
