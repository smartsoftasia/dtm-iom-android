package com.iom.dtm.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.LinearLayout;
import butterknife.BindView;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.presenter.QuestionCategoryPresenter;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupMaleFemaleFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupNoQuestionFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupOrderingNumberFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupRepeatFragment;
import com.iom.dtm.view.viewinterface.QuestionCategoryViewInterface;
import com.smartsoftasia.ssalibrary.helper.ViewHelper;
import javax.inject.Inject;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionCategoryFragment extends BaseAppFragment
    implements QuestionCategoryViewInterface {
  public static final String TAG = "QuestionCategoryFragment";
  public static final String ARG_QUESTION_CATEGORY = "arg_question_category";

  @Inject
  QuestionCategoryPresenter presenter;
  @BindView(R.id.linearlayout_question_category_fragment)
  LinearLayout linearLayout;

  public static QuestionCategoryFragment newInstance(QuestionCategory questionCategory) {
    QuestionCategoryFragment fragment = new QuestionCategoryFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION_CATEGORY, questionCategory);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_category;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgQuestionCategory(getArguments()));
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void appendQuestionGroupMaleFemale(QuestionGroup questionGroup, int position) {
    addQuestionGroupFragmentToQuestionCategoryFragment(
        QuestionGroupMaleFemaleFragment.newInstance(questionGroup), position);
  }

  @Override
  public void appendQuestionGroupNormal(QuestionGroup questionGroup, int position) {
    addQuestionGroupFragmentToQuestionCategoryFragment(
        QuestionGroupFragment.newInstance(questionGroup), position);
  }

  @Override
  public void appendQuestionGroupRepeat(QuestionGroup questionGroup, int position) {
    addQuestionGroupFragmentToQuestionCategoryFragment(
        QuestionGroupRepeatFragment.newInstance(questionGroup), position);
  }

  @Override
  public void appendQuestionGroupNoQuestion(QuestionGroup questionGroup, int position) {
    addQuestionGroupFragmentToQuestionCategoryFragment(
        QuestionGroupNoQuestionFragment.newInstance(questionGroup), position);
  }

  @Override
  public void appendQuestionGroupOrderingNumber(QuestionGroup questionGroup, int position) {
    addQuestionGroupFragmentToQuestionCategoryFragment(
        QuestionGroupOrderingNumberFragment.newInstance(questionGroup), position);
  }

  private void addQuestionGroupFragmentToQuestionCategoryFragment(Fragment fragment, int position) {
    LinearLayout linearLayoutQuestionGroup = new LinearLayout(getActivity());
    linearLayoutQuestionGroup.setOrientation(LinearLayout.VERTICAL);
    linearLayoutQuestionGroup.setId(ViewHelper.generateViewId());

    getFragmentManager().beginTransaction()
        .add(linearLayoutQuestionGroup.getId(), fragment, TAG)
        .commit();
    this.linearLayout.addView(linearLayoutQuestionGroup);
  }

  @Override
  public void displayNoQuestion() {

  }

  private QuestionCategory getArgQuestionCategory(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION_CATEGORY);
    } else {
      return null;
    }
  }
}
