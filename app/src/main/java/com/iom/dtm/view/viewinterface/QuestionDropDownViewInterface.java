package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Question;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionDropDownViewInterface extends BaseAppViewInterface {

  void setUpQuestion(Question question);

  void setUpAnswer(String answer);

  void isHasAdditionalRequiredEditText(boolean isHad);
}
