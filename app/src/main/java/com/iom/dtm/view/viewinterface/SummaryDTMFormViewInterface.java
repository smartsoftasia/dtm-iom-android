package com.iom.dtm.view.viewinterface;

import java.util.List;

/**
 * Created by Nott on 8/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface SummaryDTMFormViewInterface extends LoadingViewInterface {

  void appendListQuestionCategory(List<String> stringList);

  void displayDTMSubmitSuccessfulDialog();

  void displayAnswerRequired(String questionName);

  void openQuestionCategory(int position);
}
