package com.iom.dtm.view.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerResetPasswordComponent;
import com.iom.dtm.dependency.component.ResetPasswordComponent;
import com.iom.dtm.presenter.ResetPasswordPresenter;
import com.iom.dtm.rest.form.ResetPasswordForm;
import com.iom.dtm.view.viewinterface.ResetPasswordViewInterface;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;
import com.smartsoftasia.ssalibrary.view.widget.BaseButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPasswordActivity extends BaseAppActivity
    implements ResetPasswordViewInterface, HasComponent<ResetPasswordComponent> {
  public static final String TAG = "ResetPasswordActivity";

  @Inject
  protected ResetPasswordPresenter presenter;

  private ResetPasswordComponent mComponent;

  @BindViews({
      R.id.tv_forgot_password, R.id.reset_password_view_01, R.id.button_reset_password_activity_send
  })
  protected List<View> emailsViews;
  @BindViews({
      R.id.imageview_reset_password, R.id.tv_forgot_password_already,
      R.id.button_reset_password_activity_return_to_login
  })
  protected List<View> alreadyEmailsViews;
  @BindView(R.id.edittext_reset_password_activity_email)
  BaseEditText emailEditText;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initializeInjector();
    setActionbarTitle(R.string.action_bar_reset_password);
    setBackOnActionbar();
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerResetPasswordComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
    this.presenter.setView(this);
    this.presenter.initialize();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_reset_password;
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onInvalidEmail() {
    emailEditText.setError(getString(R.string.error_email_not_valid));
    emailEditText.requestFocus();
  }

  @Override
  public void onEmptyEmail() {
    emailEditText.setError(getString(R.string.error_field_required));
    emailEditText.requestFocus();
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void onEmailAlreadySent() {
    ButterKnife.apply(emailsViews, VISIBILITY, false);
    ButterKnife.apply(alreadyEmailsViews, VISIBILITY, true);
  }

  @Override
  public void onDisplayResetPassword() {
    ButterKnife.apply(emailsViews, VISIBILITY, true);
    ButterKnife.apply(alreadyEmailsViews, VISIBILITY, false);
  }

  @Override
  public ResetPasswordComponent getComponent() {
    return mComponent;
  }

  @OnClick(R.id.button_reset_password_activity_send)
  public void onResetPasswordClick(View v) {
    ResetPasswordForm resetPasswordForm = new ResetPasswordForm(emailEditText.getText().toString());
    presenter.resetClick(resetPasswordForm);
  }

  @OnClick(R.id.button_reset_password_activity_return_to_login)
  public void onReturnToLoginClick(View v) {
    finish();
  }
}









