package com.iom.dtm.view.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerLaunchAppComponent;
import com.iom.dtm.dependency.component.LaunchAppComponent;
import com.iom.dtm.domain.controller.SharedPreference;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;

import javax.inject.Inject;

/**
 * Created by Nott on 25/11/2559.
 * dtm-iom-android
 */

public class LaunchAppActivity extends BaseAppActivity implements HasComponent<LaunchAppComponent> {
  public static final String TAG = "LaunchAppActivity";

  LaunchAppComponent mComponent;
  @Inject
  SharedPreference sharedPreference;

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerLaunchAppComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_launch_app;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    if (sharedPreference.readUser() == null) {
      startCompactActivity(SignInActivity.class);
    } else {
      startCompactActivity(HomeActivity.class);
    }
    finish();
  }

  @Override
  public LaunchAppComponent getComponent() {
    return mComponent;
  }
}
