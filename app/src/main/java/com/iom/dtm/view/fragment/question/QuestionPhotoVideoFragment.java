package com.iom.dtm.view.fragment.question;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.Tag;
import com.iom.dtm.helper.TextViewHelper;
import com.iom.dtm.presenter.question.QuestionPhotoVideoPresenter;
import com.iom.dtm.view.dialog.AttachmentPreviewDialog;
import com.iom.dtm.view.viewinterface.QuestionPhotoVideoViewInterface;
import com.smartsoftasia.rxcamerapicker.Sources;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.view.widget.BaseCheckBox;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionPhotoVideoFragment extends BaseQuestionFragment
    implements QuestionPhotoVideoViewInterface, CompoundButton.OnCheckedChangeListener {
  public static final String TAG = "QuestionPhotoVideoFragment";
  private static final int RECORDING_VIDEO_REQUEST_CODE = 2000;
  private static final int SELECT_VIDEO_REQUEST_CODE = 2001;
  private static final int REQUEST_IMAGE_CAPTURE = 2003;

  public interface OnFragmentInteractionListener {
    void recreateActivityAfterAttachmentHasBeenUploaded();
  }

  @BindView(R.id.textview_question_photo_video_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.imageview_question_photo_video_fragment)
  BaseImageView imageView;
  @BindView(R.id.view_odd)
  LinearLayout oddLinearLayout;
  @BindView(R.id.view_even)
  LinearLayout evenLinearLayout;
  @BindView(R.id.edittext_question_photo_video_fragment_other)
  BaseEditText otherEditText;
  @BindView(R.id.edittext_question_photo_video_fragment_add_remark_here)
  BaseEditText addRemarkHereEditText;
  @BindView(R.id.textview_question_photo_video_fragment_click_here_for_preview)
  BaseTextView clickForPreviewTextView;

  protected Uri mUri;
  private List<Tag> mTags;
  private String tagAdditionalRequiredId;
  private OnFragmentInteractionListener mListener;
  private String mPath;

  @Inject
  QuestionPhotoVideoPresenter presenter;

  protected BaseCheckBox[] checkBoxes;

  public static QuestionPhotoVideoFragment newInstance(Question question, int answerTime) {
    QuestionPhotoVideoFragment fragment = new QuestionPhotoVideoFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_photo_video;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    clickForPreviewTextView.setPaintFlags(
        clickForPreviewTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void setUpQuestion(Question question) {
    labelTextView.setText(question.title);
  }

  @Override
  public void setUpAnswer(List<String> tagsId, String other, String remark, String imageURL) {

    Logger.e("SetUpAnswer", String.valueOf(imageURL));
    otherEditText.setText(other);
    addRemarkHereEditText.setText(remark);
    if (!TextUtils.isEmpty(imageURL)) {
      this.mPath = imageURL;
      clickForPreviewTextView.setVisibility(View.VISIBLE);
    } else {
      clickForPreviewTextView.setVisibility(View.GONE);
    }

    if (tagsId == null) {
      return;
    }

    for (int i = 0; i < tagsId.size(); i++) {
      for (int j = 0; j < mTags.size(); j++) {
        if (tagsId.get(i).equalsIgnoreCase(mTags.get(j).id)) {
          checkBoxes[j].setChecked(true);
        }
      }
    }
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void activityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == RECORDING_VIDEO_REQUEST_CODE) {
      if (resultCode == Activity.RESULT_OK) {
        this.mUri = data.getData();
        //uploadVideo();
      }
    } else if (requestCode == SELECT_VIDEO_REQUEST_CODE) {
      if (resultCode == Activity.RESULT_OK) {
        this.mUri = data.getData();
        //uploadVideo();
      }
    } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
      if (resultCode == Activity.RESULT_OK) {
        this.mUri = data.getData();
        //uploadImage();
      }
    } else {
      if (resultCode == Activity.RESULT_OK) {
        this.mUri = data.getData();
        //uploadImage();
      }
    }
  }

  @Override
  public void updateClickForPreviewLick(String path) {
    this.mPath = path;
    if (mListener != null) {
      mListener.recreateActivityAfterAttachmentHasBeenUploaded();
    }
  }

  @Override
  public void displayFileHasBeenUploaded() {
    displayToastMessage(R.string.global_file_has_been_uploaded);
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading(R.string.uploading_file);
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @OnClick(R.id.imageview_question_photo_video_fragment)
  public void onMediaClick(View v) {
    presenter.onPhotoVideoSelected();
    openDialog();
  }

  @OnClick(R.id.textview_question_photo_video_fragment_click_here_for_preview)
  public void onClickHereForPreview(View v) {
    if (!TextUtils.isEmpty(mPath)) {
      displayAttachmentPreview(mPath);
    }
  }

  @Override
  public void setUpTAGs(List<Tag> tagList) {
    this.mTags = tagList;
    checkBoxes = new BaseCheckBox[tagList.size()];
    for (int i = 0; i < tagList.size(); i++) {
      checkBoxes[i] = new BaseCheckBox(getContext());
      checkBoxes[i].setText(tagList.get(i).title);
      checkBoxes[i].setTag(mTags.get(i).id);
      if (i % 2 == 0) {
        evenLinearLayout.addView(checkBoxes[i]);
      } else {
        oddLinearLayout.addView(checkBoxes[i]);
      }
      if (tagList.get(i).additionalRequired) {
        this.tagAdditionalRequiredId = tagList.get(i).id;
      }
    }
    setUpCheckBox();
    setUpEditText();
  }

  @Override
  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    presenter.onTAGsCheckBoxSelected(String.valueOf(buttonView.getTag()), isChecked);

    for (int i = 0; i < mTags.size(); i++) {
      if (buttonView.getTag() == mTags.get(i).id) {
        if (mTags.get(i).additionalRequired) {
          otherEditText.setEnabled(checkBoxes[i].isChecked());
          if (!checkBoxes[i].isChecked()) {
            otherEditText.setText("");
          }
        }
      }
    }
  }

  private void setUpCheckBox() {
    for (int i = 0; i < mTags.size(); i++) {
      checkBoxes[i].setOnCheckedChangeListener(this);
    }
  }

  private void setUpEditText() {
    otherEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        presenter.onTAGsAdditionalRequired(tagAdditionalRequiredId, s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    });
    addRemarkHereEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        presenter.onRemarkEditTextHasChanged(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    });
  }

  private void openDialog() {
    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
    builderSingle.setTitle(R.string.question_photo_video_fragment_title);

    builderSingle.setItems(getResources().getStringArray(R.array.picture_video_select_array),
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            switch (which) {
              case 0:
                presenter.onImageChosen(Sources.CAMERA);
                break;
              case 1:
                presenter.onImageChosen(Sources.GALLERY);
                break;
              case 2:
                presenter.onVideoChosen(Sources.VIDEO);
                break;
              case 3:
                presenter.onVideoChosen(Sources.VIDEO_GALLERY);
                break;
            }
          }
        });

    builderSingle.create().show();
  }

  private void displayAttachmentPreview(String url) {
    AttachmentPreviewDialog dialog = AttachmentPreviewDialog.newInstance(url);
    dialog.show(getChildFragmentManager(), AttachmentPreviewDialog.TAG);
  }
}
