package com.iom.dtm.view.fragment.question;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.question.QuestionLocationPresenter;
import com.iom.dtm.view.activity.SelectLocationActivity;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.viewinterface.QuestionLocationViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionLocationFragment extends BaseQuestionFragment
    implements QuestionLocationViewInterface {
  public static final String TAG = "QuestionStringFragment";

  @BindView(R.id.edittext_question_location_fragment_latitude)
  BaseEditText latitudeEditText;
  @BindView(R.id.edittext_question_location_fragment_longitude)
  BaseEditText longitudeEditText;
  @BindView(R.id.button_group_location_fragment_select_location)
  BaseButton selectLocationButton;
  @BindView(R.id.textview_question_location_fragment_label)
  BaseTextView labelTextView;

  @Inject
  QuestionLocationPresenter presenter;

  public interface OnFragmentInteractionListener {
    void onSetCurrentPage();
  }

  public static QuestionLocationFragment newInstance(Question question, int answerTime) {
    QuestionLocationFragment fragment = new QuestionLocationFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_location;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void setUpQuestion(Question question) {
    labelTextView.setText(Html.fromHtml(question.title));
  }

  @Override
  public void setUpAnswer(String answer) {
    String lat, lng;
    if (TextUtils.isEmpty(answer)) {
      return;
    }

    lat = answer.substring(0, answer.indexOf(","));
    lng = answer.substring(answer.indexOf(",") + 1, answer.length());

    latitudeEditText.setText(lat);
    longitudeEditText.setText(lng);
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onError(Throwable error) {

  }

  @OnClick(R.id.button_group_location_fragment_select_location)
  public void onSelectLocationClick(View v) {
    presenter.onSelectLocationClick();
  }

  @Override
  public void onStartSelectLocationActivity(String questionId, int answerTime) {
    startActivity(SelectLocationActivity.newInstance(getContext(), questionId, answerTime));
  }
}
