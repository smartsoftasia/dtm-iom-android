package com.iom.dtm.view.fragment;

import android.os.Bundle;
import android.webkit.WebView;

import com.iom.dtm.R;
import com.smartsoftasia.ssalibrary.helper.WebViewHelper;

import butterknife.BindView;

/**
 * Created by androiddev03 on 7/6/2559.
 * AboutFragment
 */
public class AboutFragment extends BaseAppFragment {
  public static final String TAG = "AboutFragment";

  @BindView(R.id.webview_fragment_about)
  WebView webView;

  public static AboutFragment newInstance() {
    AboutFragment fragment = new AboutFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  public int getViewResourceId() {
    return R.layout.fragment_about;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(R.string.action_bar_about);

    webView.loadUrl("http://www.globaldtm.info/");
    webView.setWebViewClient(new WebViewHelper.AppWebViewClient(getActivity()));
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }
}




