package com.iom.dtm.view.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.iom.dtm.R;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.view.model.QuestionNumberViewModel;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;
import java.util.ArrayList;

/**
 * Created by androiddev03 on 7/6/2559.
 * NavigationAdapter
 */
public class OrderingNumberAdapter extends BaseAppLinearAdapter<QuestionNumberViewModel> {
  public static final String TAG = "OrderingNumberAdapter";

  public interface OrderingNumberAdapterListener {
    void onItemClick(QuestionNumberViewModel model, int position);
  }

  private OrderingNumberAdapterListener listener;

  public OrderingNumberAdapter(ArrayList<QuestionNumberViewModel> items, Context context) {
    super(items, context);
  }

  @SuppressWarnings("deprecation")
  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    NavigationMenuViewHolder holder;
    String numberDisplay;

    if (convertView != null) {
      holder = (NavigationMenuViewHolder) convertView.getTag();
    } else {
      convertView = getInflater().inflate(R.layout.adapter_ordering_number, null);
      holder = new NavigationMenuViewHolder(convertView);
      convertView.setTag(holder);
    }
    holder.inputEditText.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (listener != null) {
          listener.onItemClick(items.get(position), position);
        }
      }
    });

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      holder.titleTextView.setText(
          Html.fromHtml(items.get(position).getQuestionTitle(), Html.FROM_HTML_MODE_LEGACY));
    } else {
      holder.titleTextView.setText(Html.fromHtml(items.get(position).getQuestionTitle()));
    }

    if (items.get(position).getAnswerNumber().contains(AppConstant.COMMA)) {
      String[] strings = TextUtils.split(items.get(position).getAnswerNumber(),
          AppConstant.COMMA);
      numberDisplay = strings[0];
    } else {
      numberDisplay = items.get(position).getAnswerNumber();
    }
    holder.inputEditText.setText(numberDisplay);
    return convertView;
  }

  public void setListener(OrderingNumberAdapterListener listener) {
    this.listener = listener;
  }

  private class NavigationMenuViewHolder {
    BaseTextView titleTextView;

    BaseEditText inputEditText;

    private NavigationMenuViewHolder(View view) {
      titleTextView = (BaseTextView) view.findViewById(R.id.textview_ordering_number_adapter_label);
      inputEditText = (BaseEditText) view.findViewById(R.id.edittext_ordering_number_adapter);
    }
  }
}
