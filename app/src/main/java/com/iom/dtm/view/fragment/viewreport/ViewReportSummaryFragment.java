package com.iom.dtm.view.fragment.viewreport;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.ViewReportHomeComponent;
import com.iom.dtm.domain.model.ViewReport;
import com.iom.dtm.helper.AlertDialogHelper;
import com.iom.dtm.helper.DateAppHelper;
import com.iom.dtm.presenter.viewreport.ViewReportSummaryPresenter;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.dialog.InputDialog;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.viewinterface.ViewReportSummaryViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;

import javax.inject.Inject;

/**
 * Created by Nott on 9/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewReportSummaryFragment extends BaseAppFragment
    implements ConfirmationDialog.ConfirmationDialogListener, InputDialog.InputDialogListener,
    ViewReportSummaryViewInterface {
  public static final String TAG = "ViewReportSummaryFragment";
  private static final String ARG_VIEW_REPORT = "arg_view_report";

  public interface FragmentListener {
    void openViewReportSelectTimeFragmentFromSummary();
  }

  public static ViewReportSummaryFragment newInstance(ViewReport viewReport) {
    ViewReportSummaryFragment fragment = new ViewReportSummaryFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_VIEW_REPORT, viewReport);
    fragment.setArguments(args);
    return fragment;
  }

  private FragmentListener mListener;

  @Inject
  ViewReportSummaryPresenter presenter;
  @BindView(R.id.edittext_view_report_summary_fragment_event)
  BaseEditText eventEditText;
  @BindView(R.id.edittext_view_report_summary_fragment_province)
  BaseEditText provinceEditText;
  @BindView(R.id.edittext_view_report_summary_fragment_questions)
  BaseEditText questionsEditText;
  @BindView(R.id.edittext_view_report_summary_fragment_start_date)
  BaseEditText startDateEditText;
  @BindView(R.id.edittext_view_report_summary_fragment_end_date)
  BaseEditText endDateEditText;

  protected ConfirmationDialog dialog;

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_view_report_summary;
  }

  @Override
  protected void initialize() {
    this.getComponent(ViewReportHomeComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgViewReport());
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof FragmentListener) {
      mListener = (FragmentListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onCancelClick() {
    if (mListener != null) {
      mListener.openViewReportSelectTimeFragmentFromSummary();
    }
    if (dialog != null) {
      dialog.dismiss();
    }
  }

  @Override
  public void onOkClick() {
    getBaseAppActivity().finish();
    if (dialog != null) {
      dialog.dismiss();
    }
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void updateViewReport(ViewReport viewReport) {
    if (viewReport == null) {
      return;
    }
    eventEditText.setText(viewReport.event.title);
    provinceEditText.setText(viewReport.admin1s.size() == 1 ? getString(R.string.view_report_summary_fragment_province_selected, String.valueOf(viewReport.admin1s.size())) : getString(R.string.view_report_summary_fragment_provinces_selected, String.valueOf(viewReport.admin1s.size())));
    questionsEditText.setText(viewReport.questions.size() == 1 ? getString(R.string.view_report_summary_fragment_question_selected, String.valueOf(viewReport.questions.size())) : getString(R.string.view_report_summary_fragment_questions_selected, String.valueOf(viewReport.questions.size())));
    startDateEditText.setText(DateAppHelper.dateTimeToStringForViewReport(viewReport.startDate));
    endDateEditText.setText(DateAppHelper.dateTimeToStringForViewReport(viewReport.endDate));
  }

  @Override
  public void stringInput(String input) {
    Logger.e(TAG, input);
  }

  @Override
  public void displayReportHasBeenSent() {
    AlertDialogHelper.showMessageDialog(getContext(),
        R.string.view_report_summary_fragment_view_report,
        R.string.view_report_summary_fragment_report_has_been_sent, true);
  }

  @OnClick(R.id.button_view_report_summary_fragment_get_report)
  public void onGetReportClick(View view) {
    presenter.getReportClick();
  }


  public void onBackPressed() {
    displayCloseDialog();
  }

  private void displayCloseDialog() {
    dialog = new ConfirmationDialog.Builder().setTitleDialog(
        getString(R.string.view_report_summary_fragment_dialog_title))
        .setOkTextButton(getString(R.string.view_report_summary_fragment_dialog_leave_page))
        .setCancelTextButton(getString(R.string.view_report_summary_fragment_dialog_select_time))
        .createDialog();
    dialog.setConfirmationDialogListener(this);
    dialog.show(getFragmentManager(), ConfirmationDialog.TAG);
  }

  private ViewReport getArgViewReport() {
    if (getArguments() == null) {
      return null;
    } else {
      return getArguments().getParcelable(ARG_VIEW_REPORT);
    }
  }
}
