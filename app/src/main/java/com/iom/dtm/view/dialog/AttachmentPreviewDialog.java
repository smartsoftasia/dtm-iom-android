package com.iom.dtm.view.dialog;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import butterknife.BindView;
import butterknife.OnClick;
import com.iom.dtm.R;

/**
 * Created by Nott on 9/9/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AttachmentPreviewDialog extends BaseAppDialog {
  public static final String TAG = "AttachmentPreviewDialog";
  public static final String ARG_URL = "arg_url";

  @BindView(R.id.webview_attachment_preview_dialog)
  WebView webView;

  public static AttachmentPreviewDialog newInstance(String url) {
    AttachmentPreviewDialog fragment = new AttachmentPreviewDialog();
    Bundle args = new Bundle();
    args.putString(ARG_URL, url);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
    setPercentageDialogHeight(85);
    webView.getSettings().setUseWideViewPort(true);
    webView.getSettings().setLoadWithOverviewMode(true);
    webView.loadUrl(getArgURL());
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_attachment_preview;
  }

  private String getArgURL() {
    if (getArguments() != null) {
      return getArguments().getString(ARG_URL);
    } else {
      return null;
    }
  }
}
