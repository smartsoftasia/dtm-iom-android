package com.iom.dtm.view.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.iom.dtm.R;
import com.iom.dtm.view.fragment.NavigationDrawerFragment;
import com.iom.dtm.view.viewinterface.NavigationDrawerInterface;

import butterknife.BindView;

/**
 * Created by Nott on 19/04/2016 AD.
 * BaseNavigationDrawerActivity
 */
public abstract class NavigationDrawerActivity extends BaseAppActivity
    implements NavigationDrawerFragment.NavigationDrawerListener, NavigationDrawerInterface {

  public static final String TAG = "NavigationDrawerActivity";
  @BindView(R.id.drawer_layout)
  protected DrawerLayout drawerLayout;
  protected ActionBarDrawerToggle drawerToggle;
  protected NavigationDrawerFragment navigationDrawerFragment;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    initializeDrawerView();
  }

  /**
   * Initialize navigation drawer
   */
  protected void initializeDrawerView() {
    navigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(
        R.id.navigation_drawer);
    navigationDrawerFragment.setUp(R.id.navigation_drawer, drawerLayout);

    drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, getToolBar(), R.string.drawer_open,
        R.string.drawer_close) {
      @Override
      public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);
      }

      @Override
      public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
      }
    };

    drawerLayout.addDrawerListener(drawerToggle);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (drawerToggle != null)
      if (drawerToggle.onOptionsItemSelected(item)) {
        return true;
      }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    if (drawerToggle != null)
      drawerToggle.syncState();
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    drawerToggle.onConfigurationChanged(newConfig);
  }

  /**
   * Close navigation drawer.
   */
  public void closeDrawers() {
    drawerLayout.closeDrawers();
  }

  /**
   * Close navigation drawer.
   */
  @Override
  public void hideDrawer() {
    closeDrawers();
  }
}
