package com.iom.dtm.view.viewinterface;

import android.content.Intent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.Tag;
import java.util.List;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionPhotoVideoViewInterface extends LoadingViewInterface {

  void setUpQuestion(Question question);

  void setUpAnswer(List<String> tagsId, String other, String remark, String imageURL);

  void activityResult(int requestCode, int resultCode, Intent data);

  void updateClickForPreviewLick(String path);

  void setUpTAGs(List<Tag> tagList);

  void displayFileHasBeenUploaded();
}
