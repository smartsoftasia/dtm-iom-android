package com.iom.dtm.view.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.iom.dtm.R;
import com.iom.dtm.view.model.NavigationDrawerModel;
import com.smartsoftasia.ssalibrary.view.widget.BaseImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by androiddev03 on 7/6/2559.
 * NavigationAdapter
 */
public class NavigationAdapter extends BaseAppLinearAdapter<NavigationDrawerModel> {
  public static final String TAG = "NavigationAdapter";

  @Inject
  public NavigationAdapter(ArrayList<NavigationDrawerModel> items, Context context) {
    super(items, context);
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    NavigationMenuViewHolder holder;

    if (convertView != null) {
      holder = (NavigationMenuViewHolder) convertView.getTag();
    } else {
      convertView = getInflater().inflate(R.layout.adapter_navigation_drawer, null);

      holder = new NavigationMenuViewHolder(convertView);
      // tag = holder
      convertView.setTag(holder);
      convertView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (itemClickListener != null) {
            itemClickListener.onItemClick(position);
          }
        }
      });
    }
    NavigationDrawerModel item = items.get(position);
    holder.titleTextView.setText(item.title);
    return convertView;
  }

  private class NavigationMenuViewHolder {
    BaseTextView titleTextView;

    private NavigationMenuViewHolder(View view) {
      titleTextView = (BaseTextView) view.findViewById(R.id.textView_item);
    }
  }
}
