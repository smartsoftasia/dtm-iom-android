package com.iom.dtm.view.fragment;

import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.EnumeratorProfileComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.presenter.EnumeratorProfilePresenter;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.viewinterface.EnumeratorProfileViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseRoundedImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EnumeratorProfileFragment extends BaseAppFragment
    implements EnumeratorProfileViewInterface {
  public static final String TAG = "EnumeratorProfileFragment";
  private static final String ARG_ENUMERATOR = "arg_enumerator";

  public static EnumeratorProfileFragment newInstance(Enumerator enumerator) {
    EnumeratorProfileFragment fragment = new EnumeratorProfileFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_ENUMERATOR, enumerator);
    fragment.setArguments(args);
    return fragment;
  }

  @Inject
  EnumeratorProfilePresenter presenter;
  @BindView(R.id.textview_enumerator_profile_fragment_name)
  BaseTextView nameTextView;
  @BindView(R.id.textview_enumerator_profile_fragment_email)
  BaseTextView emailTextView;
  @BindView(R.id.textview_enumerator_profile_fragment_phone)
  BaseTextView phoneTextView;
  @BindView(R.id.edittext_enumerator_profile_fragment_position)
  BaseEditText positionEditText;
  @BindView(R.id.edittext_enumerator_profile_fragment_organization)
  BaseEditText organizationEditText;
  @BindView(R.id.edittext_enumerator_profile_fragment_event)
  BaseEditText eventEditText;
  @BindView(R.id.imageview_enumerator_profile_fragment_profile)
  BaseRoundedImageView profileImageView;

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_enumerator_profile;
  }

  @Override
  protected void initialize() {
    this.getComponent(EnumeratorProfileComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgEnumerator());
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void updateEnumeratorProfile(Enumerator enumerator) {
    if (enumerator == null) {
      return;
    }
    setActionbarTitle(enumerator.getFullName());
    nameTextView.setText(enumerator.getFullName());
    emailTextView.setText(enumerator.getEmail());
    profileImageView.downloadImage(this, enumerator.getProfilePictureUrl(), null);
    positionEditText.setText(enumerator.getPosition());
    organizationEditText.setText(enumerator.getOrganization());
    eventEditText.setText(enumerator.getEventFullString());
  }

  @Override
  public void goBackToEnumeratorsFragmentAfterDeactivated() {
    getActivity().finish();
  }

  @OnClick(R.id.button_enumerator_profile_fragment_deactivate_profile)
  public void onDeactivateProfileClick(View v) {
    displayEnumeratorDeleteDialog();
  }

  private Enumerator getArgEnumerator() {
    if (getArguments() == null) {
      return null;
    } else {
      return getArguments().getParcelable(ARG_ENUMERATOR);
    }
  }

  private void displayEnumeratorDeleteDialog() {
    final ConfirmationDialog dialog = new ConfirmationDialog.Builder().setTitleDialog(
        getString(R.string.enumerators_fragment_deactivate_dialog_title))
        .setOkTextButton(getString(R.string.enumerators_fragment_deactivate_dialog_deactivate))
        .setCancelTextButton(getString(R.string.enumerators_fragment_deactivate_dialog_cancel))
        .createDialog();
    dialog.setConfirmationDialogListener(new ConfirmationDialog.ConfirmationDialogListener() {
      @Override
      public void onOkClick() {
        presenter.onDeactivateClick();
        dialog.dismiss();
      }

      @Override
      public void onCancelClick() {
        dialog.dismiss();
      }
    });
    dialog.show(getFragmentManager(), ConfirmationDialog.TAG);
  }
}
