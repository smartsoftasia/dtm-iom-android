package com.iom.dtm.view.dialog;

import android.os.Bundle;
import android.view.View;
import butterknife.BindView;
import butterknife.OnClick;
import com.iom.dtm.R;
import com.smartsoftasia.ssalibrary.helper.Validator;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;

/**
 * Created by Nott on 6/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ReplaceRoleSendToNewOneDialog extends BaseAppDialog {
  public static final String TAG = "ReplaceRoleSendToNewOneDialog";

  public interface ReplaceRoleSendToNewOneDialogListener {
    void onReplaceRoleSendToNewOneDoneClick(String email, String comment);
  }

  private ReplaceRoleSendToNewOneDialogListener mListener;
  @BindView(R.id.edittext_replace_role_send_to_new_one_dialog_email)
  BaseEditText emailEditText;
  @BindView(R.id.edittext_replace_role_send_to_new_one_dialog_message)
  BaseEditText messageEditText;

  public static ReplaceRoleSendToNewOneDialog newInstance() {
    ReplaceRoleSendToNewOneDialog fragment = new ReplaceRoleSendToNewOneDialog();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_replace_role_send_to_new_one;
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
  }

  public void setReplaceRoleSendToNewOneDialogListener(
      ReplaceRoleSendToNewOneDialogListener listener) {
    this.mListener = listener;
  }

  @OnClick(R.id.button_replace_role_send_to_new_one_dialog_done)
  public void onDoneClick(View v) {

    if (!Validator.isValidEmail(emailEditText.getText().toString())) {
      emailEditText.setError(getString(R.string.error_email_not_valid));
      return;
    }

    if (mListener != null) {
      mListener.onReplaceRoleSendToNewOneDoneClick(emailEditText.getText().toString(),
          messageEditText.getText().toString());
    }
  }

  @OnClick(R.id.button_replace_role_send_to_new_one_dialog_cancel)
  public void onCancelClick(View v) {
    dismiss();
  }
}
