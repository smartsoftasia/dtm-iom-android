package com.iom.dtm.view.fragment.question;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import butterknife.BindView;
import butterknife.OnTextChanged;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.question.QuestionSingleChoicePresenter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.viewinterface.QuestionSingleChoiceViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseRadioButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionSingleChoiceFragment extends BaseQuestionFragment
    implements QuestionSingleChoiceViewInterface, RadioGroup.OnCheckedChangeListener {
  public static final String TAG = "QuestionStringFragment";

  @Inject
  QuestionSingleChoicePresenter presenter;
  @BindView(R.id.radiogroup_question_single_choice_fragment)
  RadioGroup radioGroup;
  @BindView(R.id.textview_question_single_choice_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.edittext_question_single_choice_fragment_other)
  BaseEditText otherEditText;

  protected BaseRadioButton[] radioButtons;

  public static QuestionSingleChoiceFragment newInstance(Question question, int answerTime) {
    QuestionSingleChoiceFragment fragment = new QuestionSingleChoiceFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_single_choice;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    radioGroup.setOnCheckedChangeListener(this);
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void setUpQuestion(Question question) {
    labelTextView.setText(Html.fromHtml(question.title));

    List<Choice> choices = question.choices;

    if (choices == null || choices.size() == 0) {
      return;
    }

    if (radioGroup.getChildCount() == 0) {
      radioButtons = new BaseRadioButton[choices.size()];
      for (int i = 0; i < choices.size(); i++) {
        radioButtons[i] = new BaseRadioButton(getContext());
        radioGroup.addView(radioButtons[i]);
        radioButtons[i].setText(choices.get(i).title);
      }
    }
  }

  @Override
  public void setUpAnswer(int choice, String additional, boolean isAdditionalRequired) {
    if (choice == -1) {
      return;
    }
    radioButtons[choice].setChecked(true);
    otherEditText.setVisibility(isAdditionalRequired ? View.VISIBLE : View.GONE);
    otherEditText.setText(additional);
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onError(Throwable error) {

  }

  @Override
  public void onCheckedChanged(RadioGroup group, int checkedId) {
    presenter.onSingleChoiceSelected(
        radioGroup.indexOfChild(getActivity().findViewById(checkedId)));
    presenter.checkAdditionalRequired();
  }

  @Override
  public void additionalAnswer(boolean isAdditional) {
    otherEditText.setVisibility(isAdditional ? View.VISIBLE : View.GONE);
    if (isAdditional) {
      otherEditText.setText("");
    }
  }

  @OnTextChanged(R.id.edittext_question_single_choice_fragment_other)
  public void onOtherTextChanged(CharSequence s) {
    presenter.additionalAnswer(s.toString());
  }
}
