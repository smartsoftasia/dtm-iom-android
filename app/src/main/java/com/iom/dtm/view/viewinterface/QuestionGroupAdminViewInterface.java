package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Admin2;
import com.iom.dtm.domain.model.Admin3;
import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;
import java.util.List;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionGroupAdminViewInterface extends BaseViewInterface {
  void setUpQuestionAdmin1(String label);

  void setUpQuestionAdmin2(String label);

  void setUpQuestionAdmin3(String label);

  void displayAdmin1List(List<Admin1> list);

  void displayAdmin2List(List<Admin2> list);

  void displayAdmin3List(List<Admin3> list);

  void setUpAnswerAdmin1(String admin1);

  void setUpAnswerAdmin2(String admin2);

  void setUpAnswerAdmin3(String admin3);
}
