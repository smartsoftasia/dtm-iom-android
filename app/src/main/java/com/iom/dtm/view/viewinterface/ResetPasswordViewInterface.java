package com.iom.dtm.view.viewinterface;

public interface ResetPasswordViewInterface extends LoadingViewInterface {

  void onInvalidEmail();

  void onEmptyEmail();

  void onEmailAlreadySent();

  void onDisplayResetPassword();

}
