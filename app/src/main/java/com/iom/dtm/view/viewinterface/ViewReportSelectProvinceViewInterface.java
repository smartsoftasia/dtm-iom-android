package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.view.model.Admin1CheckBoxViewModel;

import java.util.List;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public interface ViewReportSelectProvinceViewInterface extends BaseCheckboxViewInterface<Admin1CheckBoxViewModel> {
    void openSelectQuestionsFragment(List<Admin1> admin1s);

    void displayPleaseSelectAdmin1();
}
