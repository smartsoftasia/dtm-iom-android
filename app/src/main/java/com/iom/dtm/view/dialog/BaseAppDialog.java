package com.iom.dtm.view.dialog;

import android.view.ViewGroup;
import com.smartsoftasia.ssalibrary.helper.DeviceHelper;
import com.smartsoftasia.ssalibrary.view.dialog.BaseDialogFragment;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BaseAppDialog extends BaseDialogFragment {
  private static final String TAG = "BaseAppDialog";

  protected void setPercentageDialogWidth(float percent) {
    if (getDialog() == null) {
      return;
    }
    int dialogWidth = Math.round((DeviceHelper.getDisplayWidth() * percent) / 100);
    int dialogHeight = ViewGroup.LayoutParams.WRAP_CONTENT;

    getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
  }

  protected void setPercentageDialogHeight(float percent) {
    if (getDialog() == null) {
      return;
    }
    int dialogWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
    int dialogHeight = Math.round((DeviceHelper.getDisplayWidth() * percent) / 100);

    getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
  }

  protected void setPercentageDialogWidthHeight(float percentWidth, float percentHeight) {
    if (getDialog() == null) {
      return;
    }
    int dialogWidth = Math.round((DeviceHelper.getDisplayWidth() * percentWidth) / 100);
    int dialogHeight = Math.round((DeviceHelper.getDisplayWidth() * percentHeight) / 100);

    getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
  }
}
