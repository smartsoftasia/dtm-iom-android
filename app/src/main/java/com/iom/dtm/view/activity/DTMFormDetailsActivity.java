package com.iom.dtm.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.dependency.component.DaggerDTMFormDetailsComponent;
import com.iom.dtm.view.fragment.DTMFormDetailsFragment;
import com.iom.dtm.view.fragment.SummaryDTMFormFragment;
import com.iom.dtm.view.fragment.question.QuestionPhotoVideoFragment;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;

/**
 * Created by Nott on 7/4/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DTMFormDetailsActivity extends BaseAppActivity
    implements HasComponent<DTMFormDetailsComponent>,
    SummaryDTMFormFragment.OnFragmentInteractionListener,
    QuestionPhotoVideoFragment.OnFragmentInteractionListener {
  public static final String TAG = "DTMFormDetailsActivity";
  public static final String ARG_POSITION = "arg_position";

  protected DTMFormDetailsComponent mComponent;

  public static Intent newInstance(Context context, int position) {
    Intent intent = new Intent();
    intent.putExtra(ARG_POSITION, position);
    intent.setClass(context, DTMFormDetailsActivity.class);
    return intent;
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerDTMFormDetailsComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setBackOnActionbar();
    replaceFragment(R.id.container, DTMFormDetailsFragment.newInstance(getArgPosition()));
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        clearAnswerOnSingleton();
        finish();
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    clearAnswerOnSingleton();
    super.onBackPressed();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_dtm_form_details;
  }

  @Override
  public DTMFormDetailsComponent getComponent() {
    return mComponent;
  }

  @Override
  public void onOpenQuestionCategory(int position) {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    if (fragment instanceof DTMFormDetailsFragment) {
      ((DTMFormDetailsFragment) fragment).openQuestionCategory(position);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    fragment.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void recreateActivityAfterAttachmentHasBeenUploaded() {
    openAttachmentFragmentAfterFileHasBennUploaded();
  }

  private int getArgPosition() {
    return getIntent().getIntExtra(ARG_POSITION, 0);
  }

  private void clearAnswerOnSingleton() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    if (fragment instanceof DTMFormDetailsFragment) {
      ((DTMFormDetailsFragment) fragment).onBackPressedAndHomeBackPressed();
    }
  }

  private void openAttachmentFragmentAfterFileHasBennUploaded() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
    if (fragment instanceof DTMFormDetailsFragment) {
      getIntent().putExtra(ARG_POSITION,
          ((DTMFormDetailsFragment) fragment).getCurrentCategoryPosition());
    } else {
      getIntent().putExtra(ARG_POSITION, 0);
    }
    recreateActivity();
  }
}
