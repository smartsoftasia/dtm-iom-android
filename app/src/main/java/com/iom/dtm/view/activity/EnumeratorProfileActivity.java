package com.iom.dtm.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DaggerEnumeratorProfileComponent;
import com.iom.dtm.dependency.component.EnumeratorProfileComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.view.fragment.EnumeratorProfileFragment;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EnumeratorProfileActivity extends BaseAppActivity
    implements HasComponent<EnumeratorProfileComponent> {
  public static final String TAG = "EnumeratorProfileActivity";
  private static final String ARG_ENUMERATOR = "arg_enumerator";

  public static Intent newInstance(Context context, Enumerator enumerator) {
    Intent intent = new Intent();
    intent.putExtra(ARG_ENUMERATOR, enumerator);
    intent.setClass(context, EnumeratorProfileActivity.class);
    return intent;
  }

  private EnumeratorProfileComponent mComponent;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    replaceFragment(R.id.container, EnumeratorProfileFragment.newInstance(getArgEnumerator()));
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setBackOnActionbar();
  }

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerEnumeratorProfileComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_enumerator_profile;
  }

  @Override
  public EnumeratorProfileComponent getComponent() {
    return mComponent;
  }

  private Enumerator getArgEnumerator() {
    if (getIntent() == null) {
      return null;
    } else {
      return getIntent().getParcelableExtra(ARG_ENUMERATOR);
    }
  }
}
