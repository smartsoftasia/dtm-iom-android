package com.iom.dtm.view.fragment;

import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.dependency.component.ViewFilledDTMFormComponent;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.presenter.ViewFilledDTMFormPresenter;
import com.iom.dtm.view.dialog.ReportHasBeenSentDialog;
import com.iom.dtm.view.viewinterface.ViewFilledDTMFormViewInterface;
import com.smartsoftasia.ssalibrary.helper.Logger;

import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nott on 6/20/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ViewFilledDTMFormFragment extends BaseAppFragment
    implements ViewFilledDTMFormViewInterface {
  private static final String TAG = "ViewFilledDTMFormFragment";
  public static final String ARG_ENUMERATOR = "arg_enumerator";
  public static final String ARG_REPORT = "arg_report";
  private static final String BASE_URL = AppConfig.BASE_URL + "reports/";

  @BindView(R.id.webview_view_filled_dtm_from_fragment)
  WebView webview;

  @Inject
  ViewFilledDTMFormPresenter presenter;

  public static ViewFilledDTMFormFragment newInstance(Enumerator enumerator, Report report) {
    ViewFilledDTMFormFragment fragment = new ViewFilledDTMFormFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_ENUMERATOR, enumerator);
    args.putParcelable(ARG_REPORT, report);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_view_filled_dtm_form;
  }

  @Override
  protected void initialize() {
    this.getComponent(ViewFilledDTMFormComponent.class).inject(this);
    presenter.setView(this);
    presenter.initialize(getArgReport());
  }

  @Override
  public void settingUpWebView(Map<String, String> extraHeaders) {
    Report report = getArgReport();
    if (report == null) {
      return;
    }

    Logger.d(TAG, "ReportURL " + BASE_URL + report.id + " AuthKey " + extraHeaders.toString());

    webview.setWebViewClient(new DTMPassSSLWebViewClient());
    webview.loadUrl(BASE_URL + report.id, extraHeaders);
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void displayReportHasBeenSentDialog() {
    ReportHasBeenSentDialog dialog = ReportHasBeenSentDialog.newInstance(getArgEnumerator(), getArgReport());
    dialog.show(getFragmentManager(), ReportHasBeenSentDialog.TAG);
  }

  @Override
  public void onDisplayLoading() {
    displayProgressDialogLoading();
  }

  @Override
  public void onHideLoading() {
    hideProgressDialog();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @OnClick(R.id.button_view_filled_dtm_from_fragment_download)
  public void onDownLoadClick(View v) {
    presenter.onDownLoadClick();
  }

  private Enumerator getArgEnumerator() {
    if (getArguments() == null) {
      return null;
    } else {
      return getArguments().getParcelable(ARG_ENUMERATOR);
    }
  }

  private Report getArgReport() {
    if (getArguments() == null) {
      return null;
    } else {
      return getArguments().getParcelable(ARG_REPORT);
    }
  }

  private class DTMPassSSLWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
      handler.proceed();
    }
  }
}
