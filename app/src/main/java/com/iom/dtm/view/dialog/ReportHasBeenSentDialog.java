package com.iom.dtm.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.OnClick;

import com.iom.dtm.R;
import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.domain.model.Report;
import com.smartsoftasia.ssalibrary.helper.DateHelper;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;

/**
 * Created by Nott on 6/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ReportHasBeenSentDialog extends BaseAppDialog {
  public static final String TAG = "ReportHasBeenSentDialog";
  public static final String ARG_ENUMERATOR = "arg_enumerator";
  public static final String ARG_REPORT = "arg_report";

  public static ReportHasBeenSentDialog newInstance(Enumerator enumerator, Report report) {
    ReportHasBeenSentDialog fragment = new ReportHasBeenSentDialog();
    Bundle args = new Bundle();
    args.putParcelable(ARG_ENUMERATOR, enumerator);
    args.putParcelable(ARG_REPORT, report);
    fragment.setArguments(args);
    return fragment;
  }

  @BindView(R.id.edittext_report_has_been_sent_dialog_report)
  BaseEditText reportEditText;
  @BindView(R.id.edittext_report_has_been_sent_dialog_date)
  BaseEditText dateEditText;
  @BindView(R.id.edittext_report_has_been_sent_dialog_enumerator)
  BaseEditText enumeratorEditText;
  private Enumerator mEnumerator;
  private Report mReport;

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_report_has_been_sent;
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mEnumerator = getArgEnumerator();
    mReport = getArgReport();
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    if (mReport != null && mEnumerator != null) {
      reportEditText.setText(mReport.getTitle());
      dateEditText.setText(DateHelper.dateTimeToDDMMYYYY(mReport.getCreatedAt()));
      enumeratorEditText.setText(mEnumerator.getFullName());
    }
  }

  @OnClick(R.id.textview_report_has_been_sent_dialog_ok)
  public void onOkClick(View v) {
    if (getDialog() != null) {
      dismiss();
    }
  }

  private Enumerator getArgEnumerator() {
    if (getArguments() != null) {
      return getArguments().getParcelable(ARG_ENUMERATOR);
    } else {
      return null;
    }
  }

  private Report getArgReport() {
    if (getArguments() != null) {
      return getArguments().getParcelable(ARG_REPORT);
    } else {
      return null;
    }
  }
}
