package com.iom.dtm.view.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import butterknife.BindView;
import butterknife.OnClick;
import com.iom.dtm.R;
import com.iom.dtm.rest.form.SignInForm;
import com.smartsoftasia.ssalibrary.helper.Validator;
import com.smartsoftasia.ssalibrary.view.widget.BaseButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class InputDialog extends BaseAppDialog {
  public static final String TAG = "InputDialog";
  private static final String ARG_TITLE = "arg_title";
  private static final String ARG_OK = "arg_ok";
  private static final String ARG_TYPE = "arg_type";

  private String mTitle, mOk;
  private int inputType;

  public interface InputDialogListener {
    void stringInput(String input);
  }

  @BindView(R.id.button_input_dialog)
  BaseButton okButton;
  @BindView(R.id.edittext_input_dialog)
  BaseEditText editText;
  @BindView(R.id.input_dialog_title)
  BaseTextView titleTextView;

  private InputDialogListener mListener;

  public static InputDialog newInstance(String title, String buttonOk, int inputType) {
    InputDialog fragment = new InputDialog();
    Bundle args = new Bundle();
    args.putString(ARG_TITLE, title);
    args.putString(ARG_OK, buttonOk);
    args.putInt(ARG_TYPE, inputType);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_input;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mTitle = getArguments().getString(ARG_TITLE);
      mOk = getArguments().getString(ARG_OK);
      inputType = getArguments().getInt(ARG_TYPE);
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    titleTextView.setText(mTitle);
    okButton.setText(mOk);
    editText.setInputType(inputType);
  }

  @OnClick(R.id.button_input_dialog)
  public void onOkClick(View v) {
    String temp = editText.getText().toString();
    switch (editText.getInputType()) {
      case InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS: {
        if (TextUtils.isEmpty(temp)) {
          editText.setError(getString(R.string.error_field_required));
          editText.requestFocus();
        } else if (!Validator.isValidEmail(temp)) {
          editText.setError(getString(R.string.error_invalid_email));
          editText.requestFocus();
        } else {
          if (mListener != null) {
            mListener.stringInput(temp);
          }
          dismiss();
        }
      }
      default: {
        if (TextUtils.isEmpty(temp)) {
          editText.setError(getString(R.string.error_field_required));
          editText.requestFocus();
        } else {
          if (mListener != null) {
            mListener.stringInput(temp);
          }
        }
      }
    }
  }

  public void setInputDialogListener(InputDialogListener listener) {
    this.mListener = listener;
  }

  public static class Builder {
    private String nestedTitleDialog;
    private String nestedOkButton;
    private int inputType;

    public Builder() {
    }

    public Builder setInputType(int inputType) {
      this.inputType = inputType;
      return this;
    }

    public Builder setOkTextButton(String nestedOkButton) {
      this.nestedOkButton = nestedOkButton;
      return this;
    }

    public Builder setTitleDialog(String nestedTitleDialog) {
      this.nestedTitleDialog = nestedTitleDialog;
      return this;
    }

    public InputDialog createDialog() {
      return newInstance(nestedTitleDialog, nestedOkButton, inputType);
    }
  }
}
