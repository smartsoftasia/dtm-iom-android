package com.iom.dtm.view.model;

import com.iom.dtm.domain.model.Question;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public class QuestionCheckBoxViewModel extends BaseCheckBoxViewModel<Question> {

    public QuestionCheckBoxViewModel(Question question, String displayCheckBoxTitle) {
        super(question, displayCheckBoxTitle);
    }
}
