package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.Event;

import java.util.List;

/**
 * Created by Nott on 7/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface ViewReportViewInterface extends LoadingViewInterface {
  void displayEventListDialog(List<Event> eventList);

  void displayPleaseSelectEvent();

  void openViewReportHomeActivity(Event event);
}
