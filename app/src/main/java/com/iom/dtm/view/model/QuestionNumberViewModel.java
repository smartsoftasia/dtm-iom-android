package com.iom.dtm.view.model;

import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.domain.model.Question;

/**
 * Created by Nott on 9/9/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionNumberViewModel {
  public static final String TAG = "QuestionNumberViewModel";

  public Question question;
  public AnswerRModel answerRModel;

  public AnswerRModel getAnswerRModel() {
    return answerRModel;
  }

  public void setAnswerRModel(AnswerRModel answerRModel) {
    this.answerRModel = answerRModel;
  }

  public Question getQuestion() {
    return question;
  }

  public void setQuestion(Question question) {
    this.question = question;
  }

  public String getAnswerNumber() {
    getAnswerInstance();
    return answerRModel.getValue();
  }

  public String getQuestionTitle() {
    return question.title;
  }

  public void setAnswerNumber(String number) {
    getAnswerInstance();
    this.answerRModel.setValue(number);
  }

  private void getAnswerInstance() {
    if (answerRModel == null) {
      answerRModel = new AnswerRModel();
      answerRModel.setQuestionId(question.id);
      answerRModel.setAnswersTimes(1);
    }
  }
}
