package com.iom.dtm.view.model;

import com.iom.dtm.domain.model.Admin1;

/**
 * Created by Jeremy on 10/7/2016 AD.
 */

public class Admin1CheckBoxViewModel extends BaseCheckBoxViewModel<Admin1> {

    public Admin1CheckBoxViewModel(Admin1 admin1, String displayCheckBoxTitle) {
        super(admin1, displayCheckBoxTitle);
    }
}
