package com.iom.dtm.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;

import butterknife.OnClick;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.iom.dtm.R;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.dependency.component.DaggerSelectLocationComponent;
import com.iom.dtm.dependency.component.SelectLocationComponent;
import com.iom.dtm.presenter.SelectLocationPresenter;
import com.iom.dtm.view.dialog.ConfirmationDialog;
import com.iom.dtm.view.viewinterface.SelectLocationViewInterface;
import com.iom.dtm.view.widget.MapStateListener;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;
import com.smartsoftasia.ssalibrary.view.fragment.TouchableMapFragment;

import javax.inject.Inject;

/**
 * Created by Nott on 8/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SelectLocationActivity extends BaseAppActivity
    implements HasComponent<SelectLocationComponent>, OnMapReadyCallback,
    SelectLocationViewInterface, GoogleApiClient.OnConnectionFailedListener, LocationListener,
    GoogleApiClient.ConnectionCallbacks {
  public static final String TAG = "SelectLocationActivity";
  public static final int PERMISSIONS_REQUEST_LOCATION_FINE = 112;
  public static final String ARG_QUESTION_ID = "arg_question_id";
  public static final String ARG_ANSWER_TIMES = "arg_answer_times";

  @Inject
  SelectLocationPresenter presenter;

  public static Intent newInstance(Context context, String questionId, int answerTime) {
    Intent intent = new Intent();
    intent.putExtra(ARG_QUESTION_ID, questionId);
    intent.putExtra(ARG_ANSWER_TIMES, answerTime);
    intent.setClass(context, SelectLocationActivity.class);
    return intent;
  }

  protected SelectLocationComponent mComponent;
  protected TouchableMapFragment mapFragment;
  private LocationManager mLocationManager;
  private GoogleMap mMap;
  private LatLng mLatLng;
  private LocationRequest mLocationRequest;
  private GoogleApiClient mGoogleApiClient;

  @Override
  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerSelectLocationComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
    this.presenter.setView(this);
    this.presenter.initialize(getArgQuestionID(), getArgAnswerTimes());
  }

  @Override
  public int getViewResourceId() {
    return R.layout.activity_select_location;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);

    mapFragment = new TouchableMapFragment();
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.add(R.id.map, mapFragment).commit();
    mapFragment.getMapAsync(this);

    initializeLocationRequest();
    buildGoogleApiClient();
    initializeLocationManager();
  }

  @Override
  public SelectLocationComponent getComponent() {
    return mComponent;
  }

  @Override
  protected void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (!mGoogleApiClient.isConnected()) {
      mGoogleApiClient.connect();
    }
    presenter.resume();
  }

  @Override
  protected void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    stopLocationUpdates();
    presenter.destroy();
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    setMap(googleMap);
    new MapStateListener(googleMap, mapFragment, this) {

      @Override
      public void onMapTouched() {

      }

      @Override
      public void onMapReleased() {
        onMapTouchUp();
      }

      @Override
      public void onMapUnsettled() {

      }

      @Override
      public void onMapSettled() {

      }
    };
  }

  @UiThread
  protected void onMapTouchUp() {
    if (getMap() == null || getMap().getCameraPosition() == null || getMap().getCameraPosition().target == null) {
      return;
    }
    presenter.onLatLngChanged(getMap().getCameraPosition().target);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case PERMISSIONS_REQUEST_LOCATION_FINE: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
              == PackageManager.PERMISSION_GRANTED
              && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
              == PackageManager.PERMISSION_GRANTED)
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                this);
        } else {
          showToastInfo(getString(R.string.error_no_location_permission));
        }
        break;
      }
    }
  }

  @OnClick(R.id.fab)
  public void onFabClick() {
    if (!isLocationPermissionHasBeenGranted()) {
      ActivityCompat.requestPermissions(this, new String[]{
          Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
      }, PERMISSIONS_REQUEST_LOCATION_FINE);
      return;
    }

    if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
      if (mLatLng == null) {
        return;
      }
      updateCamera(mLatLng, 15);
    } else {
      openGPSSettingDialog();
    }
  }

  @OnClick(R.id.button_map_confirm_location)
  public void onConfirmClick(View v) {
    final ConfirmationDialog dialog = new ConfirmationDialog.Builder().setOkTextButton(
        getString(R.string.select_location_activity_confirm_dialog_ok))
        .setCancelTextButton(getString(R.string.select_location_activity_confirm_dialog_cancel))
        .setTitleDialog(getString(R.string.select_location_activity_confirm_dialog_title))
        .createDialog();
    dialog.setConfirmationDialogListener(new ConfirmationDialog.ConfirmationDialogListener() {
      @Override
      public void onOkClick() {
        presenter.onConfirmClick();
      }

      @Override
      public void onCancelClick() {
        dialog.dismiss();
      }
    });

    dialog.show(getSupportFragmentManager(), ConfirmationDialog.TAG);
  }

  @Override
  public void goBackToPreviousActivity() {
    finish();
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onError(Throwable error) {

  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
          != PackageManager.PERMISSION_GRANTED
          && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
          != PackageManager.PERMISSION_GRANTED) {

        ActivityCompat.requestPermissions(this, new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
        }, PERMISSIONS_REQUEST_LOCATION_FINE);
      } else {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
            this);
      }
    } else {
      LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
          this);
    }
  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onLocationChanged(Location location) {
    if (location == null) {
      return;
    }

    mLatLng = new LatLng(location.getLatitude(), location.getLongitude());
    presenter.onLatLngChanged(mLatLng);
    updateCamera(mLatLng, 15);
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    showToastInfo(connectionResult.getErrorMessage());
  }

  private void openGPSSettingDialog() {
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    alertDialogBuilder.setMessage(R.string.select_location_activity_gps_enable_message)
        .setNegativeButton(R.string.select_location_activity_cancel,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
              }
            })
        .setPositiveButton(R.string.select_location_activity_open_gps_setting,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                openGPSSettings();
              }
            });
    alertDialogBuilder.setNegativeButton(R.string.select_location_activity_cancel,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
          }
        });
    AlertDialog alert = alertDialogBuilder.create();
    alert.show();
  }

  private GoogleMap getMap() {
    return mMap;
  }

  private void setMap(GoogleMap map) {
    mMap = map;
  }

  private boolean isLocationPermissionHasBeenGranted() {
    return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        == PackageManager.PERMISSION_GRANTED;
  }

  private void initializeLocationManager() {
    mLocationManager = (LocationManager) getSystemService(Activity.LOCATION_SERVICE);
  }

  private void updateCamera(LatLng latLng, int zoomLevel) {
    if (getMap() != null && latLng != null) {
      CameraUpdate center = CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel);
      getMap().animateCamera(center);
      stopLocationUpdates();
    }
  }

  private void initializeLocationRequest() {
    mLocationRequest = new LocationRequest().setInterval(AppConfig.LOCATION_REQUEST_PER_TIME)
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
  }

  private void buildGoogleApiClient() {
    mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();
  }

  private void stopLocationUpdates() {
    if (mGoogleApiClient.isConnected()) {
      LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
      mGoogleApiClient.disconnect();
    }
  }

  private String getArgQuestionID() {
    if (getIntent() == null) {
      return null;
    }
    return getIntent().getStringExtra(ARG_QUESTION_ID);
  }

  private int getArgAnswerTimes() {
    if (getIntent() == null) {
      return 1;
    }
    return getIntent().getIntExtra(ARG_ANSWER_TIMES, 1);
  }
}
