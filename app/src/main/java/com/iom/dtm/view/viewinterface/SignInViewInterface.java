package com.iom.dtm.view.viewinterface;

public interface SignInViewInterface extends LoadingViewInterface {

  void onEmailEmpty();

  void onPasswordEmpty();

  void onEmailInvalid();

  void onPasswordInvalid();

  void openHomeActivity();

  void openSignUpActivity();
}
