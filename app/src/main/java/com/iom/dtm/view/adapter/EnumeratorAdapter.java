package com.iom.dtm.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.iom.dtm.R;
import com.iom.dtm.domain.model.Enumerator;
import com.smartsoftasia.ssalibrary.view.widget.BaseRoundedImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EnumeratorAdapter
    extends BaseAppRecyclerView<EnumeratorAdapter.ViewHolder, Enumerator> {
  public static final String TAG = "EnumeratorAdapter";

  private AdapterListener mListener;

  public interface AdapterListener {
    void onEnumeratorDeleteClick(Enumerator enumerator);
  }

  public EnumeratorAdapter(List<Enumerator> items, Context context) {
    super(items, context);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.adapter_enumerator, parent, false);

    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, final int position) {

    holder.fullNameTextView.setText(items.get(position).getFullName());
    holder.totalReportTextView.setText(
        context.getString(R.string.enumerator_adapter_total_report, items.get(position).getReportsCount()));
    holder.todayTextView.setText(context.getString(R.string.enumerator_adapter_today,
        items.get(position).getTodayReportsCount()));

    holder.deleteRelativeLayout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mListener != null) {
          mListener.onEnumeratorDeleteClick(items.get(position));
        }
      }
    });

    holder.itemRelativeLayout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (adapterClickListener != null) {
          adapterClickListener.onAdapterClick(position, items.get(position), holder);
        }
      }
    });

    holder.profileImageView.downloadImage(context, items.get(position).getProfilePictureUrl(),
        null);
  }

  public void setListener(AdapterListener listener) {
    this.mListener = listener;
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.relativelayout_enumerator_adapter_item)
    RelativeLayout itemRelativeLayout;
    @BindView(R.id.relativelayout_enumerator_adapter_delete)
    RelativeLayout deleteRelativeLayout;
    @BindView(R.id.imageview_enumerator_adapter_profile)
    BaseRoundedImageView profileImageView;
    @BindView(R.id.textview_enumerator_adapter_total_report)
    BaseTextView totalReportTextView;
    @BindView(R.id.textview_enumerator_adapter_today)
    BaseTextView todayTextView;
    @BindView(R.id.textview_enumerator_adapter_name)
    BaseTextView fullNameTextView;

    public ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}
