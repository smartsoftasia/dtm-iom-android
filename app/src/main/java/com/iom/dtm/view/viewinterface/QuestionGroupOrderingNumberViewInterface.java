package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.view.model.QuestionNumberViewModel;
import com.smartsoftasia.ssalibrary.view.viewinterface.BaseViewInterface;
import java.util.List;

/**
 * Created by Nott on 8/5/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionGroupOrderingNumberViewInterface extends BaseViewInterface {

  void appendQuestion(List<QuestionNumberViewModel> questionList);

  void setUpQuestionGroup(QuestionGroup upQuestionGroup);

  void setUpOtherEditText(boolean isVisible, String other);
}
