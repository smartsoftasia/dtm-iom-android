package com.iom.dtm.view.dialog;

import android.os.Bundle;
import android.view.View;
import butterknife.BindView;
import butterknife.OnClick;
import com.iom.dtm.R;
import com.iom.dtm.domain.model.Enumerator;
import com.smartsoftasia.ssalibrary.view.widget.BaseButton;
import com.smartsoftasia.ssalibrary.view.widget.BaseRoundedImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EnumeratorDialog extends BaseAppDialog {
  public static final String TAG = "DeactivateProfileDialog";
  public static final String ARG_ENUMERATOR = "DeactivateProfileDialog";
  protected Enumerator mEnumerator;

  public interface EnumeratorProfileDialogListener {
    void onViewReportClick(Enumerator enumerator);

    void onProfileClick(Enumerator enumerator);

    void onDeactivateClick();
  }

  private EnumeratorProfileDialogListener mListener;

  @BindView(R.id.button_enumerator_dialog_profile)
  BaseButton profileButton;
  @BindView(R.id.imageview_enumerator_dialog_profile)
  BaseRoundedImageView profileImageView;
  @BindView(R.id.textview_enumerator_dialog_name)
  BaseTextView nameTextView;
  @BindView(R.id.textview_enumerator_dialog_today_report)
  BaseTextView todayReportTextView;
  @BindView(R.id.textview_enumerator_dialog_total_report)
  BaseTextView totalReportTextView;

  public static EnumeratorDialog newInstance(Enumerator enumerator) {
    EnumeratorDialog fragment = new EnumeratorDialog();
    Bundle args = new Bundle();
    args.putParcelable(ARG_ENUMERATOR, enumerator);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onStart() {
    super.onStart();
    setPercentageDialogWidth(95);
    this.mEnumerator = getArgEnumerator();

    if (mEnumerator == null) {
      return;
    }

    nameTextView.setText(mEnumerator.getFullName());
    todayReportTextView.setText(getString(R.string.enumerator_dialog_today, "0"));
    totalReportTextView.setText(
        getString(R.string.enumerator_dialog_total_report, mEnumerator.getReportsCount()));
    profileImageView.downloadImage(this, getArgEnumerator().getProfilePictureUrl(), null);
  }

  @Override
  protected int getLayoutRes() {
    return R.layout.dialog_enumerator_profile;
  }

  public void setEnumeratorProfileDialogListener(EnumeratorProfileDialogListener listener) {
    this.mListener = listener;
  }

  @OnClick(R.id.button_enumerator_dialog_view_report)
  public void onViewReportClick(View v) {
    if (mListener != null) {
      mListener.onViewReportClick(getArgEnumerator());
    }
  }

  @OnClick(R.id.button_enumerator_dialog_profile)
  public void onProfileClick(View v) {
    if (mListener != null) {
      mListener.onProfileClick(getArgEnumerator());
    }
  }

  @OnClick(R.id.textview_enumerator_dialog_cancel)
  public void onCancelClick(View v) {
    if (getDialog() != null) {
      dismiss();
    }
  }

  private Enumerator getArgEnumerator() {
    if (getArguments() != null) {
      return getArguments().getParcelable(ARG_ENUMERATOR);
    } else {
      return null;
    }
  }

  @OnClick(R.id.textview_enumerator_dialog_deactivate)
  public void onDeactivateClick(View v) {
    if (mListener != null) mListener.onDeactivateClick();
  }
}
