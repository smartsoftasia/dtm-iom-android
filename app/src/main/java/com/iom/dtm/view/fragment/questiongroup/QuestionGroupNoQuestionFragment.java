package com.iom.dtm.view.fragment.questiongroup;

import android.os.Bundle;
import butterknife.BindView;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupNoQuestionFragment extends BaseAppFragment {
  public static final String TAG = "QuestionGroupFragment";
  public static final String ARG_QUESTION_GROUP = "arg_question_group";

  @BindView(R.id.textview_question_group_no_question_fragment_label)
  BaseTextView labelTextView;

  public static QuestionGroupNoQuestionFragment newInstance(QuestionGroup questionGroup) {
    QuestionGroupNoQuestionFragment fragment = new QuestionGroupNoQuestionFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION_GROUP, questionGroup);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    labelTextView.setText(getArgQuestionGroup(getArguments()).title);
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_group_no_question;
  }

  @Override
  protected void initialize() {
    this.getComponent(DTMFormDetailsComponent.class).inject(this);
  }

  private QuestionGroup getArgQuestionGroup(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION_GROUP);
    } else {
      return null;
    }
  }
}
