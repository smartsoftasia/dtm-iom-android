package com.iom.dtm.view.fragment.question;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import butterknife.BindView;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.presenter.question.QuestionStringPresenter;
import com.iom.dtm.presenter.question.QuestionTextPresenter;
import com.iom.dtm.view.viewinterface.QuestionStringViewInterface;
import com.iom.dtm.view.viewinterface.QuestionTextViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseEditText;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionTextFragment extends BaseQuestionFragment
    implements QuestionTextViewInterface {
  public static final String TAG = "QuestionTextFragment";

  @BindView(R.id.textview_question_text_fragment_label)
  BaseTextView labelTextView;
  @BindView(R.id.edittext_question_text_fragment)
  BaseEditText editText;

  @Inject
  QuestionTextPresenter presenter;

  public static QuestionTextFragment newInstance(Question question, int answerTime) {
    QuestionTextFragment fragment = new QuestionTextFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION, question);
    args.putInt(ARG_ANSWER_TIMES, answerTime);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.setAnswerTimes(getArgAnswerTimes(getArguments()));
    this.presenter.initialize(getArgQuestion(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_text;
  }

  @Override
  public void setUpQuestion(Question question) {
    if (TextUtils.isEmpty(question.title)) {
      labelTextView.setVisibility(View.GONE);
    } else {
      labelTextView.setVisibility(View.VISIBLE);
      labelTextView.setText(Html.fromHtml(question.title));
    }
  }

  @Override
  public void setUpAnswer(String answer) {
    editText.setText(answer);
    editText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        presenter.onTextChanged(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    });
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }
}
