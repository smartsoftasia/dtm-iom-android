package com.iom.dtm.view.fragment.question;

import android.os.Bundle;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.view.fragment.BaseAppFragment;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BaseQuestionFragment extends BaseAppFragment {
  public static final String TAG = "BaseQuestionFragment";
  public static final String ARG_QUESTION = "arg_question";
  public static final String ARG_ANSWER_TIMES = "arg_answer_time";

  protected Question getArgQuestion(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION);
    } else {
      return null;
    }
  }

  protected int getArgAnswerTimes(Bundle bundle) {
    if (bundle != null) {
      return bundle.getInt(ARG_ANSWER_TIMES);
    } else {
      return 1;
    }
  }
}
