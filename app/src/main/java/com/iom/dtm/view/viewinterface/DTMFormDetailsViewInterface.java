package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.QuestionCategory;

import java.util.List;

/**
 * Created by Nott on 8/4/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface DTMFormDetailsViewInterface extends BaseAppViewInterface {
  void setUpQuestionCategory(List<QuestionCategory> questionCategory);

  void displayAnswerRequired(String questionName);

  void displayAnswerCorrected();

  void openQuestionCategory(int position);

  void changeToSummaryPage();
}
