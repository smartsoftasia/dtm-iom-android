package com.iom.dtm.view.viewinterface;

import com.iom.dtm.domain.model.ViewReport;

/**
 * Created by Nott on 8/10/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface ViewReportSummaryViewInterface extends LoadingViewInterface {
  void updateViewReport(ViewReport viewReport);

  void displayReportHasBeenSent();
}
