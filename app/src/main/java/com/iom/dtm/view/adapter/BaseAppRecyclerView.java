package com.iom.dtm.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import com.smartsoftasia.ssalibrary.view.adapter.BaseRecyclerView;
import java.util.List;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BaseAppRecyclerView<VH extends RecyclerView.ViewHolder, T>
    extends BaseRecyclerView<VH, T> {
  private static final String TAG = "BaseAppRecyclerView";

  public BaseAppRecyclerView(List<T> items, Context context) {
    super(items, context);
  }
}
