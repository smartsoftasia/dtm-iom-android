package com.iom.dtm.view.fragment.questiongroup;

import android.animation.LayoutTransition;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;

import com.iom.dtm.R;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.dependency.component.DTMFormDetailsComponent;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.presenter.questiongroup.QuestionGroupPresenter;
import com.iom.dtm.view.fragment.BaseAppFragment;
import com.iom.dtm.view.fragment.question.QuestionDatePickerFragment;
import com.iom.dtm.view.fragment.question.QuestionDropDownFragment;
import com.iom.dtm.view.fragment.question.QuestionEmailFragment;
import com.iom.dtm.view.fragment.question.QuestionLocationFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberPickerFragment;
import com.iom.dtm.view.fragment.question.QuestionPhotoVideoFragment;
import com.iom.dtm.view.fragment.question.QuestionSingleChoiceFragment;
import com.iom.dtm.view.fragment.question.QuestionStringFragment;
import com.iom.dtm.view.fragment.question.QuestionTextFragment;
import com.iom.dtm.view.viewinterface.QuestionGroupViewInterface;
import com.smartsoftasia.ssalibrary.helper.ViewHelper;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroupFragment extends BaseAppFragment implements QuestionGroupViewInterface {
  public static final String TAG = "QuestionGroupFragment";
  public static final String ARG_QUESTION_GROUP = "arg_question_group";

  @BindView(R.id.linearlayout_question_group)
  LinearLayout linearLayout;
  @BindView(R.id.textview_question_group_fragment_label)
  BaseTextView labelTextView;

  @Inject
  QuestionGroupPresenter presenter;

  public static QuestionGroupFragment newInstance(QuestionGroup questionGroup) {
    QuestionGroupFragment fragment = new QuestionGroupFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_QUESTION_GROUP, questionGroup);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
  }

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_question_group;
  }

  @Override
  protected void initialize() {
    this.getComponent(BaseQuestionnaireComponent.class).inject(this);
    this.presenter.setView(this);
    this.presenter.initialize(getArgQuestionGroup(getArguments()));
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @Override
  public void appendQuestionDatePicker(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionDatePickerFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionLocation(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionLocationFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionNumber(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionNumberFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionSingleChoice(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionSingleChoiceFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionString(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionStringFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionEmail(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionEmailFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionAttachment(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionPhotoVideoFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionText(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionTextFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionDropDown(Question question, int position) {
    addQuestionGroupToQuestionGroupFragment(QuestionDropDownFragment.newInstance(question, 1));
  }

  @Override
  public void appendQuestionAdmin(QuestionGroup questionGroup, int answerTime) {
    addQuestionGroupToQuestionGroupFragment(
        QuestionGroupAdminFragment.newInstance(questionGroup, 1));
  }

  @Override
  public void setUpQuestionGroup(QuestionGroup questionGroup) {
    labelTextView.setVisibility(TextUtils.isEmpty(questionGroup.title) ? View.GONE : View.VISIBLE);
    labelTextView.setPaintFlags(labelTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    labelTextView.setVisibility(TextUtils.isEmpty(questionGroup.title) ? View.GONE : View.VISIBLE);
    labelTextView.setText(questionGroup.title);
  }

  private void addQuestionGroupToQuestionGroupFragment(Fragment fragment) {
    LinearLayout linearLayoutQuestionGroup = new LinearLayout(getActivity());
    linearLayoutQuestionGroup.setOrientation(LinearLayout.VERTICAL);
    linearLayoutQuestionGroup.setId(ViewHelper.generateViewId());

    getChildFragmentManager().beginTransaction()
        .add(linearLayoutQuestionGroup.getId(), fragment, TAG)
        .commit();

    this.linearLayout.addView(linearLayoutQuestionGroup);
  }

  private QuestionGroup getArgQuestionGroup(Bundle bundle) {
    if (bundle != null) {
      return bundle.getParcelable(ARG_QUESTION_GROUP);
    } else {
      return null;
    }
  }
}
