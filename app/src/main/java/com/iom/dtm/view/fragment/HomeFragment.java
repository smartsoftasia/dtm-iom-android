package com.iom.dtm.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.OnClick;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iom.dtm.R;
import com.iom.dtm.dependency.component.HomeComponent;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.presenter.HomePresenter;
import com.iom.dtm.view.activity.EditProfileActivity;
import com.iom.dtm.view.viewinterface.HomeViewInterface;
import com.smartsoftasia.ssalibrary.view.widget.BaseImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseRoundedImageView;
import com.smartsoftasia.ssalibrary.view.widget.BaseTextView;

import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Nott on 6/27/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class HomeFragment extends BaseAppFragment implements HomeViewInterface {
  public static final String TAG = "HomeFragment";

  public interface OnFragmentInteractionListener {

    void onOpenEditProfileActivity();

    void onOpenEnumeratorsFragment();

    void onOpenDTMFormFragment();
  }

  @Inject
  HomePresenter presenter;
  OnFragmentInteractionListener mListener;

  public static HomeFragment newInstance() {
    HomeFragment fragment = new HomeFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @BindView(R.id.imageview_home_fragment_profile_image)
  BaseRoundedImageView profileImageView;

  @BindView(R.id.imageview_home_fragment_profile_image_large)
  BaseImageView profileLargeImageView;
  @BindView(R.id.textview_home_fragment_name)
  BaseTextView nameTextView;
  @BindView(R.id.textview_home_fragment_organization_name)
  BaseTextView organizationTextView;
  @BindView(R.id.textview_home_fragment_position)
  BaseTextView positionTextView;
  @BindView(R.id.textview_home_fragment_location)
  BaseTextView locationTextView;
  @BindView(R.id.textview_home_fragment_enumerator)
  BaseTextView enumeratorTextView;
  @BindView(R.id.textview_home_fragment_today_report)
  BaseTextView todayReportTextView;
  @BindView(R.id.relativelayout_home_fragment_today_report)
  RelativeLayout todayReportLayout;
  @BindView(R.id.relativelayout_home_fragment_enumerator)
  RelativeLayout enumeratorLayout;
  @BindView(R.id.relativelayout_home_fragment_edit_profile)
  RelativeLayout editProfileLayout;
  @BindView(R.id.view12)
  View line;

  @Override
  public int getViewResourceId() {
    return R.layout.fragment_home;
  }

  @Override
  protected void afterLoadContentView(Bundle savedInstanceState) {
    super.afterLoadContentView(savedInstanceState);
    setActionbarTitle(R.string.action_bar_home_fragment);
  }

  @Override
  protected void initialize() {
    this.getComponent(HomeComponent.class).inject(this);
    this.presenter.setView(this);
    presenter.create();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    presenter.start();
  }

  @Override
  public void onPause() {
    super.onPause();
    presenter.pause();
  }

  @Override
  public void onResume() {
    super.onResume();
    presenter.resume();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onUpdateProfile(User user) {
    if (user == null) {
      return;
    }

    nameTextView.setText(user.getFullName());
    enumeratorTextView.setText(user.getEnumeratorsCount());
    todayReportTextView.setText(user.getTodayReportsCount());

    organizationTextView.setText(user.getOrganization());
    positionTextView.setText(user.getPosition());

    profileImageView.downloadImage(this, user.getProfilePictureUrl(), null);
    profileLargeImageView.downloadImage(this, user.getProfilePictureUrl(), null);

    if (user.userType.equalsIgnoreCase(User.ENUMERATORS)) {
      enumeratorLayout.setVisibility(View.GONE);
      line.setVisibility(View.GONE);
    }
  }

  @Override
  public void onError(String error) {
    showSnackBarError(error);
  }

  @Override
  public void onError(Throwable error) {
    showSnackBarError(error);
  }

  @OnClick(R.id.relativelayout_home_fragment_edit_profile)
  public void onEditProfileClick(View v) {
    if (mListener != null) {
      mListener.onOpenEditProfileActivity();
    }
  }

  @OnClick(R.id.relativelayout_home_fragment_enumerator)
  public void onEnumeratorsClick(View v) {
    if (mListener != null) {
      mListener.onOpenEnumeratorsFragment();
    }
  }

  @OnClick(R.id.button_home_fragment_dtm_form)
  public void onDTMFormClick(View v) {
    if (mListener != null) {
      mListener.onOpenDTMFormFragment();
    }
  }
}
