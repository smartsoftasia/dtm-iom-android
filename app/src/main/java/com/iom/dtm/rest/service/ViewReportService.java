package com.iom.dtm.rest.service;

import com.iom.dtm.domain.model.Admin0;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Report;
import com.iom.dtm.rest.form.ViewReportForm;
import com.iom.dtm.rest.model.BaseApiResult;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by androiddev01 on 5/19/2016 AD.
 * UserService
 */
public interface ViewReportService {

  @GET("administrators/fetch")
  Observable<BaseApiResult<Admin0>> getAdministrators(@Query("event_id") String eventId);

  @GET("events")
  Observable<BaseApiResult<List<Event>>> getEventList(@Query("all") String isAll);

  @POST("questionnaires/report")
  Observable<Object> getReport(@Body ViewReportForm viewReportForm);

  @GET("reports")
  Observable<BaseApiResult<List<Report>>> getEnumeratorReport(@Query("enumerator_id") String enumeratorId, @Query("page") String page);

  @GET("reports/{id}")
  Observable<Object> sendReportToEmail(@Path("id") String id, @Query("type") String email);

  @GET("reports")
  Observable<BaseApiResult<List<Report>>> getEnumeratorReportFromQuery(@Query("enumerator_id") String enumeratorId, @Query("filter[title]") String title,
                                                                       @Query("page") String page);
}
