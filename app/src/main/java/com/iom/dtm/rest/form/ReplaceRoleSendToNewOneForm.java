package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 8/26/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ReplaceRoleSendToNewOneForm {
  public static final String TAG = "ReplaceRoleSendToNewOneForm";

  @Expose
  @SerializedName("comment")
  public String comment;

  public ReplaceRoleSendToNewOneForm(String comment) {
    this.comment = comment;
  }
}
