package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 17/10/2559.
 * dtm-iom-android
 */

public class RegisterTokenForm {
  public static final String TAG = "RegisterTokenForm";

  @Expose
  @SerializedName("udid")
  public String udid;
  @Expose
  @SerializedName("token")
  public String token;
  @Expose
  @SerializedName("platform")
  public String platform;
  @Expose
  @SerializedName("os")
  public String os;

  public void setUdid(String udid) {
    this.udid = udid;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public void setOs(String os) {
    this.os = os;
  }
}
