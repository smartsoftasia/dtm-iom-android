package com.iom.dtm.rest.form;

import android.text.TextUtils;

import android.widget.EditText;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.R;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.helper.Validator;

public class SignUpDetailsForm {
  private static final String TAG = "SignUpDetailsForm";

  @Expose
  @SerializedName("first_name")
  public String firstName;
  @Expose
  @SerializedName("last_name")
  public String lastName;
  @Expose
  @SerializedName("email")
  public String email;
  @Expose
  @SerializedName("password")
  public String password;
  @Expose
  @SerializedName("is_male")
  public String gender;

  @Expose
  @SerializedName("facebook_id")
  public String facebookId;
  @Expose
  @SerializedName("google_id")
  public String googleId;

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setGender(boolean isMale) {
    this.gender = isMale ? "1" : "0";
  }

  public void setFacebookId(String facebookId) {
    this.facebookId = facebookId;
  }

  public void setGoogleId(String googleId) {
    this.googleId = googleId;
  }

  public boolean isFirstNameEmpty() {
    return TextUtils.isEmpty(firstName);
  }

  public boolean isLastNameEmpty() {
    return TextUtils.isEmpty(lastName);
  }

  public boolean isEmailEmpty() {
    return TextUtils.isEmpty(email);
  }

  public boolean isEmailValid() {
    return Validator.isValidEmail(email);
  }

  public boolean isPasswordEmpty() {
    return TextUtils.isEmpty(password);
  }

  public boolean isPasswordValid() {
    return Validator.isValidPassword(password);
  }

  public boolean validateForm(EditText firstName, EditText lastName, EditText email,
                              EditText password) {
    boolean isValidate = true;
    if (!isPasswordValid()) {
      onPasswordInvalid(password);
      isValidate = false;
    }
    if (isPasswordEmpty()) {
      onPasswordEmpty(password);
      isValidate = false;
    }
    if (!isEmailValid()) {
      onEmailInvalid(email);
      isValidate = false;
    }
    if (isEmailEmpty()) {
      onEmailEmpty(email);
      isValidate = false;
    }
    if (isLastNameEmpty()) {
      onLastNameEmpty(lastName);
      isValidate = false;
    }
    if (isFirstNameEmpty()) {
      onFirstNameEmpty(firstName);
      isValidate = false;
    }
    return isValidate;
  }

  public boolean validateSocialForm(EditText firstName, EditText lastName) {
    boolean isValidate = true;
    if (isLastNameEmpty()) {
      onLastNameEmpty(lastName);
      isValidate = false;
    }
    if (isFirstNameEmpty()) {
      onFirstNameEmpty(firstName);
      isValidate = false;
    }
    return isValidate;
  }

  public void onFirstNameEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onLastNameEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onEmailEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onEmailInvalid(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_email_not_valid));
    editText.requestFocus();
  }

  public void onPasswordEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onPasswordInvalid(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_invalid_password));
    editText.requestFocus();
  }
}
