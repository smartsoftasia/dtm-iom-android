package com.iom.dtm.rest.mapper;

import com.iom.dtm.domain.model.User;
import com.iom.dtm.rest.model.UserApiResult;
import rx.functions.Func1;

/**
 * Created by Nott on 8/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class UserMapper {
  public static final String TAG = "UserMapper";

  public static Func1<UserApiResult, User> userFunc1 = new Func1<UserApiResult, User>() {
    @Override
    public User call(UserApiResult userEntity) {
      return userEntity.t;
    }
  };
}
