package com.iom.dtm.rest.model;

import com.iom.dtm.domain.model.User;

/**
 * Created by androiddev03 on 17/6/2559.
 */
public class UserApiResult extends BaseApiResult<User> {
  public static final String TAG = "UserApiResult";
}
