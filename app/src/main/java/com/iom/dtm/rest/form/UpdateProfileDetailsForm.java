package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 9/16/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class UpdateProfileDetailsForm {
  public static final String TAG = "UpdateImageProfileForm";

  @Expose
  @SerializedName("user")
  public EditProfileForm editProfileForm;

  public UpdateProfileDetailsForm(EditProfileForm form) {
    this.editProfileForm = form;
  }
}
