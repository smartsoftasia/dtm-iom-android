package com.iom.dtm.rest.service;

import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.domain.model.Tag;
import com.iom.dtm.rest.form.AnswerForm;
import com.iom.dtm.rest.model.BaseApiResult;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface QuestionService {

  @GET("questionnaires/current")
  Observable<BaseApiResult<Questionnaire>> getFullQuestionnaire(@Query("event_id") String eventId);

  @GET("questionnaires/current")
  Observable<BaseApiResult<Questionnaire>> getLiteQuestionnaire(@Query("full") String isSmall);

  @POST("questionnaires/current/answers")
  Observable<BaseApiResult<Object>> sendAnswer(@Body AnswerForm answerForm);

  @POST("questionnaires/current/answers?full=false")
  Observable<BaseApiResult<Object>> sendAnswerLite(@Body AnswerForm answerForm);

  @GET("tags")
  Observable<BaseApiResult<List<Tag>>> getTag();

  @GET("events")
  Observable<BaseApiResult<List<Event>>> getEventList();
}
