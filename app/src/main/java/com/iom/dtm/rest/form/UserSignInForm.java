package com.iom.dtm.rest.form;

import android.support.annotation.StringDef;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Nott on 7/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class UserSignInForm {
  public static final String TAG = "UserSignInForm";
  public static final String NORMAL = "normal";
  public static final String FACEBOOK = "facebook";
  public static final String GOOGLE = "google";

  @StringDef({ NORMAL, FACEBOOK, GOOGLE })
  @Retention(RetentionPolicy.SOURCE)
  public @interface UserType {
  }

  @Expose
  @SerializedName("user")
  public SignInForm signInForm;

  @Expose
  @SerializedName("type")
  @UserType
  public String type;

  public UserSignInForm(SignInForm signInForm, @UserType String type) {
    this.signInForm = signInForm;
    this.type = type;
  }
}
