package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 8/25/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ReplaceRoleVerifyForm {
  public static final String TAG = "ReplaceRoleVerifyForm";

  @Expose
  @SerializedName("user")
  public Profile user;

  @Expose
  @SerializedName("not_reset_token")
  public String notResetPassword = "true";

  public ReplaceRoleVerifyForm(String email, String password) {
    this.user = new Profile(email, password);
  }

  class Profile {

    public Profile(String email, String password) {
      this.email = email;
      this.password = password;
    }

    @Expose
    public String email;

    @Expose
    public String password;
  }
}
