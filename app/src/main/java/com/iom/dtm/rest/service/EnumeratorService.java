package com.iom.dtm.rest.service;

import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.rest.model.BaseApiResult;

import java.util.List;

import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Nott on 8/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface EnumeratorService {

  @GET("enumerators")
  Observable<BaseApiResult<List<Enumerator>>> getEnumerators(@Query("page") String page);

  @GET("enumerators")
  Observable<BaseApiResult<List<Enumerator>>> searchEnumerators(@Query("filter[first_name]") String page);

  @DELETE("users/{id}/deactivate")
  Observable<Object> deactivate(@Path("id") String enumeratorId);
}
