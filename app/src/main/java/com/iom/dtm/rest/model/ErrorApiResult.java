package com.iom.dtm.rest.model;

/**
 * Created by Nott on 7/28/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ErrorApiResult extends BaseApiResult<ErrorDetailsApiResult> {
  public static final String TAG = "ErrorApiResult";
}
