package com.iom.dtm.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Nott on 7/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Meta {
  public static final String TAG = "Meta";

  @Expose
  @SerializedName("pagination")
  public Pagination pagination;
  @Expose
  @SerializedName("version")
  public Integer version;
  @Expose
  @SerializedName("success")
  public Boolean isSuccess;
  // Only on Debug mode
  @Expose
  @SerializedName("errors")
  public List<String> errors;
}
