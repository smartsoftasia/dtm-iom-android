package com.iom.dtm.rest.form;

import android.text.TextUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nott on 9/6/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AttachmentForm {
  public static final String TAG = "AttachmentForm";
  public static final String PHOTO = "photo";
  public static final String VIDEO = "video";

  @Expose
  @SerializedName("url")
  public String url;
  @Expose
  @SerializedName("comment")
  public String comment;
  @Expose
  @SerializedName("type")
  public String type;
  @Expose
  @SerializedName("tags")
  public List<TAGsForm> tagForms;

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setTagForms(List<String> tagForms, String tagID, String comment) {
    List<TAGsForm> list = new ArrayList<>();

    for (int i = 0; i < tagForms.size(); i++) {
      TAGsForm form = new TAGsForm();
      form.id = tagForms.get(i);
      if (!TextUtils.isEmpty(tagID) && tagForms.get(i).equalsIgnoreCase(tagID)) {
        form.comment = comment;
      }
      list.add(form);
    }

    this.tagForms = list;
  }

  public void setTAGAdditionalRequired(String tagID, String comment) {
    if (this.tagForms == null) {
      return;
    }
    for (int i = 0; i < tagForms.size(); i++) {
      if (tagForms.get(i).id.equalsIgnoreCase(tagID)) {
        tagForms.get(i).setComment(comment);
        break;
      }
    }
  }

  public List<String> getTAGsId() {
    if (tagForms == null) {
      return null;
    }
    List<String> tagsId = new ArrayList<>();
    for (int i = 0; i < tagForms.size(); i++) {
      tagsId.add(tagForms.get(i).id);
    }
    return tagsId;
  }

  public String getTAGOtherString() {
    if (tagForms == null) {
      return null;
    }

    for (int i = 0; i < tagForms.size(); i++) {
      if (!TextUtils.isEmpty(tagForms.get(i).comment)) {
        return tagForms.get(i).comment;
      }
    }
    return null;
  }

  @Override
  public String toString() {
    return "AttachmentForm{" +
        "comment='" + comment + '\'' +
        ", url='" + url + '\'' +
        ", type='" + type;
  }

  public class TAGsForm {

    @Expose
    @SerializedName("id")
    public String id;
    @Expose
    @SerializedName("comment")
    public String comment;

    public void setComment(String comment) {
      this.comment = comment;
    }

    public void setId(String id) {
      this.id = id;
    }
  }
}
