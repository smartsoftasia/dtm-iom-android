package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;

/**
 * Created by Nott on 8/25/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DeactivateProfileForm {
  public static final String TAG = "DeactivateProfileForm";

  @Expose
  public Profile user;

  public DeactivateProfileForm(String email, String password) {
    this.user = new Profile(email, password);
  }

  class Profile {

    public Profile(String email, String password) {
      this.email = email;
      this.password = password;
    }

    @Expose
    public String email;

    @Expose
    public String password;
  }
}
