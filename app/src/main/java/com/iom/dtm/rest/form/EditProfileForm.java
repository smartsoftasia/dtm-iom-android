package com.iom.dtm.rest.form;

import android.text.TextUtils;
import android.widget.EditText;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.R;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;

/**
 * Created by Nott on 6/28/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EditProfileForm {
  public static final String TAG = "EditProfileForm";

  @Expose
  @SerializedName("first_name")
  public String firstName;
  @Expose
  @SerializedName("last_name")
  public String lastName;
  @Expose
  @SerializedName("organization")
  public String organization;
  @Expose
  @SerializedName("position")
  public String titlePosition;

  public boolean isFirstNameEmpty() {
    return TextUtils.isEmpty(firstName);
  }

  public boolean isLastNameEmpty() {
    return TextUtils.isEmpty(lastName);
  }

  public boolean isOrganizationEmpty() {
    return TextUtils.isEmpty(organization);
  }

  public boolean isTitlePositionEmpty() {
    return TextUtils.isEmpty(titlePosition);
  }

  public boolean validateForm(EditText firstName, EditText lastName, EditText organization,
                              EditText titlePosition) {
    this.firstName = firstName.getText().toString();
    this.lastName = lastName.getText().toString();
    this.organization = organization.getText().toString();
    this.titlePosition = titlePosition.getText().toString();

    boolean isValidate = true;
    if (isTitlePositionEmpty()) {
      onTitlePositionEmpty(titlePosition);
      isValidate = false;
    }
    if (isOrganizationEmpty()) {
      onOrganizationEmpty(organization);
      isValidate = false;
    }
    if (isLastNameEmpty()) {
      onLastNameEmpty(lastName);
      isValidate = false;
    }
    if (isFirstNameEmpty()) {
      onFirstNameEmpty(firstName);
      isValidate = false;
    }

    return isValidate;
  }

  public void onLastNameEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onFirstNameEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onOrganizationEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onTitlePositionEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }
}
