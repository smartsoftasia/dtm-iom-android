package com.iom.dtm.rest.form;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartsoftasia.ssalibrary.helper.Validator;

public class ResetPasswordForm {
  public static final String TAG = "ResetPasswordForm";

  @Expose
  @SerializedName("email")
  public String email;

  public ResetPasswordForm() {

  }

  public ResetPasswordForm(String em) {
    this.email = em == null ? null : em.trim();
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public boolean isEmailValid() {
    return Validator.isValidEmail(email);
  }

  public boolean isEmailEmpty() {
    return TextUtils.isEmpty(email);
  }

}





