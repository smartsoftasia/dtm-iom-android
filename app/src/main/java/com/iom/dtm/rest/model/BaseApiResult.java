package com.iom.dtm.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 7/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class BaseApiResult<T> {
  public static final String TAG = "BaseApiResult";

  @Expose
  @SerializedName("response")
  public T t;
  @Expose
  @SerializedName("meta")
  public Meta meta;
}
