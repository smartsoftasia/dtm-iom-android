package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 9/16/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class UpdateImageProfileForm {
  public static final String TAG = "UpdateImageProfileForm";

  @Expose
  @SerializedName("user")
  public UserDetails userDetails;

  public UpdateImageProfileForm(String imagePath) {
    userDetails = new UserDetails();
    userDetails.image = imagePath;
  }

  public class UserDetails {

    @Expose
    @SerializedName("remote_image_url")
    public String image;
  }
}
