package com.iom.dtm.rest.form;

import android.text.TextUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Nott on 8/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AnswerDetailForm {
  public static final String TAG = "AnswerDetailForm";

  @Expose
  @SerializedName("question_id")
  public String questionId;
  @Expose
  @SerializedName("value")
  public Object value;
  @Expose
  @SerializedName("choice_id")
  public String choiceId;
  @Expose
  @SerializedName("values")
  public List<Object> values;
  @Expose
  @SerializedName("choice_ids")
  public List<String> choiceIds;

  public void setChoiceId(String choiceId) {
    this.choiceId = choiceId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public void setValue(Object value) {
/*    if (value instanceof String) {
      if (TextUtils.isEmpty((String) value)) {
        this.value = null;
      } else {
        this.value = value;
      }
    }*/
    this.value = value;
  }

  public void setValues(List<Object> values) {
    this.values = values;
  }

  public void setChoiceIds(List<String> choiceIds) {
    this.choiceIds = choiceIds;
  }
}
