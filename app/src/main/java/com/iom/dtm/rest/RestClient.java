package com.iom.dtm.rest;

import com.google.gson.Gson;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.rest.service.EnumeratorService;
import com.iom.dtm.rest.service.QuestionService;
import com.iom.dtm.rest.service.UserService;
import com.iom.dtm.rest.service.ViewReportService;
import com.smartsoftasia.ssalibrary.bus.ApplicationBus;
import com.smartsoftasia.ssalibrary.helper.GsonHelper;
import com.smartsoftasia.ssalibrary.helper.SSLHelper;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by androiddev01 on 5/19/2016 AD.
 * RestClient
 */
public class RestClient {
  public static final String TAG = "RestClient";

  private static RestClient sRestClient;
  // private LanguageController mLanguageController;
  protected SharedPreference mSharedPreference;
  protected ApplicationBus mApplicationBus;

  protected UserService userService;
  protected QuestionService questionService;
  protected EnumeratorService enumeratorService;
  protected ViewReportService viewReportService;
  protected Retrofit retrofit;
  protected OkHttpClient client;
  protected RequestInterceptor mRequestInterceptor;
  protected HttpLoggingInterceptor interceptor;

  public RestClient() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(AppConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
        .build();

    Retrofit restAdapter = new Retrofit.Builder().baseUrl(AppConfig.BASE_URL).client(client)
        .addConverterFactory(GsonConverterFactory.create(getGson()))
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .build();

    userService = restAdapter.create(UserService.class);
    questionService = restAdapter.create(QuestionService.class);
    enumeratorService = restAdapter.create(EnumeratorService.class);
    viewReportService = restAdapter.create(ViewReportService.class);
  }

  @Inject
  public RestClient(SharedPreference sharedPreference, ApplicationBus applicationBus,
                    RequestInterceptor requestInterceptor) {
    mSharedPreference = sharedPreference;
    mApplicationBus = applicationBus;
    mRequestInterceptor = requestInterceptor;

    interceptor = new HttpLoggingInterceptor().setLevel(
        AppConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

    client = new OkHttpClient.Builder().addInterceptor(interceptor)
        .sslSocketFactory(SSLHelper.getSSLSocketFactoryForTrustAllCertificate())
        .addNetworkInterceptor(mRequestInterceptor)
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .build();

    retrofit = new Retrofit.Builder().baseUrl(AppConfig.BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(getGson()))
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .build();

    userService = retrofit.create(UserService.class);
    questionService = retrofit.create(QuestionService.class);
    enumeratorService = retrofit.create(EnumeratorService.class);
    viewReportService = retrofit.create(ViewReportService.class);
  }


  public Gson getGson() {
    return GsonHelper.newBuilder().excludeFieldsWithoutExposeAnnotation().create();
  }

  public static RestClient getInstance() {
    if (sRestClient == null) {
      sRestClient = new RestClient();
    }
    return sRestClient;
  }

  /**
   * Getter of the user service.
   *
   * @return user service
   */
  public UserService getUserService() {
    return userService;
  }

  public QuestionService getQuestionService() {
    return questionService;
  }

  public EnumeratorService getEnumeratorService() {
    return enumeratorService;
  }

  public ViewReportService getViewReportService() {
    return viewReportService;
  }
}
