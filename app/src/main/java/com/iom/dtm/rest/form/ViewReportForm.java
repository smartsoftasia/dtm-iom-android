package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.domain.model.Admin1;
import com.iom.dtm.domain.model.Question;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nott on 13/10/2559.
 * dtm-iom-android
 */

public class ViewReportForm {
  public static final String TAG = "ViewReportForm";

  @Expose
  @SerializedName("options")
  Details details;

  public ViewReportForm(String eventId, Date startDate, Date endDate, List<Admin1> admin1s, List<Question> questions) {
    this.details = new Details();
    this.details.setEventId(eventId);
    this.details.setStartDate(startDate);
    this.details.setEndDate(endDate);
    this.details.setAdmin1s(admin1s);
    this.details.setQuestions(questions);
  }

  private class Details {
    @Expose
    @SerializedName("event_id")
    public String eventId;
    @Expose
    @SerializedName("admins")
    public List<String> admin1s;
    @Expose
    @SerializedName("question_ids")
    public List<String> questions;
    @Expose
    @SerializedName("started_at")
    public Date startDate;
    @Expose
    @SerializedName("ended_at")
    public Date endDate;

    public void setEventId(String eventId) {
      this.eventId = eventId;
    }

    public void setAdmin1s(List<Admin1> admin1s) {
      List<String> strings = new ArrayList<>();
      for (int i = 0; i < admin1s.size(); i++) {
        strings.add(admin1s.get(i).reference);
      }
      this.admin1s = strings;
    }

    public void setQuestions(List<Question> questions) {
      List<String> strings = new ArrayList<>();
      for (int i = 0; i < questions.size(); i++) {
        strings.add(questions.get(i).id);
      }
      this.questions = strings;
    }

    public void setStartDate(Date startDate) {
      this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
      this.endDate = endDate;
    }

  }
}
