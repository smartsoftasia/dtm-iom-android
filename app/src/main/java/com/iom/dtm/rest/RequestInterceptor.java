package com.iom.dtm.rest;

import android.text.TextUtils;

import com.iom.dtm.core.AppConfig;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.domain.controller.SharedPreference;
import com.smartsoftasia.ssalibrary.helper.DeviceHelper;
import com.smartsoftasia.ssalibrary.helper.Logger;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Nott on 6/9/2016 AD.
 * axa-provider-android
 */
public class RequestInterceptor implements Interceptor {
  public static final String TAG = "RequestInterceptor";
  public static final String KEY = "Key ";

  private SharedPreference mSharedPreference;
  private String token;

  @Inject
  public RequestInterceptor(SharedPreference sharedPreference) {
    this.mSharedPreference = sharedPreference;
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    Request original = chain.request();

    if (mSharedPreference != null) {
      token = mSharedPreference.readUserToken() == null ? AppConstant.EMPTY : mSharedPreference.readUserToken();
    }

    Request newRequest;
    if (!TextUtils.isEmpty(token)) {
      newRequest = original.newBuilder()
          .addHeader("Accept", getAcceptHeader())
          .addHeader("Authorization", getAuthorizationHeader(token))
          .build();
    } else {
      newRequest = original.newBuilder()
          .addHeader("Accept", getAcceptHeader())
          .build();
    }

    Response response = chain.proceed(newRequest);
    if (response.isSuccessful()) {
      if (response.header("Authorization") != null) {
        mSharedPreference.writeUserToken(response.header("Authorization").replace(KEY, ""));
      }
    }
    return response;
  }

  private String getAcceptHeader() {
    String type = "application/json;";
    String apiVersion = "version=" + AppConfig.API_VERSION + ";";
    String timeZone = "zone=" + DeviceHelper.getCurrentNumericTimeZone();
    return type + AppConstant.BLANK_SPACE + apiVersion + AppConstant.BLANK_SPACE + timeZone;
  }

  private String getAuthorizationHeader(String token) {
    return KEY + token;
  }
}
