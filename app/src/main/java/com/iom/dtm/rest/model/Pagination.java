package com.iom.dtm.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 7/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Pagination {
  public static final String TAG = "Pagination";

  @Expose
  @SerializedName("current_page")
  public Integer currentPage;
  @Expose
  @SerializedName("next_page")
  public Integer nextPage;
  @Expose
  @SerializedName("last_name")
  public Integer lastName;
  @Expose
  @SerializedName("total_pages")
  public Integer totalPages;
  @Expose
  @SerializedName("total_count")
  public Integer totalCount;
}
