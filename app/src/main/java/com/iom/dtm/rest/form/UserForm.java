package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 3/11/2559.
 * dtm-iom-android
 */

public class UserForm<T> {
  public static final String TAG = "UserForm";

  @Expose
  @SerializedName("user")
  public T t;

  public UserForm(T t) {
    this.t = t;
  }
}
