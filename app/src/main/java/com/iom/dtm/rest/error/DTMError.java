package com.iom.dtm.rest.error;

import com.google.gson.Gson;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.smartsoftasia.ssalibrary.helper.Logger;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Nott on 6/9/2016 AD.
 * axa-provider-android
 */
public class DTMError extends RuntimeException {
  public static final String TAG = "PromptSupportError";
  protected ResponseBody mResponseBody;
  private HttpException mHttpException;
  private JSONObject jsonError = null;
  private Gson gson = new Gson();
  private ErrorApiResult errorApiResult;

  public DTMError() {
    super();
  }

  public DTMError(String detailMessage) {
    super(detailMessage);
  }

  public DTMError(String detailMessage, Throwable throwable) {
    super(detailMessage, throwable);
  }

  public DTMError(HttpException e) throws Exception {
    this.mHttpException = e;
    initialize();
  }

  public int getResponseCode() {
    if (mHttpException == null) {
      return 400;
    }
    return mHttpException.response().code();
  }

  public void initialize() {
    if (mHttpException == null) {
      return;
    }

    this.mResponseBody = mHttpException.response().errorBody();

    try {
      jsonError = new JSONObject(mResponseBody.string());
    } catch (Exception e) {
      Logger.e(TAG, e.toString());
    }

    errorApiResult = gson.fromJson(jsonError.toString(), ErrorApiResult.class);
  }

  public ErrorApiResult getErrorApiResult() {
    return errorApiResult;
  }
}
