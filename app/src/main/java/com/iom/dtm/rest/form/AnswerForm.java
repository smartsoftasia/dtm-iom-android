package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Nott on 8/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AnswerForm {
  public static final String TAG = "AnswerForm";

  @Expose
  @SerializedName("answers")
  public List<AnswerDetailForm> answerDetailFormList;

  @Expose
  @SerializedName("event_id")
  public String eventId;

  public AnswerForm(List<AnswerDetailForm> answerDetailFormList) {
    this.answerDetailFormList = answerDetailFormList;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }
}
