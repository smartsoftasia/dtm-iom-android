package com.iom.dtm.rest.service;

import com.iom.dtm.rest.form.RegisterTokenForm;
import com.iom.dtm.rest.form.ReplaceRoleSendToNewOneForm;
import com.iom.dtm.rest.form.ReplaceRoleVerifyForm;
import com.iom.dtm.rest.form.ResetPasswordForm;
import com.iom.dtm.rest.form.SignInForm;
import com.iom.dtm.rest.form.UpdateImageProfileForm;
import com.iom.dtm.rest.form.UpdateProfileDetailsForm;
import com.iom.dtm.rest.form.UserForm;
import com.iom.dtm.rest.form.UserSignInForm;
import com.iom.dtm.rest.form.UserSignUpForm;
import com.iom.dtm.rest.model.UserApiResult;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by androiddev01 on 5/19/2016 AD.
 * UserService
 */
public interface UserService {

  @POST("users/login")
  Observable<UserApiResult> signIn(@Body UserSignInForm user);

  @POST("users/login")
  Observable<UserApiResult> signInSocial(@Body SignInForm user);

  @POST("users/{id}/device")
  Observable<UserApiResult> registerFCM(@Path("id") String id, @Body RegisterTokenForm registerTokenForm);

  @DELETE("users/me/deactivate")
  Observable<UserApiResult> deactivate();

  @GET("users/me")
  Observable<UserApiResult> getUser();

  @POST("users")
  Observable<UserApiResult> signUp(@Body UserSignUpForm user);

  @POST("users/reset")
  Observable<UserApiResult> resetPassword(@Body UserForm<ResetPasswordForm> user);

  @PUT("users/me")
  Observable<UserApiResult> updateUser(@Body UpdateImageProfileForm updateUserForm);

  @PUT("users/me")
  Observable<UserApiResult> updateUser(@Body UpdateProfileDetailsForm updateProfileDetailsForm);

  @POST("users/login")
  Observable<UserApiResult> replaceRoleVerify(@Body ReplaceRoleVerifyForm replaceRoleVerifyForm);

  @POST("users/me/replace_role")
  Observable<UserApiResult> replaceRole(@Query("email") String email, @Body
      ReplaceRoleSendToNewOneForm replaceRoleSendToNewOneForm);
}
