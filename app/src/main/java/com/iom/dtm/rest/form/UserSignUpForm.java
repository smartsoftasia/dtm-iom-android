package com.iom.dtm.rest.form;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 8/19/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class UserSignUpForm {
  public static final String TAG = "UserSignUpForm";

  @Expose
  @SerializedName("user")
  public SignUpDetailsForm signUpDetailsForm;

  public UserSignUpForm(SignUpDetailsForm signUpDetailsForm) {
    this.signUpDetailsForm = signUpDetailsForm;
  }
}
