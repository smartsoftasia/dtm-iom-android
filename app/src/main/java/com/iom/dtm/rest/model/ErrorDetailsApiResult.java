package com.iom.dtm.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 7/22/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ErrorDetailsApiResult {
  public static final String TAG = "ErrorDetailsApiResult";

  @Expose
  @SerializedName("title")
  public String title;
  @Expose
  @SerializedName("error")
  public String error;
  @Expose
  @SerializedName("type")
  public String type;

  public String getTitleMessage() {
    return title;
  }

  public String getErrorType() {
    return type;
  }

  public String getErrorMessage() {
    return error;
  }
}
