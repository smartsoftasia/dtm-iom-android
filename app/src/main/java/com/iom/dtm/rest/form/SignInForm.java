package com.iom.dtm.rest.form;

import android.support.annotation.StringDef;
import android.text.TextUtils;
import android.widget.EditText;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.R;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import com.smartsoftasia.ssalibrary.helper.Validator;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SignInForm {
  public static final String TAG = "SignInForm";

  public static final String NORMAL = "normal";
  public static final String FACEBOOK = "facebook";
  public static final String GOOGLE = "google";

  @StringDef({ NORMAL, FACEBOOK, GOOGLE })
  @Retention(RetentionPolicy.SOURCE)
  public @interface UserType {
  }

  public SignInForm() {
  }

  @Expose
  @SerializedName("email")
  public String email;
  @Expose
  @SerializedName("password")
  public String password;
  @Expose
  @SerializedName("token")
  public String token;
  @Expose
  @SerializedName("type")
  @UserType
  public String type;

  public static SignInForm newFacebookSignInForm(String token) {
    return new SignInForm(null, null, token, FACEBOOK);
  }

  public static SignInForm newGoogleSignInForm(String token) {
    return new SignInForm(null, null, token, GOOGLE);
  }

  public SignInForm(String email, String password, String token, String type) {
    this.email = email;
    this.password = password;
    this.token = token;
    this.type = type;
  }

  public SignInForm(String email, String password) {
    super();
    this.email = email == null ? null : email.trim();
    this.password = password == null ? null : password.trim();
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public boolean isPasswordValid() {
    return Validator.isValidLenght(password, 100, 6);
  }

  public boolean isPasswordEmpty() {
    return TextUtils.isEmpty(password);
  }

  public boolean isEmailValid() {
    return Validator.isValidEmail(email);
  }

  public boolean isEmailEmpty() {
    return TextUtils.isEmpty(email);
  }

  public boolean isTokenValid() {return Validator.isValid(token);}

  public boolean isFormValid(EditText emailEditText, EditText passwordEditText) {
    boolean isValidate = true;
    if (!isPasswordValid()) {
      onPasswordInvalid(passwordEditText);
      isValidate = false;
    }
    if (isPasswordEmpty()) {
      onPasswordEmpty(passwordEditText);
      isValidate = false;
    }
    if (!isEmailValid()) {
      onEmailInvalid(emailEditText);
      isValidate = false;
    }
    if (isEmailEmpty()) {
      onEmailEmpty(emailEditText);
      isValidate = false;
    }
    return isValidate;
  }

  public void onEmailEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onPasswordEmpty(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_field_required));
    editText.requestFocus();
  }

  public void onEmailInvalid(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_invalid_email));
    editText.requestFocus();
  }

  public void onPasswordInvalid(EditText editText) {
    editText.setError(TranslationHelper.get(R.string.error_invalid_password));
    editText.requestFocus();
  }
}




