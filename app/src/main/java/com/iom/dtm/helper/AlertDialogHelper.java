package com.iom.dtm.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import com.iom.dtm.R;

/**
 * Created by Nott on 8/25/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AlertDialogHelper {
  public static final String TAG = "AlertDialogHelper";

  public static void showMessageDialog(Context context, String title, String body,
                                       boolean isCancelable) {
    new AlertDialog.Builder(context).setTitle(title)
        .setMessage(body)
        .setNegativeButton(R.string.global_ok, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {

          }
        })
        .setCancelable(isCancelable)
        .show();
  }

  public static void showMessageDialog(Context context, @StringRes int title, @StringRes int body,
                                       boolean isCancelable) {
    new AlertDialog.Builder(context).setTitle(title)
        .setMessage(body)
        .setNegativeButton(R.string.global_ok, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {

          }
        })
        .setCancelable(isCancelable)
        .show();
  }
}
