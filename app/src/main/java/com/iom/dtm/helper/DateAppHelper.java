package com.iom.dtm.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Nott on 7/7/2016 AD.
 * AxaProvider
 */
public class DateAppHelper {
  public static final String TAG = "DateAppHelper";

  public static String dateTimeToStringForUploadImage(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy-HHmmssSSS", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }

  public static String dateTimeToStringForAPI(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }

  public static String dateTimeToStringForViewReport(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }

  public static String dateTimeToString(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
    if (date != null) {
      return (formatter.format(date));
    } else {
      return "";
    }
  }
}
