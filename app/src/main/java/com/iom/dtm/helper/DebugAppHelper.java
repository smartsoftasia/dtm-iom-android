package com.iom.dtm.helper;

import com.iom.dtm.domain.model.Admin0;
import com.iom.dtm.domain.model.User;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nott on 9/15/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DebugAppHelper {
  public static final String TAG = "DebugAppHelper";
  public static final String USER_EMAIL = "";
  public static final String USER_PASSWORD = "";

  private static DebugAppHelper sDebugAppHelper;

  public static DebugAppHelper getInstance() {
    if (sDebugAppHelper == null) {
      sDebugAppHelper = new DebugAppHelper();
    }
    return sDebugAppHelper;
  }

  public User getUser() {
    User user = new User();
    user.id = "7";
    user.setUserType(User.ENUMERATORS_ADMIN);
    user.setFirstName("Cristiano");
    user.setLastName("Ronaldo");
    user.setEnumeratorsCount("100000");
    user.setReportsCount("100000");
    user.setOrganization("Real Madrid FC.");
    user.setPosition("Left Attacking Midfielder");
    return user;
  }

  public List<Admin0> getProvinceList() {
    List<Admin0> countries = new ArrayList<>();

    for (int i = 0; i < 10; i++) {
      Admin0 admin0 = new Admin0();
      admin0.setName("United State of America");
      countries.add(admin0);
    }
    return countries;
  }
}
