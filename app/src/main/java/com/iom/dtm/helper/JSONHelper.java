package com.iom.dtm.helper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.iom.dtm.rest.form.AttachmentForm;
import com.smartsoftasia.ssalibrary.helper.GsonHelper;

/**
 * Created by Nott on 9/7/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class JSONHelper {
  public static final String TAG = "JSONHelper";

  private JSONHelper() {}

  public static boolean isAttachmentJSONValid(String jsonInString) {
    AttachmentForm form;
    try {
      form = JSONHelper.getAttachmentFormFromJson(jsonInString);
    } catch (Exception e) {
      return false;
    }
    return form != null;
  }

  public static AttachmentForm getAttachmentFormFromJson(String string) {
    Gson gson = GsonHelper.newBuilder().setPrettyPrinting().create();
    return gson.fromJson(string, AttachmentForm.class);
  }
}
