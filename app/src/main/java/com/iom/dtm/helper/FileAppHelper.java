package com.iom.dtm.helper;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.UiThread;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * Created by Nott on 8/31/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class FileAppHelper {
  public static final String TAG = "FileAppHelper";

  /**
   * Get a file path from a Uri. This will get the the path for Storage Access
   * Framework Documents, as well as the _data field for the MediaStore and
   * other file-based ContentProviders.
   *
   * @param context The context.
   * @param uri The Uri to query.
   * @author paulburke
   */
  public static String getPath(final Context context, final Uri uri) {

    // DocumentProvider
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(
        context, uri)) {
      // ExternalStorageProvider
      if (isExternalStorageDocument(uri)) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        final String type = split[0];

        if ("primary".equalsIgnoreCase(type)) {
          return Environment.getExternalStorageDirectory() + "/" + split[1];
        }

        // TODO handle non-primary volumes
      }
      // DownloadsProvider
      else if (isDownloadsDocument(uri)) {

        final String id = DocumentsContract.getDocumentId(uri);
        final Uri contentUri = ContentUris.withAppendedId(
            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

        return getDataColumn(context, contentUri, null, null);
      }
      // MediaProvider
      else if (isMediaDocument(uri)) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        final String type = split[0];

        Uri contentUri = null;
        if ("image".equals(type)) {
          contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        } else if ("video".equals(type)) {
          contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        } else if ("audio".equals(type)) {
          contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        }

        final String selection = "_id=?";
        final String[] selectionArgs = new String[] {
            split[1]
        };

        return getDataColumn(context, contentUri, selection, selectionArgs);
      }
    }
    // MediaStore (and general)
    else if ("content".equalsIgnoreCase(uri.getScheme())) {
      return getDataColumn(context, uri, null, null);
    }
    // File
    else if ("file".equalsIgnoreCase(uri.getScheme())) {
      return uri.getPath();
    }

    return null;
  }

  /**
   * Get the value of the data column for this Uri. This is useful for
   * MediaStore Uris, and other file-based ContentProviders.
   *
   * @param context The context.
   * @param uri The Uri to query.
   * @param selection (Optional) Filter used in the query.
   * @param selectionArgs (Optional) Selection arguments used in the query.
   * @return The value of the _data column, which is typically a file path.
   */
  public static String getDataColumn(Context context, Uri uri, String selection,
                                     String[] selectionArgs) {

    Cursor cursor = null;
    final String column = "_data";
    final String[] projection = {
        column
    };

    try {
      cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
      if (cursor != null && cursor.moveToFirst()) {
        final int column_index = cursor.getColumnIndexOrThrow(column);
        return cursor.getString(column_index);
      }
    } finally {
      if (cursor != null) cursor.close();
    }
    return null;
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is ExternalStorageProvider.
   */
  public static boolean isExternalStorageDocument(Uri uri) {
    return "com.android.externalstorage.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is DownloadsProvider.
   */
  public static boolean isDownloadsDocument(Uri uri) {
    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is MediaProvider.
   */
  public static boolean isMediaDocument(Uri uri) {
    return "com.android.providers.media.documents".equals(uri.getAuthority());
  }

  @UiThread
  public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
    Bitmap bitmap = null;
    MediaMetadataRetriever mediaMetadataRetriever = null;
    try {
      mediaMetadataRetriever = new MediaMetadataRetriever();
      if (Build.VERSION.SDK_INT >= 14) {
        mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
      } else {
        mediaMetadataRetriever.setDataSource(videoPath);
      }
      //   mediaMetadataRetriever.setDataSource(videoPath);
      bitmap = mediaMetadataRetriever.getFrameAtTime();
    } catch (Exception e) {
      e.printStackTrace();
      throw new Throwable(
          "Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());
    } finally {
      if (mediaMetadataRetriever != null) {
        mediaMetadataRetriever.release();
      }
    }
    return bitmap;
  }

  @UiThread
  public static boolean isImageURL(String path) {
    URLConnection connection;
    try {
      connection = new URL(path).openConnection();
    } catch (IOException e) {
      e.printStackTrace();
      return true;
    }
    String contentType = connection.getHeaderField("Content-Type");
    return contentType.startsWith("image/");
  }

  public static Bitmap getThumbnailBitmap(String videoUrl) {
    return ThumbnailUtils.createVideoThumbnail(videoUrl,
        MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
  }

  public static File getThumbnailFile(String videoUrl, String folderName) {
    return saveBitmap(getThumbnailBitmap(videoUrl), folderName);
  }

  public static File saveBitmap(Bitmap bitmap, String folderName) {
    String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
    OutputStream outStream = null;

    File folderFile = Environment.getExternalStoragePublicDirectory(
        Environment.DIRECTORY_PICTURES + File.separator + folderName);
    if (!folderFile.exists()) {
      folderFile.mkdirs();
    }
    File file = new File(
        folderFile.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpg");
    if (file.exists()) {
      file.delete();
    }
    try {
      file.createNewFile();
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      // make a new bitmap from your file
      outStream = new FileOutputStream(file);
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
      outStream.flush();
      outStream.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    Log.e("file", "" + file);
    return file;
  }
}
