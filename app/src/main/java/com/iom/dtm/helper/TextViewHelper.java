package com.iom.dtm.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;
import com.iom.dtm.R;

/**
 * Created by Nott on 4/19/2016 AD.
 * TextViewHelper
 */
public class TextViewHelper {
  public static final String TAG = "TextViewHelper";

  /**
   * Set sub text on TextView click for phone dial
   *
   * @param context Context App
   * @param textView TextView reference
   * @param fullText Full of text
   * @param linkText Sub text
   * @param numberPhone Number
   */
  public static void setOnClickLinkDialing(final Context context, TextView textView,
                                           final String fullText, String linkText,
                                           final String numberPhone) {
    ClickableSpan cs = new ClickableSpan() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + numberPhone));
        context.startActivity(intent);
      }
    };

    SpannableString span = new SpannableString(fullText);
    int index = fullText.indexOf(linkText);
    span.setSpan(cs, index, index + linkText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary)), index,
        index + linkText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    textView.setText(span);
    textView.setTag(linkText);
    textView.setMovementMethod(LinkMovementMethod.getInstance());
  }

  /**
   * Set sub text on TextView click for email intent
   *
   * @param context Context App
   * @param textView TextView reference
   * @param fullText Full of text
   * @param linkText Sub text
   * @param email Set for receiver email
   */
  public static void setOnClickLinkEmail(final Context context, TextView textView,
                                         final String fullText, String linkText,
                                         final String email) {
    ClickableSpan cs = new ClickableSpan() {
      @Override
      public void onClick(View v) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
      }
    };

    SpannableString span = new SpannableString(fullText);
    int index = fullText.indexOf(linkText);
    span.setSpan(cs, index, index + linkText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary)), index,
        index + linkText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    textView.setText(span);
    textView.setTag(linkText);
    textView.setMovementMethod(LinkMovementMethod.getInstance());
  }

  public static String removeHtml(String html) {
    html = html.replaceAll("<(.*?)\\>", " ");
    html = html.replaceAll("<(.*?)\\\n", " ");
    html = html.replaceFirst("(.*?)\\>", " ");
    html = html.replaceAll("&nbsp;", " ");
    html = html.replaceAll("&amp;", " ");
    return html;
  }
}
