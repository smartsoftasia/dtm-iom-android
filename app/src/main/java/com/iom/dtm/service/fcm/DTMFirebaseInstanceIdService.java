package com.iom.dtm.service.fcm;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Nott on 17/10/2559.
 * dtm-iom-android
 */

public class DTMFirebaseInstanceIdService extends FirebaseInstanceIdService {
  public static final String TAG = "DTMFirebaseInstanceIdSe";

  @Override
  public void onTokenRefresh() {
    Intent intent = new Intent(this, DTMRegistrationIntentService.class);
    startService(intent);
  }
}
