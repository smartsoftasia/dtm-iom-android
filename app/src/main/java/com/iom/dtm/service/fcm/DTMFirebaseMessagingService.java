package com.iom.dtm.service.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.iom.dtm.R;
import com.iom.dtm.domain.bus.AdminMessage;
import com.iom.dtm.domain.bus.RxBus;
import com.iom.dtm.view.activity.HomeActivity;
import com.smartsoftasia.ssalibrary.core.BaseApplication;
import com.smartsoftasia.ssalibrary.helper.Logger;

import java.util.Date;

/**
 * Created by Nott on 17/10/2559.
 * dtm-iom-android
 */

public class DTMFirebaseMessagingService extends FirebaseMessagingService {
  public static final String TAG = "FCMMessaging";
  public static final String ARG_MESSAGE = "arg_message";
  private static final String ALERT = "alert";
  private static final int NOTIFICATION_ID = 0;

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    Logger.d(TAG, "MessageReceived " + new Date().toString());

    if (remoteMessage == null) {
      return;
    }

    if (remoteMessage.getData() == null) {
      return;
    }

    String message = remoteMessage.getData().get(ALERT);

    if (TextUtils.isEmpty(message)) {
      return;
    }

    Logger.d(TAG, message);

    if (!((BaseApplication) getApplication()).getMonitor().isInForeground()) {
      sendNotification(message);
    } else {
      RxBus.getInstance().post(new AdminMessage(message));
    }
  }

  private void sendNotification(String message) {
    Intent intent = new Intent(this, HomeActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.putExtra(ARG_MESSAGE, message);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
        PendingIntent.FLAG_ONE_SHOT);

    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
        this).setSmallIcon(getNotificationIcon())
        .setContentTitle(getString(R.string.push_notification_dialog_title))
        .setContentText(message)
        .setAutoCancel(true)
        .setSound(defaultSoundUri)
        .setContentIntent(pendingIntent);

    NotificationManager notificationManager = (NotificationManager) getSystemService(
        Context.NOTIFICATION_SERVICE);

    notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
  }

  private int getNotificationIcon() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? R.drawable.dtm_icon_white : R.drawable.dtm_icon_white;
  }
}
