package com.iom.dtm.service.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import com.google.firebase.iid.FirebaseInstanceId;
import com.iom.dtm.core.AppConstant;
import com.iom.dtm.core.DTM;
import com.iom.dtm.dependency.component.ApplicationComponent;
import com.iom.dtm.dependency.component.DaggerFCMComponent;
import com.iom.dtm.dependency.component.FCMComponent;
import com.iom.dtm.presenter.RegisterFCMPresenter;
import com.iom.dtm.rest.form.RegisterTokenForm;
import com.smartsoftasia.ssalibrary.dependency.component.HasComponent;
import com.smartsoftasia.ssalibrary.helper.Logger;

import javax.inject.Inject;

/**
 * Created by Nott on 17/10/2559.
 * dtm-iom-android
 */

public class DTMRegistrationIntentService extends IntentService implements HasComponent<FCMComponent> {
  private static final String TAG = "RegIntentService";
  public static final String USER_ID = "user_id";

  private FCMComponent mComponent;
  @Inject
  RegisterFCMPresenter presenter;

  public DTMRegistrationIntentService() {
    super(TAG);
  }

  @Override
  public void onCreate() {
    super.onCreate();
    initializeInjector();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.destroy();
  }

  protected void initializeInjector() {
    this.getApplicationComponent().inject(this);
    this.mComponent = DaggerFCMComponent.builder()
        .applicationComponent(getApplicationComponent())
        .build();
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    Logger.d(TAG, "Refreshed token: " + FirebaseInstanceId.getInstance().getToken());

    RegisterTokenForm form = new RegisterTokenForm();
    form.setUdid(Settings.Secure.getString(getContentResolver(),
        Settings.Secure.ANDROID_ID));
    form.setToken(FirebaseInstanceId.getInstance().getToken());
    form.setOs(getOSInfo());
    form.setPlatform(AppConstant.ANDROID);
    presenter.sendToken(form, intent.getStringExtra(USER_ID));
  }

  @Override
  public FCMComponent getComponent() {
    return mComponent;
  }

  protected ApplicationComponent getApplicationComponent() {
    return ((DTM) getApplication()).getApplicationComponent();
  }

  private String getOSInfo() {
    return Build.BRAND + AppConstant.BLANK_SPACE + Build.MODEL + AppConstant.BLANK_SPACE +
        Build.VERSION.RELEASE;
  }
}
