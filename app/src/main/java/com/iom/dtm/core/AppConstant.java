package com.iom.dtm.core;

/**
 * Created by Nott on 9/20/2016 AD.
 * DTM
 */

public class AppConstant {
  public static final String TAG = "AppConstant";

  public static final String EMPTY = "";
  public static final String HYPHEN = "-";
  public static final String BLANK_SPACE = " ";
  public static final String HYPHEN_WITH_SPACE = " - ";
  public static final String COMMA = ",";
  public static final String UNDERSCORE = "_";
  public static final String LINE_SEPARATOR = "\n";
  public static final String ANDROID = "android";
  public static final int UNAUTHORIZED_RESPONSE = 401;

  public static final long SEARCH_DELAY = 500; // In ms

  public static String getStringWithBracket(String temp) {
    return "(" + temp + ")";
  }
}
