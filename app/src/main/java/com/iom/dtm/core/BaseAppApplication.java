package com.iom.dtm.core;

import com.smartsoftasia.ssalibrary.core.BaseApplication;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BaseAppApplication extends BaseApplication {
  private static final String TAG = "BaseAppApplication";
}
