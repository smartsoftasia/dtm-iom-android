package com.iom.dtm.core;

import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import com.iom.dtm.BuildConfig;
import com.iom.dtm.database.realm.RealmManager;
import com.iom.dtm.dependency.component.ApplicationComponent;
import com.iom.dtm.dependency.component.DaggerApplicationComponent;
import com.iom.dtm.dependency.module.ApplicationModule;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.smartsoftasia.ssalibrary.helper.Logger;
import javax.inject.Inject;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DTM extends BaseAppApplication {
  public static final String TAG = "DTM";
  private ApplicationComponent applicationComponent;

  @Inject
  QuestionnaireController questionnaireController;

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  @Override
  public void onCreate() {
    super.onCreate();

    initStrictMode();
    initLogger();
    initDatabase();
  }

  @Override
  protected void initializeInjector() {
    this.applicationComponent = DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .build();
    this.getApplicationComponent().inject(this);
    questionnaireController = this.applicationComponent.questionnaireController();
  }

  private void initStrictMode() {

  }

  private void initLogger() {
    Logger.init(BuildConfig.DEBUG);
  }

  private void initDatabase() {
    RealmManager.init(this);
  }

  public ApplicationComponent getApplicationComponent() {
    return this.applicationComponent;
  }
}
