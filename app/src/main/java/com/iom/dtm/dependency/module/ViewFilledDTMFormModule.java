package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.DownloadReportInteractor;
import com.iom.dtm.domain.interactor.GetReportViaEmailInteractor;
import com.iom.dtm.presenter.ViewFilledDTMFormPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 6/20/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class ViewFilledDTMFormModule {
  private static final String TAG = "ViewFilledDTMFormModule";

  @Provides
  @PerActivity
  ViewFilledDTMFormPresenter provideViewFilledDTMFormPresenter(
      GetReportViaEmailInteractor getReportViaEmailInteractor, SharedPreference sharedPreference) {
    return new ViewFilledDTMFormPresenter(getReportViaEmailInteractor, sharedPreference);
  }

  @Provides
  @PerActivity
  GetReportViaEmailInteractor provideGetReportViaEmailInteractor(ThreadExecutor threadExecutor,
                                                                 PostExecutionThread postExecutionThread,
                                                                 RestClient restClient) {
    return new GetReportViaEmailInteractor(threadExecutor, postExecutionThread, restClient);
  }
}
