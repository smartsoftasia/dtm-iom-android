package com.iom.dtm.dependency.module;

import android.app.Activity;

import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class ActivityModule {

  private final Activity activity;

  public ActivityModule(Activity activity) {
    this.activity = activity;
  }

  @Provides
  @PerActivity
  Activity activity() {
    return this.activity;
  }
}
