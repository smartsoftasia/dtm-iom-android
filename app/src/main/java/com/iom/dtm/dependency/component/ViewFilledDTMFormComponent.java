package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.EnumeratorProfileModule;
import com.iom.dtm.dependency.module.ViewFilledDTMFormModule;
import com.iom.dtm.view.fragment.EnumeratorProfileFragment;
import com.iom.dtm.view.fragment.ViewFilledDTMFormFragment;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, ViewFilledDTMFormModule.class })
public interface ViewFilledDTMFormComponent {

  void inject(ViewFilledDTMFormFragment viewFilledDTMFormFragment);
}
