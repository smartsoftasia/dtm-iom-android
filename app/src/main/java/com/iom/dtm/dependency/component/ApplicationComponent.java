package com.iom.dtm.dependency.component;

import android.content.Context;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.iom.dtm.core.DTM;
import com.iom.dtm.dependency.module.ApplicationModule;
import com.iom.dtm.domain.controller.LanguageController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.rest.RequestInterceptor;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.service.fcm.DTMRegistrationIntentService;
import com.iom.dtm.view.activity.BaseAppActivity;
import com.iom.dtm.view.activity.DTMFormDetailsActivity;
import com.iom.dtm.view.activity.EditProfileActivity;
import com.iom.dtm.view.activity.EnumeratorProfileActivity;
import com.iom.dtm.view.activity.HomeActivity;
import com.iom.dtm.view.activity.LaunchAppActivity;
import com.iom.dtm.view.activity.ResetPasswordActivity;
import com.iom.dtm.view.activity.SelectLocationActivity;
import com.iom.dtm.view.activity.SignInActivity;
import com.iom.dtm.view.activity.SignUpActivity;
import com.iom.dtm.view.activity.ViewEnumeratorReportActivity;
import com.iom.dtm.view.activity.ViewFilledDTMFormActivity;
import com.iom.dtm.view.activity.ViewReportHomeActivity;
import com.smartsoftasia.ssalibrary.bus.ApplicationBus;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import com.smartsoftasia.ssalibrary.domain.executor.UiThreadHandler;
import com.smartsoftasia.ssalibrary.view.activity.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})

public interface ApplicationComponent {

  void inject(DTM dtm);

  void inject(BaseAppActivity baseAppActivity);

  void inject(BaseActivity baseActivity);

  void inject(SignInActivity signInActivity);

  void inject(SignUpActivity signUpActivity);

  void inject(ResetPasswordActivity resetPasswordActivity);

  void inject(HomeActivity homeActivity);

  void inject(EnumeratorProfileActivity enumeratorProfileActivity);

  void inject(ViewFilledDTMFormActivity viewFilledDTMFormActivity);

  void inject(LaunchAppActivity launchAppActivity);

  void inject(ViewEnumeratorReportActivity viewEnumeratorReportActivity);

  void inject(EditProfileActivity editProfileActivity);

  void inject(DTMFormDetailsActivity dtmFormDetailsActivity);

  void inject(ViewReportHomeActivity viewReportHomeActivity);

  void inject(SelectLocationActivity selectLocationActivity);

  void inject(DTMRegistrationIntentService dtmRegistrationIntentService);

  Context context();

  ThreadExecutor threadExecutor();

  PostExecutionThread postExecutionThread();

  UiThreadHandler uiThreadHandler();

  ApplicationBus applicationBus();

  SharedPreference sharedPreference();

  LanguageController languageController();

  RestClient restClient();

  RequestInterceptor requestInterceptor();

  QuestionnaireController questionnaireController();

  AmazonS3Client amazonS3Client();

  TransferUtility transferUtility();

  CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider();
}
