package com.iom.dtm.dependency.module;

import android.content.Context;

import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.AnswerLiteInteractor;
import com.iom.dtm.domain.interactor.DeactivateEnumeratorInteractor;
import com.iom.dtm.domain.interactor.GetEnumeratorListInteractor;
import com.iom.dtm.domain.interactor.GetEventInteractor;
import com.iom.dtm.domain.interactor.GetQuestionnaireFromApiInteractor;
import com.iom.dtm.domain.interactor.GetTagsInteractor;
import com.iom.dtm.domain.interactor.GetUserFromAPIInteractor;
import com.iom.dtm.domain.interactor.GetUserInteractor;
import com.iom.dtm.domain.interactor.SearchEnumeratorInteractor;
import com.iom.dtm.presenter.DTMFormPresenter;
import com.iom.dtm.presenter.EnumeratorsPresenter;
import com.iom.dtm.presenter.HomePresenter;
import com.iom.dtm.presenter.NavigationDrawerPresenter;
import com.iom.dtm.presenter.QuestionnaireForGeneralPublicPresenter;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.view.adapter.NavigationAdapter;
import com.iom.dtm.view.model.NavigationDrawerModel;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

import java.util.ArrayList;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class HomeModule {
  /**
   * Interactor
   */
  SearchEnumeratorInteractor provideSearchEnumeratorInteractor(ThreadExecutor threadExecutor,
                                                               PostExecutionThread postExecutionThread,
                                                               RestClient restClient) {
    return new SearchEnumeratorInteractor(threadExecutor, postExecutionThread, restClient);
  }

  GetQuestionnaireFromApiInteractor provideGetQuestionnaireInteractor(ThreadExecutor threadExecutor,
                                                                      PostExecutionThread postExecutionThread,
                                                                      RestClient restClient,
                                                                      QuestionnaireController questionnaireController) {
    return new GetQuestionnaireFromApiInteractor(threadExecutor, postExecutionThread, restClient,
        questionnaireController);
  }

  GetEnumeratorListInteractor provideGetEnumeratorListInteractor(ThreadExecutor threadExecutor,
                                                                 PostExecutionThread postExecutionThread,
                                                                 RestClient restClient) {
    return new GetEnumeratorListInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  GetTagsInteractor provideGetTagsInteractor(ThreadExecutor threadExecutor,
                                             PostExecutionThread postExecutionThread,
                                             RestClient restClient) {
    return new GetTagsInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  AnswerLiteInteractor provideAnswerLiteInteractor(ThreadExecutor threadExecutor,
                                                   PostExecutionThread postExecutionThread,
                                                   RestClient restClient) {
    return new AnswerLiteInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  GetEventInteractor provideGetEventInteractor(ThreadExecutor threadExecutor,
                                               PostExecutionThread postExecutionThread,
                                               RestClient restClient) {
    return new GetEventInteractor(threadExecutor, postExecutionThread, restClient);
  }

  /**
   * Presenter
   */
  @Provides
  @PerActivity
  NavigationDrawerPresenter provideNavigationDrawerPresenter(GetUserInteractor getUserInteractor) {
    return new NavigationDrawerPresenter(getUserInteractor);
  }

  @Provides
  NavigationAdapter provideNavigationAdapter(Context context) {
    return new NavigationAdapter(new ArrayList<NavigationDrawerModel>(), context);
  }

  @Provides
  @PerActivity
  HomePresenter provideHomePresenter(GetUserFromAPIInteractor getUserFromAPIInteractor,
                                     GetUserInteractor getUserInteractor) {
    return new HomePresenter(getUserFromAPIInteractor, getUserInteractor);
  }

  @Provides
  @PerActivity
  EnumeratorsPresenter provideEnumeratorsPresenter(
      SearchEnumeratorInteractor searchEnumeratorInteractor,
      GetEnumeratorListInteractor getEnumeratorListInteractor,
      DeactivateEnumeratorInteractor deactivateEnumeratorInteractor) {
    return new EnumeratorsPresenter(searchEnumeratorInteractor, getEnumeratorListInteractor,
        deactivateEnumeratorInteractor);
  }

  @Provides
  @PerActivity
  DTMFormPresenter provideDTMFormPresenter(SharedPreference sharedPreference,
                                           GetQuestionnaireFromApiInteractor getQuestionnaireFromApiInteractor,
                                           QuestionnaireController questionnaireController,
                                           GetTagsInteractor getTagsInteractor,
                                           GetEventInteractor getEventInteractor) {
    return new DTMFormPresenter(sharedPreference, getQuestionnaireFromApiInteractor,
        questionnaireController, getTagsInteractor, getEventInteractor);
  }

  @Provides
  @PerActivity
  QuestionnaireForGeneralPublicPresenter provideQuestionnaireForGeneralPublicPresenter(
      SharedPreference sharedPreference,
      GetQuestionnaireFromApiInteractor getQuestionnaireFromApiInteractor,
      AnswerController answerController,
      AnswerLiteInteractor answerLiteInteractor) {
    return new QuestionnaireForGeneralPublicPresenter(sharedPreference,
        getQuestionnaireFromApiInteractor, answerController, answerLiteInteractor);
  }
}
