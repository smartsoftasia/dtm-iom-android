package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.interactor.SendDeviceTokenInteractor;
import com.iom.dtm.presenter.RegisterFCMPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 17/10/2559.
 * dtm-iom-android
 */
@Module
public class FCMModule {

  @Provides
  SendDeviceTokenInteractor provideSendDeviceTokenInteractor(ThreadExecutor threadExecutor,
                                                             PostExecutionThread postExecutionThread,
                                                             RestClient restClient) {
    return new SendDeviceTokenInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  RegisterFCMPresenter provideRegisterFCMPresenter(SendDeviceTokenInteractor sendDeviceTokenInteractor) {
    return new RegisterFCMPresenter(sendDeviceTokenInteractor);
  }
}
