package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.EditProfileModule;
import com.iom.dtm.dependency.module.EnumeratorProfileModule;
import com.iom.dtm.view.fragment.EditProfileFragment;
import com.iom.dtm.view.fragment.EnumeratorProfileFragment;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, EditProfileModule.class })
public interface EditProfileComponent {
  void inject(EditProfileFragment editProfileFragment);
}
