package com.iom.dtm.dependency.component;

import android.app.Activity;

import com.iom.dtm.dependency.module.ActivityModule;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
  Activity activity();

}
