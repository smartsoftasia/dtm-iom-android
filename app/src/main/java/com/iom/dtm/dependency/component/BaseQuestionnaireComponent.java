package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.BaseQuestionnaireModule;
import com.iom.dtm.dependency.module.DTMFormDetailsModule;
import com.iom.dtm.view.fragment.QuestionCategoryFragment;
import com.iom.dtm.view.fragment.question.QuestionDatePickerFragment;
import com.iom.dtm.view.fragment.question.QuestionDropDownFragment;
import com.iom.dtm.view.fragment.question.QuestionEmailFragment;
import com.iom.dtm.view.fragment.question.QuestionLocationFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberPickerFragment;
import com.iom.dtm.view.fragment.question.QuestionPhotoVideoFragment;
import com.iom.dtm.view.fragment.question.QuestionSingleChoiceFragment;
import com.iom.dtm.view.fragment.question.QuestionStringFragment;
import com.iom.dtm.view.fragment.question.QuestionTextFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupAdminFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupMaleFemaleFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupNoQuestionFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupOrderingNumberFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupRepeatFragment;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = {ActivityModule.class, BaseQuestionnaireModule.class})
public interface BaseQuestionnaireComponent {

  void inject(QuestionGroupFragment questionGroupFragment);

  void inject(QuestionGroupMaleFemaleFragment questionGroupMaleFemaleFragment);

  void inject(QuestionGroupRepeatFragment questionGroupRepeatFragment);

  void inject(QuestionGroupNoQuestionFragment questionGroupNoQuestionFragment);

  void inject(QuestionGroupOrderingNumberFragment questionGroupOrderingNumberFragment);

  void inject(QuestionGroupAdminFragment questionGroupAdminFragment);

  void inject(QuestionCategoryFragment questionCategoryFragment);

  void inject(QuestionStringFragment questionStringFragment);

  void inject(QuestionTextFragment questionTextFragment);

  void inject(QuestionPhotoVideoFragment questionPhotoVideoFragment);

  void inject(QuestionDatePickerFragment questionDatePickerFragment);

  void inject(QuestionNumberPickerFragment questionNumberPickerFragment);

  void inject(QuestionSingleChoiceFragment questionSingleChoiceFragment);

  void inject(QuestionNumberFragment questionNumberFragment);

  void inject(QuestionEmailFragment questionEmailFragment);

  void inject(QuestionLocationFragment questionLocationFragment);

  void inject(QuestionDropDownFragment questionDropDownFragment);
}
