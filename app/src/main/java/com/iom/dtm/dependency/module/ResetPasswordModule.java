package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.interactor.ResetPasswordInteractor;
import com.iom.dtm.presenter.ResetPasswordPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

@Module
public class ResetPasswordModule {

  @Provides
  @PerActivity
  ResetPasswordPresenter provideResetPasswordPresenter(
      ResetPasswordInteractor resetPasswordInteractor) {
    return new ResetPasswordPresenter(resetPasswordInteractor);
  }

  @PerActivity
  @Provides
  ResetPasswordInteractor provideResetPasswordInteractor(ThreadExecutor threadExecutor,
                                                         PostExecutionThread postExecutionThread, RestClient restClient) {
    return new ResetPasswordInteractor(threadExecutor, postExecutionThread, restClient);
  }

}