package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.interactor.DeactivateEnumeratorInteractor;
import com.iom.dtm.domain.interactor.EditEnumeratorProfileInteractor;
import com.iom.dtm.domain.interactor.GetEnumeratorProfileInteractor;
import com.iom.dtm.presenter.EnumeratorProfilePresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@Module
public class EnumeratorProfileModule {

  @Provides
  @PerActivity
  GetEnumeratorProfileInteractor provideGetEnumeratorProfileInteractor(
      ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
      RestClient restClient) {
    return new GetEnumeratorProfileInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  EditEnumeratorProfileInteractor provideEditEnumeratorProfileInteractor(
      ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
      RestClient restClient) {
    return new EditEnumeratorProfileInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  EnumeratorProfilePresenter provideEnumeratorProfilePresenter(
      GetEnumeratorProfileInteractor getEnumeratorProfileInteractor,
      EditEnumeratorProfileInteractor editEnumeratorProfileInteractor,
      DeactivateEnumeratorInteractor deactivateEnumeratorInteractor) {
    return new EnumeratorProfilePresenter(getEnumeratorProfileInteractor,
        editEnumeratorProfileInteractor, deactivateEnumeratorInteractor);
  }
}
