package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetAddressListInteractor;
import com.iom.dtm.domain.interactor.GetQuestionnaireFromApiByIdInteractor;
import com.iom.dtm.domain.interactor.PostGetViewReportInteractor;
import com.iom.dtm.presenter.ViewReportHomePresenter;
import com.iom.dtm.presenter.viewreport.ViewReportSelectPeriodTimePresenter;
import com.iom.dtm.presenter.viewreport.ViewReportSelectQuestionPresenter;
import com.iom.dtm.presenter.viewreport.ViewReportSummaryPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@Module
public class ViewReportHomeModule {

  @Provides
  @PerActivity
  ViewReportHomePresenter provideViewReportHomePresenter() {
    return new ViewReportHomePresenter();
  }

  @Provides
  @PerActivity
  ViewReportSelectPeriodTimePresenter provideViewReportSelectPeriodTimePresenter() {
    return new ViewReportSelectPeriodTimePresenter();
  }

  @Provides
  @PerActivity
  PostGetViewReportInteractor providePostGetViewReportInteractor(ThreadExecutor threadExecutor,
                                                                 PostExecutionThread postExecutionThread,
                                                                 RestClient restClient) {
    return new PostGetViewReportInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  GetAddressListInteractor provideGetCountryListInteractor(ThreadExecutor threadExecutor,
                                                           PostExecutionThread postExecutionThread,
                                                           RestClient restClient) {
    return new GetAddressListInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  GetQuestionnaireFromApiByIdInteractor provideGetQuestionnaireFromApiByIdInteractor(ThreadExecutor threadExecutor,
                                                                                     PostExecutionThread postExecutionThread,
                                                                                     RestClient restClient) {
    return new GetQuestionnaireFromApiByIdInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  ViewReportSummaryPresenter provideViewReportSummaryPresenter(SharedPreference sharedPreference,
                                                               PostGetViewReportInteractor postGetViewReportInteractor) {
    return new ViewReportSummaryPresenter(sharedPreference, postGetViewReportInteractor);
  }

  @Provides
  @PerActivity
  ViewReportSelectQuestionPresenter provideViewReportSelectQuestionPresenter(GetQuestionnaireFromApiByIdInteractor getQuestionnaireFromApiByIdInteractor, SharedPreference sharedPreference) {
    return new ViewReportSelectQuestionPresenter(getQuestionnaireFromApiByIdInteractor, sharedPreference);
  }

}
