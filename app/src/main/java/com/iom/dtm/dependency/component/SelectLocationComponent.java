package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.SelectLocationModule;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, SelectLocationModule.class })
public interface SelectLocationComponent {
}
