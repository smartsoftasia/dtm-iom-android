package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.SignUpModule;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, SignUpModule.class })

public interface SignUpComponent {

}
