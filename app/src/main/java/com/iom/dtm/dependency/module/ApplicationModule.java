package com.iom.dtm.dependency.module;

import android.content.Context;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.iom.dtm.core.AppConfig;
import com.iom.dtm.core.DTM;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.PhotoVideoController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.UploadFileToS3Interactor;
import com.iom.dtm.rest.RequestInterceptor;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.services.amazon.s3.S3Uploader;
import com.smartsoftasia.ssalibrary.bus.ApplicationBus;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.JobExecutor;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import com.smartsoftasia.ssalibrary.domain.executor.UIThread;
import com.smartsoftasia.ssalibrary.domain.executor.UiTheadHandlerImpl;
import com.smartsoftasia.ssalibrary.domain.executor.UiThreadHandler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

  private final DTM application;

  public ApplicationModule(DTM application) {
    this.application = application;
  }

  @Provides
  @Singleton
  Context provideApplicationContext() {
    return this.application;
  }

  @Provides
  @Singleton
  ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
    return jobExecutor;
  }

  @Provides
  @Singleton
  PostExecutionThread providePostExecutionThread(UIThread uiThread) {
    return uiThread;
  }

  @Provides
  @Singleton
  UiThreadHandler provideUiThreadHandler(Context context) {
    return new UiTheadHandlerImpl(context);
  }

  @Provides
  @Singleton
  ApplicationBus provideApplicationBus() {
    return new ApplicationBus();
  }

  @Provides
  @Singleton
  SharedPreference provideSharedPreference(Context context) {
    return new SharedPreference(context);
  }

  @Provides
  @Singleton
  RestClient provideRestClient(SharedPreference sharedPreference, ApplicationBus applicationBus,
                               RequestInterceptor requestInterceptor) {
    return new RestClient(sharedPreference, applicationBus, requestInterceptor);
  }

  @Provides
  @Singleton
  RequestInterceptor provideRequestInterceptor(SharedPreference sharedPreference) {
    return new RequestInterceptor(sharedPreference);
  }

  @Provides
  @Singleton
  QuestionnaireController provideQuestionnaireController(SharedPreference sharedPreference,
                                                         Context context) {
    return new QuestionnaireController(sharedPreference, context);
  }

  @Provides
  @Singleton
  AnswerController provideAnswerController() {
    return new AnswerController();
  }

  @Provides
  @Singleton
  PhotoVideoController providePhotoVideoController() {
    return new PhotoVideoController();
  }

  @Provides
  @Singleton
  CognitoCachingCredentialsProvider provideCognitoCachingCredentialsProvider(Context context) {
    return new CognitoCachingCredentialsProvider(context, AppConfig.S3_KEY, // Identity Pool ID
        Regions.US_WEST_2 // Region
    );
  }

  @Provides
  @Singleton
  AmazonS3Client provideAmazonS3Client(
      CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider) {
    return new AmazonS3Client(cognitoCachingCredentialsProvider);
  }

  @Provides
  @Singleton
  TransferUtility provideTransferUtility(AmazonS3Client amazonS3Client, Context context) {
    return new TransferUtility(amazonS3Client, context);
  }

  @Provides
  @PerActivity
  S3Uploader provideS3Uploader(TransferUtility transferUtility) {
    return new S3Uploader(transferUtility);
  }
}
