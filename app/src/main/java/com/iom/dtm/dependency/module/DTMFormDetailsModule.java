package com.iom.dtm.dependency.module;

import android.content.Context;
import com.iom.dtm.dependency.component.BaseQuestionnaireComponent;
import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.PhotoVideoController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.AnswerInteractor;
import com.iom.dtm.domain.interactor.CompressImageInteractor;
import com.iom.dtm.domain.interactor.GetImageInteractor;
import com.iom.dtm.domain.interactor.GetQuestionnaireFromDatabaseInteractor;
import com.iom.dtm.domain.interactor.GetVideoInteractor;
import com.iom.dtm.domain.interactor.UploadFileToS3Interactor;
import com.iom.dtm.presenter.DTMFormDetailsPresenter;
import com.iom.dtm.presenter.QuestionCategoryPresenter;
import com.iom.dtm.presenter.SummaryDTMFormPresenter;
import com.iom.dtm.presenter.question.QuestionDatePickerPresenter;
import com.iom.dtm.presenter.question.QuestionEmailPresenter;
import com.iom.dtm.presenter.question.QuestionLocationPresenter;
import com.iom.dtm.presenter.question.QuestionNumberPickerPresenter;
import com.iom.dtm.presenter.question.QuestionNumberPresenter;
import com.iom.dtm.presenter.question.QuestionPhotoVideoPresenter;
import com.iom.dtm.presenter.question.QuestionSingleChoicePresenter;
import com.iom.dtm.presenter.question.QuestionStringPresenter;
import com.iom.dtm.presenter.question.QuestionTextPresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupMaleFemalePresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupOrderingNumberPresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupPresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupRepeatPresenter;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.services.amazon.s3.S3Uploader;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class DTMFormDetailsModule extends BaseQuestionnaireModule {

  @Provides
  @PerActivity
  DTMFormDetailsPresenter provideDTMFormDetailsPresenter(SharedPreference sharedPreference,
                                                         GetQuestionnaireFromDatabaseInteractor getQuestionnaireFromDatabaseInteractor,
                                                         AnswerController answerController,
                                                         PhotoVideoController photoVideoController) {
    return new DTMFormDetailsPresenter(sharedPreference, getQuestionnaireFromDatabaseInteractor,
        answerController, photoVideoController);
  }

  @Provides
  @PerActivity
  AnswerInteractor provideAnswerInteractor(ThreadExecutor threadExecutor,
                                           PostExecutionThread postExecutionThread,
                                           RestClient restClient) {
    return new AnswerInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  GetQuestionnaireFromDatabaseInteractor provideGetQuestionnaireFromDatabaseInteractor(
      ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
      QuestionnaireController questionnaireController) {
    return new GetQuestionnaireFromDatabaseInteractor(threadExecutor, postExecutionThread,
        questionnaireController);
  }

  @Provides
  @PerActivity
  UploadFileToS3Interactor provideUploadFileToS3Interactor(ThreadExecutor threadExecutor,
                                                           PostExecutionThread postExecutionThread,
                                                           S3Uploader s3Uploader) {
    return new UploadFileToS3Interactor(threadExecutor, postExecutionThread, s3Uploader);
  }

  @Provides
  @PerActivity
  GetImageInteractor provideGetImageInteractor(ThreadExecutor threadExecutor,
                                               PostExecutionThread postExecutionThread,
                                               Context context) {
    return new GetImageInteractor(threadExecutor, postExecutionThread, context);
  }

  @Provides
  @PerActivity
  GetVideoInteractor provideGetVideoInteractor(ThreadExecutor threadExecutor,
                                               PostExecutionThread postExecutionThread,
                                               Context context) {
    return new GetVideoInteractor(threadExecutor, postExecutionThread, context);
  }

  @Provides
  @PerActivity
  CompressImageInteractor provideCompressImageInteractor(ThreadExecutor threadExecutor,
                                                         PostExecutionThread postExecutionThread,
                                                         Context context) {
    return new CompressImageInteractor(threadExecutor, postExecutionThread, context);
  }
}
