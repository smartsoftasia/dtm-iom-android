package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.controller.AnswerController;
import com.iom.dtm.domain.controller.PhotoVideoController;
import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.AnswerInteractor;
import com.iom.dtm.domain.interactor.GetAddressListInteractor;
import com.iom.dtm.domain.interactor.GetImageInteractor;
import com.iom.dtm.domain.interactor.GetVideoInteractor;
import com.iom.dtm.domain.interactor.UploadFileToS3Interactor;
import com.iom.dtm.presenter.QuestionCategoryPresenter;
import com.iom.dtm.presenter.SummaryDTMFormPresenter;
import com.iom.dtm.presenter.question.QuestionDatePickerPresenter;
import com.iom.dtm.presenter.question.QuestionDropDownPresenter;
import com.iom.dtm.presenter.question.QuestionEmailPresenter;
import com.iom.dtm.presenter.question.QuestionLocationPresenter;
import com.iom.dtm.presenter.question.QuestionNumberPickerPresenter;
import com.iom.dtm.presenter.question.QuestionNumberPresenter;
import com.iom.dtm.presenter.question.QuestionPhotoVideoPresenter;
import com.iom.dtm.presenter.question.QuestionSingleChoicePresenter;
import com.iom.dtm.presenter.question.QuestionStringPresenter;
import com.iom.dtm.presenter.question.QuestionTextPresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupAdminPresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupMaleFemalePresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupOrderingNumberPresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupPresenter;
import com.iom.dtm.presenter.questiongroup.QuestionGroupRepeatPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class BaseQuestionnaireModule {

  @Provides
  QuestionGroupPresenter provideQuestionGroupPresenter() {
    return new QuestionGroupPresenter();
  }

  @Provides
  QuestionCategoryPresenter provideQuestionCategoryPresenter() {
    return new QuestionCategoryPresenter();
  }

  @Provides
  QuestionStringPresenter provideQuestionStringPresenter(AnswerController answerController) {
    return new QuestionStringPresenter(answerController);
  }

  @Provides
  QuestionDropDownPresenter provideQuestionDropDownPresenter(AnswerController answerController) {
    return new QuestionDropDownPresenter(answerController);
  }

  @Provides
  QuestionEmailPresenter provideQuestionEmailPresenter(AnswerController answerController) {
    return new QuestionEmailPresenter(answerController);
  }

  @Provides
  QuestionDatePickerPresenter provideQuestionDatePickerPresenter(
      AnswerController answerController) {
    return new QuestionDatePickerPresenter(answerController);
  }

  @Provides
  QuestionPhotoVideoPresenter provideQuestionPhotoVideoPresenter(AnswerController answerController,
                                                                 UploadFileToS3Interactor uploadFileToS3Interactor,
                                                                 PhotoVideoController photoVideoController,
                                                                 GetImageInteractor getImageInteractor,
                                                                 GetVideoInteractor getVideoInteractor) {
    return new QuestionPhotoVideoPresenter(answerController, photoVideoController,
        uploadFileToS3Interactor, getImageInteractor, getVideoInteractor);
  }

  @Provides
  QuestionTextPresenter provideQuestionTextPresenter(AnswerController answerController) {
    return new QuestionTextPresenter(answerController);
  }

  @Provides
  QuestionGroupAdminPresenter provideQuestionGroupAdminPresenter(
      GetAddressListInteractor getAddressListInteractor, AnswerController answerController, SharedPreference sharedPreference) {
    return new QuestionGroupAdminPresenter(getAddressListInteractor, answerController, sharedPreference);
  }

  @Provides
  QuestionNumberPresenter provideQuestionNumberPresenter(AnswerController answerController) {
    return new QuestionNumberPresenter(answerController);
  }

  @Provides
  QuestionSingleChoicePresenter provideQuestionSingleChoicePresenter(
      AnswerController answerController) {
    return new QuestionSingleChoicePresenter(answerController);
  }

  @Provides
  QuestionNumberPickerPresenter provideQuestionNumberPickerPresenter(
      AnswerController answerController) {
    return new QuestionNumberPickerPresenter(answerController);
  }

  @Provides
  QuestionGroupRepeatPresenter provideQuestionGroupRepeatPresenter(
      AnswerController answerController) {
    return new QuestionGroupRepeatPresenter(answerController);
  }

  @Provides
  QuestionGroupOrderingNumberPresenter provideQuestionGroupOrderingNumberPresenter(
      AnswerController answerController) {
    return new QuestionGroupOrderingNumberPresenter(answerController);
  }

  @Provides
  QuestionLocationPresenter provideQuestionLocationPresenter(AnswerController answerController) {
    return new QuestionLocationPresenter(answerController);
  }

  @Provides
  QuestionGroupMaleFemalePresenter provideQuestionGroupMaleFemalePresenter(
      AnswerController answerController) {
    return new QuestionGroupMaleFemalePresenter(answerController);
  }

  @Provides
  SummaryDTMFormPresenter provideSummaryDTMFormPresenter(
      QuestionnaireController questionnaireController, AnswerController answerController,
      AnswerInteractor answerInteractor, SharedPreference sharedPreference) {
    return new SummaryDTMFormPresenter(questionnaireController, answerController, answerInteractor,
        sharedPreference);
  }

  @Provides
  @PerActivity
  GetAddressListInteractor provideGetCountryListInteractor(ThreadExecutor threadExecutor,
                                                           PostExecutionThread postExecutionThread,
                                                           RestClient restClient) {
    return new GetAddressListInteractor(threadExecutor, postExecutionThread, restClient);
  }
}
