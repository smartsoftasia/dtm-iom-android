package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.FCMModule;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {FCMModule.class})
public interface FCMComponent {
}
