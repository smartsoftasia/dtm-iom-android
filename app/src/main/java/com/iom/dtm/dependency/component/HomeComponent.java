package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.HomeModule;
import com.iom.dtm.view.fragment.DTMFormFragment;
import com.iom.dtm.view.fragment.EnumeratorsFragment;
import com.iom.dtm.view.fragment.AboutFragment;
import com.iom.dtm.view.fragment.HomeFragment;
import com.iom.dtm.view.fragment.NavigationDrawerFragment;
import com.iom.dtm.view.fragment.QuestionnaireForGeneralPublicFragment;
import com.iom.dtm.view.fragment.ViewReportFragment;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, HomeModule.class })
public interface HomeComponent extends BaseQuestionnaireComponent{

  void inject(NavigationDrawerFragment navigationDrawerFragment);

  void inject(DTMFormFragment dtmFormFragment);

  void inject(EnumeratorsFragment enumeratorsFragment);

  void inject(AboutFragment aboutFragment);

  void inject(QuestionnaireForGeneralPublicFragment questionnaireForGeneralPublicFragment);

  void inject(ViewReportFragment viewReportFragment);

  void inject(HomeFragment homeFragment);
}
