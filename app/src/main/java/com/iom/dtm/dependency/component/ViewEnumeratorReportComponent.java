package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.ViewEnumeratorReportModule;
import com.iom.dtm.dependency.module.ViewFilledDTMFormModule;
import com.iom.dtm.view.activity.ViewEnumeratorReportActivity;
import com.iom.dtm.view.fragment.ViewEnumeratorReportFragment;
import com.iom.dtm.view.fragment.ViewFilledDTMFormFragment;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, ViewEnumeratorReportModule.class })
public interface ViewEnumeratorReportComponent {

  void inject(ViewEnumeratorReportFragment viewEnumeratorReportFragment);
}
