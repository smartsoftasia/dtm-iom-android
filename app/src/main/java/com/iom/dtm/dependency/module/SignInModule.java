package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GoogleSignInInteractor;
import com.iom.dtm.domain.interactor.SignInInteractor;
import com.iom.dtm.domain.interactor.FacebookSignInInteractor;
import com.iom.dtm.presenter.SignInPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class SignInModule {

  @Provides
  @PerActivity
  SignInPresenter provideSignInPresenter(SignInInteractor signInInteractor,
                                         FacebookSignInInteractor facebookSignInInteractor,
                                         GoogleSignInInteractor googleSignInInteractor,
                                         SharedPreference sharedPreference) {
    return new SignInPresenter(signInInteractor, facebookSignInInteractor, googleSignInInteractor,
        sharedPreference);
  }

  @Provides
  @PerActivity
  SignInInteractor provideSignInInteractor(ThreadExecutor threadExecutor,
                                           PostExecutionThread postExecutionThread,
                                           RestClient restClient,
                                           SharedPreference sharedPreference) {
    return new SignInInteractor(threadExecutor, postExecutionThread, restClient, sharedPreference);
  }
}
