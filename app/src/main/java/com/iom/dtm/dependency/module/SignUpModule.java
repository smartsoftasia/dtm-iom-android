package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.GetUserInteractor;
import com.iom.dtm.domain.interactor.SignUpInteractor;
import com.iom.dtm.presenter.SignUpPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class SignUpModule {
  @Provides
  @PerActivity
  SignUpPresenter provideSignUpPresenter(SignUpInteractor signUpInteractor,
                                         GetUserInteractor getUserInteractor,
                                         SharedPreference sharedPreference) {
    return new SignUpPresenter(signUpInteractor, getUserInteractor, sharedPreference);
  }

  @Provides
  @PerActivity
  SignUpInteractor provideSignUpInteractor(ThreadExecutor threadExecutor,
                                           PostExecutionThread postExecutionThread,
                                           RestClient restClient,
                                           SharedPreference sharedPreference) {
    return new SignUpInteractor(threadExecutor, postExecutionThread, restClient, sharedPreference);
  }
}
