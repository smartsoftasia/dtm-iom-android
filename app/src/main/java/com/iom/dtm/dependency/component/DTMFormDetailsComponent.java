package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.DTMFormDetailsModule;
import com.iom.dtm.dependency.module.SignInModule;
import com.iom.dtm.view.fragment.DTMFormDetailsFragment;
import com.iom.dtm.view.fragment.QuestionCategoryFragment;
import com.iom.dtm.view.fragment.SummaryDTMFormFragment;
import com.iom.dtm.view.fragment.question.QuestionDatePickerFragment;
import com.iom.dtm.view.fragment.question.QuestionEmailFragment;
import com.iom.dtm.view.fragment.question.QuestionLocationFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberFragment;
import com.iom.dtm.view.fragment.question.QuestionNumberPickerFragment;
import com.iom.dtm.view.fragment.question.QuestionPhotoVideoFragment;
import com.iom.dtm.view.fragment.question.QuestionSingleChoiceFragment;
import com.iom.dtm.view.fragment.question.QuestionStringFragment;
import com.iom.dtm.view.fragment.question.QuestionTextFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupMaleFemaleFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupNoQuestionFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupOrderingNumberFragment;
import com.iom.dtm.view.fragment.questiongroup.QuestionGroupRepeatFragment;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, DTMFormDetailsModule.class })

public interface DTMFormDetailsComponent extends BaseQuestionnaireComponent {
  void inject(DTMFormDetailsFragment dtmFormDetailsFragment);

  void inject(SummaryDTMFormFragment summaryDTMFormFragment);
}
