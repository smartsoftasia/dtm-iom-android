package com.iom.dtm.dependency.module;

import android.content.Context;
import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.interactor.CompressImageInteractor;
import com.iom.dtm.domain.interactor.DeactivateProfileInteractor;
import com.iom.dtm.domain.interactor.EditUserProfileInteractor;
import com.iom.dtm.domain.interactor.GetImageInteractor;
import com.iom.dtm.domain.interactor.GetUserFromAPIInteractor;
import com.iom.dtm.domain.interactor.GetUserInteractor;
import com.iom.dtm.domain.interactor.ReplaceRoleSendToNewOneInteractor;
import com.iom.dtm.domain.interactor.UploadFileToS3Interactor;
import com.iom.dtm.domain.interactor.UserVerificationInteractor;
import com.iom.dtm.presenter.EditProfilePresenter;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.services.amazon.s3.S3Uploader;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@Module
public class EditProfileModule {

  @Provides
  @PerActivity
  EditProfilePresenter provideHomeProfilePresenter(
      GetUserFromAPIInteractor getUserFromAPIInteractor, GetUserInteractor getUserInteractor,
      EditUserProfileInteractor editUserProfileInteractor,
      DeactivateProfileInteractor deactivateProfileInteractor,
      UserVerificationInteractor userVerificationInteractor,
      ReplaceRoleSendToNewOneInteractor replaceRoleSendToNewOneInteractor,
      CompressImageInteractor compressImageInteractor,
      UploadFileToS3Interactor uploadFileToS3Interactor, GetImageInteractor getImageInteractor) {
    return new EditProfilePresenter(getUserFromAPIInteractor, getUserInteractor,
        editUserProfileInteractor, deactivateProfileInteractor, userVerificationInteractor,
        replaceRoleSendToNewOneInteractor, compressImageInteractor, uploadFileToS3Interactor,
        getImageInteractor);
  }

  @Provides
  @PerActivity
  EditUserProfileInteractor provideEditUserProfileInteractor(ThreadExecutor threadExecutor,
                                                             PostExecutionThread postExecutionThread,
                                                             RestClient restClient,
                                                             SharedPreference sharedPreference) {
    return new EditUserProfileInteractor(threadExecutor, postExecutionThread, restClient,
        sharedPreference);
  }

  @Provides
  @PerActivity
  DeactivateProfileInteractor provideDeactivateProfileInteractor(ThreadExecutor threadExecutor,
                                                                 PostExecutionThread postExecutionThread,
                                                                 RestClient restClient) {
    return new DeactivateProfileInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  UserVerificationInteractor provideReplaceRoleVerifyInteractor(ThreadExecutor threadExecutor,
                                                                PostExecutionThread postExecutionThread,
                                                                RestClient restClient) {
    return new UserVerificationInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  GetUserInteractor provideGetUserInteractor(ThreadExecutor threadExecutor,
                                             PostExecutionThread postExecutionThread,
                                             SharedPreference sharedPreference) {
    return new GetUserInteractor(threadExecutor, postExecutionThread, sharedPreference);
  }

  @Provides
  @PerActivity
  GetUserFromAPIInteractor provideGetUserFromAPIInteractor(ThreadExecutor threadExecutor,
                                                           PostExecutionThread postExecutionThread,
                                                           RestClient restClient,
                                                           SharedPreference sharedPreference) {
    return new GetUserFromAPIInteractor(threadExecutor, postExecutionThread, restClient,
        sharedPreference);
  }

  @Provides
  @PerActivity
  ReplaceRoleSendToNewOneInteractor provideReplaceRoleSendToNewOneInteractor(
      ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
      RestClient restClient) {
    return new ReplaceRoleSendToNewOneInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  UploadFileToS3Interactor provideUploadFileToS3Interactor(ThreadExecutor threadExecutor,
                                                           PostExecutionThread postExecutionThread,
                                                           S3Uploader s3Uploader) {
    return new UploadFileToS3Interactor(threadExecutor, postExecutionThread, s3Uploader);
  }

  @Provides
  @PerActivity
  GetImageInteractor provideGetImageInteractor(ThreadExecutor threadExecutor,
                                               PostExecutionThread postExecutionThread,
                                               Context context) {
    return new GetImageInteractor(threadExecutor, postExecutionThread, context);
  }

  @Provides
  @PerActivity
  CompressImageInteractor provideCompressImageInteractor(ThreadExecutor threadExecutor,
                                                         PostExecutionThread postExecutionThread,
                                                         Context context) {
    return new CompressImageInteractor(threadExecutor, postExecutionThread, context);
  }
}
