package com.iom.dtm.dependency.module;

import com.iom.dtm.domain.interactor.GetEnumeratorReportListInteractor;
import com.iom.dtm.domain.interactor.GetReportViaEmailInteractor;
import com.iom.dtm.domain.interactor.SearchReportInteractor;
import com.iom.dtm.presenter.ViewEnumeratorReportPresenter;
import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nott on 6/20/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@Module
public class ViewEnumeratorReportModule {
  private static final String TAG = "ViewFilledDTMFormModule";

  @Provides
  @PerActivity
  GetEnumeratorReportListInteractor provideGetEnumeratorReportListInteractor(ThreadExecutor threadExecutor,
                                                                             PostExecutionThread postExecutionThread,
                                                                             RestClient restClient) {
    return new GetEnumeratorReportListInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  SearchReportInteractor provideSearchReportInteractor(ThreadExecutor threadExecutor,
                                                       PostExecutionThread postExecutionThread,
                                                       RestClient restClient) {
    return new SearchReportInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  GetReportViaEmailInteractor provideGetReportViaEmailInteractor(ThreadExecutor threadExecutor,
                                                                 PostExecutionThread postExecutionThread,
                                                                 RestClient restClient) {
    return new GetReportViaEmailInteractor(threadExecutor, postExecutionThread, restClient);
  }

  @Provides
  @PerActivity
  ViewEnumeratorReportPresenter provideViewEnumeratorReportPresenter(SearchReportInteractor searchReportInteractor,
                                                                     GetEnumeratorReportListInteractor getEnumeratorReportListInteractor,
                                                                     GetReportViaEmailInteractor getReportViaEmailInteractor) {
    return new ViewEnumeratorReportPresenter(searchReportInteractor, getEnumeratorReportListInteractor, getReportViaEmailInteractor);
  }
}
