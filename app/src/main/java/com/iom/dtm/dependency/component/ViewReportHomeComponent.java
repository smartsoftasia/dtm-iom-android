package com.iom.dtm.dependency.component;

import com.iom.dtm.dependency.module.ActivityModule;
import com.iom.dtm.dependency.module.ViewReportHomeModule;
import com.iom.dtm.view.fragment.viewreport.ViewReportSelectPeriodTimeFragment;
import com.iom.dtm.view.fragment.viewreport.ViewReportSelectProvinceFragment;
import com.iom.dtm.view.fragment.viewreport.ViewReportSelectQuestionsFragment;
import com.iom.dtm.view.fragment.viewreport.ViewReportSummaryFragment;
import com.smartsoftasia.ssalibrary.dependency.component.PerActivity;
import dagger.Component;

/**
 * Created by Nott on 6/17/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
    modules = { ActivityModule.class, ViewReportHomeModule.class })
public interface ViewReportHomeComponent {

  void inject(ViewReportSelectPeriodTimeFragment viewReportSelectPeriodTimeFragment);

  void inject(ViewReportSelectProvinceFragment viewReportSelectProvinceFragment);

  void inject(ViewReportSelectQuestionsFragment viewReportSelectQuestionsFragment);

  void inject(ViewReportSummaryFragment viewReportSummaryFragment);
}
