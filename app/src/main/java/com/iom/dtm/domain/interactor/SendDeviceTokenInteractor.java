package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.RegisterTokenForm;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 9/26/2016 AD.
 * DTM
 */

public class SendDeviceTokenInteractor extends RestInteractor<Object, Object> {
  public static final String TAG = "GetAddressListInteractor";
  private RegisterTokenForm registerTokenForm;
  private String userId;


  @Inject
  public SendDeviceTokenInteractor(ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread, RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<Object, Object> mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getUserService().registerFCM(userId, registerTokenForm);
  }

  public void setRegisterTokenForm(RegisterTokenForm registerTokenForm) {
    this.registerTokenForm = registerTokenForm;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
