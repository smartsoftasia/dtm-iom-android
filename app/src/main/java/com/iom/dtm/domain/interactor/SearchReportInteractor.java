package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.model.Report;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 7/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SearchReportInteractor extends BasePaginationInteractor<BaseApiResult<List<Report>>, List<Report>> {
  public static final String TAG = "SearchReportInteractor";

  private String textQuery;
  private String enumeratorId;

  @Inject
  public SearchReportInteractor(ThreadExecutor threadExecutor,
                                PostExecutionThread postExecutionThread, RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<BaseApiResult<List<Report>>, List<Report>> mapper() {
    return new Func1<BaseApiResult<List<Report>>, List<Report>>() {
      @Override
      public List<Report> call(BaseApiResult<List<Report>> listBaseApiResult) {
        meta = listBaseApiResult.meta;
        return listBaseApiResult.t;
      }
    };
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getViewReportService().getEnumeratorReportFromQuery(enumeratorId, textQuery, String.valueOf(page)).map(mapper());
  }

  public void setTextQuery(String textQuery) {
    this.textQuery = textQuery;
  }

  public void setEnumeratorId(String enumeratorId) {
    this.enumeratorId = enumeratorId;
  }
}
