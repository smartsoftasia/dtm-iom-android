package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.mapper.UserMapper;
import com.iom.dtm.rest.model.UserApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 6/14/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetUserFromAPIInteractor extends RestInteractor<UserApiResult, User> {
  public static final String TAG = "GetUserFromAPIInteractor";
  protected SharedPreference mSharedPreference;

  @Inject
  public GetUserFromAPIInteractor(ThreadExecutor threadExecutor,
                                  PostExecutionThread postExecutionThread, RestClient restClient,
                                  SharedPreference sharedPreference) {
    super(threadExecutor, postExecutionThread, restClient);
    this.mSharedPreference = sharedPreference;
  }

  @Override
  protected Func1<UserApiResult, User> mapper() {
    return UserMapper.userFunc1;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getUserService()
        .getUser()
        .map(mapper())
        .doOnNext(mSharedPreference.writeUserAction());
  }
}
