package com.iom.dtm.domain.interactor;

import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import com.smartsoftasia.ssalibrary.domain.interactor.UseCase;

/**
 * Created by androiddev01 on 5/16/2016 AD.
 * BaseInteractor
 */
public abstract class BaseInteractor extends UseCase {
  public static final String TAG = "BaseInteractor";

  public BaseInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
  }
}
