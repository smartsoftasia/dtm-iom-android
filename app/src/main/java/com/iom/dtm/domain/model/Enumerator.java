package com.iom.dtm.domain.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.core.AppConstant;

import java.util.List;

/**
 * Created by Nott on 8/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Enumerator extends User {
  public static final String TAG = "Enumerator";

  @Expose
  @SerializedName("events")
  public List<Event> events;

  public String getEventFullString() {
    if (events == null || events.isEmpty()) {
      return AppConstant.HYPHEN;
    }
    String temp = "";
    for (int i = 0; i < events.size(); i++) {
      temp += events.get(i).title;
      if (i != events.size() - 1) {
        temp += ", ";
      }
    }
    return temp;
  }

  public Enumerator() {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeTypedList(this.events);
    dest.writeString(this.email);
    dest.writeString(this.firstName);
    dest.writeString(this.lastName);
    dest.writeString(this.password);
    dest.writeString(this.gender);
    dest.writeString(this.state);
    dest.writeString(this.profilePictureUrl);
    dest.writeString(this.googleId);
    dest.writeString(this.enumeratorsCount);
    dest.writeString(this.reportsCount);
    dest.writeString(this.facebookId);
    dest.writeString(this.organization);
    dest.writeString(this.position);
    dest.writeParcelable(this.address, flags);
    dest.writeString(this.userType);
    dest.writeString(this.id);
  }

  protected Enumerator(Parcel in) {
    super(in);
    this.events = in.createTypedArrayList(Event.CREATOR);
    this.email = in.readString();
    this.firstName = in.readString();
    this.lastName = in.readString();
    this.password = in.readString();
    this.gender = in.readString();
    this.state = in.readString();
    this.profilePictureUrl = in.readString();
    this.googleId = in.readString();
    this.enumeratorsCount = in.readString();
    this.reportsCount = in.readString();
    this.facebookId = in.readString();
    this.organization = in.readString();
    this.position = in.readString();
    this.address = in.readParcelable(Address.class.getClassLoader());
    this.userType = in.readString();
    this.id = in.readString();
  }

  public static final Creator<Enumerator> CREATOR = new Creator<Enumerator>() {
    @Override
    public Enumerator createFromParcel(Parcel source) {
      return new Enumerator(source);
    }

    @Override
    public Enumerator[] newArray(int size) {
      return new Enumerator[size];
    }
  };
}
