package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.model.Tag;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 8/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetTagsInteractor extends RestInteractor<BaseApiResult<List<Tag>>, List<Tag>> {
  public static final String TAG = "GetTagsInteractor";

  @Inject
  public GetTagsInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
                           RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<BaseApiResult<List<Tag>>, List<Tag>> mapper() {
    return new Func1<BaseApiResult<List<Tag>>, List<Tag>>() {
      @Override
      public List<Tag> call(BaseApiResult<List<Tag>> userEntity) {
        return userEntity.t;
      }
    };
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getQuestionService().getTag().map(mapper());
  }
}
