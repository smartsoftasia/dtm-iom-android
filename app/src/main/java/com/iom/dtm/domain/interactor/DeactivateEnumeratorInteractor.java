package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 6/21/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class DeactivateEnumeratorInteractor extends RestInteractor {
  public static final String TAG = "DeactivateEnumeratorInteractor";

  private String enumeratorId;

  @Inject
  public DeactivateEnumeratorInteractor(ThreadExecutor threadExecutor,
                                        PostExecutionThread postExecutionThread,
                                        RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1 mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getEnumeratorService().deactivate(enumeratorId);
  }

  public void setEnumeratorId(String enumeratorId) {
    this.enumeratorId = enumeratorId;
  }
}
