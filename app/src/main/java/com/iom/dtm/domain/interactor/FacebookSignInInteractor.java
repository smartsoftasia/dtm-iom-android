package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.SignInForm;
import com.iom.dtm.rest.form.UserSignInForm;
import com.iom.dtm.rest.model.UserApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by androiddev03 on 16/6/2559.
 */
public class FacebookSignInInteractor extends RestInteractor<UserApiResult, User> {
  public static final String TAG = "FacebookSignInInteractor";

  private String mFacebookId;
  private RestClient mRestClient;
  protected SharedPreference mSharedPreference;

  @Inject
  protected FacebookSignInInteractor(ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread, RestClient restClient,
                                     SharedPreference sharedPreference) {
    super(threadExecutor, postExecutionThread, restClient);
    this.mRestClient = restClient;
    this.mSharedPreference = sharedPreference;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return mRestClient.getUserService()
        .signInSocial(SignInForm.newFacebookSignInForm(mFacebookId))
        .map(mapper()).doOnNext(mSharedPreference.writeUserAction());
  }

  @Override
  protected Func1<UserApiResult, User> mapper() {
    return new Func1<UserApiResult, User>() {
      @Override
      public User call(UserApiResult userApiResult) {
        return userApiResult.t;
      }
    };
  }

  public FacebookSignInInteractor setFacebookId(String facebookId) {
    mFacebookId = facebookId;
    return this;
  }
}


