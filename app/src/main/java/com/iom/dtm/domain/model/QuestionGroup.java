package com.iom.dtm.domain.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.iom.dtm.domain.annotation.QuestionGroupType;

import java.util.List;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionGroup extends BaseModel {
  public static final String TAG = "QuestionGroup";

  public static final String REPEAT = "QuestionGroups::Repeat";
  public static final String MALE_FEMALE = "QuestionGroups::MaleFemale";
  public static final String NO_QUESTION = "QuestionGroups::NoQuestion";
  public static final String ORDERING_NUMBER = "QuestionGroups::Ordering";
  public static final String NONE = "";

  @Expose
  public String title;

  @Expose
  public String type;

  @Expose
  public List<Question> questions;

  @Override
  public int describeContents() {
    return 0;
  }

  public String getTitle() {
    return title;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.title);
    dest.writeString(this.type);
    dest.writeTypedList(this.questions);
    dest.writeString(this.id);
  }

  public QuestionGroup() {
  }

  protected QuestionGroup(Parcel in) {
    super(in);
    this.title = in.readString();
    this.type = in.readString();
    this.questions = in.createTypedArrayList(Question.CREATOR);
    this.id = in.readString();
  }

  @QuestionGroupType
  public String getType() {
    if (type == null) {
      return QuestionGroup.NONE;
    }
    return type;
  }

  public static final Creator<QuestionGroup> CREATOR = new Creator<QuestionGroup>() {
    @Override
    public QuestionGroup createFromParcel(Parcel source) {
      return new QuestionGroup(source);
    }

    @Override
    public QuestionGroup[] newArray(int size) {
      return new QuestionGroup[size];
    }
  };
}
