package com.iom.dtm.domain.subscriber.listener;

/**
 * Created by Nott on 9/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public interface GetImageListener {
  void getImageOnNext(String absolutePath);

  void getImageOnError(Throwable e);
}
