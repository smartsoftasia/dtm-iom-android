package com.iom.dtm.domain.subscriber;

import com.iom.dtm.rest.error.DTMError;
import com.iom.dtm.rest.model.ErrorApiResult;
import com.smartsoftasia.ssalibrary.domain.subscriber.DefaultSubscriber;
import com.smartsoftasia.ssalibrary.helper.Logger;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Nott on 5/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class DTMApiSubscriber<T> extends DefaultSubscriber<T> {
  public T object;

  @Override
  public void onCompleted() {

  }

  @Override
  public final void onError(Throwable e) {

    if (e instanceof HttpException) {
      try {
        DTMError error = new DTMError((HttpException) e);
        onApiError(error.getResponseCode(), error.getErrorApiResult());
      } catch (Exception e1) {
        Logger.e("DEBUG Mode:", e.toString());
        onAppError(e);
      }
    } else {
      Logger.e("DEBUG Mode:", e.toString());
      onAppError(e);
    }
  }

  @Override
  public void onNext(T t) {
    object = t;
  }

  public abstract void onApiError(int responseCode, ErrorApiResult errorApiResult);

  public abstract void onAppError(Throwable e);
}
