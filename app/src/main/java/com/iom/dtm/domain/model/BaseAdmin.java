package com.iom.dtm.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.core.AppConstant;

/**
 * Created by Nott on 10/4/2016 AD.
 * DTM
 */

public abstract class BaseAdmin extends BaseModel {
  public static final String TAG = "BaseAdmin";

  @Expose
  @SerializedName("name")
  public String name;

  @Expose
  @SerializedName("reference")
  public String reference;

  @Override
  public String toString() {
    return name;
  }

  public String displayAdmin() {
    return name + AppConstant.BLANK_SPACE + AppConstant.getStringWithBracket(reference);
  }
}
