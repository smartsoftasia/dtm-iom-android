package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 4/21/2016 AD.
 * BaseModel
 */
public abstract class BaseModel implements Parcelable {
  private static final String TAG = "BaseModel";

  public BaseModel() {
  }

  @Expose
  @SerializedName("id")
  public String id;

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {dest.writeString(this.id);}

  protected BaseModel(Parcel in) {this.id = in.readString();}
}
