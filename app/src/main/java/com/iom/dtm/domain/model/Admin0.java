package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Nott on 9/26/2016 AD.
 * DTM
 */

public class Admin0 extends BaseAdmin implements Parcelable {
  public static final String TAG = "Admin0";

  @Expose
  @SerializedName("level_1_administrators")
  public List<Admin1> admin1s;

  public void setName(String name) {
    this.name = name;
  }

  public Admin0() {}

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeTypedList(this.admin1s);
    dest.writeString(this.name);
    dest.writeString(this.reference);
    dest.writeString(this.id);
  }

  protected Admin0(Parcel in) {
    this.admin1s = in.createTypedArrayList(Admin1.CREATOR);
    this.name = in.readString();
    this.reference = in.readString();
    this.id = in.readString();
  }

  public static final Creator<Admin0> CREATOR = new Creator<Admin0>() {
    @Override
    public Admin0 createFromParcel(Parcel source) {return new Admin0(source);}

    @Override
    public Admin0[] newArray(int size) {return new Admin0[size];}
  };
}
