package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.SharedPreference;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;

/**
 * Created by Nott on 6/21/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetUserInteractor extends BaseInteractor {
  private static final String TAG = "GetUserInteractor";

  protected SharedPreference mSharedPreference;

  @Inject
  public GetUserInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
                           SharedPreference sharedPreference) {
    super(threadExecutor, postExecutionThread);
    this.mSharedPreference = sharedPreference;
  }

  @Override
  protected Observable buildUseCaseObservable() {
    return Observable.just(mSharedPreference.readUser());
  }
}
