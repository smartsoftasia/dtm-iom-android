package com.iom.dtm.domain.model;

import android.os.Parcel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 8/26/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Address extends BaseModel {
  public static final String TAG = "Address";

  @Expose
  @SerializedName("latitude")
  public Float latitude;

  @Expose
  @SerializedName("longitude")
  public Float longitude;

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeValue(this.latitude);
    dest.writeValue(this.longitude);
  }

  public Address() {}

  protected Address(Parcel in) {
    super(in);
    this.latitude = (Float) in.readValue(Float.class.getClassLoader());
    this.longitude = (Float) in.readValue(Float.class.getClassLoader());
  }

  public static final Creator<Address> CREATOR = new Creator<Address>() {
    @Override
    public Address createFromParcel(Parcel source) {return new Address(source);}

    @Override
    public Address[] newArray(int size) {return new Address[size];}
  };
}
