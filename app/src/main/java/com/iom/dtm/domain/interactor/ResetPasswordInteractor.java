package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.ResetPasswordForm;
import com.iom.dtm.rest.form.UserForm;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import com.smartsoftasia.ssalibrary.domain.interactor.UseCase;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

public class ResetPasswordInteractor extends RestInteractor {

  public static final String TAG = "ResetPasswordInteractor";
  public ResetPasswordForm resetPasswordForm;

  @Inject
  public ResetPasswordInteractor(ThreadExecutor threadExecutor,
                                 PostExecutionThread postExecutionThread, RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1 mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getUserService().resetPassword(new UserForm<>(resetPasswordForm));
  }

  public void setResetPasswordForm(ResetPasswordForm resetPasswordForm) {
    this.resetPasswordForm = resetPasswordForm;
  }
}


