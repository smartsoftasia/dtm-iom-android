package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Nott on 9/26/2016 AD.
 * DTM
 */

public class Admin2 extends BaseAdmin implements Parcelable {
  public static final String TAG = "Admin2";

  public Admin2() {}

  @Expose
  @SerializedName("level_3_administrators")
  public List<Admin3> admin3s;

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeTypedList(this.admin3s);
    dest.writeString(this.name);
    dest.writeString(this.reference);
    dest.writeString(this.id);
  }

  protected Admin2(Parcel in) {
    this.admin3s = in.createTypedArrayList(Admin3.CREATOR);
    this.name = in.readString();
    this.reference = in.readString();
    this.id = in.readString();
  }

  public static final Creator<Admin2> CREATOR = new Creator<Admin2>() {
    @Override
    public Admin2 createFromParcel(Parcel source) {return new Admin2(source);}

    @Override
    public Admin2[] newArray(int size) {return new Admin2[size];}
  };
}
