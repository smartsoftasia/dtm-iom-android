package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.model.Enumerator;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 7/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SearchEnumeratorInteractor
    extends BasePaginationInteractor<BaseApiResult<List<Enumerator>>, List<Enumerator>> {
  public static final String TAG = "SearchEnumeratorInteractor";

  private String keyQuery;

  @Inject
  public SearchEnumeratorInteractor(ThreadExecutor threadExecutor,
                                    PostExecutionThread postExecutionThread,
                                    RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<BaseApiResult<List<Enumerator>>, List<Enumerator>> mapper() {

    return new Func1<BaseApiResult<List<Enumerator>>, List<Enumerator>>() {
      @Override
      public List<Enumerator> call(BaseApiResult<List<Enumerator>> userApiResult) {
        meta = userApiResult.meta;
        return userApiResult.t;
      }
    };
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getEnumeratorService().searchEnumerators(keyQuery).map(mapper());
  }

  public void setKeyQuery(String keyQuery) {
    this.keyQuery = keyQuery;
  }
}
