package com.iom.dtm.domain.subscriber;

import com.iom.dtm.domain.subscriber.listener.GetImageListener;

/**
 * Created by Nott on 9/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetImageSubscriberTest extends AppSubscriber<String> {
  public static final String TAG = "GetImageSubscriberTest";

  public static GetImageSubscriberTest create(GetImageListener getImageListener) {
    GetImageSubscriberTest getImageSubscriberTest = new GetImageSubscriberTest();
    getImageSubscriberTest.setListener(getImageListener);
    return getImageSubscriberTest;
  }

  private GetImageListener listener;

  @Override
  public void onError(Throwable e) {
    if (listener != null) {
      listener.getImageOnError(e);
    }
  }

  @Override
  public void onNext(String s) {
    if (listener != null) {
      listener.getImageOnNext(s);
    }
  }

  public void setListener(GetImageListener listener) {
    this.listener = listener;
  }
}
