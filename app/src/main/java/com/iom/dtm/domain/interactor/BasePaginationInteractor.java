package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.Meta;
import com.iom.dtm.rest.model.Pagination;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

/**
 * Created by Nott on 8/24/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class BasePaginationInteractor<T, R> extends RestInteractor<T, R> {
  public static final String TAG = "BasePaginationInteractor";
  protected Meta meta;
  protected int page = 1;

  public BasePaginationInteractor(ThreadExecutor threadExecutor,
                                  PostExecutionThread postExecutionThread, RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  public boolean isNextPage() {
    if (meta == null) {
      return false;
    }

    Pagination pagination = meta.pagination;
    if (pagination.nextPage != null) {
      this.page++;
      return true;
    } else {
      return false;
    }
  }

  public void reset() {
    this.page = 1;
    this.meta = null;
  }
}
