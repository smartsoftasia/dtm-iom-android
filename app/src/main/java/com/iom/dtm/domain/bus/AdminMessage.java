package com.iom.dtm.domain.bus;

/**
 * Created by Nott on 20/10/2559.
 * dtm-iom-android
 */

public class AdminMessage {
  public static final String TAG = "AdminMessage";

  public String message;

  public AdminMessage(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
