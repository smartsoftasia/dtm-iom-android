package com.iom.dtm.domain.annotation;

import android.support.annotation.StringDef;
import com.iom.dtm.domain.model.QuestionGroup;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */

@StringDef(value = {
    QuestionGroup.REPEAT, QuestionGroup.MALE_FEMALE, QuestionGroup.NONE, QuestionGroup.NO_QUESTION,
    QuestionGroup.ORDERING_NUMBER
})
@Retention(RetentionPolicy.SOURCE)
public @interface QuestionGroupType {
}
