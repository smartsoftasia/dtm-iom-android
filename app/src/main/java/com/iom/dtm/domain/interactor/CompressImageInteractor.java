package com.iom.dtm.domain.interactor;

import android.content.Context;
import android.net.Uri;
import com.iom.dtm.core.AppConfig;
import com.smartsoftasia.rxcamerapicker.RxImageConverters;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;

/**
 * Created by Nott on 9/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class CompressImageInteractor extends BaseInteractor {
  public static final String TAG = "CompressImageInteractor";
  protected Context mContext;
  protected Uri mUri;

  @Inject
  public CompressImageInteractor(ThreadExecutor threadExecutor,
                                 PostExecutionThread postExecutionThread, Context context) {
    super(threadExecutor, postExecutionThread);
    this.mContext = context;
  }

  @Override
  protected Observable buildUseCaseObservable() {
    return RxImageConverters.uriToCompressImageFullPath(mContext, AppConfig.S3_FOLDER_NAME, mUri);
  }

  public void setUri(Uri uri) {
    this.mUri = uri;
  }
}
