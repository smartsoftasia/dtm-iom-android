package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Nott on 7/11/2559.
 * dtm-iom-android
 */

public class Report extends BaseModel implements Parcelable {
  public static final String TAG = "Report";

  @Expose
  public String title;

  @Expose
  @SerializedName("created_at")
  public Date createdAt;


  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.title);
    dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
    dest.writeString(this.id);
  }

  public Report() {
  }

  protected Report(Parcel in) {
    super(in);
    this.title = in.readString();
    long tmpCreatedAt = in.readLong();
    this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
    this.id = in.readString();
  }

  public static final Creator<Report> CREATOR = new Creator<Report>() {
    @Override
    public Report createFromParcel(Parcel source) {
      return new Report(source);
    }

    @Override
    public Report[] newArray(int size) {
      return new Report[size];
    }
  };
}
