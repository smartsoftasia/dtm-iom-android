package com.iom.dtm.domain.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Choice extends BaseModel {
  public static final String TAG = "Choice";

  @Expose
  public String title;

  @Expose
  @SerializedName("additional_required")
  public boolean additionalRequired;

  @Expose
  @SerializedName("relative_question_id")
  public Long relativeQuestionId;

  public Choice() {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.title);
    dest.writeByte(this.additionalRequired ? (byte) 1 : (byte) 0);
    dest.writeValue(this.relativeQuestionId);
    dest.writeString(this.id);
  }

  @Override
  public String toString() {
    return title;
  }

  protected Choice(Parcel in) {
    super(in);
    this.title = in.readString();
    this.additionalRequired = in.readByte() != 0;
    this.relativeQuestionId = (Long) in.readValue(Long.class.getClassLoader());
    this.id = in.readString();
  }

  public static final Creator<Choice> CREATOR = new Creator<Choice>() {
    @Override
    public Choice createFromParcel(Parcel source) {
      return new Choice(source);
    }

    @Override
    public Choice[] newArray(int size) {
      return new Choice[size];
    }
  };
}
