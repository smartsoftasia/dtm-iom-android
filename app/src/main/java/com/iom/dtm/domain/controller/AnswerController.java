package com.iom.dtm.domain.controller;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.iom.dtm.database.realm.RealmManager;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.database.realm.model.AnswerTimesRModel;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.domain.model.RelativeQuestion;
import com.iom.dtm.helper.JSONHelper;
import com.iom.dtm.rest.form.AnswerDetailForm;
import com.iom.dtm.rest.form.AttachmentForm;
import com.smartsoftasia.ssalibrary.helper.GsonHelper;
import com.smartsoftasia.ssalibrary.helper.Logger;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Nott on 8/18/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AnswerController {
  public static final String TAG = "AnswerController";
  public static HashMap<String, AnswerTimesRModel> timesRModelHashMap;
  private static final String UNDERSCORE = "_";
  private static HashMap<String, AnswerRModel> answerHashMap;

  @Inject
  public AnswerController() {
  }

  public void getInstance() {
    if (answerHashMap == null) {
      answerHashMap = new HashMap<>();
    }
    if (timesRModelHashMap == null) {
      timesRModelHashMap = new HashMap<>();
    }
  }

  public static HashMap<String, AnswerRModel> getAnswerHashMap() {
    return answerHashMap;
  }

  public void checkAnswerOnDatabase() {
    getInstance();
    RealmResults<AnswerRModel> answerRModels = RealmManager.getRealm()
        .where(AnswerRModel.class)
        .findAll();

    RealmResults<AnswerTimesRModel> timesRModels = RealmManager.getRealm()
        .where(AnswerTimesRModel.class)
        .findAll();

    for (int i = 0; i < answerRModels.size(); i++) {
      AnswerRModel rModel = answerRModels.get(i);
      AnswerRModel model = new AnswerRModel();
      model.setQuestionId(rModel.getQuestionId());
      model.setChoiceId(rModel.getChoiceId());
      model.setValue(rModel.getValue());
      model.setAnswersTimes(rModel.getAnswersTimes());
      answerHashMap.put(model.getQuestionId() + UNDERSCORE + model.getAnswersTimes(), model);
    }

    for (int i = 0; i < timesRModels.size(); i++) {
      AnswerTimesRModel rModel = timesRModels.get(i);
      AnswerTimesRModel model = new AnswerTimesRModel();
      model.setId(rModel.id);
      model.setAnswerTimes(rModel.answerTimes);
      timesRModelHashMap.put(rModel.id, model);
    }
  }

  public void clearAnswerSingleton() {
    getInstance();
    answerHashMap.clear();
    timesRModelHashMap.clear();
  }

  public void clearAnswerOnDatabase() {
    RealmManager.getRealm().executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.delete(AnswerRModel.class);
        realm.delete(AnswerTimesRModel.class);
      }
    });
  }

  public void addOrUpdateAnswer(String questionId, AnswerRModel answerRModel) {
    if (questionId == null || answerRModel == null) {
      return;
    }
    answerHashMap.put(questionId + UNDERSCORE + answerRModel.getAnswersTimes(), answerRModel);
  }

  public void updateAnswerTimes(String questionId, AnswerTimesRModel timesRModel) {
    if (questionId == null || timesRModel == null) {
      return;
    }
    timesRModelHashMap.put(questionId, timesRModel);
  }

  public AnswerRModel getAnswer(String questionId, int answerTime) {
    getInstance();
    return answerHashMap.get(questionId + UNDERSCORE + answerTime);
  }

  public void deleteAnswer(String questionId, int answerTime) {
    getInstance();
    answerHashMap.remove(questionId + UNDERSCORE + answerTime);
  }

  public void saveAnswerToRealm() {
    final List<AnswerRModel> values = new ArrayList<AnswerRModel>(answerHashMap.values());
    final List<AnswerTimesRModel> timeValues = new ArrayList<AnswerTimesRModel>(
        timesRModelHashMap.values());

    RealmManager.getRealm().executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.delete(AnswerRModel.class);
        for (int i = 0; i < values.size(); i++) {
          realm.copyToRealm(values.get(i));
        }
        Logger.d(TAG, "Answer has been saved " + values.size());
      }
    });

    RealmManager.getRealm().executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.delete(AnswerTimesRModel.class);
        for (int i = 0; i < timeValues.size(); i++) {
          realm.copyToRealm(timeValues.get(i));
        }
        Logger.d(TAG, "Answer times has been saved " + timeValues.size());
      }
    });
  }

  public List<AnswerDetailForm> getAnswerForm() {
    List<AnswerDetailForm> answerJsonForm = new ArrayList<>();
    for (int i = 0; i < getQuestionList().size(); i++) {
      Question question = getQuestionList().get(i);

      AnswerDetailForm answerDetailForm = new AnswerDetailForm();
      answerDetailForm.setQuestionId(question.id);

      if (timesRModelHashMap.containsKey(question.id)) {
        if (timesRModelHashMap.get(question.id).answerTimes == 1) {
          AnswerRModel rModel = answerHashMap.get(question.id + UNDERSCORE + "1");
          if (JSONHelper.isAttachmentJSONValid(rModel.getValue())) {
            answerDetailForm.setValue(JSONHelper.getAttachmentFormFromJson(rModel.getValue()));
          } else {
            answerDetailForm.setValue(
                TextUtils.isEmpty(rModel.getValue()) ? null : rModel.getValue());
          }
          answerDetailForm.setChoiceId(rModel.getChoiceId());
          answerJsonForm.add(answerDetailForm);
        } else {
          List<Object> values = new ArrayList<>();
          List<String> choices = new ArrayList<>();
          for (int j = 0; j < timesRModelHashMap.get(question.id).answerTimes; j++) {
            AnswerRModel rModel = answerHashMap.get(
                question.id + UNDERSCORE + String.valueOf(j + 1));
            if (rModel == null) {
              continue;
            }
            if (rModel.getValue() != null) {
              if (JSONHelper.isAttachmentJSONValid(rModel.getValue())) {
                values.add(JSONHelper.getAttachmentFormFromJson(rModel.getValue()));
              } else {
                values.add(TextUtils.isEmpty(rModel.getValue()) ? null : rModel.getValue());
              }
            }
            if (rModel.getChoiceId() != null) {
              choices.add(rModel.getChoiceId());
            }
          }

          if (values.size() == 0) {
            values = null;
          }
          if (choices.size() == 0) {
            choices = null;
          }
          answerDetailForm.setValues(values);
          answerDetailForm.setChoiceIds(choices);
          answerJsonForm.add(answerDetailForm);
        }
      } else {
        AnswerRModel rModel = answerHashMap.get(question.id + UNDERSCORE + "1");
        if (rModel == null) {
          continue;
        }
        if (JSONHelper.isAttachmentJSONValid(rModel.getValue())) {
          answerDetailForm.setValue(JSONHelper.getAttachmentFormFromJson(rModel.getValue()));
        } else {
          answerDetailForm.setValue(
              TextUtils.isEmpty(rModel.getValue()) ? null : rModel.getValue());
        }
        answerDetailForm.setChoiceId(rModel.getChoiceId());
        answerJsonForm.add(answerDetailForm);
      }
    }
    return answerJsonForm;
  }

  private List<Question> getQuestionList() {
    List<Question> questions = new ArrayList<>();
    Questionnaire questionnaire = QuestionnaireController.getQuestionnaire();
    for (int i = 0; i < questionnaire.questionCategories.size(); i++) {
      QuestionCategory category = questionnaire.questionCategories.get(i);
      for (int j = 0; j < category.questionGroupList.size(); j++) {
        QuestionGroup group = category.questionGroupList.get(j);
        for (int k = 0; k < group.questions.size(); k++) {
          questions.add(group.questions.get(k));
        }
      }
    }
    return questions;
  }
}
