package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.ViewReportForm;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 9/15/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class PostGetViewReportInteractor extends RestInteractor<Object, Object> {
  public static final String TAG = "PostGetViewReportInteractor";

  private ViewReportForm viewReportForm;

  @Inject
  public PostGetViewReportInteractor(ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread,
                                     RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<Object, Object> mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getViewReportService().getReport(viewReportForm);
  }

  public void setViewReportForm(ViewReportForm viewReportForm) {
    this.viewReportForm = viewReportForm;
  }
}
