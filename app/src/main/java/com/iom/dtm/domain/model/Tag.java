package com.iom.dtm.domain.model;

import android.os.Parcel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nott on 8/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Tag extends BaseModel {
  public static final String TAG = "Tag";

  @Expose
  @SerializedName("title")
  public String title;
  @Expose
  @SerializedName("additional_required")
  public boolean additionalRequired;

  public Tag() {}

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.title);
    dest.writeByte(this.additionalRequired ? (byte) 1 : (byte) 0);
    dest.writeString(this.id);
  }

  protected Tag(Parcel in) {
    super(in);
    this.title = in.readString();
    this.additionalRequired = in.readByte() != 0;
    this.id = in.readString();
  }

  public static final Creator<Tag> CREATOR = new Creator<Tag>() {
    @Override
    public Tag createFromParcel(Parcel source) {return new Tag(source);}

    @Override
    public Tag[] newArray(int size) {return new Tag[size];}
  };
}
