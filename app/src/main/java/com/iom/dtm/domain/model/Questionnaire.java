package com.iom.dtm.domain.model;

import android.os.Parcel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.List;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Questionnaire extends BaseModel {
  public static final String TAG = "Questionnaire";

  @Expose
  @SerializedName("published_at")
  public Date publishAt;

  @Expose
  @SerializedName("question_categories")
  public List<QuestionCategory> questionCategories;

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeLong(this.publishAt != null ? this.publishAt.getTime() : -1);
    dest.writeTypedList(this.questionCategories);
    dest.writeString(this.id);
  }

  public Questionnaire() {}

  protected Questionnaire(Parcel in) {
    super(in);
    long tmpPublishAt = in.readLong();
    this.publishAt = tmpPublishAt == -1 ? null : new Date(tmpPublishAt);
    this.questionCategories = in.createTypedArrayList(QuestionCategory.CREATOR);
    this.id = in.readString();
  }

  public static final Creator<Questionnaire> CREATOR = new Creator<Questionnaire>() {
    @Override
    public Questionnaire createFromParcel(Parcel source) {return new Questionnaire(source);}

    @Override
    public Questionnaire[] newArray(int size) {return new Questionnaire[size];}
  };
}
