package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.support.annotation.StringDef;

import com.google.gson.annotations.Expose;
import com.iom.dtm.domain.annotation.QuestionType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Question extends BaseModel {
  public static final String TAG = "Question";

  public static final String STRING = "Questions::String";
  public static final String DATE_PICKER = "Questions::DatePicker";
  public static final String NUMBER_PICKER = "Questions::NumberPicker";
  public static final String LOCATION = "Questions::Location";
  public static final String SINGLE_CHOICE = "Questions::SingleChoice";
  public static final String NUMBER = "Questions::Number";
  public static final String EMAIL = "Questions::Email";
  public static final String TEXT = "Questions::Text";
  public static final String DROP_DOWN = "Questions::DropDown";
  public static final String ATTACHMENT = "Questions::Attachment";
  public static final String ADMIN_1 = "Questions::AdminOne";
  public static final String ADMIN_2 = "Questions::AdminTwo";
  public static final String ADMIN_3 = "Questions::AdminThree";
  public static final String ADMIN_1_PRIORITY = "Questions::AdminThreePriority";

  @Expose
  public String title;

  @Expose
  public String type;

  @Expose
  public List<Choice> choices = new ArrayList<>();

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.title);
    dest.writeString(this.type);
    dest.writeTypedList(this.choices);
    dest.writeString(this.id);
  }

  public Question() {
  }

  protected Question(Parcel in) {
    super(in);
    this.title = in.readString();
    this.type = in.readString();
    this.choices = in.createTypedArrayList(Choice.CREATOR);
    this.id = in.readString();
  }

  @QuestionType
  public String getType() {
    return type;
  }

  public static final Creator<Question> CREATOR = new Creator<Question>() {
    @Override
    public Question createFromParcel(Parcel source) {
      return new Question(source);
    }

    @Override
    public Question[] newArray(int size) {
      return new Question[size];
    }
  };
}
