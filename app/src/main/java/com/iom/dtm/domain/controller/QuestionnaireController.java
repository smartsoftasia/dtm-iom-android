package com.iom.dtm.domain.controller;

import android.content.Context;
import com.iom.dtm.database.realm.RealmManager;
import com.iom.dtm.database.realm.mapper.QuestionnaireToRealmMapper;
import com.iom.dtm.database.realm.mapper.RealmToQuestionnaireMapper;
import com.iom.dtm.database.realm.mapper.TagsRealmMapper;
import com.iom.dtm.database.realm.model.AnswerRModel;
import com.iom.dtm.database.realm.model.AnswerTimesRModel;
import com.iom.dtm.database.realm.model.ChoiceRModel;
import com.iom.dtm.database.realm.model.QuestionCategoryRModel;
import com.iom.dtm.database.realm.model.QuestionGroupRModel;
import com.iom.dtm.database.realm.model.QuestionRModel;
import com.iom.dtm.database.realm.model.QuestionnaireRModel;
import com.iom.dtm.database.realm.model.TagRModel;
import com.iom.dtm.domain.model.Choice;
import com.iom.dtm.domain.model.Question;
import com.iom.dtm.domain.model.QuestionCategory;
import com.iom.dtm.domain.model.QuestionGroup;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.domain.model.Tag;
import com.smartsoftasia.ssalibrary.helper.Logger;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Nott on 8/3/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionnaireController {
  public static final String TAG = "QuestionnaireController";
  private SharedPreference mSharedPreference;
  private Context mContext;

  private static Questionnaire sQuestionnaire;
  private static List<Tag> sTags = new ArrayList<>();

  @Inject
  public QuestionnaireController(SharedPreference sharedPreference, Context context) {
    this.mSharedPreference = sharedPreference;
    this.mContext = context;
  }

  public static Questionnaire getQuestionnaire() {
    return sQuestionnaire;
  }

  public static void setQuestionnaire(Questionnaire questionnaire) {
    QuestionnaireController.sQuestionnaire = questionnaire;
  }

  /**
   * Checking questionnaire version and update
   *
   * @param questionnaire from API
   */
  public void checkQuestionnaireVersion(Questionnaire questionnaire, String eventId) {
    if (questionnaire == null) {
      return;
    }

    String questionnaireDate = questionnaire.publishAt.toString();
    String eventIdFromApp = mSharedPreference.readEventId();

    if (eventIdFromApp == null
        || !eventIdFromApp.equalsIgnoreCase(eventId)
        || isQuestionnaireHasBeenUpdated(questionnaireDate)) {
      setQuestionnaireCreatedDateLocal(questionnaireDate);
      addQuestionnaireToDatabase(questionnaire);
      setQuestionnaire(RealmToQuestionnaireMapper.transform(getQuestionnaireFromDatabase()));
      mSharedPreference.writeEventId(eventId);
      Logger.d(TAG, "Questionnaire: Updated from API");
    } else {
      setQuestionnaire(RealmToQuestionnaireMapper.transform(getQuestionnaireFromDatabase()));
      Logger.d(TAG, "Questionnaire: Used local database");
    }
  }

  /**
   * Save Tags on Database
   */
  public void saveTagsOnDatabase(final List<Tag> tags) {
    RealmManager.getRealm().executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.delete(TagRModel.class);
        realm.copyToRealm(TagsRealmMapper.transformToRealm(tags));
      }
    });
  }

  public static List<Tag> getTags() {
    return sTags;
  }

  /**
   * Set TAGs on Singleton
   */
  public void setTagsOnSingleton() {
    sTags = getTagsOnDatabase();
    Logger.d(TAG, "TAGs: " + sTags.size());
  }

  /**
   * Get Tags on Database
   */
  private List<Tag> getTagsOnDatabase() {
    List<Tag> tags;
    final List<TagRModel> rModels = new ArrayList<>();
    RealmResults<TagRModel> results = RealmManager.getRealm().where(TagRModel.class).findAll();

    for (int i = 0; i < results.size(); i++) {
      rModels.add(results.get(i));
    }

    tags = TagsRealmMapper.transformToModel(rModels);
    return tags;
  }

  /**
   * Questionnaire from Database
   *
   * @return getQuestionnaireFromDatabase
   */
  public QuestionnaireRModel getQuestionnaireFromDatabase() {
    return RealmManager.getRealm().where(QuestionnaireRModel.class).findFirst();
  }

  /**
   * Checking questionnaire date version
   *
   * @param createdDate Form API
   * @return Is QuestionnaireHasBeenUpdated
   */
  private boolean isQuestionnaireHasBeenUpdated(String createdDate) {
    return getQuestionnaireCreatedDateLocal() == null
        || !getQuestionnaireCreatedDateLocal().equalsIgnoreCase(createdDate);
  }

  /**
   * Get Questionnaire Created Date on local
   *
   * @return Questionnaire Created Date on local
   */
  private String getQuestionnaireCreatedDateLocal() {
    return mSharedPreference.readQuestionnaireCreatedDate();
  }

  /**
   * Clear QuestionnaireDate on local
   */
  public void clearQuestionnaireCreatedDateLocal() {
    mSharedPreference.writeQuestionnaireCreatedDate(null);
  }

  /**
   * Set Questionnaire Created Date on local
   */
  private void setQuestionnaireCreatedDateLocal(String createdDateLocal) {
    mSharedPreference.writeQuestionnaireCreatedDate(createdDateLocal);
  }

  /**
   * Add Questionnaire to Database
   *
   * @param questionnaire from API
   */
  private void addQuestionnaireToDatabase(Questionnaire questionnaire) {
    final QuestionnaireRModel rModel = QuestionnaireToRealmMapper.transform(questionnaire);

    RealmManager.getRealm().executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.delete(QuestionnaireRModel.class);
        realm.delete(AnswerRModel.class);
        realm.delete(AnswerTimesRModel.class);
        realm.copyToRealmOrUpdate(rModel);
      }
    });
  }
}
