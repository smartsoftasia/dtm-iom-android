package com.iom.dtm.domain.model;

import android.os.Parcel;

/**
 * Created by Nott on 9/22/2016 AD.
 * DTM
 */

public class RelativeQuestion extends BaseModel {
  public static final String TAG = "RelativeQuestion";

  public String questionId;
  public String choiceId;
  public String relativeQuestionId;

  public RelativeQuestion() {}

  public void setChoiceId(String choiceId) {
    this.choiceId = choiceId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public void setRelativeQuestionId(String relativeQuestionId) {
    this.relativeQuestionId = relativeQuestionId;
  }

  @Override
  public int describeContents() { return 0; }

  public String getChoiceId() {
    return choiceId;
  }

  public String getQuestionId() {
    return questionId;
  }

  public String getRelativeQuestionId() {
    return relativeQuestionId;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.questionId);
    dest.writeString(this.choiceId);

    dest.writeString(this.relativeQuestionId);
  }

  protected RelativeQuestion(Parcel in) {
    super(in);
    this.questionId = in.readString();
    this.choiceId = in.readString();
    this.relativeQuestionId = in.readString();
  }

  public static final Creator<RelativeQuestion> CREATOR = new Creator<RelativeQuestion>() {
    @Override
    public RelativeQuestion createFromParcel(Parcel source) {return new RelativeQuestion(source);}

    @Override
    public RelativeQuestion[] newArray(int size) {return new RelativeQuestion[size];}
  };
}
