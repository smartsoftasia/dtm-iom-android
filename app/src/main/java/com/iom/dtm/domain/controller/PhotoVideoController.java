package com.iom.dtm.domain.controller;

import android.content.Intent;
import android.support.annotation.Nullable;
import com.smartsoftasia.ssalibrary.helper.Logger;
import javax.inject.Inject;

/**
 * Created by Nott on 8/29/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class PhotoVideoController {
  public static final String TAG = "PhotoVideoController";

  private static MediaData sMediaData;

  @Inject
  public PhotoVideoController() {
  }

  public void getInstance() {
    if (sMediaData == null) {
      sMediaData = new MediaData();
    }
  }

  public MediaData getMediaData() {
    return sMediaData;
  }

  public void clearMedia() {
    sMediaData = null;
  }

  public void setMedia(int requestCode, int resultCode, Intent data) {
    getInstance();
    sMediaData.setRequestCode(requestCode);
    sMediaData.setResultCode(resultCode);
    sMediaData.setData(data);
    Logger.e(TAG, requestCode + " " + resultCode + " " + data.toString());
  }

  public void setMedia(String questionId) {
    getInstance();
    sMediaData.setQuestionId(questionId);
  }

  public class MediaData {
    public String questionId;
    public int requestCode;
    public int resultCode;
    public Intent data;

    public MediaData() {
    }

    public void setData(@Nullable Intent data) {
      this.data = data;
    }

    public void setRequestCode(int requestCode) {
      this.requestCode = requestCode;
    }

    public void setResultCode(int resultCode) {
      this.resultCode = resultCode;
    }

    public void setQuestionId(String questionId) {
      this.questionId = questionId;
    }
  }
}
