package com.iom.dtm.domain.controller;

import android.content.Context;
import java.util.Locale;
import javax.inject.Inject;

/**
 * Created by Nott on 12/23/2015 AD.
 * LanguageController
 */
public class LanguageController {
  public static final String TAG = "LanguageController";

  private Context mContext;

  @Inject
  public LanguageController(Context context) {
    mContext = context;
  }

  public String getLanguageCode(){
    Locale current = mContext.getResources().getConfiguration().locale;
    return current.getLanguage();
  }
}
