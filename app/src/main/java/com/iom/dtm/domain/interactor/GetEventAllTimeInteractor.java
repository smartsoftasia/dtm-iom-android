package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.model.Event;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 9/12/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetEventAllTimeInteractor
    extends RestInteractor<BaseApiResult<List<Event>>, List<Event>> {
  public static final String TAG = "GetEventInteractor";

  @Inject
  public GetEventAllTimeInteractor(ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread, RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<BaseApiResult<List<Event>>, List<Event>> mapper() {
    return new Func1<BaseApiResult<List<Event>>, List<Event>>() {
      @Override
      public List<Event> call(BaseApiResult<List<Event>> userEntity) {
        return userEntity.t;
      }
    };
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getViewReportService().getEventList("true").map(mapper());
  }
}
