package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.model.Admin0;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 9/26/2016 AD.
 * DTM
 */

public class GetAddressListInteractor extends RestInteractor<BaseApiResult<Admin0>, Admin0> {
  public static final String TAG = "GetAddressListInteractor";

  private String eventId;

  @Inject
  public GetAddressListInteractor(ThreadExecutor threadExecutor,
                                  PostExecutionThread postExecutionThread, RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<BaseApiResult<Admin0>, Admin0> mapper() {
    return new Func1<BaseApiResult<Admin0>, Admin0>() {
      @Override
      public Admin0 call(BaseApiResult<Admin0> userEntity) {
        return userEntity.t;
      }
    };
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getViewReportService().getAdministrators(eventId).map(mapper());
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }
}
