package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nott on 9/26/2016 AD.
 * DTM
 */

public class Admin3 extends BaseAdmin implements Parcelable {
  public static final String TAG = "Admin2";

  public Admin3() {}

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.name);
    dest.writeString(this.reference);
    dest.writeString(this.id);
  }

  protected Admin3(Parcel in) {
    this.name = in.readString();
    this.reference = in.readString();
    this.id = in.readString();
  }

  public static final Creator<Admin3> CREATOR = new Creator<Admin3>() {
    @Override
    public Admin3 createFromParcel(Parcel source) {return new Admin3(source);}

    @Override
    public Admin3[] newArray(int size) {return new Admin3[size];}
  };
}
