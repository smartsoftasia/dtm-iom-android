package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.domain.model.User;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.SignInForm;
import com.iom.dtm.rest.form.UserSignInForm;
import com.iom.dtm.rest.model.UserApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 5/27/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SignInInteractor extends RestInteractor<UserApiResult, User> {
  public static final String TAG = "SignInInteractor";

  public SignInForm signInForm;
  private SharedPreference mSharedPreference;

  @Inject
  public SignInInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
                          RestClient restClient, SharedPreference sharedPreference) {
    super(threadExecutor, postExecutionThread, restClient);
    this.mSharedPreference = sharedPreference;
  }

  @Override
  protected Func1<UserApiResult, User> mapper() {
    return new Func1<UserApiResult, User>() {
      @Override
      public User call(UserApiResult userApiResult) {
        return userApiResult.t;
      }
    };
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getUserService()
        .signIn(new UserSignInForm(signInForm, UserSignInForm.NORMAL))
        .map(mapper())
        .doOnNext(mSharedPreference.writeUserAction());
  }

  public void setSignInForm(SignInForm signInForm) {
    this.signInForm = signInForm;
  }
}
