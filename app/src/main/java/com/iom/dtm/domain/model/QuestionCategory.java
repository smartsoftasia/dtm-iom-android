package com.iom.dtm.domain.model;

import android.os.Parcel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class QuestionCategory extends BaseModel {
  public static final String TAG = "QuestionCategoryItem";

  @Expose
  public String title;

  @Expose
  @SerializedName("question_groups")
  public List<QuestionGroup> questionGroupList;

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.title);
    dest.writeTypedList(this.questionGroupList);
    dest.writeString(this.id);
  }

  public QuestionCategory() {}

  protected QuestionCategory(Parcel in) {
    super(in);
    this.title = in.readString();
    this.questionGroupList = in.createTypedArrayList(QuestionGroup.CREATOR);
    this.id = in.readString();
  }

  public static final Creator<QuestionCategory> CREATOR = new Creator<QuestionCategory>() {
    @Override
    public QuestionCategory createFromParcel(Parcel source) {return new QuestionCategory(source);}

    @Override
    public QuestionCategory[] newArray(int size) {return new QuestionCategory[size];}
  };

  public String getTitle() {
    return title;
  }
}
