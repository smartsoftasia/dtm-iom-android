package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nott on 9/26/2016 AD.
 * DTM
 */

public class Admin1 extends BaseAdmin implements Parcelable {
  public static final String TAG = "Admin1";

  @Expose
  @SerializedName("level_2_administrators")
  public List<Admin2> admin2s;

  public Admin1() {}

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeTypedList(this.admin2s);
    dest.writeString(this.name);
    dest.writeString(this.reference);
    dest.writeString(this.id);
  }

  protected Admin1(Parcel in) {
    this.admin2s = in.createTypedArrayList(Admin2.CREATOR);
    this.name = in.readString();
    this.reference = in.readString();
    this.id = in.readString();
  }

  public static final Creator<Admin1> CREATOR = new Creator<Admin1>() {
    @Override
    public Admin1 createFromParcel(Parcel source) {return new Admin1(source);}

    @Override
    public Admin1[] newArray(int size) {return new Admin1[size];}
  };
}
