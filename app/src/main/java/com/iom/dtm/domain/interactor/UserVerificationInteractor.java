package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.model.User;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.ReplaceRoleVerifyForm;
import com.iom.dtm.rest.model.UserApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 8/25/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class UserVerificationInteractor extends RestInteractor<UserApiResult, User> {
  public static final String TAG = "UserVerificationInteractor";
  private String email;
  private String password;

  @Inject
  public UserVerificationInteractor(ThreadExecutor threadExecutor,
                                    PostExecutionThread postExecutionThread,
                                    RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<UserApiResult, User> mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getUserService()
        .replaceRoleVerify(new ReplaceRoleVerifyForm(email, password));
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
