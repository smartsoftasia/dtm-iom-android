package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.model.Report;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 7/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetReportViaEmailInteractor extends RestInteractor<BaseApiResult<List<Report>>, List<Report>> {
  public static final String TAG = "GetEnumeratorReportList";

  private String resultId;

  @Inject
  public GetReportViaEmailInteractor(ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread, RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<BaseApiResult<List<Report>>, List<Report>> mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getViewReportService().sendReportToEmail(resultId, "email");
  }

  public void setResultId(String resultId) {
    this.resultId = resultId;
  }
}
