package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.SignUpDetailsForm;
import com.iom.dtm.rest.form.UserSignUpForm;
import com.iom.dtm.rest.mapper.UserMapper;
import com.iom.dtm.rest.model.UserApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 5/27/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class SignUpInteractor extends RestInteractor {
  private static final String TAG = "SignInInteractor";

  public SignUpDetailsForm signUpDetailsForm;
  protected SharedPreference mSharedPreference;

  @Inject
  public SignUpInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
                          RestClient restClient, SharedPreference sharedPreference) {
    super(threadExecutor, postExecutionThread, restClient);
    this.mSharedPreference = sharedPreference;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getUserService()
        .signUp(new UserSignUpForm(signUpDetailsForm))
        .map(mapper())
        .doOnNext(mSharedPreference.writeUserAction());
  }

  @Override
  protected Func1 mapper() {
    return UserMapper.userFunc1;
  }

  public void setSignUpDetailsForm(SignUpDetailsForm signUpDetailsForm) {
    this.signUpDetailsForm = signUpDetailsForm;
  }
}
