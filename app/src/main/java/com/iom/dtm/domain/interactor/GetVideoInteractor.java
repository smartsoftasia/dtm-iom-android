package com.iom.dtm.domain.interactor;

import android.content.Context;
import android.net.Uri;
import com.iom.dtm.core.AppConfig;
import com.smartsoftasia.rxcamerapicker.RxImageConverters;
import com.smartsoftasia.rxcamerapicker.RxImagePicker;
import com.smartsoftasia.rxcamerapicker.Sources;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 9/1/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetVideoInteractor extends BaseInteractor {
  public static final String TAG = "GetImageInteractor";
  protected Context mContext;
  protected Sources sources;

  @Inject
  public GetVideoInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
                            Context context) {
    super(threadExecutor, postExecutionThread);
    this.mContext = context;
  }

  @Override
  protected Observable buildUseCaseObservable() {
    return RxImagePicker.with(mContext)
        .requestImage(sources)
        .flatMap(new Func1<Uri, Observable<String>>() {
          @Override
          public Observable<String> call(Uri uri) {
            return RxImageConverters.uriToFullPath(mContext, uri);
          }
        });
  }

  public void setSources(Sources sources) {
    this.sources = sources;
  }
}
