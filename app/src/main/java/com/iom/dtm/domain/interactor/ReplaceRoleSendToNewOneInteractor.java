package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.ReplaceRoleSendToNewOneForm;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 8/26/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class ReplaceRoleSendToNewOneInteractor extends RestInteractor<Object, Object> {
  public static final String TAG = "ReplaceRoleSendToNewOneInteractor";

  private String email;
  private String comment;

  @Inject
  public ReplaceRoleSendToNewOneInteractor(ThreadExecutor threadExecutor,
                                           PostExecutionThread postExecutionThread,
                                           RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<Object, Object> mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getUserService().replaceRole(email, new ReplaceRoleSendToNewOneForm(comment));
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }
}
