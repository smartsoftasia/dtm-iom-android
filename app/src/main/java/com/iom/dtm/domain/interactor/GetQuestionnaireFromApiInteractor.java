package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetQuestionnaireFromApiInteractor
    extends RestInteractor<BaseApiResult<Questionnaire>, Questionnaire> {
  public static final String TAG = "GetQuestionnaireFromApiInteractor";
  private QuestionnaireController mQuestionnaireController;

  private Event event;
  private boolean isFullQuestionnaire;

  @Inject
  public GetQuestionnaireFromApiInteractor(ThreadExecutor threadExecutor,
                                           PostExecutionThread postExecutionThread,
                                           RestClient restClient,
                                           QuestionnaireController questionnaireController) {
    super(threadExecutor, postExecutionThread, restClient);
    this.mQuestionnaireController = questionnaireController;
  }

  @Override
  protected Func1<BaseApiResult<Questionnaire>, Questionnaire> mapper() {
    return new Func1<BaseApiResult<Questionnaire>, Questionnaire>() {
      @Override
      public Questionnaire call(BaseApiResult<Questionnaire> baseApiResult) {
        mQuestionnaireController.checkQuestionnaireVersion(baseApiResult.t, event.id);
        return mQuestionnaireController.getQuestionnaire();
      }
    };
  }

  protected Func1<BaseApiResult<Questionnaire>, Questionnaire> mapper2() {
    return new Func1<BaseApiResult<Questionnaire>, Questionnaire>() {
      @Override
      public Questionnaire call(BaseApiResult<Questionnaire> baseApiResult) {
        return baseApiResult.t;
      }
    };
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    if (isFullQuestionnaire) {
      return restClient.getQuestionService()
          .getFullQuestionnaire(String.valueOf(event.id))
          .map(mapper());
    } else {
      return restClient.getQuestionService().getLiteQuestionnaire("false").map(mapper2()).doOnNext(new Action1<Questionnaire>() {
        @Override
        public void call(Questionnaire questionnaire) {
          mQuestionnaireController.setQuestionnaire(questionnaire);
        }
      });
    }
  }

  public void setEvent(Event event) {
    this.event = event;
  }

  public void setFullQuestionnaire(boolean fullQuestionnaire) {
    isFullQuestionnaire = fullQuestionnaire;
  }
}
