package com.iom.dtm.domain.model;

import android.os.Parcel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.R;
import com.smartsoftasia.ssalibrary.helper.TranslationHelper;
import java.util.Date;

/**
 * Created by Nott on 9/13/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class Event extends BaseModel {
  public static final String TAG = "Event";

  @Expose
  @SerializedName("title")
  public String title;
  @Expose
  @SerializedName("started_at")
  public Date startedAt;
  @Expose
  @SerializedName("ended_at")
  public Date endedAt;

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.title);
    dest.writeLong(this.startedAt != null ? this.startedAt.getTime() : -1);
    dest.writeLong(this.endedAt != null ? this.endedAt.getTime() : -1);
    dest.writeString(this.id);
  }

  public Event() {}

  protected Event(Parcel in) {
    super(in);
    this.title = in.readString();
    long tmpStartedAt = in.readLong();
    this.startedAt = tmpStartedAt == -1 ? null : new Date(tmpStartedAt);
    long tmpEndedAt = in.readLong();
    this.endedAt = tmpEndedAt == -1 ? null : new Date(tmpEndedAt);
    this.id = in.readString();
  }

  public static final Creator<Event> CREATOR = new Creator<Event>() {
    @Override
    public Event createFromParcel(Parcel source) {return new Event(source);}

    @Override
    public Event[] newArray(int size) {return new Event[size];}
  };

  @Override
  public String toString() {
    return title;
  }

  public static Event getDefaultEvent() {
    Event model = new Event();
    model.id = "-1";
    model.setTitle(TranslationHelper.get(R.string.dtm_from_fragment_select_event_spinner));
    return model;
  }
}
