package com.iom.dtm.domain.controller;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.iom.dtm.domain.model.User;
import com.smartsoftasia.ssalibrary.domain.controller.BaseSharedPreference;
import com.smartsoftasia.ssalibrary.helper.SharedPreferenceHelper;
import com.smartsoftasia.ssalibrary.helper.Validator;
import javax.inject.Inject;
import rx.functions.Action1;

/**
 * Created by Nott on 12/3/2015 AD.
 * SharedPreference
 */
public class SharedPreference extends BaseSharedPreference {
  public static final String TAG = "SharedPreference";
  public static final String USER = "user";
  public static final String USER_TOKEN = "user_token";
  public static final String QUESTIONNAIRE_CREATED_DATE = "questionnaire_created_date";
  public static final String EVENT_ID = "event_id";

  @Inject
  public SharedPreference(Context context) {
    super(context);
  }

  /**
   * Read the saved user from the shared preferences.
   *
   * @return saved user. null if any users have been saved
   */
  @Nullable
  public User readUser() {
    Gson gson = new Gson();
    String json = SharedPreferenceHelper.getPreferenceString(mContext, USER, null);
    if (json != null) {
      return gson.fromJson(json, User.class);
    } else {
      return null;
    }
  }

  /**
   * Save the user on the shared preference. If the user is null, the preference will be reset.
   *
   * @param userModel user to save.
   */
  public void writeUser(@Nullable User userModel) {
    Gson gson = new Gson();
    String jsonUser = userModel == null ? null : gson.toJson(userModel);
    SharedPreferenceHelper.setPreference(mContext, USER, jsonUser);
  }

  /**
   * Write User model Action
   *
   * @return user action
   */
  public Action1<User> writeUserAction() {
    return new Action1<User>() {

      @Override
      public void call(User user) {
        writeUser(user);
      }
    };
  }

  /**
   * Read the saved user token from the shared preferences.
   *
   * @return saved user token. null if any users have been saved
   */

  @Nullable
  public String readUserToken() {
    return SharedPreferenceHelper.getPreferenceString(mContext, USER_TOKEN, null);
  }

  /**
   * Save the user token on the shared preference. If the user is null, the preference will be
   * reset.
   *
   * @param userToken user to save.
   */

  public void writeUserToken(@Nullable String userToken) {
    String token = userToken == null ? null : userToken;
    SharedPreferenceHelper.setPreference(mContext, USER_TOKEN, token);
  }

  /**
   * Checking user presence
   *
   * @return if user logged in return true, if not return false
   */

  public boolean isUserLoggedIn() {
    return readUser() != null && Validator.isValid(readUser().id);
  }

  @Nullable
  public String readQuestionnaireCreatedDate() {
    return SharedPreferenceHelper.getPreferenceString(mContext, QUESTIONNAIRE_CREATED_DATE, null);
  }

  public void writeQuestionnaireCreatedDate(@Nullable String createDate) {
    String token = createDate == null ? null : createDate;
    SharedPreferenceHelper.setPreference(mContext, QUESTIONNAIRE_CREATED_DATE, token);
  }

  @Nullable
  public String readEventId() {
    return SharedPreferenceHelper.getPreferenceString(mContext, EVENT_ID, null);
  }

  public void writeEventId(@Nullable String eventId) {
    SharedPreferenceHelper.setPreference(mContext, EVENT_ID, eventId);
  }
}

