package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringDef;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iom.dtm.core.AppConfig;
import com.smartsoftasia.ssalibrary.helper.NumberHelper;
import com.smartsoftasia.ssalibrary.helper.Validator;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Nott on 4/21/2016 AD.
 * User
 */
public class User extends BaseModel implements Parcelable {

  public static final String TAG = "User";
  public static final String UNPROCESSED = "unprocessed";
  public static final String UNVERIFIED = "unverified";
  public static final String VERIFIED = "verified";
  public static final String COMPLETED = "completed";

  @StringDef(value = {UNPROCESSED, UNVERIFIED, VERIFIED, COMPLETED})
  @Retention(RetentionPolicy.SOURCE)
  public @interface UserStatus {

  }

  public static final String ENUMERATORS = "Users::Enumerator";
  public static final String ENUMERATORS_ADMIN = "Users::EnumeratorAdmin";

  @StringDef({ENUMERATORS, ENUMERATORS_ADMIN})
  @Retention(RetentionPolicy.SOURCE)
  public @interface UserType {
  }

  @Expose
  @SerializedName("email")
  public String email;
  @Expose
  @SerializedName("first_name")
  public String firstName;
  @Expose
  @SerializedName("last_name")
  public String lastName;
  @Expose
  @SerializedName("password")
  public String password;
  @Expose
  @SerializedName("gender")
  public String gender;
  @Expose
  @SerializedName("state")
  public String state;
  @Expose
  @SerializedName("image")
  public String profilePictureUrl;
  @Expose
  @SerializedName("google_id")
  public String googleId;
  @Expose
  @SerializedName("enumerators_count")
  public String enumeratorsCount;
  @Expose
  @SerializedName("reports_count")
  public String reportsCount;
  @Expose
  @SerializedName("facebook_id")
  public String facebookId;
  @Expose
  @SerializedName("organization")
  public String organization;
  @Expose
  @SerializedName("today_reports_count")
  public String todayReportsCount;
  @Expose
  @SerializedName("position")
  public String position;
  @Expose
  @SerializedName("address")
  public Address address;
  @Expose
  @SerializedName("type")
  public String userType;

  public boolean isNewUser() {
    return !Validator.isValid(id);
  }

  public boolean isSignInBySocialMedia() {
    return Validator.isValid(googleId) || Validator.isValid(facebookId);
  }

  public String getProfilePictureUrl() {
    return this.profilePictureUrl;
  }

  public void setOrganization(String organization) {
    this.organization = organization;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public void setProfilePictureUrl(String profilePictureUrl) {
    this.profilePictureUrl = profilePictureUrl;
  }

  public void setReportsCount(String reportsCount) {
    this.reportsCount = reportsCount;
  }

  public void setEnumeratorsCount(String enumeratorsCount) {
    this.enumeratorsCount = enumeratorsCount;
  }

  public String getReportsCount() {
    return NumberHelper.getDecimalWithCommaFormatString(reportsCount);
  }

  public String getEnumeratorsCount() {
    return NumberHelper.getDecimalWithCommaFormatString(enumeratorsCount);
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(@UserType String userType) {
    this.userType = userType;
  }

  public String getFullName() {
    return firstName + " " + lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getEmail() {
    return email;
  }

  public User() {
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getOrganization() {
    return organization;
  }

  public String getPosition() {
    return position;
  }

  public String getTodayReportsCount() {
    return NumberHelper.getDecimalWithCommaFormatString(todayReportsCount);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);
    dest.writeString(this.email);
    dest.writeString(this.firstName);
    dest.writeString(this.lastName);
    dest.writeString(this.password);
    dest.writeString(this.gender);
    dest.writeString(this.state);
    dest.writeString(this.profilePictureUrl);
    dest.writeString(this.googleId);
    dest.writeString(this.enumeratorsCount);
    dest.writeString(this.reportsCount);
    dest.writeString(this.facebookId);
    dest.writeString(this.organization);
    dest.writeString(this.todayReportsCount);
    dest.writeString(this.position);
    dest.writeParcelable(this.address, flags);
    dest.writeString(this.userType);
    dest.writeString(this.id);
  }

  protected User(Parcel in) {
    super(in);
    this.email = in.readString();
    this.firstName = in.readString();
    this.lastName = in.readString();
    this.password = in.readString();
    this.gender = in.readString();
    this.state = in.readString();
    this.profilePictureUrl = in.readString();
    this.googleId = in.readString();
    this.enumeratorsCount = in.readString();
    this.reportsCount = in.readString();
    this.facebookId = in.readString();
    this.organization = in.readString();
    this.todayReportsCount = in.readString();
    this.position = in.readString();
    this.address = in.readParcelable(Address.class.getClassLoader());
    this.userType = in.readString();
    this.id = in.readString();
  }

  public static final Creator<User> CREATOR = new Creator<User>() {
    @Override
    public User createFromParcel(Parcel source) {
      return new User(source);
    }

    @Override
    public User[] newArray(int size) {
      return new User[size];
    }
  };
}
