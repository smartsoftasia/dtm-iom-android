package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.QuestionnaireController;
import com.iom.dtm.domain.model.Event;
import com.iom.dtm.domain.model.Questionnaire;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.model.BaseApiResult;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetQuestionnaireFromApiByIdInteractor
    extends RestInteractor<BaseApiResult<Questionnaire>, Questionnaire> {
  public static final String TAG = "GetQuestionnaireFromApiInteractor";
  private QuestionnaireController mQuestionnaireController;

  private Event event;

  @Inject
  public GetQuestionnaireFromApiByIdInteractor(ThreadExecutor threadExecutor,
                                               PostExecutionThread postExecutionThread,
                                               RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<BaseApiResult<Questionnaire>, Questionnaire> mapper() {
    return new Func1<BaseApiResult<Questionnaire>, Questionnaire>() {
      @Override
      public Questionnaire call(BaseApiResult<Questionnaire> baseApiResult) {
        return baseApiResult.t;
      }
    };

  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getQuestionService()
        .getFullQuestionnaire(String.valueOf(event.id))
        .map(mapper());

  }

  public void setEvent(Event event) {
    this.event = event;
  }

}
