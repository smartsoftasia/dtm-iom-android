package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.iom.dtm.view.viewinterface.NoInternetConnectionViewInterface;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.functions.Func1;
import rx.subjects.PublishSubject;

/**
 * Created by Nott on 5/19/2016 AD.
 * RestInteractor
 */
public abstract class RestInteractor<T, R> extends BaseInteractor {
  public static final String TAG = "RestInteractor";

  protected RestClient restClient;

  protected NoInternetConnectionViewInterface noInternetConnectionInterface;

  public RestInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
                        RestClient restClient) {
    super(threadExecutor, postExecutionThread);
    this.restClient = restClient;
  }

  protected abstract Func1<T, R> mapper();

  public abstract Observable buildApiUseCaseObservable();

  /**
   * RetryWhen: handle onError
   */
  public Func1<Observable<? extends Throwable>, Observable<?>> retryWhenFunc1 = new Func1<Observable<? extends Throwable>, Observable<?>>() {
    @Override
    public Observable<?> call(Observable<? extends Throwable> observable) {
      return observable.flatMap(new Func1<Throwable, Observable<?>>() {
        @Override
        public Observable<?> call(final Throwable throwable) {
          if (noInternetConnectionInterface != null && (throwable instanceof IOException)) {
            return noInternetConnectionInterface.noInternetConnection(throwable);
          } else {
            return Observable.error(throwable);
          }
        }
      });
    }
  };

  @Override
  public Observable buildUseCaseObservable() {
    return buildApiUseCaseObservable().retryWhen(retryWhenFunc1);
  }

  public void setNoInternetConnectionInterfaceListener(
      NoInternetConnectionViewInterface noInternetConnectionInterface) {
    this.noInternetConnectionInterface = noInternetConnectionInterface;
  }
}
