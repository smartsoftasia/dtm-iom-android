package com.iom.dtm.domain.annotation;

import android.support.annotation.StringDef;

import com.iom.dtm.domain.model.Question;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Nott on 8/2/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
@StringDef(value = {
    Question.STRING, Question.DATE_PICKER, Question.NUMBER_PICKER, Question.LOCATION,
    Question.SINGLE_CHOICE, Question.NUMBER, Question.ATTACHMENT, Question.TEXT, Question.DROP_DOWN,
    Question.ADMIN_1, Question.ADMIN_2, Question.ADMIN_3, Question.ADMIN_1_PRIORITY
})
@Retention(RetentionPolicy.SOURCE)
public @interface QuestionType {

}
