package com.iom.dtm.domain.subscriber;

import com.smartsoftasia.ssalibrary.domain.subscriber.DefaultSubscriber;
import com.smartsoftasia.ssalibrary.helper.Logger;

/**
 * Created by Nott on 7/28/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public abstract class AppSubscriber<T> extends DefaultSubscriber<T> {
  public static final String TAG = "AppSubscriber";

  @Override
  public void onError(Throwable e) {
    super.onError(e);
    Logger.e("DEBUG Mode:", e.toString());
  }
}
