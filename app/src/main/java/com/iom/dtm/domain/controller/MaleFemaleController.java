package com.iom.dtm.domain.controller;

/**
 * Created by Nott on 8/30/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class MaleFemaleController {
  public static final String TAG = "MaleFemaleController";
  public class Total {
    public String QuestionId;
    public boolean isTotal;
    public String male;
    public String female;
    public String total;
  }
}