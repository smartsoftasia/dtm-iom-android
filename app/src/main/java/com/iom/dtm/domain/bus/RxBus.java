package com.iom.dtm.domain.bus;

import android.os.Handler;
import android.os.Looper;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Nott on 20/10/2559.
 * dtm-iom-android
 */
public class RxBus {
  public static final String TAG = "RxBus";

  private static RxBus sRxBus;
  private final Handler mHandler;
  private PublishSubject<Object> subject = PublishSubject.create();

  private RxBus() {
    mHandler = new Handler(Looper.myLooper());
  }

  public static RxBus getInstance() {
    if (sRxBus == null) {
      sRxBus = new RxBus();
    }
    return sRxBus;
  }

  public void post(final Object object) {
    mHandler.post(new Runnable() {
      public void run() {
        subject.onNext(object);
      }
    });
  }

  public Observable<Object> getEvents() {
    return subject;
  }
}
