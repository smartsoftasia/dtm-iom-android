package com.iom.dtm.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

/**
 * Created by Nott on 11/10/2559.
 * dtm-iom-android
 */

public class ViewReport implements Parcelable {
  public static final String TAG = "ViewReport";

  public Event event;
  public List<Admin1> admin1s;
  public List<Question> questions;
  public Date startDate;
  public Date endDate;

  public ViewReport() {
  }

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

  public List<Admin1> getAdmin1s() {
    return admin1s;
  }

  public void setAdmin1s(List<Admin1> admin1s) {
    this.admin1s = admin1s;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  public void setQuestions(List<Question> questions) {
    this.questions = questions;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  protected ViewReport(Parcel in) {
    admin1s = in.createTypedArrayList(Admin1.CREATOR);
    questions = in.createTypedArrayList(Question.CREATOR);
    event = in.readParcelable(Event.class.getClassLoader());
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeTypedList(admin1s);
    dest.writeTypedList(questions);
    dest.writeParcelable(event, flags);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<ViewReport> CREATOR = new Creator<ViewReport>() {
    @Override
    public ViewReport createFromParcel(Parcel in) {
      return new ViewReport(in);
    }

    @Override
    public ViewReport[] newArray(int size) {
      return new ViewReport[size];
    }
  };
}
