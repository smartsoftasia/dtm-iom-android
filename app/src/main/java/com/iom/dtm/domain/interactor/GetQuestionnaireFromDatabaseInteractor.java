package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.QuestionnaireController;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;

/**
 * Created by Nott on 8/4/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class GetQuestionnaireFromDatabaseInteractor extends BaseInteractor {
  public static final String TAG = "GetQuestionnaireFromDatabaseInteractor";

  protected QuestionnaireController mQuestionnaireController;

  @Inject
  public GetQuestionnaireFromDatabaseInteractor(ThreadExecutor threadExecutor,
                                                PostExecutionThread postExecutionThread,
                                                QuestionnaireController questionnaireController) {
    super(threadExecutor, postExecutionThread);
    this.mQuestionnaireController = questionnaireController;
  }

  @Override
  protected Observable buildUseCaseObservable() {
    return Observable.just(QuestionnaireController.getQuestionnaire().questionCategories);
  }
}
