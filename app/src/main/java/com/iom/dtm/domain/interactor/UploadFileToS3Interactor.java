package com.iom.dtm.domain.interactor;

import com.iom.dtm.services.amazon.s3.S3Uploader;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;

/**
 * Created by Nott on 8/31/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class UploadFileToS3Interactor extends BaseInteractor {
  public static final String TAG = "UploadFileToS3Interactor";

  private S3Uploader mS3Uploader;

  private String filePath;

  @Inject
  public UploadFileToS3Interactor(ThreadExecutor threadExecutor,
                                  PostExecutionThread postExecutionThread, S3Uploader s3Uploader) {
    super(threadExecutor, postExecutionThread);
    mS3Uploader = s3Uploader;
  }

  @Override
  protected Observable buildUseCaseObservable() {
    return mS3Uploader.rxUploadFile(filePath);
  }

  public void setMfilePath(String mFilePath) {
    this.filePath = mFilePath;
  }
}
