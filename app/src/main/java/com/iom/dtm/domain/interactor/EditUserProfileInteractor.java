package com.iom.dtm.domain.interactor;

import com.iom.dtm.domain.controller.SharedPreference;
import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.EditProfileForm;
import com.iom.dtm.rest.form.UpdateImageProfileForm;
import com.iom.dtm.rest.form.UpdateProfileDetailsForm;
import com.iom.dtm.rest.mapper.UserMapper;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 6/21/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class EditUserProfileInteractor extends RestInteractor {
  public static final String TAG = "EditUserProfileInteractor";

  protected SharedPreference mSharedPreference;
  protected boolean isChangeImage = false;
  protected EditProfileForm editProfileForm;
  private String imageURL;

  @Inject
  public EditUserProfileInteractor(ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread, RestClient restClient,
                                   SharedPreference sharedPreference) {
    super(threadExecutor, postExecutionThread, restClient);
    this.mSharedPreference = sharedPreference;
  }

  @Override
  protected Func1 mapper() {
    return UserMapper.userFunc1;
  }

  @Override
  public Observable buildApiUseCaseObservable() {

    if (isChangeImage) {
      return restClient.getUserService()
          .updateUser(new UpdateImageProfileForm(imageURL))
          .map(mapper())
          .doOnNext(mSharedPreference.writeUserAction());
    } else {
      return restClient.getUserService()
          .updateUser(new UpdateProfileDetailsForm(editProfileForm))
          .map(mapper())
          .doOnNext(mSharedPreference.writeUserAction());
    }
  }

  public void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }

  public void setChangeImage(boolean changeImage) {
    isChangeImage = changeImage;
  }

  public void setEditProfileForm(EditProfileForm editProfileForm) {
    this.editProfileForm = editProfileForm;
  }
}
