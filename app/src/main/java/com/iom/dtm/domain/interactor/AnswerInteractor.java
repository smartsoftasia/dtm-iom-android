package com.iom.dtm.domain.interactor;

import com.iom.dtm.rest.RestClient;
import com.iom.dtm.rest.form.AnswerForm;
import com.smartsoftasia.ssalibrary.domain.executor.PostExecutionThread;
import com.smartsoftasia.ssalibrary.domain.executor.ThreadExecutor;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Nott on 8/23/2016 AD.
 * Displacement Tracking Matrix (DTM)
 */
public class AnswerInteractor extends RestInteractor<String, String> {
  public static final String TAG = "AnswerInteractor";

  public AnswerForm answerForm;

  @Inject
  public AnswerInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread,
                          RestClient restClient) {
    super(threadExecutor, postExecutionThread, restClient);
  }

  @Override
  protected Func1<String, String> mapper() {
    return null;
  }

  @Override
  public Observable buildApiUseCaseObservable() {
    return restClient.getQuestionService().sendAnswer(answerForm);
  }

  public void setAnswerForm(AnswerForm answerForm) {
    this.answerForm = answerForm;
  }
}
